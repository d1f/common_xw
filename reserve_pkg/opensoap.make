PKG_VERSION = 2.0.1
PKG_SITES   = http://www.opensoap.jp/download

include $(DEFS)


configure : $(call tstamp_f,install,toolchain openssl-0.9 libxml2)
uninstall : $(call tstamp_f,uninstall,        openssl-0.9 libxml2)

define CONFIGURE_CMD
  echo ac_cv_cxx_compiler_gnu=true > $(PKG_BLD_DIR)/config.cache

  cd $(PKG_BLD_DIR) && ./configure	$(LOG)	\
	--config-cache				\
	--build=$(BUILD) --host=$(TARGET)	\
	     --prefix=$(INST_DIR)		\
	--exec-prefix=$(ROOT_DIR)		\
	--with-gnu-ld 				\
	--localstatedir=/var			\
	--enable-shared				\
	--disable-static			\
	--enable-buildin-mbfuncs		\
	LDFLAGS="$(STD_LDFLAGS)"		\
	 CFLAGS="$(STD_CPPFLAGS) $(STD_CFLAGS)"
endef



define BUILD_CMD
  $(DOMAKE) $(MKJ) -C $(PKG_BLD_DIR)/src/api         $(MK_VARS) $(LOG)
  $(DOMAKE) $(MKJ) -C $(PKG_BLD_DIR)/src/security    $(MK_VARS) $(LOG)
endef

define INSTALL_CMD
  $(IWMAKE) -C $(PKG_BLD_DIR)/src/api      install $(LOG)
  $(IWMAKE) -C $(PKG_BLD_DIR)/src/security install $(LOG)
endef

    CLEAN_CMD = $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(CLEAN_CMD_DEFAULT)


include $(RULES)
