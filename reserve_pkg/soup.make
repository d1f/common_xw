PKG_VERSION = 0.7.11
PKG_SITES   = ftp://ftp.gnome.org/pub/GNOME/sources/soup/0.7

include $(DEFS)


#POST_EXTRACT_CMD = $(RM) $(_SRC_DIR)/$(PKG_DIR)/aclocal.m4 $(LOG)


pre-configure : $(call tstamp_f,install,toolchain pkg-config glib2 openssl-0.9 popt libxml2)
    uninstall : $(call tstamp_f,uninstall,                   glib2 openssl-0.9 popt libxml2)

ifeq (0,1)
AC_VER = 2.59
include $(TOOLS_PKG_DIR)/autoconf.mk

AM_VER = 1.9
include $(TOOLS_PKG_DIR)/automake.mk

define PRE_CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && $(ACLOCAL)   $(ACLOCAL_FLAGS) $(LOG)
  cd $(PKG_BLD_DIR) && $(AUTOMAKE) $(AUTOMAKE_FLAGS) $(LOG)
  cd $(PKG_BLD_DIR) && $(AUTOCONF) $(AUTOCONF_FLAGS) $(LOG)
endef
endif

STD_IDIRS += $(IINC_DIR)/glib-2.0
STD_LDFLAGS += -lglib-2.0 -lgmodule-2.0

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure $(LOG)	\
	--build=$(BUILD) --host=$(TARGET)	\
	--exec-prefix=$(ROOT_DIR)		\
	     --prefix=$(INST_DIR)		\
	 --sysconfdir=$(RETC_DIR)/soup		\
	--with-gnu-ld --disable-static		\
	--disable-glibtest --enable-glib2	\
	--with-popt-includes=$(IINC_DIR)	\
	--with-popt-libs=$(RLIB_DIR)		\
	--with-openssl-includes=$(IINC_DIR)	\
	--with-libxml=2				\
	$(STD_VARS)
endef

define INSTALL_CMD
  $(IW) $(INST_FILES) $(PKG_BLD_DIR)/soup $(RBIN_DIR) $(LOG)
endef

    BUILD_CMD = $(BUILD_CMD_DEFAULT)
    CLEAN_CMD = $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(CLEAN_CMD_DEFAULT)


include $(RULES)
