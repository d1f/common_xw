PKG_TYPE      = DEBIAN1

ifeq ($(PKG_TYPE),DEBIAN1)
  PKG_VER_MAJOR     = 1.8
  PKG_VERSION       = 1.8.5
  PKG_VER_PATCH     = -4etch1
  PKG_SITE_PATH = pool/main/r/$(PKG_BASE)
  PKG_DIR       = $(PKG)-$(PKG_VERSION)
else
  PKG_VERSION       = 1.8.5
  PKG_SITES     = ftp://ftp.ruby-lang.org/pub/ruby
endif

include $(DEFS)


ifeq ($(PKG_TYPE),DEBIAN1)
  define POST_EXTRACT_CMD
    $(TAR) -C  $(_SRC_DIR) -xzf \
            $(_SRC_DIR)/$(PKG_BASE)-$(PKG_VERSION).orig/$(PKG)-$(PKG_VERSION).tar.gz $(LOG)
    $(RM_R) $(_SRC_DIR)/$(PKG_BASE)-$(PKG_VERSION).orig                              $(LOG)
    $(RM_R) $(_SRC_DIR)/$(PKG_DIR)/parse.c $(_SRC_DIR)/$(PKG_DIR)/lex.c               $(LOG)
  endef
endif


PKG_PATCH2_FILES = $(PKG_SRC_DIR)/debian/patches/*.patch


AC_VER = 2.59
include $(TOOLS_PKG_DIR)/autoconf.mk

PRE_CONFIGURE_CMD = cd $(PKG_BLD_DIR) && $(AUTOCONF) $(LOG)

configure : $(call bstamp_f,install,bison flex-old gperf)
configure : $(call tstamp_f,install,toolchain zlib)

define CONFIGURE_CMD
  $(RM) $(PKG_BLD_DIR)/config.cache                                   $(LOG)
  echo "ac_cv_func_getpgrp_void=no"    >> $(PKG_BLD_DIR)/config.cache
  echo "ac_cv_func_setpgrp_void=yes"   >> $(PKG_BLD_DIR)/config.cache
  echo "rb_cv_negative_time_t=no"      >> $(PKG_BLD_DIR)/config.cache
  echo "ac_cv_func_memcmp_working=yes" >> $(PKG_BLD_DIR)/config.cache
  chmod a-w                               $(PKG_BLD_DIR)/config.cache $(LOG)

  cd $(PKG_BLD_DIR) && ./configure	$(LOG)	\
	--config-cache				\
	--build=$(BUILD) --host=$(TARGET)	\
	     --prefix=$(INST_DIR)		\
	--exec-prefix=$(ROOT_DIR)		\
	--with-gnu-ld				\
	--localstatedir=/var			\
	LDFLAGS="$(STD_LDFLAGS)"		\
	 CFLAGS="$(STD_CFLAGS)"
endef

MK_VARS  = CC=$(CC)
MK_VARS += LD=$(LD)
MK_VARS += AR=$(AR)
MK_VARS += RANLIB=$(RANLIB)
#MK_VARS += RUBY_INSTALL_NAME=$(PKG_BLD_DIR)/ruby
    RUBY = $(PKG_BLD_DIR)/ruby
MINIRUBY = "$(RUBY) -I$(PKG_BLD_DIR) -rfake"
RBCONFIG = $(PKG_BLD_DIR)/.rbconfig.time

define BUILD_CMD
  $(DOMAKE) $(MKJ) -C $(PKG_BLD_DIR) ruby     $(MK_VARS) $(LOG)
  $(DOMAKE) $(MKJ) -C $(PKG_BLD_DIR) miniruby $(MK_VARS) $(LOG)

  # incompatible with cross:
  echo "$(MINIRUBY) $(PKG_BLD_DIR)/mkconfig.rb -timestamp=$(RBCONFIG) -install_name=$(RUBY) -so_name=$(RUBY) $(PKG_BLD_DIR)/rbconfig.rb"
        $(MINIRUBY) $(PKG_BLD_DIR)/mkconfig.rb -timestamp=$(RBCONFIG) -install_name=$(RUBY) -so_name=$(RUBY) $(PKG_BLD_DIR)/rbconfig.rb $(LOG)
# $(DOMAKE) -C $(PKG_BLD_DIR) $(MK_VARS) $(LOG)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
