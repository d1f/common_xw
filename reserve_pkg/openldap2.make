PKG_TYPE      = DEBIAN1
PKG_VERSION   = 2.1.30
PKG_VER_PATCH = -8
PKG_SITE_PATH = pool/main/o/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain openssl-0.9)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure $(LOG) \
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--exec-prefix=$(ROOT_DIR)	\
	     --prefix=$(INST_DIR)	\
	--with-gnu-ld			\
	--disable-static		\
	--enable-shared			\
	--enable-dynamic		\
	--enable-local			\
	--enable-x-compile		\
	--disable-slapd			\
	--with-yielding_select=yes	\
	--with-tls			\
	--disable-bdb			\
	--without-cyrus-sasl		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(IW) $(RM) -v $(call root_static_libs_f, ldap ldap_r lber) $(LOG)
endef


include $(RULES)
