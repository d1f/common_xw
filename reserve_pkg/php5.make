PKG_TYPE      = DEBIAN1
PKG_VERSION   = 5.2.0+dfsg
PKG_VER_PATCH = -8+etch16
PKG_SITE_PATH = pool/main/p/$(PKG_BASE)

include $(DEFS)


#include $(TOOLS_PKG_DIR)/quilt.mk
PKG_PATCH2_FILES = $(PKG_SRC_DIR)/debian/patches/*.patch


AC_VER = 2.67
include $(TOOLS_PKG_DIR)/autoconf.mk

AM_VER = 1.11
include $(TOOLS_PKG_DIR)/automake.mk

pre-configure : $(call bstamp_f,install,sed libtool)

define PRE_CONFIGURE_CMD
  $(RELINK) $(addprefix $(PKG_BLD_DIR)/,\
		aclocal.m4 configure ltmain.sh build/libtool.m4) $(LOG)

  cd $(PKG_BLD_DIR) && ./buildconf --force		$(LOG)
endef


configure : $(TOOLCHAIN_INSTALL_STAMP)

define CONFIGURE_CMD
  #touch $(PKG_BLD_DIR)/confdefs.h
  cd $(PKG_BLD_DIR) && ./configure	$(LOG)	\
	--build=$(BUILD) --host=$(TARGET)	\
	     --prefix=$(INST_DIR)		\
	--exec-prefix=$(ROOT_DIR)		\
	--enable-libxml				\
	--disable-simplexml			\
	--disable-xmlreader			\
	--disable-xmlwriter			\
	--disable-dom				\
	--with-libxml-dir=$(INST_DIR)		\
	--with-zlib-dir=$(INST_DIR)		\
	--without-pear				\
	--without-iconv				\
	--enable-fastcgi                        \
	--enable-discard-path                   \
	--enable-force-cgi-redirect             \
	$(STD_VARS)
endef


   #BUILD_CMD_TARGET = sapi/cli/php
    BUILD_CMD_TARGET = sapi/cgi/php
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


install   : $(call tstamp_f,install,libxml2)

define INSTALL_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/sapi/cgi/php  $(RBIN_DIR) $(LOG)
endef


include $(RULES)
