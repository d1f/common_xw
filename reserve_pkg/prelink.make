PKG_TYPE      = DEBIAN1
PKG_VERSION   = 0.0.20090925
PKG_VER_PATCH = -6
PKG_SITE_PATH = pool/main/p/$(PKG_BASE)

include $(DEFS)


include $(TOOLS_PKG_DIR)/dpatch.mk


configure : $(call tstamp_f,install,toolchain libelf)
configure : $(call bstamp_f,install,gawk sed libtool)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--disable-shared	\
	--disable-64-bit	\
	--disable-largefile	\
	--with-gnu-ld		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
