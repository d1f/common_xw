PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.1.14
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/n/$(PKG_BASE)

include $(DEFS)


conf_opts += $(if $(CONFIG_WITH_IPv6), --with-ipv6)

ifeq ($(CONFIG_NGINX_WITH_OPENSSL),y)
  configure : $(call tstamp_f,install,openssl-0.9)
  conf_opts += --with-http_ssl_module --with-openssl=$(INST_DIR)
  conf_opts += --with-sha1=$(IINC_DIR)openssl
  conf_opts += --with-md5=$(IINC_DIR)openssl
endif

MODULESDIR = $(PKG_BLD_DIR)/debian/modules

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure $(LOG)	\
	--prefix=$(INST_DIR)			\
	--builddir=$(PKG_BLD_DIR)		\
	--without-select_module			\
	--with-poll_module			\
	--http-log-path=/var/log/nginx_acces.log \
	--http-client-body-temp-path=/tmp	\
	--without-http-cache			\
	--without-mail_pop3_module		\
	--without-mail_imap_module		\
	--without-mail_smtp_module		\
	--without-http_charset_module		\
	--without-http_gzip_module		\
	--without-http_ssi_module		\
	--without-http_userid_module		\
	--without-http_geo_module		\
	--without-http_map_module		\
	--without-http_split_clients_module	\
	--without-http_proxy_module		\
	--without-http_fastcgi_module		\
	--without-http_uwsgi_module		\
	--without-http_scgi_module		\
	--without-http_memcached_module		\
	--without-http_limit_conn_module	\
	--without-http_limit_req_module		\
	--without-http_referer_module		\
	--without-http_empty_gif_module		\
	--without-http_browser_module		\
	--without-http_upstream_ip_hash_module	\
	--with-http_stub_status_module		\
	--add-module=$(MODULESDIR)/nginx-upload-progress \
	--with-cc="$(CC)"			\
	--with-cpp="$(CPP)"			\
	--with-cc-opt="$(STD_CFLAGS)"		\
	--with-ld-opt="$(STD_LDFLAGS)"		\
	--without-pcre				\
	$(conf_opts)
endef

define _conf_
  --sbin-path=/sbin                         set nginx binary pathname
  --conf-path=/etc/nginx.conf               set nginx.conf pathname
  --error-log-path=/var/log/nginx_error.log set error log pathname
  --pid-path=/var/run/nginx.pid             set nginx.pid pathname
  --lock-path=/var/lock/nginx.lock          set nginx.lock pathname
  --user=USER                        set non-privileged user for worker processes
  --group=GROUP                      set non-privileged group for worker processes
endef


include $(RULES)
