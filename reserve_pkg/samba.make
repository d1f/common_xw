PKG_TYPE      = DEBIAN1
PKG_VERSION   = 3.0.24
PKG_VER_PATCH = -6etch10
PKG_SITE_PATH = pool/main/s/$(PKG_BASE)
PKG_SUB_DIR   = source

include $(DEFS)


POST_EXTRACT_CMD = $(RMDIR) $(_SRC_DIR)/$(PKG_DIR)/source/bin $(LOG)


PKG_PATCH_FILES += $(PRJ_PATCH_DIR)/$(PKG)_*.patch

include $(TOOLS_PKG_DIR)/quilt.mk


AC_VER = 2.59
include $(TOOLS_PKG_DIR)/autoconf.mk

pre-configure : $(TOOLCHAIN_INSTALL_STAMP)

define PRE_CONFIGURE_CMD
  cd $(PKG_BLD_SUB_DIR) && $(AUTOCONF)	$(LOG)
endef

MK_VARS  =     CC=$(CC)        LD=$(LD)
MK_VARS += RANLIB=$(RANLIB) STRIP=$(STRIP)

define CONFIGURE_CMD
  $(RM)                                       $(PKG_BLD_SUB_DIR)/config.cache
  echo "samba_cv_HAVE_GETTIMEOFDAY_TZ=yes"  > $(PKG_BLD_SUB_DIR)/config.cache
  #cp $(PKG_BLD_DIR)/debian/config.cache      $(PKG_BLD_SUB_DIR)/config.cache
  chmod a-w                                   $(PKG_BLD_SUB_DIR)/config.cache

  cd $(PKG_BLD_DIR)/source &&			\
	./configure			$(LOG)	\
	--build=$(BUILD) --host=$(TARGET)	\
	--config-cache				\
	     --prefix=$(INST_DIR)		\
	--exec-prefix=$(ROOT_DIR)		\
	--sysconfdir=/etc			\
	--with-privatedir=/etc/samba		\
	--localstatedir=/var			\
	--with-lockdir=/var/lock		\
	--with-piddir=/var/run			\
	--with-gnu-ld 				\
	--disable-pie				\
	--disable-cups				\
	--disable-iprint			\
	--disable-xmltest			\
	--with-fhs				\
	--without-readline			\
	--without-ldap				\
	--without-ads				\
	--without-krb5				\
	--without-expsam			\
	--with-automount			\
	--with-smbmount				\
	--without-pam				\
	--without-sys-quotas			\
	--with-utmp				\
	--with-syslog				\
	--with-sendfile-support			\
	--with-winbind				\
	$(MK_VARS)				\
	 CFLAGS="$(STD_CFLAGS) -I$(LK_INC_DIR) -I$(IINC_DIR)"
endef
#	LDFLAGS="$(STD_LDFLAGS) -L$(RLIB_DIR) -L$(LK_DIR)/lib" \


define  BUILD_CMD
  $(DOMAKE) $(MKJ) -C $(PKG_BLD_SUB_DIR) headers $(MK_VARS) $(LOG)
  $(BUILD_CMD_DEFAULT)
endef

define  INSTALL_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/source/bin/smbd        $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/source/bin/nmbd        $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/source/bin/smbclient    $(RBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/source/bin/rpcclient    $(RBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/source/bin/smbmount     $(RBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/source/bin/smbumount    $(RBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/source/bin/smbmnt       $(RBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/source/bin/mount.cifs  $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/source/bin/umount.cifs $(RSBIN_DIR) $(LOG)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
