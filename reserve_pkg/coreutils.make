PKG_TYPE      = DEBIAN1
PKG_VERSION   = 8.20
PKG_VER_PATCH = -3
PKG_SITE_PATH = pool/main/c/$(PKG_BASE)

include $(DEFS)


include $(TOOLS_PKG_DIR)/dpatch.mk


STD_CPPFLAGS += -D_GL_USE_STDLIB_ALLOC # disable rpl_malloc

configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call bstamp_f,install,sed flex bison gawk gettext)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)			\
	--exec-prefix=$(ROOT_DIR)		\
	--build=$(BUILD) --host=$(TARGET)	\
	--disable-largefile			\
	--enable-threads=posix			\
	--disable-acl				\
	--disable-assert			\
	--disable-rpath				\
	--disable-xattr				\
	--disable-libcap			\
	--disable-nls				\
	--with-gnu-ld				\
	--without-libiconv-prefix		\
	--without-libpth-prefix			\
	--without-included-regex		\
	--without-selinux			\
	--without-gmp				\
	--without-libintl-prefix		\
	$(STD_VARS)
endef


define BUILD_CMD
  touch    $(PKG_BLD_DIR)/src/make-prime-list.o
  $(BUILD_CC) -O -I $(PKG_BLD_DIR)/lib \
	   $(PKG_BLD_DIR)/src/make-prime-list.c \
	-o $(PKG_BLD_DIR)/src/make-prime-list
  $(BUILD_CMD_DEFAULT)
endef


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)


include $(RULES)
