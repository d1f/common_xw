PKG_TYPE      = DEBIAN1
PKG_VERSION   = 0.16
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/f/$(PKG_BASE)

PLATFORM      = build

include $(DEFS)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure	$(LOG)	\
	--prefix=$(INST_DIR)	\
	--with-memdir=/dev/shm	\
	--disable-static	\
	--with-gnu-ld		\
	$(STD_VARS) CXX=g++
endef
# FIXME: CXX=g++


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
