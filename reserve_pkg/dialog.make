#FIXME: use debian version
PKG_VERSION  = 1.0-20040920
PKG_SITES    = ftp://invisible-island.net/dialog
PKG_SRC_FILE = $(PKG_NAME).tgz

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure	$(LOG)		\
  --build=$(BUILD) --host=$(TARGET)			\
  --disable-nls --without-ncurses --without-ncursesw	\
  CFLAGS="$(STD_CFLAGS)"
endef

   DEPEND_CMD =    $(DEPEND_CMD_DEFAULT)
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
