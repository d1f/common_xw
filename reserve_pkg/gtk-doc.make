PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.18
PKG_VER_PATCH     = -2
PKG_SITE_PATH     = pool/main/g/$(PKG_BASE)

PLATFORM          = build

include $(DEFS)


configure : $(call bstamp_f,install,sed gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--with-gnu-ld		\
	$(STD_VARS)		\
	XSLTPROC=/bin/true
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD = $(INSTALL_CMD_DEFAULT)


include $(RULES)
