file          = usb.ids
PKG_SITES     = http://www.linux-usb.org
PKG_FILE_BASE = $(file)
PKG_REMOTE_FILE_SUFFICES =
PKG_LOCAL__FILE_SUFFICES =
PKG_UPDATEABLE = defined

include $(DEFS)


EXTRACT_FILES = $(file)

define INSTALL_CMD
  $(IW) $(INST_FILES) $(PKG_SRC_DIR)/$(file) $(RSHARE_DIR)/misc $(LOG)
endef


include $(RULES)
