# common project defines

ifndef	common/defs.mk
	common/defs.mk = included


ifeq ($(filter $(NOT_BUILD_TARGETS),$(MAKECMDGOALS)),)
  ifndef    PLATFORM
    $(error PLATFORM undefined)
  endif
endif


include     $(TOOLS_DIR)/defs.mk


ifneq ($(PLATFORM),build)
  goal_config := $(wildcard $(PLATFORM_DIR)/goal.config)
  ifneq ($(goal_config),)
    include $(PLATFORM_DIR)/goal.config
  endif
endif


KERN_IMAGE ?= $(IMAGES_DIR)/kernel.bin
CONF_IMAGE ?= $(IMAGES_DIR)/conf.bin
ROOT_IMAGE ?= $(LABEL_IMAGES_DIR)/root.bin
BOOT_IMAGE ?= $(LABEL_IMAGES_DIR)/boot_$(PART_ROOT_ON).img

     LABEL_PARAMS = $(LABEL)
ROOT_IMAGE_PARAMS = $(LABEL)_$(PART_ROOT_ON)
BOOT_IMAGE_PARAMS = $(LABEL)_$(PART_ROOT_ON)



# make functions:

# usage: $(IW)   mv -fv $(call root_static_libs_f, base names list) $(ILIB_DIR) $(LOG)
# usage: $(IW) $(RM) -v $(call root_static_libs_f, base names list) $(LOG)
root_static_libs_f = $(call mul_sets2_concat_f,$(addprefix $(RLIB_DIR)/lib,$(1)),.a .la)


# Usage: INSTALL_CMD = $(call INSTALL_PKG_ETC_FILES_F, etc files ...)
# Usage: INSTALL_CMD = $(call INSTALL_PKG_INIT_XFILES_F, init files ...)
# Usage:
# define INSTALL_CMD
#   $(call INSTALL_PKG_ETC_FILES_F, etc files ...)
#   $(call INSTALL_PKG_INIT_XFILES_F, init files ...)
# endef
#
INSTALL_PKGPKG_ETC_FILES_F = \
  $(IW) $(INST_FILES) $(addprefix $(PKG_PKG_DIR),$(1)) $(RETC_DIR) $(LOG)

INSTALL_PKGPKG_INIT_FILES_F = \
  $(IW) $(INST_FILES) $(addprefix $(PKG_PKG_DIR),$(1)) $(RINIT_DIR) $(LOG)

INSTALL_PKGPKG_INIT_XFILES_F = \
  $(IW) $(INSTxFILES) $(addprefix $(PKG_PKG_DIR),$(1)) $(RINIT_DIR) $(LOG)


endif # common/defs.mk
