PKG_TYPE      = DEBIAN3
PKG_VERSION   = 6.9.3
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/d/$(PKG_BASE)

include $(DEFS)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure $(LOG)		\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--os=linux		\
	--disable-debug		\
	--disable-static	\
	--enable-ipv4		\
	--disable-ipv6		\
	--enable-embedded	\
	--without-getline	\
	--without-strlcpy	\
	--with-poll		\
	--fork			\
	CC=$(CC)		\
	CPPFLAGS="$(STD_CPPFLAGS)" \
	CFLAGS="$(STD_CFLAGS)"	\
	LDFLAGS="$(STD_LDFLAGS)"
endef


build : $(call tstamp_f,install,toolchain)

#MK_VARS += HAVE_FORK=yes
#MK_VARS += HAVE_INIT=no
MK_VARS += $(STD_VARS)

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD =     $(CLEAN_CMD_DEFAULT)


INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/dhcpcd $(RSBIN_DIR) $(LOG)


include $(RULES)
