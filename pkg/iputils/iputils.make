PKG_TYPE          = DEBIAN3
PKG_VERSION       = 20101006
PKG_VER_PATCH     = -3
PKG_SITE_PATH     = pool/main/i/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES = $(PKG_PKG_DIR)/*.patch


define PRE_CONFIGURE_CMD
  $(SED) -ire 's/-lsysfs//g' $(PKG_BLD_DIR)/Makefile
endef


build : $(call tstamp_f,install,toolchain sysfsutils)
install-root : $(call tstamp_f,install-root,sysfsutils)

MK_VARS  = CC=$(CC)
MK_VARS += CCOPT="-D_GNU_SOURCE $(STD_CPPFLAGS) $(STD_CFLAGS)"
MK_VARS += LDFLAGS="$(STD_LDFLAGS) -lsysfs"
MK_VARS += KERNEL_INCLUDE=$(LK_INC_DIR)

ifneq ($(CONFIG_WITH_IPv6),y)
MK_VARS += IPV6_TARGETS=
endif

    BUILD_CMD_TARGET = ping
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILE) $(PKG_BLD_DIR)/ping $(RBIN_DIR)/ping.iputils $(LOG)
endef


include $(RULES)
