PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.0.28
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/a/$(PKG_BASE)

include $(DEFS)


pre-configure : $(call bstamp_f,install,autotools-dev)

define PRE_CONFIGURE_CMD
  $(RM)     $(PKG_BLD_DIR)/config.guess     $(PKG_BLD_DIR)/config.sub $(LOG)
  cp    $(BSHARE_DIR)/misc/config.guess $(BSHARE_DIR)/misc/config.sub \
			$(PKG_BLD_DIR) $(LOG)
  $(RELINK) $(PKG_BLD_DIR)/configure $(LOG)
endef


configure : $(call tstamp_f,install,toolchain alsa-lib ncurses)
install-root : $(call tstamp_f,install-root,alsa-lib ncurses)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--prefix=				\
	--build=$(BUILD) --host=$(TARGET)	\
	--disable-nls				\
	--disable-alsatest			\
	--disable-alsamixer			\
	--disable-xmlto				\
	--disable-largefile			\
	--with-gnu-ld				\
	--with-alsa-prefix=$(ILIB_DIR)		\
	--with-alsa-inc-prefix=$(IINC_DIR)	\
	--without-libiconv-prefix		\
	--without-libintl-prefix		\
	--with-librt				\
	--with-udev-rules-dir=""		\
	$(STD_VARS)
endef

define _cond_options_
		    --with-asound-state-dir=/var/run \
		    --disable-alsaconf \
  --disable-alsaconf      Disable alsaconf packaging
  --disable-alsaloop      Disable alsaloop packaging
endef


    MK_VARS += DESTDIR=$(INST_DIR)
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(IBIN_DIR)/amixer $(RBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(IBIN_DIR)/aplay  $(RBIN_DIR) $(LOG)
endef


include $(RULES)
