PKG_ORIG_DIR     = $(PKG_PKG_DIR)/src
PKG_FETCH_METHOD = DONE
SEPARATE_SRC_BLD_DIRS = defined
PLATFORM         = build

include $(DEFS)


configure : $(call bstamp_f,install,libxslt)

define CONFIGURE_CMD
  echo -n "$(I1)Preprocess wsdl and xsd files ... "
  for w in $(PKG_ORIG_DIR)/*.*; do \
	ext=$${w##*.}; \
	test $$ext = wsdl -o $$ext = xsd || continue;	\
	$(BBIN_DIR)/xsltproc -o $(PKG_BLD_DIR)/`basename $$w`	\
			$(PKG_ORIG_DIR)/patch.xslt $$w || exit 1; \
  done $(LOG)
  echo done
endef


build : $(call bstamp_f,install,gsoap)

gen =	$(BBIN_DIR)/wsdl2h -o /dev/stdout -t $(PKG_ORIG_DIR)/typemap.dat \
	$(filter-out %rw-2.wsdl %bw-2.wsdl,$(wildcard $(PKG_BLD_DIR)/*.wsdl)) 2>> $(LOG_FILE) \
	| $(BBIN_DIR)/soapcpp2 -inxL$${client_server} -f100 -I$(BSHARE_DIR)/gsoap/import $(LOG)

define BUILD_CMD
  $(MKDIR_P) $(PKG_BLD_DIR)/client $(PKG_BLD_DIR)/server

  echo -n "$(I1)Generate client *.h and *.cpp from wsdl ... "
  cd $(PKG_BLD_DIR)/client && { export client_server=C; $(gen); }
  echo done

  echo -n "$(I1)Generate server *.h and *.cpp from wsdl ... "
  cd $(PKG_BLD_DIR)/server && { export client_server=S; $(gen); }
  echo done
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


#define INSTALL_CMD
#endef


include $(RULES)
