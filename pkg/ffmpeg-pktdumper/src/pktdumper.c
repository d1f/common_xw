/*
 * Copyright (c) 2005 Francois Revol
 *
 * This file is part of FFmpeg.
 *
 * FFmpeg is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * FFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FFmpeg; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <stdio.h>
#include <libavformat/avformat.h>

static int usage(int ret)
{
    fprintf(stderr, "Dump (up to maxpkts) AVPackets as they are demuxed by libavformat.\n");
    fprintf(stderr, "pktdumper file [maxpkts]\n");
    return ret;
}

static void pkt_dump(const AVPacket *pkt, AVRational time_base, size_t pkt_seq)
{
    printf("stream #%d, time_base: %2d/%3d:\n", pkt->stream_index, time_base.num, time_base.den);
    printf("  seq num : %5zu\n", pkt_seq);
    printf("  keyframe: %5d\n", (pkt->flags & AV_PKT_FLAG_KEY) != 0);
    printf("  duration: %5d %0.3f\n", pkt->duration, pkt->duration * av_q2d(time_base));

    /* DTS is _always_ valid after av_read_frame() */
    printf("  dts     : ");
    if (pkt->dts == AV_NOPTS_VALUE)
        printf("    N/A");
    else
        printf("%5lld %0.3f", pkt->dts, pkt->dts * av_q2d(time_base));
    printf("\n");

    /* PTS may not be known if B-frames are present. */
    printf("  pts     : ");
    if (pkt->pts == AV_NOPTS_VALUE)
        printf("    N/A");
    else
        printf("%5lld %0.3f", pkt->pts, pkt->pts * av_q2d(time_base));
    printf("\n");

    printf("  pos     : %5lld\n", pkt->pos);
    printf("  size    : %5d\n", pkt->size);
}

int main(int argc, char **argv)
{
    const char *input_filename = NULL;
    AVFormatContext *fctx = NULL;
    AVPacket pkt;
    int64_t pktnum  = 0;
    int64_t maxpkts = 0;
    int err;

    av_log_set_level(AV_LOG_INFO);

    input_filename = argv[1];

    if (argc < 2)
        return usage(1);
    if (argc > 2)
        maxpkts = atoi(argv[2]);

    // register all file formats
    av_register_all();

    err = avformat_open_input(&fctx, input_filename, NULL, NULL);
    if (err < 0) {
        fprintf(stderr, "cannot open input: %s\n", av_err2str(err));
        return 1;
    }

    err = avformat_find_stream_info(fctx, NULL);
    if (err < 0) {
        fprintf(stderr, "avformat_find_stream_info: error %d\n", err);
        return 1;
    }

    fprintf(stderr, "Streams: %u\n", fctx->nb_streams);

    size_t pkt_seq[fctx->nb_streams];
    memset(pkt_seq, 0, sizeof(pkt_seq));

    av_dump_format(fctx, 0, input_filename, 0);

    av_init_packet(&pkt);

    while ((err = av_read_frame(fctx, &pkt)) >= 0) {

	AVStream *st = fctx->streams[pkt.stream_index];
	pkt_dump(&pkt, st->time_base, pkt_seq[pkt.stream_index]);
	pkt_seq[pkt.stream_index]++;

        av_free_packet(&pkt);
        pktnum++;
        if (maxpkts && (pktnum >= maxpkts))
            break;
    }

    avformat_close_input(&fctx);

    return 0;
}
