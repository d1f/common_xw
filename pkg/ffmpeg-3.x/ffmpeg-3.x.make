PKG_TYPE      = ORIGIN
#PKG_TYPE      = DEBIAN3

PKG_VERSION   = 3.0.2
PKG_BASE      = ffmpeg
PKG_DIR       = $(PKG)

ifeq ($(PKG_TYPE),ORIGIN)
  PKG_SITES     = http://ffmpeg.org
  PKG_SITE_PATH = releases
else ifeq ($(PKG_TYPE),DEBIAN3)
  PKG_VER_PATCH = -1
  PKG_SITE_PATH = pool/main/f/$(PKG_BASE)
else
  $(error Unsupported PKG_TYPE $(PKG_TYPE))
endif

include $(DEFS)


PKG_PATCH2_FILES = $(PKG_PKG_DIR)/*.patch


PKG_MENUCONFIG_FILE = $(PLATFORM_DIR)/$(PKG).config

#  parse error on --env="ENV=override"
define CONFIGURE_PARSE_CMD
  echo -n "$(I1)Generate Config.in and Config.list ... "
  $(PKG_BLD_SUB_DIR)/configure --help | fgrep -v -- "--env=" | 	\
	$(TOOLS_DIR)/script/configure-parse	\
		$(PKG_BLD_SUB_DIR)/Config.in	\
		$(PKG_BLD_SUB_DIR)/Config.list
  echo done
endef

include $(TOOLS_PKG_DIR)/configure-menuconfig.mk


configure : $(call tstamp_f,install,toolchain pkg-config)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&				\
     ./configure $(LOG)				\
	--prefix=$(INST_DIR)			\
	--libdir=$(ILIB_DIR)			\
	--incdir=$(IINC_DIR)			\
	--mandir=$(IMAN_DIR)			\
	--cross-prefix=$(CROSS_PREFIX)		\
	--nm=$(NM) --ar=$(AR) --as=$(AS)	\
	--cc=$(CC) --cxx=$(CXX) --dep-cc=$(CC)	\
	--ld=$(CC) --ranlib=$(RANLIB)		\
	--host-cc=$(BUILD_CC)			\
	--host-cflags='-O -pipe -g0'		\
	--extra-cflags="$(STD_CFLAGS) $(STD_CPPFLAGS)" \
	--extra-ldflags="$(STD_LDFLAGS)"	\
	--arch=$(NARCH) --cpu=$(BFD_ARCH)	\
	--pkg-config=$(TARGET)-pkg-config	\
	$(CONFIGURE_OPTIONS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(IW) $(INST_FILES) $(PKG_BLD_DIR)/libavformat/url.h $(IINC_DIR)/libavformat $(LOG)
endef

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libavutil.so*   $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libavformat.so* $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libavcodec.so*  $(RLIB_DIR) $(LOG)
endef


include $(RULES)
