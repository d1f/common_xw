PKG_TYPE          = DEBIAN3
PKG_VERSION       = 6.3
PKG_VER_PATCH     = -8
PKG_SITE_PATH     = pool/main/r/$(PKG_BASE)
SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


install : $(call tstamp_f,uninstall,readline5)


PKG_PATCH2_FILES = $(PKG_PKG_DIR)/*.patch


configure : $(call tstamp_f,install,toolchain ncurses)
install-root : $(call tstamp_f,install-root,ncurses)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
	$(PKG_SRC_DIR)/configure -C $(LOG)	\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=$(INST_DIR)			\
	--disable-multibyte			\
	--enable-shared --disable-static	\
	--disable-largefile			\
	--with-curses				\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libreadline.so* $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libhistory.so*  $(RLIB_DIR) $(LOG)
endef


include $(RULES)
