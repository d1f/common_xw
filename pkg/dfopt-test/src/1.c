#define _GNU_SOURCE
#include <df/opt.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h> // basename

static int usage(const dfopt_t opts[], const char *av0)
{
    printf("Usage: %s \\\n", basename(av0));
    dfopt_print(opts, stdout);
    exit(EXIT_SUCCESS);
}

int main(int ac, char *av[])
{
    static int help_flag = 0;
    static int opt1_flag = 0;
    static int num = -1;
    static char out[256] = "";
    static const char *ref = NULL;

    static const dfopt_t opts[] =
    {
	{ .key = "help", .desc = "this text", .int_ptr = &help_flag, .int_val = 1 },
	{ .key = "opt1", .desc = "option 1" , .int_ptr = &opt1_flag, .int_val = 1 },
	{ .key = "num" , .desc = "<int> : number", .scan_fmt = "%d", .scan_ptr = &num,
	  .has_value = 1
	},
	{
	    .key = "out", .desc = "<string> : output file or '-' for stdout",
	    .has_value = 1, .scan_fmt = "%s", .scan_ptr = out
	},
	{
	    .key = "ref", .desc = "<string> : string parameter by reference",
	    .has_value = 1, .ccp_ptr = &ref,
	},
	{ .key = NULL }
    };

    size_t rc = dfopt_parse(opts, ac, av);
    printf("index: %d, help: %d, opt1: %d, num: %d, out_file: '%s', ref: '%s'\n",
	   rc, help_flag, opt1_flag, num, out, ref);

    if (help_flag)
	usage(opts, av[0]);

    return EXIT_SUCCESS;
}
