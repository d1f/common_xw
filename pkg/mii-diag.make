PKG_TYPE      = DEBIAN1
PKG_VERSION   = 2.11
PKG_VER_PATCH = -3
PKG_SITE_PATH = pool/main/m/$(PKG_BASE)

include $(DEFS)


build : $(call tstamp_f,install,toolchain)

MK_VARS = $(STD_VARS)
BUILD_CMD_TARGET = mii-diag
BUILD_CMD = $(BUILD_CMD_DEFAULT)

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/mii-diag $(RBIN_DIR) $(LOG)


include $(RULES)
