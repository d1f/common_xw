PKG_VERSION   = 0.2.3
PKG_SITES     = http://downloads.yoctoproject.org
PKG_SITE_PATH = releases/opkg

include $(DEFS)


PKG_PATCH_FILES += $(PKG_PKG_DIR)/*.patch


ifeq ($(PLATFORM),build)
  conf_opts += --prefix=$(INST_DIR)

  conf_opts += --with-opkgetcdir=$(INST_DIR)/etc
  conf_opts += --with-opkglibdir=$(INST_DIR)/var # /opkg appended
  conf_opts += --with-opkglockfile=$(INST_DIR)/var/lock/opkg.lock
else
  conf_opts += --prefix=/
  conf_opts += --build=$(BUILD) --host=$(TARGET)

  conf_opts += --with-opkgetcdir=$(ETC_DIR)
  conf_opts += --with-opkglibdir=/var # /opkg appended
  conf_opts += --with-opkglockfile=/var/lock/opkg.lock
endif

conf_opts += --disable-shared
conf_opts += --enable-static
conf_opts += --disable-pathfinder
conf_opts += --disable-curl
conf_opts += --disable-sha256
conf_opts += --disable-openssl
conf_opts += --disable-ssl-curl
conf_opts += --disable-gpg
conf_opts += --disable-shave
conf_opts += --with-gnu-ld

configure : $(call bstamp_f,install,sed gawk pkg-config)
configure : $(call tstamp_f,install,toolchain)

define CONFIGURE_CMD
 cd $(PKG_BLD_DIR) && \
    ./configure -C $(LOG)	\
       $(conf_opts)		\
       $(STD_VARS)
endef


ifneq ($(PLATFORM),build)
  MK_VARS += DESTDIR=$(INST_DIR)
endif

BUILD_CMD = $(BUILD_CMD_DEFAULT)


#FIXME: /etc/opkg.conf

_INSTALL_CMD = $(IW) $(INSTxFILE) $(PKG_BLD_DIR)/src/opkg-cl $(IBIN_DIR)/opkg $(LOG)

ifeq ($(PLATFORM),build)
  define INSTALL_CMD
    $(_INSTALL_CMD)
    $(IW) $(MKDIR_P) $(INST_DIR)/var/lock $(LOG)
  endef
else
  INSTALL_CMD = $(_INSTALL_CMD)
  define INSTALL_ROOT_CMD
    $(IW) $(INSTxFILES)    $(IBIN_DIR)/opkg      $(RBIN_DIR) $(LOG)
    $(IW) $(INST_FILES) $(PKG_PKG_DIR)/opkg.conf $(RETC_DIR) $(LOG)
  endef
endif


include $(RULES)
