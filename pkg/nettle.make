PKG_TYPE      = DEBIAN3
PKG_VERSION   = 3.2
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/n/$(PKG_BASE)

include $(DEFS)


# avoid reconf
PRE_CONFIGURE_CMD = $(RELINK) $(PKG_BLD_DIR)/stamp-h.in $(LOG)
MK_VARS += AUTOHEADER=true


configure : $(call bstamp_f,install,ccache m4)
configure : $(call tstamp_f,install,pkg-config)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
  ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--disable-static	\
	--enable-shared		\
	--disable-openssl	\
	--disable-documentation	\
	--enable-fat		\
	--enable-mini-gmp	\
	--with-include-path=$(INST_DIR)/include \
	    --with-lib-path=$(INST_DIR)/lib \
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libnettle.so*  $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libhogweed.so* $(RLIB_DIR) $(LOG)
endef


include $(RULES)
