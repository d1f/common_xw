PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.0.28
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/a/$(PKG_BASE)

include $(DEFS)


AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk

AM_VER = 1.11
include $(TOOLS_PKG_DIR)/automake.mk


pre-configure : $(call bstamp_f,install,autotools-dev gawk sed libtool)

LIBTOOLIZE_FLAGS = -f -c --verbose

define PRE_CONFIGURE_CMD
  $(RM)     $(PKG_BLD_DIR)/version                  $(LOG)
  $(RM_R)   $(PKG_BLD_DIR)/include/alsa             $(LOG)
  $(RM)     $(PKG_BLD_DIR)/include/asoundlib.h      $(LOG)
  $(RELINK) $(PKG_BLD_DIR)/include/asoundlib-head.h $(LOG)
  $(RM)     $(PKG_BLD_DIR)/config.guess     $(PKG_BLD_DIR)/config.sub $(LOG)
  cp    $(BSHARE_DIR)/misc/config.guess $(BSHARE_DIR)/misc/config.sub \
			$(PKG_BLD_DIR) $(LOG)

  cd $(PKG_BLD_DIR) && $(ACLOCAL)    $(ACLOCAL_FLAGS) $(LOG)
  cd $(PKG_BLD_DIR) && $(AUTOMAKE)  $(AUTOMAKE_FLAGS) $(LOG)
  cd $(PKG_BLD_DIR) && $(AUTOCONF)  $(AUTOCONF_FLAGS) $(LOG)
  cd $(PKG_BLD_DIR) && libtoolize $(LIBTOOLIZE_FLAGS) $(LOG)
endef


configure : $(call tstamp_f,install,toolchain)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--prefix=				\
	--build=$(BUILD) --host=$(TARGET)	\
	--enable-shared --disable-static	\
	--disable-python			\
	--with-gnu-ld				\
	--without-debug				\
	--with-libdl				\
	--with-pthread				\
	--with-librt				\
	--with-alsa-devdir=/dev			\
	$(STD_VARS)
endef

define _cond_options_
  --enable-resmgr         support resmgr (optional)
  --disable-aload         disable reading /dev/aload*
  --disable-mixer         disable the mixer component
  --disable-pcm           disable the PCM component
  --disable-rawmidi       disable the raw MIDI component
  --disable-hwdep         disable the hwdep component
  --disable-seq           disable the sequencer component
  --disable-ucm           disable the use-case-manager component
  --disable-alisp         disable the alisp component
  --disable-old-symbols   disable old obsoleted symbols

  --with-configdir=dir    path where ALSA config files are stored
  --with-plugindir=dir    path where ALSA plugin files are stored
  --with-versioned        shared library will be compiled with versioned
                          symbols (default = yes)
endef


    MK_VARS += DESTDIR=$(INST_DIR)
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

define INSTALL_CMD
  $(IW) $(MKDIR_P) $(IBIN_DIR) $(LOG)
  $(INSTALL_CMD_DEFAULT)
endef

INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libasound.so* $(RLIB_DIR) $(LOG)


include $(RULES)
