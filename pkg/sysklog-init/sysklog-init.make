# busybox' syslog, klog daemons
PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install-root : $(call tstamp_f,install-root,busybox base-init)

INSTALL_ROOT_CMD = $(call INSTALL_PKGPKG_INIT_XFILES_F, syslog klog)


include $(RULES)
