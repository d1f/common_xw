PKG_TYPE      = DEBIAN1
PKG_VERSION   = 1.4
PKG_VER_PATCH = -5
PKG_SITE_PATH = pool/main/b/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES = $(PKG_PKG_DIR)/*.patch


AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk
PRE_CONFIGURE_CMD = cd $(PKG_BLD_DIR) && $(AUTOCONF) $(AUTOCONF_FLAGS) $(LOG)


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call tstamp_f,depend,linux)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C	$(LOG)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--prefix=$(INST_DIR)	\
	--libdir=$(ILIB_DIR)	\
	--with-linux-headers=$(LK_INC_DIR)	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ISBIN_DIR)/brctl $(RSBIN_DIR) $(LOG)


include $(RULES)
