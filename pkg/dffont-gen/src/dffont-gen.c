#define _GNU_SOURCE
#include <string.h> // basename strerror
#include <stdio.h>  // printf
#include <utf8.h>
#include <df/log.h>
#include <df/error.h>
#include <df/opt.h>
#include <df/font.h>
#include <df/cairo.h>


static int usage(const dfopt_t opts[], const char *av0)
{
    printf("\n");
    printf("Usage: %s [options --] family size out_file code-range ...\n", basename(av0));
    printf("\n");
    printf("code-range: unicode points range, from-to, hexadecimal\n");
    printf("\n");
    printf("options:\n");
    dfopt_print(opts, stdout);
    printf("\n");
    exit(EXIT_FAILURE);
}

int main(int ac, char *av[])
{
    dflog_open(basename(av[0]), DFLOG_STDERR);

    static int help                = 0;
    static int verb                = 0;
    static const char *font_slant  = "normal";
    static const char *font_weight = "normal";

    static const dfopt_t opts[] =
    {
	{ .key = "help", .desc = "this text", .int_ptr = &help, .int_val = 1 },
	{ .key = "v"   , .desc = "verbose"  , .int_ptr = &verb, .int_val = 1 },
	{
	    .key = "slant", .desc = "font slant: normal | italic | oblique; normal by default",
	    .has_value = 1, .ccp_ptr = &font_slant
	},
	{
	    .key = "weight", .desc = "font weight: normal | bold; normal by default",
	    .has_value = 1, .ccp_ptr = &font_weight
	},
	{ .key = NULL }
    };

    size_t arg_idx = dfopt_parse(opts, ac, av);

    if (help || ac == 1)
	usage(opts, av[0]);

    if ((ac - arg_idx) < 3)
	dferror(EXIT_FAILURE, 0, "font family, size, out file parameters are not supplied");

    const char *font_family =      av[arg_idx++];
    double      font_size   = atof(av[arg_idx++]);
    const char *out_dffont  =      av[arg_idx++];


    cairo_surface_t *surface =
	cairo_image_surface_create(CAIRO_FORMAT_A1, 0, 0);
    cairo_t *cr = cairo_create(surface);
    //cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
    if (dfcairo_set_font(cr, font_family, font_slant, font_weight, font_size) < 0)
	return EXIT_FAILURE;

    cairo_font_extents_t fe;
    cairo_font_extents (cr, &fe);
    if (verb)
	dfcairo_print_font_extents(stdout, &fe);

    // clear
    cairo_destroy (cr); cr = NULL;
    cairo_surface_destroy (surface); surface = NULL;


    // again
    surface = cairo_image_surface_create (CAIRO_FORMAT_A1, fe.max_x_advance, fe.height);
    cr = cairo_create (surface);
    //cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
    if (dfcairo_set_font(cr, font_family, font_slant, font_weight, font_size) < 0)
	return EXIT_FAILURE;

    unsigned char *data = cairo_image_surface_get_data(surface);
    size_t height = cairo_image_surface_get_height(surface);
    size_t stride = cairo_image_surface_get_stride(surface);


    FILE *out = fopen(out_dffont, "w");
    if (out == NULL)
	dferror(EXIT_FAILURE, errno, "Can't create %s", out_dffont);

    char font[128];
    snprintf(font, sizeof(font),
	     "%s/%s/%s/%.f", font_family, font_slant, font_weight, font_size);

    int rc = dffont_write_header(out, fe.height, font);
    if (rc < 0)
	return EXIT_FAILURE;


    for ( ; arg_idx < (size_t)ac; ++arg_idx)
    {
	unsigned long from=0, to=0;
	if (sscanf(av[arg_idx], "%lx-%lx", &from, &to) != 2)
	    dferror(EXIT_FAILURE, 0, "wrong syntax of unicode range: %s", av[arg_idx]);
	//printf("%04x-%04x\n", from, to);

	char utext[8];

	wchar_t wchar;
	for (wchar = from; (unsigned long)wchar <= to; ++wchar)
	{
	    if ( wchar < 0x0020 || // space
		(wchar >= 0xd800 &&
		 wchar <= 0xdfff) // Surrogate pairs
	       )
		continue;

	    memset(utext, 0, sizeof(utext));
	    size_t wlen = wchar_to_utf8(&wchar, 1, utext, sizeof(utext), 0);
	    if (wlen == 0)
	    {
		dferror(EXIT_SUCCESS, 0, "Error conversion %04lx to utf-8", (unsigned long)wchar);
		continue;
	    }


	    memset(data, 0, height * stride);

	    cairo_move_to(cr, 0.0, fe.ascent);
	    cairo_show_text(cr, utext);
	    cairo_surface_flush(surface);

	    cairo_text_extents_t te;
	    cairo_text_extents(cr, utext, &te);

	    if (wchar != 0x0020)
		if (te.width == 0 && te.height == 0) // no pixel data in source font
		    continue;

	    if (verb)
	    {
		printf("%04lx, utf-8 length: %zu, char: |%s|   ",
		       (unsigned long)wchar, strlen(utext), utext);
		dfcairo_print_text_extents(stdout, &te);
	    }

	    rc = dffont_write_glyph(out, wchar, (const char *)data,
				    te.x_advance, stride, height);
	    if (rc < 0)
		return EXIT_FAILURE;
	}
    }

    rc = dffont_write_empty_glyph(out);
    if (rc < 0)
	return EXIT_FAILURE;
    if (fclose(out) < 0)
	dferror(EXIT_FAILURE, errno, "Error writing %s", out_dffont);

    cairo_destroy(cr);
    cairo_surface_destroy(surface);
    return EXIT_SUCCESS;
}
