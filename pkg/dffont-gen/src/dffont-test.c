#define _GNU_SOURCE
#include <string.h> // basename strerror
#include <stdio.h>  // printf
#include <limits.h> // CHAR_BIT
#include <utf8.h>
#include <df/log.h>
#include <df/error.h>
#include <df/opt.h>
#include <df/font.h>
#include <df/cairo.h>


static void print_line(int character, size_t count)
{
    while (count-- > 0)
	putchar(character);
    putchar('\n');
}

static int usage(const dfopt_t opts[], const char *av0)
{
    printf("\n");
    printf("Usage: echo -n 'Text' | %s [options --] family size\n", basename(av0));
    printf("\n");
    printf("options:\n");
    dfopt_print(opts, stdout);
    printf("\n");
    exit(EXIT_FAILURE);
}

int main(int ac, char *av[])
{
    dflog_open(basename(av[0]), DFLOG_STDERR);

    static int help                = 0;
    static int verb                = 0;
    static int std_in              = 0;
    static int out_bitmap          = 0;
    static const char *out_png     = NULL;
    static const char *font_slant  = "normal";
    static const char *font_weight = "normal";

    static const dfopt_t opts[] =
    {
	{ .key = "help", .desc = "this text", .int_ptr = &help, .int_val = 1 },
	{ .key = "v"   , .desc = "verbose"  , .int_ptr = &verb, .int_val = 1 },
	{
	    .key = "stdin", .desc = "input characters from stdin",
	    .int_ptr = &std_in, .int_val = 1
	},
	{
	    .key = "out-bitmap", .desc = "output ASCII bit map to stdout",
	    .int_ptr = &out_bitmap, .int_val = 1
	},
	{
	    .key = "out-png", .desc = "output PNG file",
	    .has_value = 1, .ccp_ptr = &out_png
	},
	{
	    .key = "slant", .desc = "font slant: normal | italic | oblique; normal by default",
	    .has_value = 1, .ccp_ptr = &font_slant
	},
	{
	    .key = "weight", .desc = "font weight: normal | bold; normal by default",
	    .has_value = 1, .ccp_ptr = &font_weight
	},
	{ .key = NULL }
    };

    size_t arg_idx = dfopt_parse(opts, ac, av);

    if (help || ac == 1)
	usage(opts, av[0]);

    if ((ac - arg_idx) < 2)
	dferror(EXIT_FAILURE, 0, "font family and size parameters are not supplied");
    if ((ac - arg_idx) > 2)
	dferror(EXIT_FAILURE, 0, "extra parameters supplied");

    const char *font_family           =      av[arg_idx++];
    double      font_size             = atof(av[arg_idx++]);


    static char text[256];
    if (std_in)
    {
	int c;
	size_t i = 0;
	do
	{
	    c = getc(stdin);
	    if (c != EOF && i < sizeof(text)-1)
		text[i++] = c;
	} while (c != EOF);

	if (ferror(stdin))
	    dferror(EXIT_FAILURE, errno, "Error reading stdin");

	if (i >= sizeof(text)-1)
	    dferror(EXIT_FAILURE, 0, "Too long input, only %zu allowed", sizeof(text)-1);

	text[i] = 0;

	//flags: UTF8_IGNORE_ERROR | UTF8_SKIP_BOM
	size_t wlen = utf8_to_wchar(text, strlen(text), NULL, 0, 0);
	if (wlen == 0)
	    dferror(EXIT_FAILURE, errno, "Text input error");
	if (verb)
	    printf("Text input length: %zu, wide characters length: %zu\n", strlen(text), wlen);
    }


    cairo_surface_t *surface =
	cairo_image_surface_create(CAIRO_FORMAT_A1, 0, 0);
    cairo_t *cr = cairo_create(surface);
    //cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
    if (dfcairo_set_font(cr, font_family, font_slant, font_weight, font_size) < 0)
	return EXIT_FAILURE;


    cairo_font_extents_t fe;
    cairo_font_extents (cr, &fe);
    if (verb)
	dfcairo_print_font_extents(stdout, &fe);


    cairo_text_extents_t te;
    cairo_text_extents (cr, text, &te);
    if (verb)
	dfcairo_print_text_extents(stdout, &te);


    // clear
    cairo_destroy (cr); cr = NULL;
    cairo_surface_destroy (surface); surface = NULL;


    // again
    surface = cairo_image_surface_create (CAIRO_FORMAT_A1, te.x_advance, fe.height);
    cr = cairo_create (surface);
    //cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
    if (dfcairo_set_font(cr, font_family, font_slant, font_weight, font_size) < 0)
	return EXIT_FAILURE;

    //cairo_move_to (cr, 0.0, fe.height-fe.descent);
    cairo_move_to (cr, 0.0, fe.ascent);
    cairo_show_text (cr, text);

    cairo_surface_flush(surface);
    size_t width  = cairo_image_surface_get_width (surface);
    size_t height = cairo_image_surface_get_height(surface);
    size_t stride = cairo_image_surface_get_stride(surface);
    if (verb)
	printf("surface: %d x %d pixels, stride: %d bytes\n", width, height, stride);

    if (out_bitmap)
    {
	size_t width_stride = DFFONT_BIT_WIDTH_TO_CHAR_STRIDE(width) * CHAR_BIT;
	char *data = (char*)cairo_image_surface_get_data(surface);
	print_line('=', width_stride);
	dffont_out_ascii_bitmap(stdout, data, stride, width, height);
	print_line('=', width_stride);
    }

    cairo_destroy (cr);
    if (out_png != NULL)
	cairo_surface_write_to_png (surface, out_png);
    cairo_surface_destroy (surface);

    return EXIT_SUCCESS;
}
