#define _GNU_SOURCE
#include <string.h> // basename strerror
#include <stdio.h>  // printf
#include <utf8.h>
#include <df/log.h>
#include <df/error.h>
#include <df/opt.h>
#include <df/font.h>


static int usage(const dfopt_t opts[], const char *av0)
{
    printf("\n");
    printf("Usage: %s [options --] file\n", basename(av0));
    printf("\n");
    printf("options:\n");
    dfopt_print(opts, stdout);
    printf("\n");
    exit(EXIT_FAILURE);
}

static void pr_header(void)
{
    printf(" UC  width first  last char\n");
}

static void pr_table_line(void)
{
    printf("---- ----- ----- ----- ----\n");
}

int main(int ac, char *av[])
{
    dflog_open(basename(av[0]), DFLOG_STDERR);

    static int help                = 0;
    static int bitmap              = 0;

    static const dfopt_t opts[] =
    {
	{ .key = "help", .desc = "this text", .int_ptr = &help, .int_val = 1 },
	{ .key = "bitmap", .desc = "print glyph bit map", .int_ptr = &bitmap, .int_val = 1 },
	{ .key = NULL }
    };

    size_t arg_idx = dfopt_parse(opts, ac, av);

    if (help || ac == 1)
	usage(opts, av[0]);

    if ((ac - arg_idx) < 1)
	dferror(EXIT_FAILURE, 0, "font file parameter is not supplied");
    if ((ac - arg_idx) > 1)
	dferror(EXIT_FAILURE, 0, "extra parameters supplied");

    const char *in_dffont = av[arg_idx++];

    dffont_t *font = dffont_load(in_dffont);
    if (font == NULL)
	return EXIT_FAILURE;

    printf("\n");
    printf("Height: %u, Font: %s\n", font->header->height, font->header->name);
    printf("\n");
    pr_header();
    pr_table_line();
    size_t glyph_count = 0;
    size_t max_width = 0;
    const dffont_glyph_t *glyph = dffont_glyph_first(font);
    do
    {
	wchar_t wchar = le32toh(glyph->unicode_point);
	if (wchar == 0) // correct EOF
	    break;

	char utext[8];
	memset(utext, 0, sizeof(utext));

	size_t wlen = wchar_to_utf8(&wchar, 1, utext, sizeof(utext), 0);
	if (wlen == 0)
	    dferror(EXIT_FAILURE, errno, "Error conversion %04lx to utf-8", (unsigned long)wchar);

	printf("%04x %5u %5u %5u %s\n",
	       le32toh(glyph->unicode_point),
	       glyph->width, glyph->first_row, glyph->last_row, utext);

	if (bitmap)
	{
	    dffont_out_ascii_bitmap(stdout, (const char*)(glyph->bits),
				    DFFONT_BIT_WIDTH_TO_CHAR_STRIDE(glyph->width),
				    glyph->width,
				    glyph->last_row - glyph->first_row + 1);
	}

	++glyph_count;
	if (glyph->width > max_width)
	    max_width = glyph->width;

	glyph = dffont_glyph_next(glyph);

    } while (1);

    pr_table_line();
    pr_header();
    printf("\ncount: %zu, max width: %zu\n", glyph_count, max_width);

    return EXIT_SUCCESS;
}
