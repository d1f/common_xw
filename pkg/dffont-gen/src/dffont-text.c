#define _GNU_SOURCE
#include <df/font.h>
#include <df/log.h>
#include <df/error.h>
#include <string.h> // basename
#include <stdio.h>
#include <stdlib.h> // EXIT_FAILURE exit
#include <utf8.h>
#include <limits.h> // UCHAR_MAX

static void usage(const char *av0)
{
    printf("Usage: %s font-file text\n", basename(av0));
    exit(EXIT_FAILURE);
}

static void out_charmap(FILE *out, const char *map, size_t width, size_t height)
{
    size_t x, y;
    for (y = 0; y < height; ++y)
    {
	size_t ystart = y * width;
	for (x = 0; x < width; ++x)
	{
	    char ch = map[ystart + x];
	    fputc(ch?'*':'0', out);
	}
	fputc('\n', out);
    }
}

int main(int ac, char *av[])
{
    if (ac != 3)
	usage(av[0]);

    dflog_open(basename(av[0]), DFLOG_STDERR);

    const char *font_file = av[1];
    const char *utext     = av[2];

    dffont_t *font = dffont_load(font_file);
    if (font == NULL)
	return EXIT_FAILURE;

    // returns malloced struct with data.
    // NULL on error, message issued.
    df_charmap_t *map = dffont_text_to_charmap(utext, font, UCHAR_MAX);
    if (map == NULL)
	return EXIT_FAILURE;

    printf("Font height: %zu, Pix width: %zd\n", map->height, map->width);

    out_charmap(stdout, map->map, map->width, map->height);

    df_charmap_delete(&map);
    dffont_delete(&font);
    return EXIT_SUCCESS;
}
