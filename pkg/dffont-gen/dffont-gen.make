PKG_ORIG_DIR     = $(PKG_PKG_DIR)/src
PKG_FETCH_METHOD = DONE
SEPARATE_SRC_BLD_DIRS = defined
PLATFORM         = build

include $(DEFS)


libs  = dffont unicode dfcairo
libs += df-charmap dfopt df df-mem df-pthread

build : $(call bstamp_f, install, $(libs))
#build : $(call bstamp_f, install, libpng zlib)

BUILD_TYPE     = SINGLE_SRC_BINS
LIBS           = $(libs) cairo pixman-1 freetype png z pthread m
MAKES_MAKEFILE = $(TOOLS_DIR)/makes/all.Makefile
include          $(TOOLS_DIR)/makes/conf.mk

CONFIGURE_CMD  = $(MAKES_CONFIGURE_CMD)


#MK_VARS = Q=
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(INSTALL_ROOT_CMD_DEFAULT)


.PHONY: fonts
fonts : install
	@$(IWSH) "dffont_gen=$(BBIN_DIR)/dffont-gen $(PKG_PKG_DIR)/gen-fonts $(INST_DIR)/share/fonts/video"

fonts-clean :
	@$(RM_R) fonts


include $(RULES)
