PKG_TYPE      = DEBIAN1
PKG_VERSION   = 0.94.14rc21
PKG_VER_PATCH = -3.1
PKG_SITE_PATH = pool/main/b/$(PKG_BASE)

include $(DEFS)


include $(TOOLS_PKG_DIR)/quilt.mk

PKG_PATCH2_FILES = $(PKG_PKG_DIR)/*.patch


configure : $(call tstamp_f,install,toolchain)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ac_cv_func_memcmp_working=yes	\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--disable-gunzip		\
	--disable-debug			\
	--with-poll			\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/src/boa         $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/src/boa_indexer $(RSBIN_DIR) $(LOG)
endef


include $(RULES)
