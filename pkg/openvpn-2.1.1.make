PKG_TYPE      = ORIGIN
#PKG_VERSION  = 2.1.1
#PKG_URL       = https://openvpn.net/release/$(PKG).tar.gz
PKG_SITES  = http://openvpn.net/release
PKG_SITES += http://ftp.slackware.com/pub/slackware/slackware64-13.1/source/n/openvpn
PKG_SITES += http://ftp.heanet.ie/mirrors/slackware/pub/slackware/slackware-13.1/source/n/openvpn
PKG_SITES += http://ftp.ist.utl.pt/pub/slackware/slackware-current/source/n/openvpn
PKG_REMOTE_FILE_SUFFICES = .tar.gz

include $(DEFS)


include $(TOOLS_PKG_DIR)/configure-menuconfig.mk


configure : $(call tstamp_f,install,toolchain openssl-0.9) #lzo
configure : $(call bstamp_f,install,sed gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure $(LOG)		\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	$(CONFIGURE_OPTIONS)	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


install      : $(call tstamp_f,uninstall,openvpn)
install-root : $(call tstamp_f,uninstall-root,openvpn)
install-root : $(call tstamp_f,install-root,openssl-0.9)

INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/openvpn  $(RSBIN_DIR)         $(LOG)
 #$(IW) $(INSTxFILES) $(PINIT_DIR)/openvpn  $(RINIT_DIR)         $(LOG)
 #$(IW) $(INST_FILES)  $(PETC_DIR)/openvpn/* $(RETC_DIR)/openvpn $(LOG)
endef


include $(RULES)
