PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.0.1.deb1
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/libb/$(PKG_BASE)

include $(DEFS)


AC_VER = 2.69
AM_VER = 1.14
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


configure : $(call tstamp_f,install,toolchain pkg-config)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ac_cv_prog_HAVE_ANT="no"		\
     ./configure -C	$(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--disable-extra-warnings	\
	--disable-examples		\
	--disable-bdjava		\
	--disable-bdjava-jar		\
	--enable-shared			\
	--enable-static			\
	--disable-doxygen-doc		\
	--disable-doxygen-dot		\
	--disable-doxygen-html		\
	--disable-doxygen-ps		\
	--disable-doxygen-pdf		\
	--without-libxml2		\
	--without-freetype		\
	--without-fontconfig		\
	--without-bdj-type		\
	--with-gnu-ld			\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libbluray.so* $(RLIB_DIR) $(LOG)


include $(RULES)
