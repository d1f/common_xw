PKG_TYPE          = DEBIAN3
PKG_VERSION       = 8.26
PKG_VER_PATCH     = -3
PKG_SITES         = $(DEB_ORG_SITES)
PKG_SITE_PATH     = pool/main/c/$(PKG_BASE)

include $(DEFS)


# debian deps: gettext autotools-dev texinfo groff libattr1-dev libacl1-dev gperf bison

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR)	&&		\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--enable-threads=posix		\
	--disable-acl			\
	--disable-assert		\
	--disable-libsmack		\
	--disable-xattr			\
	--disable-libcap		\
	--disable-nls			\
	--without-openssl		\
	--with-gnu-ld			\
	--without-libiconv-prefix	\
	--without-libpth-prefix		\
	--without-included-regex	\
	--without-selinux		\
	--without-gmp			\
	--without-libintl-prefix	\
	$(STD_VARS)
endef
#--enable-no-install-program=PROG_LIST


MK_VARS += INFO_DEPS= MANS= man1_MANS= EXTRA_MANS= ALL_MANS= INSTALL=install
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
