#include <df/hostinfo.h>
#include "hostinfo_private.hh"

#include <stdlib.h>     // EXIT_SUCCESS
#include <errno.h>      // errno
#include <string.h>     // strncpy
#include <unistd.h>     // close
#include <sys/ioctl.h>  // ioctl
#include <sys/socket.h> // socket
#include <df/error.h>


// internal
// returns 0 on success, -1 on error and errno set
int hostinfo_get_ifreq(const char *ifname, const char *func,
		       int ioctlreq, struct ifreq *ifr)
{
    int skfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (skfd < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): %s: socket creation failed",
		__func__, func);
	return -1;
    }

    strncpy(ifr->ifr_name, ifname, IFNAMSIZ);

    if (ioctl(skfd, ioctlreq, ifr) < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): ioctl %s on %s failed",
		__func__, func, ifname);
	close(skfd);
	return -1;
    }

    close(skfd);
    return 0;
}
