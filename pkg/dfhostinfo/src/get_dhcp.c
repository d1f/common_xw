#include <df/hostinfo.h>
#include "hostinfo_private.hh"

#include <df/shcfg.h>
#include <df/x.h>
#include <strings.h>     // strcasecmp


// return boolean status whether network is configured by DHCP
// or -1 on error
int hostinfo_get_dhcp(void)
{
    int rc = 0;

    df_shcfg_t *knetconf = df_shcfg_create("/etc/network.conf", '=');
    if (knetconf == NULL || df_shcfg_length(knetconf) == 0)
    {
	df_shcfg_delete(&knetconf);
	knetconf = df_shcfg_create("/etc/network_wired.conf", '=');
    }
    if (knetconf == NULL || df_shcfg_length(knetconf) == 0)
	    goto out;

    const char *found = df_shcfg_lsearch_val(knetconf, "dhcp");
    if (found == NULL)
	goto out;

    if (strcasecmp(found, "on") == 0)
	rc = 1;
    else if (strcasecmp(found, "off") == 0)
	rc = 0;

out:
    df_shcfg_delete(&knetconf);
    return rc;
}
