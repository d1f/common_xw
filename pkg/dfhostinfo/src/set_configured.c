#include <df/hostinfo.h>
#include "hostinfo_private.hh"

#include <errno.h>
#include <df/error.h>

#include <fcntl.h>


int hostinfo_set_shdcp_configured(void)
{
    static const char stfile[] = HOSTINFO_NETWORK_CONFIGURED;

    int rc = open(stfile, O_CREAT|O_TRUNC, O_RDWR);
    if (rc < 0)
	dferror(EXIT_SUCCESS, errno, "%s(): Can't create %s", __func__, stfile);

    return rc >= 0 ? 0 : -1;
}
