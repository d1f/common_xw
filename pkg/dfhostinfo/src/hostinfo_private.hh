#ifndef  HOSTINFO_PRIVATE_H
# define HOSTINFO_PRIVATE_H

# include <net/if.h>     // struct ifreq


// internal
int hostinfo_get_ifreq(const char *ifname, const char *func,
		       int ioctlreq, struct ifreq *ifr);

// returns malloced line, NULL on error:
char *hostinfo_get_1line(const char *fname, char sep);


# define HOSTINFO_NETWORK_CONFIGURED	"/etc/network.configured"
# define HOSTINFO_RESOLV_CONF		"/etc/resolv.conf"
# define HOSTINFO_SW_VERSION_CONF	"/conf/release"
# define HOSTINFO_HW_VERSION_CONF	"/proc/device_version"
# define HOSTINFO_MODEL_CONF		"/var/run/model"
# define HOSTINFO_SENSOR_CONF		"/var/run/sensor"


#endif //HOSTINFO_PRIVATE_H
