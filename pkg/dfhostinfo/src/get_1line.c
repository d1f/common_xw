#include <df/hostinfo.h>
#include "hostinfo_private.hh"

#include <df/get-line.h>
#include <df/error.h>
#include <string.h> // strrchr
//#include <df/log.h>


// returns malloced line, NULL on error:
char *hostinfo_get_1line(const char *fname, char sep)
{
    char *line = NULL;

    FILE *in = fopen(fname, "r");
    if (in == NULL)
    {
	dferror(EXIT_SUCCESS, errno, "Can't open %s", fname);
    }
    else
    {
	// Returns: 0 on success, >0 - EOF, <0 - -errno.
	int rc = df_get_line(in, &line);
	if (rc == 0)
	{
	    char *nl = strrchr(line, '\n');
	    if (nl != NULL)
		*nl = '\0';

	    if (sep)
	    {
		char *ep = strchr(line, sep);
		if (ep != NULL)
		    ++ep;
		memmove(line, ep, strlen(ep)+1);
	    }
	}
	else if (rc < 0)
	    dferror(EXIT_SUCCESS, -rc, "Can't read %s", fname);
	else // rc > 0 - EOF
	    dferror(EXIT_SUCCESS, 0, "Unexpected EOF on %s", fname);
    }

    if (in != NULL) fclose(in);
    return line;
}
