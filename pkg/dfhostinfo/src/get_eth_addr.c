#include <df/hostinfo.h>
#include "hostinfo_private.hh"

#include <sys/ioctl.h>  // ioctl
#include <df/endian.h>


// returns 0 on success, -1 on error and errno set
int hostinfo_get_eth_addr(const char *ifname, df_ethaddr_t *ethaddr)
{
    *ethaddr = (df_ethaddr_t)0;

    struct ifreq ifr;
    int rc = hostinfo_get_ifreq(ifname,
				"SIOCGIFHWADDR",
				 SIOCGIFHWADDR, &ifr);
    if (rc < 0) return rc;

    const unsigned char *hwaddr = (const unsigned char *)
	&ifr.ifr_ifru.ifru_hwaddr.sa_data;

    for (size_t i = 0; i < IFHWADDRLEN; ++i)
	*ethaddr |= ((df_ethaddr_t)hwaddr[i] << (40 - i*8));

    *ethaddr = htobe64(*ethaddr);

    return 0;
}
