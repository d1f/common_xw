#include <df/hostinfo.h>
#include "hostinfo_private.hh"

#include <netinet/in.h> // INADDR_NONE
#include <sys/ioctl.h>  // ioctl


// returns 0 on success, -1 on error and errno set
int hostinfo_get_ip4_bcast_addr(const char *ifname, df_ip4addr_t *ip4addr)
{
    *ip4addr = INADDR_NONE;

    struct ifreq ifr;
    int rc = hostinfo_get_ifreq(ifname,
				"SIOCGIFBRDADDR",
				 SIOCGIFBRDADDR, &ifr);
    if (rc < 0) return rc;

    struct sockaddr_in *saddr = (struct sockaddr_in *) ((void*) &ifr.ifr_broadaddr);
    *ip4addr = saddr->sin_addr.s_addr;

    return 0;
}
