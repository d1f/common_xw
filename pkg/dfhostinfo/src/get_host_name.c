#include <df/hostinfo.h>

#include <unistd.h>	// gethostname
#include <limits.h>	// HOST_NAME_MAX
#include <df/x.h>	// df_xstrdup

// returns malloced line, NULL on error:
char *hostinfo_get_host_name(void)
{
    char hostname[HOST_NAME_MAX+1] = { 0 };
    gethostname(hostname, HOST_NAME_MAX);
    return df_xstrdup(hostname);
}
