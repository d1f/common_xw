#include <df/hostinfo.h>
#include "hostinfo_private.hh"

#include <df/shcfg.h>
#include <string.h>     // strcmp
#include <netinet/in.h> // INADDR_ANY


int hostinfo_get_ip4_dns_addr(df_ip4addr_t dnsaddr[4])
{
    dnsaddr[0] = INADDR_ANY;
    dnsaddr[1] = INADDR_ANY;
    dnsaddr[2] = INADDR_ANY;
    dnsaddr[3] = INADDR_ANY;

    df_shcfg_t *kresconf = df_shcfg_create(HOSTINFO_RESOLV_CONF, ' ');
    if (kresconf == NULL)
	return -1;

    size_t dnscount = 0;
    DF_SHCFG_FOREACH(kresconf, kv, idx)
    {
	if (dnscount >= 4)
	    break;

	if (kv->key.ccPtr != NULL && strcmp(kv->key.ccPtr, "nameserver") == 0)
	{
	    if (kv->val.ccPtr == NULL)
		continue;

	    df_ip4addr_sscanf(kv->val.ccPtr, &dnsaddr[dnscount]);
	    ++dnscount;
	}
    }

    df_shcfg_delete(&kresconf);

    return dnscount==0 ? -1 : 0;
}
