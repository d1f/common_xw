#ifndef  DF_HOSTINFO_H
# define DF_HOSTINFO_H

# include <df/netaddr.h> // df_ethaddr_t, df_ip4addr_t


// returns 0 on success, -1 on error and errno set
int hostinfo_get_eth_addr      (const char *ifname, df_ethaddr_t *ethaddr);
int hostinfo_get_ip4_addr      (const char *ifname, df_ip4addr_t *ip4addr);
int hostinfo_get_ip4_net_mask  (const char *ifname, df_ip4addr_t *ip4mask);
int hostinfo_get_ip4_bcast_addr(const char *ifname, df_ip4addr_t *ip4addr);
int hostinfo_get_ip4_gw_addr   (const char *ifname, df_ip4addr_t *ip4addr);
int hostinfo_get_ip4_dns_addr  (                    df_ip4addr_t  dnsaddr[4]);
int hostinfo_set_ip4_dns_addr  (              const df_ip4addr_t  dnsaddr[4]);


// returns boolean status whether network is configured in sense of SHDCP
// or -1 on other error
int hostinfo_get_shdcp_configured(void);
int hostinfo_set_shdcp_configured(void);


// return boolean status whether network is configured by DHCP
// or -1 on error
int hostinfo_get_dhcp(void);


// returns malloced line, NULL on error:
char *hostinfo_get_model_name      (void);
char *hostinfo_get_sensor_name     (void);
char *hostinfo_get_software_version(void);
char *hostinfo_get_hardware_version(void);
char *hostinfo_get_host_name       (void);
char *hostinfo_get_domain_name     (void);


#endif //DF_HOSTINFO_H
