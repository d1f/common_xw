#include <df/hostinfo.h>
#include "hostinfo_private.hh"

// returns malloced line, NULL on error:
char *hostinfo_get_sensor_name(void)
{
    return hostinfo_get_1line(HOSTINFO_SENSOR_CONF, '=');
}
