#include <df/hostinfo.h>
#include "hostinfo_private.hh"

#include <errno.h>
#include <df/error.h>

#include <sys/stat.h>


// returns boolean status whether network is configured in sense of SHDCP
// or -1 on other error
int hostinfo_get_shdcp_configured(void)
{
    static const char stfile[] = HOSTINFO_NETWORK_CONFIGURED;

    int ret = 0;
    struct stat stbuf;
    int rc = stat(stfile, &stbuf);
    if (rc == 0) // file exists
	ret = 1; // configured
    else if (rc < 0)
    {
	if (errno == ENOENT) // file not exists
	    ret = 0; // not configured
	else
	{
	    dferror(EXIT_SUCCESS, errno, "%s(): Can't stat %s", __func__, stfile);
	    ret = -1; // error
	}
    }

    return ret;
}
