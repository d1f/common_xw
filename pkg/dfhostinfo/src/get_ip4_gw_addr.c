#include <df/hostinfo.h>

#include <stdlib.h>     // EXIT_SUCCESS
#include <errno.h>      // errno
#include <string.h>     // strcmp
#include <netinet/in.h> // INADDR_ANY
#include <net/route.h>  // RTF_*
#include <df/error.h>


int hostinfo_get_ip4_gw_addr(const char *ifname, df_ip4addr_t *ip4addr)
{
    *ip4addr = INADDR_ANY; // no gateway found

    char devname[64];
    unsigned long d, g, m;
    int flgs, ref, use, metric, mtu, win, ir;

    static const char rtfile[] = "/proc/net/route";
    FILE *fp = fopen(rtfile, "r");
    if (fp == NULL)
    {
	dferror(EXIT_SUCCESS, errno, "Can't open %s", rtfile);
	return -1;
    }

    if (fscanf(fp, "%*[^\n]\n") < 0) // skip the first line
	goto ERROR;		     // empty or missing line, or read error

    int rc = 0;

    while (1)
    {
	int r;
	r = fscanf(fp, "%63s%lx%lx%X%d%d%d%lx%d%d%d\n",
		   devname, &d, &g, &flgs, &ref, &use, &metric, &m,
		   &mtu, &win, &ir);
	if (r != 11)
	{
	    if ((r < 0) && feof(fp)) // EOF with no (nonspace) chars read
	    {
		rc = 0; // no error and no gateway found
		break;
	    }
ERROR:
	    dferror(EXIT_SUCCESS, errno, "%s(): fscanf error", __func__);
	    rc = -1;
	    break;
	}

	// skip interfaces that are down and not gateway
	if ( (flgs & (RTF_UP | RTF_GATEWAY)) != (RTF_UP | RTF_GATEWAY) )
	    continue;

	if (strcmp(devname, ifname) != 0) // skip other interfaces
	    continue;

	// here interface with specified name and up and default gateway

	*ip4addr = g;
	rc = 0;
	break; // fall to out
    }

    fclose(fp);
    return rc;
}
