#include <df/hostinfo.h>
#include "hostinfo_private.hh"


// returns malloced line, NULL on error:
char *hostinfo_get_software_version(void)
{
    return hostinfo_get_1line(HOSTINFO_SW_VERSION_CONF, 0);
}
