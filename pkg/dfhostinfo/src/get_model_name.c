#include <df/hostinfo.h>
#include "hostinfo_private.hh"

// returns malloced line, NULL on error:
char *hostinfo_get_model_name(void)
{
    return hostinfo_get_1line(HOSTINFO_MODEL_CONF, '=');
}
