#include <df/hostinfo.h>
#include "hostinfo_private.hh"

#include <df/shcfg.h>
#include <df/x.h>
#include <string.h>     // strcmp


char *hostinfo_get_domain_name(void)
{
    char *ret  = NULL;

    df_shcfg_t *kresconf = df_shcfg_create(HOSTINFO_RESOLV_CONF, ' ');
    if (kresconf == NULL)
	return NULL;

    const char *found = df_shcfg_lsearch_val(kresconf, "domain");
    if (found != NULL)
	ret = df_xstrdup(found);

    df_shcfg_delete(&kresconf);

    return ret;
}
