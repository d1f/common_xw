#include <df/hostinfo.h>
#include "hostinfo_private.hh"

#include <df/shcfg.h>
#include <df/x.h>

#include <string.h>     // strcmp
#include <stdlib.h>     // free
#include <netinet/in.h> // INADDR_ANY


int hostinfo_set_ip4_dns_addr(const df_ip4addr_t dnsaddr[4])
{
    df_ip4addr_t dns[4] = { INADDR_ANY, INADDR_ANY, INADDR_ANY, INADDR_ANY };

    size_t dnsaddr_count = 0;
    for (size_t i=0, o=0; i < 4; ++i)
    {
	if (dnsaddr[i] != INADDR_ANY)
	{
	    ++dnsaddr_count;
	    dns[o++] = dnsaddr[i]; // squeeze valid addresses to array start
	}
    }

    if (dnsaddr_count == 0)
	return 0;

    df_shcfg_t *kresconf = df_shcfg_create(HOSTINFO_RESOLV_CONF, ' ');
    if (kresconf == NULL)
	return 0;

    size_t dnscount = 0;
    DF_SHCFG_FOREACH(kresconf, kv, idx)
    {
	if (dnscount >= dnsaddr_count)
	    break;

	if (kv->key.ccPtr != NULL && strcmp(kv->key.ccPtr, "nameserver") == 0)
	{
	    char out[16];
	    df_ip4addr_sprintf(out, dns[dnscount], DF_IP4ADDR_NO_ALIGN);
	    free(kv->val.cPtr);
	    kv->val.cPtr = df_xstrdup(out);
	    ++dnscount;
	}
    }

    for (size_t i = dnscount; i < dnsaddr_count; ++i)
    {
	if (dns[i] != INADDR_ANY)
	{
	    char out[16];
	    df_ip4addr_sprintf(out, dns[i], DF_IP4ADDR_NO_ALIGN);

	    df_shcfg_add(kresconf, "nameserver", out);
	}
    }

    bool rc = df_shcfg_write(kresconf, HOSTINFO_RESOLV_CONF, ' ', 0);

    df_shcfg_delete(&kresconf);

    return rc ? -1 : 0;
}
