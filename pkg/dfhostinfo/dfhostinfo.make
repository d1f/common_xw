PKG_ORIG_DIR     = $(PKG_PKG_DIR)/src
PKG_FETCH_METHOD = DONE
SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


libs = df-shcfg df df-da

configure : $(call tstamp_f,install,toolchain $(libs))
ifneq ($(PLATFORM),build)
configure : $(call tstamp_f,configure,linux)
endif

ifeq ($(shell $(VERCMP) $(GCC_VERSION) '>=' 4.1.3),true)
 STD_CFLAGS += -fgnu89-inline
endif

ifeq ($(PLATFORM),build)
  BUILD_TYPE   = LIB
else
  BUILD_TYPE   = SHARED_LIB
  LIBS         = $(libs)
endif
LIB_NAME       = $(PKG)
MAKES_MAKEFILE = $(TOOLS_DIR)/makes/all.Makefile
include          $(TOOLS_DIR)/makes/conf.mk

CONFIGURE_CMD  = $(MAKES_CONFIGURE_CMD)


#MK_VARS = Q=

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(INSTALL_ROOT_CMD_DEFAULT)


include $(RULES)
