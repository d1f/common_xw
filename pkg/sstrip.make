PKG_HAS_NO_SOURCES = defined

PLATFORM = build

include $(DEFS)


extract : $(call bstamp_f,extract,buildroot)

define POST_EXTRACT_CMD
  $(MKDIR_P) -v	$(PKG_BLD_DIR) $(LOG)
  cp -v $(ORIG_SRC_DIR)/buildroot_*/toolchain/sstrip/sstrip.c \
		$(PKG_BLD_DIR) $(LOG)
endef


define  BUILD_CMD
  '$(BUILD_CC)' $(PKG_BLD_DIR)/sstrip.c -o $(PKG_BLD_DIR)/sstrip \
	$(STD_CPPFLAGS) $(STD_CFLAGS) $(STD_LDFLAGS)
endef

    CLEAN_CMD = $(RM) $(PKG_BLD_DIR)/sstrip
DISTCLEAN_CMD = $(CLEAN_CMD)

define INSTALL_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/sstrip $(BBIN_DIR) $(LOG)
endef


include $(RULES)
