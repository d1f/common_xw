#include <df/opt.h>
#include <df/error.h>
#include <errno.h>
#include <stdlib.h>  // EXIT_FAILURE
#include <strings.h> // strcasecmp


// returns: < 0 - stop, break outer foreach loop; >= 0 - continue loop
typedef ssize_t (*foreach_f)(const dfopt_t *opt, const void *data);

// returns index of option on which function stopped or -1 if not.
static ssize_t dfopt_foreach(const dfopt_t opts[], foreach_f f, const void *data)
{
    for (size_t idx=0; opts[idx].key != NULL; ++idx)
    {
	if ( f( &opts[idx], data ) < 0 )
	    return idx;
    }

    return -1;
}


static ssize_t check_opt(const dfopt_t *opt, const void *data)
{
    (void)data;

    if (opt->ccp_ptr != NULL && !opt->has_value)
    {
	dferror(EXIT_FAILURE, 0,
		"-%s: ccp_ptr != NULL requires has_value", opt->key);
	return -1;
    }
    if (opt->scan_fmt != NULL && !opt->has_value)
    {
	dferror(EXIT_FAILURE, 0,
		"-%s: scan_fmt != NULL requires has_value", opt->key);
	return -1;
    }
    if (opt->scan_ptr != NULL && !opt->has_value)
    {
	dferror(EXIT_FAILURE, 0,
		"-%s: scan_ptr != NULL requires has_value", opt->key);
	return -1;
    }

    if (opt->scan_fmt == NULL && opt->scan_ptr != NULL)
    {
	dferror(EXIT_FAILURE, 0,
		"-%s: scan_fmt == NULL and scan_ptr != NULL", opt->key);
	return -1;
    }

    if (opt->scan_fmt != NULL && opt->scan_ptr == NULL)
    {
	dferror(EXIT_FAILURE, 0,
		"-%s: scan_fmt == NULL and scan_ptr != NULL", opt->key);
	return -1;
    }

    return 0;
}

static ssize_t check_table(const dfopt_t opts[])
{
    return dfopt_foreach(opts, check_opt, NULL);
}


static ssize_t cmp_key(const dfopt_t *opt, const void *data)
{
    if ( strcasecmp((const char *)data, opt->key) == 0 )
	return -1;
    else
	return 0;
}

static const dfopt_t *dfopt_lookup(const dfopt_t opts[], const char *optname)
{
    ssize_t idx = dfopt_foreach(opts, cmp_key, optname);
    if (idx >= 0)
	return &opts[idx];

    return NULL;
}


// returns argv index after options
size_t dfopt_parse(const dfopt_t opts[], size_t ac, char *const av[])
{
    check_table(opts);

    size_t i; // argv index
    for (i=1; i < ac && av[i] != NULL; ++i) // argv loop
    {
	if (av[i][0] == '-') // option started
	{
	    const char *optname = &av[i][1]; // skip first dash

	    if (optname[0] == '-') // '--' - end of options or double dash prefix
	    {
		++optname; // skip second dash

		if (optname[0] == '\0') // end of options
		{
		    ++i; // advance index after '--'
		    break;
		}
	    }


	    const dfopt_t *opt = dfopt_lookup(opts, optname);
	    if (opt == NULL) // no option found
		dferror(EXIT_FAILURE, 0,
			"Unknown option: '%s'", optname);

	    if (!opt->has_value)
	    {
		if ( opt->int_ptr != NULL)
		    *opt->int_ptr  = opt->int_val;

		if (opt->func != NULL)
		    opt->func(opt, NULL);

		continue; // argv loop
	    }


	    // option has value

	    ++i; // skip option to value

	    const char *value = av[i];
	    if (value == NULL)
		dferror(EXIT_FAILURE, 0,
			"Option '%s' must have value", optname);

	    if ( opt->int_ptr != NULL)
		*opt->int_ptr  = opt->int_val;

	    if ( opt->ccp_ptr != NULL)
		*opt->ccp_ptr  = value;

	    if (opt->scan_fmt != NULL && opt->scan_ptr != NULL)
	    {
		if (sscanf(value, opt->scan_fmt, opt->scan_ptr) != 1)
		    dferror(EXIT_FAILURE, 0,
			    "Option '%s' value '%s' does not matched"
			    " scanf format: '%s'",
			    optname, value, opt->scan_fmt);
	    }

	    if (opt->func != NULL)
		opt->func(opt, value);
	} // option started
	else
	{
	    // not an option, positional parameter
	    break;
	}
    } // argv loop

    return i;
}
