#include <df/opt.h>
#include <string.h>

// prints options list with description
void dfopt_print(const dfopt_t opts[], FILE *out)
{
    size_t keylenmax = 0;

    for (size_t idx=0; opts[idx].key != NULL; ++idx)
    {
	size_t keylen = strlen(opts[idx].key);
	if (keylen > keylenmax)
	    keylenmax = keylen;
    }

    for (size_t idx=0; opts[idx].key != NULL; ++idx)
    {
	const dfopt_t *opt = &opts[idx];

	fprintf(out, "\t-%s", opt->key);
	size_t padlen = keylenmax - strlen(opt->key);
	while (padlen--)
	    fputc(' ', out);
	fprintf(out, " %s\n", opt->desc);
    }
}
