#ifndef  DF_OPT_H
# define DF_OPT_H

# include <stddef.h> // size_t
# include <stdio.h>  // FILE


struct dfopt_s;
typedef struct dfopt_s dfopt_t;

typedef int (*dfopt_f)(const dfopt_t *opt, const char *value);

struct dfopt_s
{
    // mandatory members:

    const char *key;    // option name string
    const char *desc;   // option description
    int has_value;      // option has mandatory value

    // optional members:

    int  *int_ptr;         // *int_ptr assigned to int_val if int_ptr != NULL
    int   int_val;

    const char **ccp_ptr; // *ccp_ptr assigned to value if ccp_ptr != NULL

    const char *scan_fmt; // sscanf parameter if != NULL
    void       *scan_ptr; //

    dfopt_f func;          // function for complex option processing with and without value
    void *data;            // function context data
};

// returns argv index after options
size_t dfopt_parse(const dfopt_t opts[], size_t ac, char *const av[]);

// prints options list with description
void   dfopt_print(const dfopt_t opts[], FILE *out);


#endif //DF_OPT_H
