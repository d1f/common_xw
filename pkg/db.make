PKG_TYPE          = DEBIAN3
PKG_VERSION       = 5.1.29
PKG_VER_PATCH     = -9
PKG_SITE_PATH     = pool/main/d/$(PKG_BASE)

SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call bstamp_f,install,sed)

 enable  = static
disable  = shared
 enable += smallbuild
disable += largefile
disable += compression
disable += hash
disable += mutexsupport
disable += partition
disable += queue
disable += replication
disable += statistics
disable += verify
 enable += compat185
disable += cxx
disable += debug
disable += java
disable += mingw
   with  = cryptography=no
   with += gnu-ld

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && $(PKG_SRC_DIR)/dist/configure	\
	--prefix=$(INST_DIR)				\
        --build=$(BUILD) --host=$(TARGET)		\
	$(addprefix --enable-,$(enable))		\
	$(addprefix --disable-,$(disable))		\
	$(addprefix --with-,$(with))			\
	$(STD_VARS)			$(LOG)
endef


  INSTALL_CMD_TARGET = install_setup install_include install_lib #install_utilities
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
