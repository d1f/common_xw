PKG_HAS_NO_SOURCES = defined

include $(DEFS)


cairo := $(strip $(wildcard /usr/include/cairo*))

ifneq ($(cairo),)

build : $(call bstamp_f, install, dffont-gen)

define INSTALL_ROOT_CMD
  @$(IWSH) "dffont_gen=$(BBIN_DIR)/dffont-gen INDENTS=$(INDENTS1) \
            $(COMMON_PKG_DIR)/dffont-gen/gen-fonts \
	    $(RSHARE_DIR)/fonts/video"
endef

endif


include $(RULES)
