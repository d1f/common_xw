PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.8.0
PKG_VER_PATCH     = -6
PKG_SITE_PATH     = pool/main/g/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain openssl-0.9)
configure : $(call bstamp_f,install,gawk sed)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
        --build=$(BUILD)		\
	--host=$(TARGET)		\
	--disable-threads		\
	--enable-static			\
	--disable-shared		\
	--disable-nls			\
	--with-gnu-ld			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	--without-gnutls		\
	--without-libpth-prefix		\
	--without-lasso			\
	$(STD_VARS)
endef
#	--enable-threads=posix		\


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
