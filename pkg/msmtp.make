PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.4.32
PKG_VER_PATCH     = -2
PKG_SITE_PATH     = pool/main/m/$(PKG_BASE)

include $(DEFS)


PKG_PATCH_FILES += $(PKG_PKG_DIR)/$(PKG)*.patch


configure : $(call tstamp_f,install,toolchain pkg-config gnutls26)
configure : $(call bstamp_f,install,gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
        --build=$(BUILD)		\
	--host=$(TARGET)		\
	--disable-nls			\
	--with-gnu-ld			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	--with-ssl=gnutls		\
	--without-libgsasl		\
	--without-libidn		\
	--without-gnome-keyring		\
	--without-macosx-keyring	\
	$(STD_VARS)
endef
# --with-ssl=gnutls|openssl|no


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES)  $(IBIN_DIR)/msmtp $(RBIN_DIR) $(LOG)


include $(RULES)
