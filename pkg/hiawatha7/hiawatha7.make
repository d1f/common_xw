PKG_VERSION   = 7.8.2
PKG_BASE      = hiawatha
PKG_SITES     = http://www.hiawatha-webserver.org
PKG_SITE_PATH = files/hiawatha-7

include $(DEFS)


PKG_PATCH2_FILES += $(PKG_PKG_DIR)/*.patch


conf_opts += $(if $(CONFIG_WITH_IPv6), --enable-ipv6, --disable-ipv6)

ifeq ($(CONFIG_HIAWATHA7_WITH_OPENSSL),y)
  configure : $(call tstamp_f,install,openssl-0.9)
  conf_opts += --enable-ssl
else
  conf_opts += --disable-ssl
endif

conf_opts += $(if $(CONFIG_HIAWATHA7_MONITOR),   --enable-monitor,   --disable-monitor)
conf_opts += $(if $(CONFIG_HIAWATHA7_LARGEFILE), --enable-largefile, --disable-largefile)
conf_opts += $(if $(CONFIG_HIAWATHA7_CACHE),     --enable-cache,     --disable-cache)
conf_opts += $(if $(CONFIG_HIAWATHA7_CHROOT),    --enable-chroot,    --disable-chroot)
conf_opts += $(if $(CONFIG_HIAWATHA7_COMMAND),   --enable-command,   --disable-command)
conf_opts += $(if $(CONFIG_HIAWATHA7_TOOLKIT),   --enable-toolkit,   --disable-toolkit)

ifeq ($(CONFIG_HIAWATHA7_XSLT),y)
  configure : $(call tstamp_f,install,libxslt)
  conf_opts += --enable-xslt
else
  conf_opts += --disable-xslt
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	$(conf_opts)			\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

 INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
 INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/hiawatha $(RSBIN_DIR) $(LOG)


include $(RULES)
