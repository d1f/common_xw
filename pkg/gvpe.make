ifeq (1,1)
  PKG_TYPE      = DEBIAN3
  PKG_VERSION   = 2.25
  PKG_VER_PATCH = -2
  PKG_SITE_PATH = pool/main/g/$(PKG_BASE)
else
  PKG_SITES     = $(GNU_ORG_SITES)
  PKG_VERSION   = 2.25
  PKG_SITE_PATH = gvpe
endif

include $(DEFS)


PKG_PATCH2_FILES = $(PKG_PKG_DIR)/$(PKG)*.patch


configure : $(call tstamp_f,install,toolchain openssl-0.9)
configure : $(call tstamp_f,depend,linux)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--disable-nls			\
	--enable-iftype=native/linux	\
	--disable-compression		\
	--disable-cipher		\
	--with-gnu-ld			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	--with-kernel=$(LK_DIR)		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/gvpe    $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES)  $(IBIN_DIR)/gvpectrl $(RBIN_DIR) $(LOG)
endef


include $(RULES)
