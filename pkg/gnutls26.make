PKG_TYPE      = DEBIAN3
PKG_VERSION   = 2.12.20
PKG_VER_PATCH = -8+deb7u5
PKG_SITE_PATH = pool/main/g/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES = $(PKG_PKG_DIR)/$(PKG)_*.patch


configure : $(call tstamp_f,install,toolchain)
configure : $(call tstamp_f,install,zlib libgcrypt11)
configure : $(call bstamp_f,install,gawk)

ifeq ($(PLATFORM),build)
  conf_opts = --disable-threads
else
  conf_opts = --with-libgcrypt
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--disable-gtk-doc		\
	--disable-gtk-doc-html		\
	--disable-gtk-doc-pdf		\
	$(conf_opts)			\
	--disable-cxx			\
	--disable-guile			\
	--enable-shared			\
	--disable-static		\
	--with-gnu-ld			\
	--without-libnettle		\
	--without-libnettle-prefix	\
	--with-included-libtasn1	\
	--without-lzo			\
	--without-p11-kit		\
	--without-libpth-prefix		\
	--without-libreadline-prefix	\
	--with-packager=Debian		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libgnutls.so*         $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libgnutls-openssl.so* $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libgnutls-extra.so*   $(RLIB_DIR) $(LOG)
endef


include $(RULES)
