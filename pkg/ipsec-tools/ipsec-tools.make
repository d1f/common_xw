PKG_TYPE      = DEBIAN1
PKG_VERSION   = 0.7.3
PKG_VER_PATCH = -12
PKG_SITE_PATH = pool/main/i/$(PKG_BASE)

include $(DEFS)


include $(TOOLS_PKG_DIR)/quilt.mk

PKG_PATCH2_FILES = $(PKG_PKG_DIR)/*.patch


configure : $(call tstamp_f,configure,linux)
configure : $(call tstamp_f,install,toolchain openssl-0.9)

define CONFIGURE_CMD
  $(RELINK)	$(PKG_BLD_DIR)/src/setkey/token.c	\
		$(PKG_BLD_DIR)/src/racoon/cftoken.c	$(LOG)
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--build=$(BUILD)			\
	--host=$(TARGET)			\
	--prefix=$(INST_DIR)			\
	--disable-static			\
	--enable-shared				\
	--with-gnu-ld				\
	--with-kernel-headers=$(LK_INC_DIR)	\
	--without-readline			\
	--without-flex				\
	--with-openssl=$(INST_DIR)		\
	--without-libiconv			\
	--without-libradius			\
	--without-libpam			\
	--without-libldap			\
	--enable-security-context=no		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(ILIB_DIR)/libracoon.so*  $(RLIB_DIR) $(LOG)
  $(IW) $(INST_FILES) $(ILIB_DIR)/libipsec.so*   $(RLIB_DIR) $(LOG)
  $(IW) $(INST_FILES) $(ISBIN_DIR)/setkey       $(RSBIN_DIR) $(LOG)
  $(IW) $(INST_FILES) $(ISBIN_DIR)/racoonctl    $(RSBIN_DIR) $(LOG)
  $(IW) $(INST_FILES) $(ISBIN_DIR)/racoon       $(RSBIN_DIR) $(LOG)
  $(IW) $(INST_FILES) $(ISBIN_DIR)/plainrsa-gen $(RSBIN_DIR) $(LOG)
  $(IW) $(INST_FILES) $(PKG_BLD_DIR)/src/racoon/samples/racoon.conf $(RETC_DIR) $(LOG)
endef


include $(RULES)
