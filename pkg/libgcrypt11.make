PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.5.0
PKG_VER_PATCH = -5+deb7u4
PKG_SITE_PATH = pool/main/libg/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain libgpg-error)
configure : $(call bstamp_f,install,gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--enable-shared			\
	--disable-static		\
	--disable-padlock-support	\
	--disable-aesni-support		\
	--with-gnu-ld			\
	--with-gpg-error-prefix=$(INST_DIR)	\
	$(STD_VARS)
endef

# --enable-ciphers=ciphers		select the symmetric ciphers to include
# --enable-pubkey-ciphers=ciphers	select the public-key ciphers to include
# --enable-digests=digests		select the message digests to include

#cipher algorithms: arcfour blowfish cast5 des aes twofish serpent rfc2268 seed camellia idea
#digest algorithms: crc md4 md5 rmd160 sha1 sha256 sha512 tiger whirlpool
#pubkey algorithms: dsa elgamal rsa ecc


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libgcrypt.so* $(RLIB_DIR) $(LOG)


include $(RULES)
