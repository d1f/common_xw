PKG_TYPE      = DEBIAN3
pkg_verbase   = 4.2.6
pkg_p         = p5
pkg_verbase_p = $(pkg_verbase).$(pkg_p)
PKG_VERSION   = $(pkg_verbase_p)+dfsg
PKG_VER_PATCH = -3.1
# -5 requires ssl 1.x
PKG_SITE_PATH = pool/main/n/$(PKG_BASE)

include $(DEFS)


configure : $(call bstamp_f,install,sed libtool)
configure : $(call tstamp_f,install,toolchain)

define CONFIGURE_CMD
  $(RELINK) $(PKG_BLD_DIR)/configure $(LOG)
  cd $(PKG_BLD_DIR) &&			\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--disable-static		\
	--enable-getifaddrs		\
	--disable-debugging		\
	--enable-dst-minutes		\
	--enable-ignore-dns-errors	\
	--disable-all-clocks		\
	--enable-step-slew		\
	--enable-ntpdate-step		\
	--enable-hourly-todr-sync	\
	--enable-linuxcap		\
	--with-gnu-ld			\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(IBIN_DIR)/ntpd     $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(IBIN_DIR)/ntpdate  $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(IBIN_DIR)/ntpq      $(RBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(IBIN_DIR)/ntpdc     $(RBIN_DIR) $(LOG)
endef


include $(RULES)
