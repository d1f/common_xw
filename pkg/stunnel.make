PKG_TYPE          = DEBIAN3
PKG_VER_MAJOR     = 4
PKG_VERSION       = 5.06
PKG_VER_PATCH     = -2
PKG_SITE_PATH     = pool/main/s/$(PKG_BASE)

include $(DEFS)


AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


configure : $(call tstamp_f,install,toolchain openssl-0.9)

ifeq ($(CONFIG_WITH_IPv6),y)
 configure_opts += --enable-ipv6
else
 configure_opts += --disable-ipv6
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&				\
     ./configure -C			$(LOG)	\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=$(INST_DIR)			\
	--disable-static			\
	--enable-shared				\
	--disable-libwrap			\
	--disable-fips				\
	--with-gnu-ld				\
	--with-random=/dev/urandom		\
	--with-threads=pthread			\
	--with-ssl=$(INST_DIR)			\
	$(configure_opts)			\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/stunnel/libstunnel.so $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(IBIN_DIR)/stunnel               $(RBIN_DIR) $(LOG)
endef


include $(RULES)
