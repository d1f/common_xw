PKG_HAS_NO_SOURCES = defined

include $(DEFS)


define FHS_3.0
# http://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.html

/bin		Essential command binaries
/boot		Static files of the boot loader
/dev		Device files
/etc		Host-specific system configuration
/etc/opt
/lib		Essential shared libraries and kernel modules
/lib/modules
/media		Mount point for removable media
/media/floppy		Floppy drive (optional)
/media/cdrom		CD-ROM drive (optional)
/media/cdrecorder	CD writer (optional)
/media/zip		Zip drive (optional)
/mnt		Mount point for mounting a filesystem temporarily
/opt		Add-on application software packages
/root		Home directory for the root user (optional)
/run		Data relevant to running processes
/sbin		Essential system binaries
/srv		Data for services provided by this system
/tmp		Temporary files
/usr		Secondary hierarchy
/var		Variable data
/var/cache	Application cache data
/var/lib	Variable state information
/var/local	Variable data for /usr/local
/var/lock	Lock files
/var/log	Log files and directories
/var/opt	Variable data for /opt
/var/run	Data relevant to running processes
/var/spool	Application spool data
/var/tmp	Temporary files preserved between system reboots
/home		User home directories (optional)

endef

define fhs_add_ons

/dev/shm - tmpfs mount point
/dev/pts
/conf - my extension for R/O conf files

endef

root_dirs        = proc  sys dev conf etc_ var
root_tmpfs_links = media mnt run tmp
 var_tmpfs_links = run lock
 var_dirs        = cache log lib

ifneq ($(PART_CONF_ON),)
  # strip leading '/' if present:
  _CONF_MNT      = $(patsubst /%,%,$(PART_CONF_MNT))
  mkroot_dir_cmd = $(IW) $(LN_S) -v $(_CONF_MNT)/root $(ROOT_DIR)/root
  mkroot_etc_cmd = $(IW) $(LN_S) -v $(_CONF_MNT)/etc  $(ROOT_DIR)/etc
  var_dirs += tmp
  mkvar_dirs_cmd = $(IWSH) 'for l in $(var_dirs); do $(LN_S) -v ../$(_CONF_MNT)/$$l $(ROOT_DIR)/var/$$l; done'
else
  mkroot_dir_cmd = $(IW) $(MKDIR_P) $(ROOT_DIR)/root $(LOG)
  mkroot_etc_cmd = $(IW) $(LN_S) etc_ $(ROOT_DIR)/etc
  mkvar_dirs_cmd = $(IWSH) 'umask 0022; $(MKDIR_P) $(addsuffix $(var_dirs),$(ROOT_DIR)/var/)'
  mkvar_dirs_cmd+= ; $(MKDIR_P) -m1777 $(ROOT_DIR)/var/tmp
endif

define INSTALL_ROOT_CMD
  $(IWSH) 'umask 0022; $(MKDIR_P)  $(addprefix $(ROOT_DIR)/,$(root_dirs))'                       $(LOG)
  $(IWSH) 'for l in $(root_tmpfs_links); do $(LN_S) -v    dev/shm/$$l $(ROOT_DIR)/$$l    ; done' $(LOG)
  $(IWSH) 'for l in  $(var_tmpfs_links); do $(LN_S) -v ../dev/shm/$$l $(ROOT_DIR)/var/$$l; done' $(LOG)
  $(mkroot_dir_cmd) $(LOG)
  $(mkroot_etc_cmd) $(LOG)
  $(mkvar_dirs_cmd) $(LOG)
endef


include $(RULES)
