PKG_TYPE      = DEBIAN3
PKG_VERSION   = 8.35
PKG_VER_PATCH = -3.3+deb8u4
PKG_SITE_PATH = pool/main/p/$(PKG_BASE)

include $(DEFS)


AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call bstamp_f,install,libtool)

STD_LDIRSFLAGS += -Wl,-rpath-link=$(PKG_BLD_DIR)/.libs

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure	$(LOG)	\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=$(INST_DIR)			\
	--enable-shared --disable-static	\
	--disable-cpp				\
	--disable-rebuild-chartables		\
	--enable-utf8				\
	--enable-unicode-properties		\
	--enable-newline-is-any			\
	--disable-ebcdic			\
	--disable-stack-for-recursion		\
	--disable-pcregrep-libz			\
	--disable-pcregrep-libbz2		\
	--disable-pcretest-libreadline		\
	--with-gnu-ld				\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libpcre*.so* $(RLIB_DIR) $(LOG)


include $(RULES)
