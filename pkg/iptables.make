PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.4.21
PKG_VER_PATCH     = -2
PKG_SRC_FILE_SUFFIX = .orig.tar.bz2
PKG_SITE_PATH     = pool/main/i/$(PKG_BASE)

include $(DEFS)


# iptables >= 1.4 requires gcc 3, no-op for 2.95
ifeq      ($(filter 2.95.%,$(GCC_VERSION)),)

PKG_PATCH3_FILES += $(PKG_PKG_DIR)/$(PKG)_*.patch


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call tstamp_f,depend,linux)

ifneq ($(CONFIG_WITH_IPv6),y)
 configure_opts += --disable-ipv6
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure	$(LOG)	\
	--prefix=$(INST_DIR)			\
	--build=$(BUILD) --host=$(TARGET)	\
	--disable-static --enable-shared	\
	--disable-largefile			\
	--enable-libipq				\
	--with-gnu-ld				\
	--with-kernel=$(LK_DIR)			\
	--with-kbuild=$(LK_DIR)			\
	--with-ksource=$(LK_DIR)		\
	--with-pkgconfigdir=$(ILIB_DIR)/pkgconfig \
	$(configure_opts)			\
	$(STD_VARS)
endef


#MK_VARS += DESTDIR=$(INST_DIR)
 MK_VARS += V=1

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/xtables-multi $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/iptables*     $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES)  $(ILIB_DIR)/xtables/*.so   $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES)  $(ILIB_DIR)/libxtables.so* $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES)  $(ILIB_DIR)/libiptc.so*    $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES)  $(ILIB_DIR)/libipq.so*     $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES)  $(ILIB_DIR)/libip6tc.so*   $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES)  $(ILIB_DIR)/libip4tc.so*   $(RLIB_DIR) $(LOG)
endef

endif # iptables >= 1.4 requires gcc 3, no-op for 2.95


include $(RULES)
