#include <stdlib.h> // EXIT_*
#include <termios.h>
#include <unistd.h>
#include <error.h>
#include <errno.h>

int main(void)
{
    if (tcdrain(STDERR_FILENO) < 0)
	error(EXIT_FAILURE, errno, "Can't tcdrain stderr");
    if (tcdrain(STDOUT_FILENO) < 0)
	error(EXIT_FAILURE, errno, "Can't tcdrain stdout");

    return EXIT_SUCCESS;
}
