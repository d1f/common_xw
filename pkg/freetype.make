PKG_TYPE      = DEBIAN1
PKG_VERSION   = 2.6.1
PKG_VER_PATCH = -0.1
PKG_SITE_PATH = pool/main/f/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


define POST_EXTRACT_CMD
  $(TAR) -x -C $(PKG_ORIG_SRC_DIR) --strip-components=1 \
         -f    $(PKG_ORIG_SRC_DIR)/freetype-$(PKG_VERSION).tar.bz2 $(LOG)
  $(RM)        $(PKG_ORIG_SRC_DIR)/freetype-$(PKG_VERSION).tar.bz2 $(LOG)
  $(RM)        $(PKG_ORIG_SRC_DIR)/freetype-doc-*.tar.bz2          $(LOG)
  $(RM)        $(PKG_ORIG_SRC_DIR)/ft2demos-*.tar.bz2              $(LOG)
endef


QUILT_PATCH_DIRS = debian/patches-freetype debian/patches-ft2demos
include $(TOOLS_PKG_DIR)/quilt.mk


pre-configure : $(call bstamp_f,install,libtool)

AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autoconf.mk
include $(TOOLS_PKG_DIR)/automake.mk

define PRE_CONFIGURE_CMD
  $(RELINK) $(PKG_BLD_DIR)/builds/unix/configure.ac $(LOG)
  cd $(PKG_BLD_DIR) && $(AC_VARS) $(AM_VARS) ./autogen.sh $(LOG)
endef


configure : $(call bstamp_f,install,sed gawk libpng pkg-config)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR)	&&	\
     ./builds/unix/configure	\
		-C	$(LOG)	\
	--prefix=$(INST_DIR)	\
	--disable-shared	\
	--enable-static		\
	--with-gnu-ld		\
	--without-zlib		\
	--without-bzip2		\
	--without-harfbuzz	\
	--with-png=yes		\
	$(STD_VARS)

  $(BUILD_CMD_DEFAULT)
endef

    MK_VARS = prefix=$(INST_DIR)
  BUILD_CMD =   $(BUILD_CMD_DEFAULT)
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)


include $(RULES)
