PKG_TYPE          = DEBIAN3
PKG_VERSION       = 2014.10+dfsg1
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/u/$(PKG_BASE)

PLATFORM = build

include $(DEFS)


pre-configure : $(call bstamp_f,install,sed)

define PRE_CONFIGURE_CMD
  $(SED) -i -e 's/-Wno-error=unused-but-set-variable//g' \
	$(PKG_BLD_DIR)/arch/x86/cpu/config.mk
endef


MK_VARS  = HOSTCC=$(BUILD_CC)


ifeq ($(filter $(NOT_BUILD_TARGETS),$(MAKECMDGOALS)),)
 ifeq ($(PLATFORM),build)

  MK_VARS += CC=$(BUILD_CC) CROSS_COMPILE=""
  MK_VARS += PLATFORM_CPPFLAGS="-D__I386__"


  BUILD_CMD_TARGET = tools

  install : $(call bstamp_f,uninstall,uboot-mkimage)
  define INSTALL_CMD
    $(IW) $(INSTxFILES)	$(PKG_BLD_DIR)/tools/mkimage $(BBIN_DIR) $(LOG)
  endef

 else
  ifndef XDEP
  $(warning build for $(TARGET) is not supported yet)
  endif
 endif
endif


# FIXME
define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && $(DOMAKE) sandbox_defconfig $(MK_VARS) $(LOG)
  $(MKDIR_P) $(PKG_BLD_DIR)/include/config
  touch      $(PKG_BLD_DIR)/include/config/auto.conf
  cd $(PKG_BLD_DIR) && $(DOMAKE) tools-only $(MK_VARS) $(LOG)
endef


   DEPEND_CMD =    $(DEPEND_CMD_DEFAULT)
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
