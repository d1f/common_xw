PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.61
PKG_VER_PATCH = -4.1
PKG_SITE_PATH = pool/main/w/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain wvstreams pkg-config)
configure : $(call bstamp_f,install,sed)

MK_VARS  = prefix=$(INST_DIR) PKG_CONFIG=$(TARGET)-pkg-config PPPDIR=$(RETC_DIR)/ppp/peers

ENV_VARS = $(STD_VARS)

define CONFIGURE_CMD
  $(SED) -i -e 's/pkg-config/$$(PKG_CONFIG)/g' $(PKG_BLD_DIR)/Makefile.in $(LOG)
  cd $(PKG_BLD_DIR) && ./configure $(LOG)
  cd $(PKG_BLD_DIR) && $(ENV_VARS) $(DOMAKE) CC CXX $(MK_VARS) $(LOG)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
