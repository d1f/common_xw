PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.12
PKG_VER_PATCH = -0.2~bpo70+1
PKG_SITE_PATH = pool/main/libg/$(PKG_BASE)

include $(DEFS)


define PRE_CONFIGURE_CMD
  $(RM) $(PKG_BLD_DIR)/src/gpg-error.h $(LOG)
endef

configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call bstamp_f,install,gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--enable-shared			\
	--disable-static		\
	--disable-nls			\
	--disable-languages		\
	--with-gnu-ld			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libgpg-error.so* $(RLIB_DIR) $(LOG)


include $(RULES)
