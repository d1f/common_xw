BUSYBOX_VERSION ?= 1.27.1
PKG_VERSION = $(BUSYBOX_VERSION)
PKG_SITES   = http://busybox.net/downloads

include $(DEFS)


PKG_PATCH_FILES  = $(sort $(wildcard $(PKG_PKG_DIR)/fixes-$(PKG_VERSION)/*.patch))
PKG_PATCH_FILES += $(sort $(wildcard $(PKG_PKG_DIR)/patches-$(PKG_VERSION)/*.patch))

ifeq ($(shell $(VERCMP) $(BUSYBOX_VERSION) '>=' 1.26.0),true)
  ifeq ($(shell $(VERCMP) $(LK_VERSION) '<=' 2.6.26),true)
    # reverse patch for old kernels
    define PATCH_CMD
      patch -d $(PKG_PATCHED_SRC_DIR) \
	-R -p1 < $(PKG_PKG_DIR)/mdev-create-devices-from-sys-dev.patch >> $(LOG_FILE)
    endef
  endif

  ifeq ($(shell $(VERCMP) $(BUSYBOX_VERSION) '>=' 1.26.2),true)
    # reverse patch for 1.26.2 which introduced bug in stdio redirect in ash.
    PATCH_CMD += ; patch -d $(PKG_PATCHED_SRC_DIR) \
	-R -p1 < $(PKG_PKG_DIR)/ash-redirect.patch >> $(LOG_FILE)
  endif
endif


configure : $(call tstamp_f,configure,linux)
build     : $(TOOLCHAIN_INSTALL_STAMP)

# 1.3.2 only:
ifeq ($(shell $(VERCMP) $(PKG_VERSION) '>=' 1.3),true)
ifeq ($(shell $(VERCMP) $(PKG_VERSION) '<'  1.4),true)
STD_LDIRSFLAGS := $(filter-out -Wl%,$(STD_LDIRSFLAGS))
endif
endif

STD_CPPFLAGS += -DINIT_SCRIPT=\"/init/rcS\"
STD_CPPFLAGS += -DINITTAB=\"/etc_/inittab\"
MK_VARS  = CC=$(CC)
MK_VARS += HOSTCC=$(BUILD_CC)
MK_VARS += HOSTCXX=$(BUILD_CXX)
MK_VARS += CROSS_COMPILE=$(CROSS_PREFIX)
MK_VARS += CROSS=$(CROSS_PREFIX) # for old versions
MK_VARS += ARCH=$(ARCH) SUBARCH=$(NARCH)
MK_VARS += CONFIG_EXTRA_CFLAGS='$(STD_CFLAGS) $(STD_CPPFLAGS)'
MK_VARS +=        EXTRA_CFLAGS='$(STD_CFLAGS) $(STD_CPPFLAGS)'
MK_VARS +=       EXTRA_LDFLAGS="$(STD_LDFLAGS)"
MK_VARS += PREFIX=$(ROOT_DIR) CONFIG_PREFIX=$(ROOT_DIR)
MK_VARS += SKIP_STRIP=y
#MK_VARS += V=99


MCONF_MK_VARS +=      HOSTNCURSES='-I$(BINC_DIR) -DCURSES_LOC="<ncurses.h>" -DLOCALE'
MCONF_MK_VARS +=   NATIVE_LDFLAGS='-L$(BLIB_DIR) -Wl,-rpath=$(BLIB_DIR) -lncurses -ltinfo'
MCONF_MK_VARS +=   LIBS="-L$(BLIB_DIR) -Wl,-rpath=$(BLIB_DIR) -lncurses -ltinfo"
MCONF_MK_VARS += HOST_EXTRACFLAGS='-I$(BINC_DIR) -DCURSES_LOC="<ncurses.h>" -DLOCALE'
MCONF_MK_VARS +=   HOST_LOADLIBES='-L$(BLIB_DIR) -Wl,-rpath=$(BLIB_DIR) -lncurses -ltinfo'

MENUCONFIG_CMD = $(MENUCONFIG_CMD_DEFAULT)

define CONFIGURE_CMD
  cp $(PKG_MENUCONFIG_FILE) $(PKG_BLD_DIR)/.config		        $(LOG)
  yes '' 2>/dev/null | $(DOMAKE) -C $(PKG_BLD_DIR) $(MK_VARS) oldconfig	$(LOG)
endef


    BUILD_CMD_TARGET = busybox
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

ifeq ($(shell $(VERCMP) $(PKG_VERSION) '<' 1.19),true)
 define install_groups_cmd
  # busybox < 1.19 is not implemented groups command, install script emulator
  $(IW) $(INSTxFILES) $(PKG_PKG_DIR)/groups $(RBIN_DIR) $(LOG)
 endef
endif

define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(install_groups_cmd)
endef


include $(RULES)
