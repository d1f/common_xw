PKG_TYPE      = ORIGIN
PKG_VERSION   = 1.0.7
PKG_SITES     = http://hipl.hiit.fi
PKG_SITE_PATH = $(PKG)/release/$(PKG_VERSION)/source
PKG_REMOTE_FILE_SUFFICES = .tar.gz

include $(DEFS)


# Linux kernel version 2.6.27 or newer is the minimum version.
# Build-Depends: python, libconfig8-dev, iptables-dev
# For Perl Net::IP and Net::DNS modules are required.

configure : $(call tstamp_f,install,toolchain openssl-0.9 libconfig-1.3.2 iptables)
configure : $(call bstamp_f,install,libtool sed gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--enable-shared		\
	--disable-static	\
	--enable-firewall	\
	--enable-rvs		\
	--disable-profiling	\
	--disable-debug		\
	--disable-performance	\
	--with-gnu-ld		\
	$(STD_VARS)
endef
# --with-nomodules=list   comma-separated list of disabled modules


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
