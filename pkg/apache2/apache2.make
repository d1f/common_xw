PKG_TYPE          = DEBIAN3
PKG_VERSION       = 2.2.22
PKG_VER_PATCH     = -13+deb7u11
PKG_SITES         = $(SECDEB_ORG_SITES)
PKG_SITE_PATH     = pool/updates/main/a/$(PKG_BASE)
#PKG_SITE_PATH    = pool/main/a/$(PKG_BASE)

include $(DEFS)


define POST_EXTRACT_CMD
  $(RM_R) $(PKG_ORIG_SRC_DIR)/srclib $(LOG)
endef


PKG_PATCH2_FILES += $(PKG_PKG_DIR)/*.patch


include $(TOOLS_PKG_DIR)/configure-menuconfig.mk


configure : $(call tstamp_f,install,toolchain openssl-0.9 pcre3)
configure : $(call tstamp_f,install,apr apr-util pkg-config)
configure : $(call bstamp_f,install,gawk)

install-root : $(call tstamp_f,install-root,openssl-0.9 pcre3 apr apr-util)


STD_CPPFLAGS += -DAPR_IOVEC_DEFINED
STD_CPPFLAGS += -DBIG_SECURITY_HOLE # allow user root in config

ifeq ($(LIBC_TYPE),uc) # for uClibc
  STD_LDFLAGS += -lpthread
endif

# hack:
CONFIGURE_OPTIONS += --disable-proxy-balancer

STD_VARS += PKGCONFIG=$(PKG_CONFIG)

define CONFIGURE_CMD
  $(RELINK)                                    $(PKG_BLD_DIR)/configure
  $(SED) -ire 's/\$$LDFLAGS \$$i/\$$LDFLAGS/g' $(PKG_BLD_DIR)/configure

  unset DEFS; 				\
  cd $(PKG_BLD_DIR) 	&&		\
     ac_cv_func_mmap=yes		\
     ac_cv_file__dev_zero=yes		\
     ac_cv_func_setpgrp_void=no		\
     apr_cv_process_shared_works=yes	\
     apr_cv_mutex_robust_shared=yes	\
     apr_cv_tcp_nodelay_with_cork=yes	\
     ssize_t_fmt=z			\
     ap_cv_void_ptr_lt_long=no		\
     ac_cv_struct_rlimit=yes		\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)/apache2	\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	$(CONFIGURE_OPTIONS)		\
	$(STD_VARS)
endef

#--enable-modules=all|most
#--enable-mods-shared=all|most
#--with-mpm=event|worker|prefork

MK_VARS += IINC_DIR=$(IINC_DIR)
MK_VARS += BUILD_CC=$(BUILD_CC)

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILE) $(INST_DIR)/apache2/bin/httpd $(RSBIN_DIR)/apache2 $(LOG)


include $(RULES)
