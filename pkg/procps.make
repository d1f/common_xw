PKG_TYPE          = DEBIAN3
PKG_VERSION       = 3.3.9
PKG_VER_PATCH     = -9
PKG_SITE_PATH     = pool/main/p/$(PKG_BASE)

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call bstamp_f,install,libtool)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ac_cv_func_malloc_0_nonnull=yes	\
     ac_cv_func_realloc_0_nonnull=yes	\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--enable-static			\
	--disable-shared		\
	--disable-nls			\
	--disable-kill			\
	--with-gnu-ld			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	--without-ncurses		\
	$(STD_VARS)
endef
# --enable-watch8bit      enable watch to be 8bit clean (requires ncursesw)


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_ROOT_CMD = $(IW) $(INSTxFILE) $(PKG_BLD_DIR)/ps/pscommand $(RBIN_DIR)/ps $(LOG)


include $(RULES)
