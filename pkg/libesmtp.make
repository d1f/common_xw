PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.0.6
PKG_VER_PATCH     = -4
PKG_SITE_PATH     = pool/main/libe/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain openssl-0.9)
configure : $(call bstamp_f,install,gawk sed bison flex)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)			\
        --build=$(BUILD) --host=$(TARGET)	\
	--enable-shared --disable-static	\
	--enable-more-warnings=no		\
	--disable-isoc				\
	--enable-pthreads			\
	--with-gnu-ld				\
	--with-openssl				\
	$(STD_VARS)
endef
#	--with-lwres=DIR        use lwres library for getaddrinfo (default=no)
#	--with-auth-plugin-dir=$(ROOT_DIR)/usr/local/lib/esmtp-plugins \


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libesmtp.so*       $(RLIB_DIR)               $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/esmtp-plugins/*.so $(RLIB_DIR)/esmtp-plugins $(LOG)
endef


include $(RULES)
