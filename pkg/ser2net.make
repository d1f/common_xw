PKG_TYPE        = DEBIAN3

ifeq ($(PKG_TYPE),DEBIAN3)
  PKG_VERSION   = 2.9.1
  PKG_VER_PATCH = -1
  PKG_SITE_PATH = pool/main/s/$(PKG_BASE)
else
  PKG_VERSION   = 2.7
  PKG_SITES     = $(SF_NET_SITES)
  PKG_SITE_PATH = $(PKG_BASE)
  PKG_REMOTE_FILE_SUFFICES = .tar.gz
endif

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=$(INST_DIR)			\
	--with-gnu-ld				\
	$(STD_VARS)
endef
# --with-uucp-locking
# --with-tcp-wrappers


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ISBIN_DIR)/ser2net $(RSBIN_DIR) $(LOG)


include $(RULES)
