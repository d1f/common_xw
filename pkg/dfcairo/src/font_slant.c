#include <df/cairo.h>
#include <df/error.h>

#include <string.h> // strcmp


// returns -1 on wrong name, error message issued
// norm[al] | italic | oblique
int dfcairo_font_slant(const char *slant_name, cairo_font_slant_t *slant)
{
    if (slant == NULL)
    {
	dferror(EXIT_SUCCESS, 0, "%s(): slant pointer == NULL", __func__);
	return -1;
    }

    if (strcmp(slant_name, "norm") == 0 || strcmp(slant_name, "normal") == 0)
	*slant = CAIRO_FONT_SLANT_NORMAL;
    else if (strcmp(slant_name, "italic") == 0)
	*slant = CAIRO_FONT_SLANT_ITALIC;
    else if (strcmp(slant_name, "oblique") == 0)
	*slant = CAIRO_FONT_SLANT_OBLIQUE;
    else
    {
	dferror(EXIT_SUCCESS, 0, "%s(): Unknown font slant \"%s\"",
		__func__, slant_name);
	return -1;
    }

    return 0;
}
