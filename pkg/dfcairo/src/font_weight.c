#include <df/cairo.h>
#include <df/error.h>

#include <string.h> // strcmp


// returns -1 on wrong name, error message issued
// norm[al] | bold
int dfcairo_font_weight(const char *weight_name, cairo_font_weight_t *weight)
{
    if (weight == NULL)
    {
	dferror(EXIT_SUCCESS, 0, "%s(): weight pointer == NULL", __func__);
	return -1;
    }

    if (strcmp(weight_name, "norm") == 0 ||  strcmp(weight_name, "normal") == 0)
	*weight = CAIRO_FONT_WEIGHT_NORMAL;
    else if (strcmp(weight_name, "bold") == 0)
	*weight = CAIRO_FONT_WEIGHT_BOLD;
    else
    {
	dferror(EXIT_SUCCESS, 0, "%s(): Unknown font weight \"%s\"",
		__func__, weight_name);
	return -1;
    }

    return 0;
}
