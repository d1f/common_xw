#include <df/cairo.h>

void dfcairo_print_font_extents(FILE *out, const cairo_font_extents_t *fe)
{
    /*
     cairo_font_extents_t:

     double ascent
        the distance that the font extends above the baseline.
	Note that this is not always exactly equal to the maximum
	of the extents of all the glyphs in the font, but rather
	is picked to express the font designer's intent as to how
	the font should align with elements above it.

     double descent
        the distance that the font extends below the baseline.
	This value is positive for typical fonts that include portions
	below the baseline.
	Note that this is not always exactly equal to the maximum
	of the extents of all the glyphs in the font, but rather
	is picked to express the font designer's intent as to how
	the font should align with elements below it.

     double height
        the recommended vertical distance between baselines
	when setting consecutive lines of text with the font.
	This is greater than ascent+descent by a quantity known as
	the line spacing or external leading.
	When space is at a premium, most fonts can be set with only a
	distance of ascent+descent between lines.

     double max_x_advance
        the maximum distance in the X direction that the origin is advanced
	for any glyph in the font.

     double max_y_advance
        the maximum distance in the Y direction that the origin is advanced
	for any glyph in the font.
	This will be zero for normal fonts used for horizontal writing.
	(The scripts of East Asia are sometimes written vertically.)
     */

    fprintf(out, "font extents: ascent: %.1f, descent: %.1f, height: %.1f, x_advance: %.1f, y_advance: %.1f\n",
	    fe->ascent, fe->descent,
	    fe->height,
	    fe->max_x_advance, fe->max_y_advance);
}
