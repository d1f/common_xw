#include <df/cairo.h>

void dfcairo_print_text_extents(FILE *out, const cairo_text_extents_t *te)
{
    /*
     cairo_text_extents_t:

     double x_bearing:
        the horizontal distance from the origin to the leftmost part
	of the glyphs as drawn. Positive if the glyphs lie entirely
	to the right of the origin.

     double y_bearing
        the vertical distance from the origin to the topmost part
	of the glyphs as drawn. Positive only if the glyphs lie
	completely below the origin; will usually be negative.

     double width
        width of the glyphs as drawn

     double height
        height of the glyphs as drawn

     double x_advance
        distance to advance in the X direction after drawing these glyphs

     double y_advance
        distance to advance in the Y direction after drawing these glyphs.
	Will typically be zero except for vertical text layout
	as found in East-Asian languages.
     */
    fprintf(out, "text extents: bearing: %5.1f x %5.1f, WxH: %5.1f x %5.1f, advance: %5.1f x %5.1f\n",
	   te->x_bearing, te->y_bearing,
	   te->width    , te->height,
	   te->x_advance, te->y_advance);
}
