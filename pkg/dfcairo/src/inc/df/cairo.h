#ifndef  DF_CAIRO_H
# define DF_CAIRO_H

# include <stdio.h>  // FILE

# include <cairo/cairo.h>


void dfcairo_print_font_extents(FILE *out, const cairo_font_extents_t *fe);
void dfcairo_print_text_extents(FILE *out, const cairo_text_extents_t *te);

// returns -1 on wrong name, error message issued
// norm[al] | italic | oblique
int dfcairo_font_slant (const char * slant_name, cairo_font_slant_t  *slant);

// returns -1 on wrong name, error message issued
// norm[al] | bold
int dfcairo_font_weight(const char *weight_name, cairo_font_weight_t *weight);

// returns -1 on wrong name, error message issued
int dfcairo_set_font(cairo_t *cr,
		     const char *font_family,
		     const char *font_slant, const char *font_weight,
		     double font_size);


#endif //DF_CAIRO_H
