#include <df/cairo.h>
#include <df/error.h>


// returns -1 on wrong name, error message issued
int dfcairo_set_font(cairo_t *cr,
		     const char *font_family,
		     const char *font_slant, const char *font_weight,
		     double font_size)
{
    cairo_font_slant_t slant = CAIRO_FONT_SLANT_NORMAL;

    int rc = dfcairo_font_slant (font_slant, &slant);
    if (rc < 0)
	return rc;

    cairo_font_weight_t weight = CAIRO_FONT_WEIGHT_NORMAL;

    rc = dfcairo_font_weight(font_weight, &weight);
    if (rc < 0)
	return rc;

    cairo_select_font_face(cr, font_family, slant, weight);
    cairo_set_font_size(cr, font_size);

    return 0;
}
