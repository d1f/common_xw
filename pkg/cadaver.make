PKG_TYPE      = DEBIAN1
PKG_VERSION   = 0.23.3
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/c/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES = $(PKG_PKG_DIR)/$(PKG)_*.patch


configure : $(call tstamp_f,install,toolchain openssl-0.9 neon27)


define CONFIGURE_CMD
 cd $(PKG_BLD_DIR) &&			\
    ac_cv_path_NEON_CONFIG=$(IBIN_DIR)/neon-config; \
    ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--exec-prefix=$(ROOT_DIR)	\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--disable-debugging		\
	--disable-readline		\
	--disable-netrc			\
	--enable-threadsafe-ssl=posix	\
	--disable-nls			\
	--enable-threads=posix		\
	--with-neon			\
	--with-ssl=openssl		\
	--without-libproxy		\
	--with-gnu-ld			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	ac_cv_path_NEON_CONFIG=$(IBIN_DIR)/neon-config; \
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
