PKG_TYPE          = DEBIAN3
PKG_VERSION       = 0.17
PKG_VER_PATCH     = -31
PKG_SITE_PATH     = pool/main/n/$(PKG_BASE)

include $(DEFS)


pre-configure : $(call bstamp_f,install,sed)

define PRE_CONFIGURE_CMD
  $(SED) -i -e 's,./__conftest,true,g'                  $(PKG_BLD_DIR)/configure $(LOG)
  $(SED) -i -e 's,-I/usr/include/ncurses,$(IINC_DIR),g' $(PKG_BLD_DIR)/configure $(LOG)
endef


configure : $(call tstamp_f,install,ncurses)
install-root : $(call tstamp_f,install-root,ncurses)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     CFLAGS="$(STD_CFLAGS) $(STD_CPPFLAGS) $(STD_LDFLAGS)"	\
     ./configure $(LOG)			\
	--without-readline		\
        --prefix=			\
        --exec-prefix=			\
	--installroot=$(INST_DIR)	\
	--with-c-compiler=$(CC)

  $(SED) -i -e 's/^CFLAGS=\(.*\)$$/CFLAGS= $(STD_CFLAGS) -fno-strict-aliasing \1/' \
	$(PKG_BLD_DIR)/MCONFIG
endef


BUILD_CMD = $(BUILD_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(IBIN_DIR)/ftp  $(RBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(IBIN_DIR)/pftp $(RBIN_DIR) $(LOG)
endef


include $(RULES)
