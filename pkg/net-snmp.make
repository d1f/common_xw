PKG_TYPE      = DEBIAN3
PKG_VERSION   = 5.7.2.1~dfsg
PKG_VER_PATCH = -7
PKG_SITE_PATH = pool/main/n/$(PKG_BASE)

include $(DEFS)


define PRE_CONFIGURE_CMD
  $(RM) $(PKG_BLD_DIR)/stamp-h $(LOG)
endef

configure : $(call tstamp_f,install,toolchain)
configure : $(call bstamp_f,install,gawk sed libtool)

ifneq ($(CONFIG_WITH_IPv6),y)
 configure_opts += --disable-ipv6
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
        --build=$(BUILD)		\
	--host=$(TARGET)		\
	--enable-agent			\
	--disable-applications		\
	--disable-manuals		\
	--disable-scripts		\
	--disable-mibs			\
	--enable-new-features		\
	--disable-des			\
	--disable-privacy		\
	--disable-md5			\
	--disable-internal-md5		\
	--disable-ipv6			\
	--enable-snmpv1			\
	--enable-snmpv2c		\
	--disable-debugging		\
	--disable-deprecated		\
	--enable-minimalist		\
	--enable-mini-agent		\
	--disable-embedded-perl		\
	--disable-shared		\
	--enable-static			\
	--with-endianness=$(endianess)	\
	--with-cc=$(CC)			\
	--with-linkcc=$(CC)		\
	--with-ar=$(AR)			\
	--with-cflags="$(STD_CFLAGS) $(STD_CPPFLAGS)"	\
	--with-ldflags="$(STD_LDFLAGS)"	\
	--without-rpm			\
	--with-defaults			\
	--with-logfile="/var/log/snmpd.log"		\
	--with-persistent-directory=/var/conf/snmp	\
	--with-sys-contact="support@sigrand.com"	\
	--with-sys-location=Unknown	\
	--with-gnu-ld			\
	$(configure_opts)		\
	$(STD_VARS)
endef


    MK_JOBS = 1
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ISBIN_DIR)/snmpd $(RSBIN_DIR) $(LOG)


include $(RULES)
