pkg_ver_major = 8
PKG_VERSION   = $(pkg_ver_major).0
PKG_BASE      = hiawatha
PKG_SITES     = http://www.hiawatha-webserver.org
PKG_SITE_PATH = files/hiawatha-$(pkg_ver_major)

SEPARATE_SRC_BLD_DIRS=defined

include $(DEFS)


PKG_PATCH2_FILES += $(PKG_PKG_DIR)/*.patch


conf_opts  = -DCMAKE_INSTALL_PREFIX=$(INST_DIR)

conf_opts += -DENABLE_CACHE=$(if $(CONFIG_HIAWATHA8_CACHE),on,off)
conf_opts += -DENABLE_CHROOT=$(if $(CONFIG_HIAWATHA8_CHROOT),on,off)
conf_opts += -DENABLE_COMMAND=$(if $(CONFIG_HIAWATHA8_COMMAND),on,off)
conf_opts += -DENABLE_DEBUG=off
conf_opts += -DENABLE_IPV6=$(if $(CONFIG_WITH_IPv6),on,off)
conf_opts += -DENABLE_MONITOR=$(if $(CONFIG_HIAWATHA8_MONITOR),on,off)

#ifeq ($(CONFIG_HIAWATHA8_WITH_POLARSSL),y)
#  configure : $(call tstamp_f,install,polarssl)
#endif
conf_opts += -DENABLE_SSL=$(if $(CONFIG_HIAWATHA8_WITH_POLARSSL),on,off)

conf_opts += -DENABLE_TOOLKIT=$(if $(CONFIG_HIAWATHA8_TOOLKIT),on,off)

ifeq ($(CONFIG_HIAWATHA8_XSLT),y)
  configure : $(call tstamp_f,install,libxslt)
endif
conf_opts += -DENABLE_XSLT=$(if $(CONFIG_HIAWATHA8_XSLT),on,off)


STD_CPPFLAGS += -DNDEBUG

conf_opts += -DCMAKE_CROSSCOMPILING=TRUE
conf_opts += -DCMAKE_SYSTEM=Linux-$(LK_VERSION)
conf_opts += -DCMAKE_SYSTEM_NAME=Linux
conf_opts += -DCMAKE_SYSTEM_VERSION=$(LK_VERSION)
conf_opts += -DCMAKE_SYSTEM_PROCESSOR=$(NARCH)

conf_opts += -DCMAKE_AR=$(AR)
conf_opts += -DCMAKE_C_COMPILER=$(CC)
conf_opts += -DCMAKE_C_FLAGS="$(STD_CPPFLAGS) $(STD_CFLAGS)"
conf_opts += -DCMAKE_EXE_LINKER_FLAGS="$(STD_LDFLAGS)"
conf_opts += -DCMAKE_LINKER=$(LD)
conf_opts += -DCMAKE_NM=$(NM)
conf_opts += -DCMAKE_OBJCOPY=$(OBJCOPY)
conf_opts += -DCMAKE_OBJDUMP=$(OBJDUMP)
conf_opts += -DCMAKE_RANLIB=$(RANLIB)
conf_opts += -DCMAKE_STRIP=$(STRIP)
conf_opts += -DPKG_CONFIG_EXECUTABLE=$(PKG_CONFIG)

conf_opts += -DPID_DIR:STRING=/var/run


configure : $(call bstamp_f,install,cmake)
configure : $(call tstamp_f,install,pkg-config)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     $(BBIN_DIR)/cmake $(PKG_SRC_DIR) $(conf_opts) $(LOG)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/hiawatha  $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES)    $(ILIB_DIR)/hiawatha/* $(RLIB_DIR) $(LOG)
endef


include $(RULES)
