PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.4.0
PKG_VER_PATCH = -5
PKG_SITE_PATH = pool/main/p/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain)
configure : $(call bstamp_f,install,gawk sed)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure	$(LOG)	\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=$(INST_DIR)			\
	--disable-bcrelay			\
	$(STD_VARS)
endef
#--enable-facility=name  Use another syslog facility, default LOG_DAEMON
#--with-bsdppp           Use BSD user-space ppp
#--with-slirp            Use SLIRP instead of pppd
#--with-libwrap          Use libwrap (tcp wrappers)


# plugins avoided
MK_VARS += subdirs=
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/pptpd    $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/pptpctrl $(RSBIN_DIR) $(LOG)
endef


include $(RULES)
