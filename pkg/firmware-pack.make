PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install : $(call tstamp_f,install-root,linux)
install : $(call tstamp_f,install,root-image)

reinstall-root : reinstall

chip = $(shell echo $(PLATFORM) | cut -d_ -f2-3)
rel  = $(shell cat $(or $(wildcard $(RCONF_DIR)/release),$(wildcard $(RETC_DIR)/release)) /dev/null)

pack_list = $(if $(NOKERNEL),,../$(notdir $(KERN_IMAGE)) ../$(notdir $(KERN_IMAGE)).md5)
pack_list +=  $(if $(NOROOT),,$(notdir $(ROOT_IMAGE)) $(notdir $(ROOT_IMAGE)).md5)

inst_file = $(PRJ_NAME)-$(chip)-$(rel).tar

define INSTALL_CMD
  echo "$(I1)Creating $(inst_file)"
  $(IW) $(TAR) -C $(LABEL_IMAGES_DIR) -cvf $(LABEL_IMAGES_DIR)/$(inst_file) $(pack_list) $(LOG)
endef


include $(RULES)
