#include <swaacenc.h>
#include <faac.h>
#include <df/log.h>


static sig_atomic_t use_count = 0;

static unsigned long maxInputSamples;
static unsigned long maxOutputBytes;
static faacEncHandle handle;

// returns 0 on success or -1 on error
int swAACenc_create(size_t sample_rate, size_t bit_rate)
{
    if (++use_count == 1)
    {
	handle = faacEncOpen(sample_rate, 1, &maxInputSamples, &maxOutputBytes);
	if (handle == NULL)
	{
	    dflog(LOG_ERR, "faac: Can't open encoder");
	    --use_count;
	    return -1;
	}

	// maxInputSamples: 1024, maxOutputBytes: 768

	faacEncConfigurationPtr conf = faacEncGetCurrentConfiguration(handle);

	//... set parameters
	//conf->useTns //Use Temporal Noise Shaping
	conf->bitRate = bit_rate;
	//conf->outputFormat; // 0 = Raw, 1 - ADTS (default)
	conf->inputFormat = FAAC_INPUT_16BIT;
	//conf->shortctl = SHORTCTL_NOSHORT;//block type enforcing (SHORTCTL_NORMAL/SHORTCTL_NOSHORT/SHORTCTL_NOLONG)

	faacEncSetConfiguration(handle, conf);
    }

    return OSA_SOK;
}

void swAACenc_delete(void)
{
    if (--use_count == 0)
	faacEncClose(handle);
    else if (use_count < 0)
	use_count=0;
}

// returns encoded length or -1 on error
int swAACenc_process(void *dst, size_t dstlen, const void *src, size_t srclen)
{
    // dstlen: 2048, srclen: 2048
    int rc = faacEncEncode(handle,
			   src, srclen,
			   dst, dstlen
			  );
    return rc;
}

/*
samplesInput

The number of valid samples in inputBuffer, this should be
the number received in inputSamples in the call to faacEncOpen(),
as long as that number of samples is available.
Once you have called faacEncEncode() with zero samples input,
the flushing process is initiated.•

outputBuffer

Pointer to a buffer receiving the bitstream data.
This buffer should at least be of size maxOutputBytes
received in the call to faacEncOpen().

Return value

A negative value to indicate a failure, the number of vaid bytes
in the output buffer otherwise. A return value of zero does not
indicate failure.
*/
