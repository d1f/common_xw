PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install-root : $(call tstamp_f,install-root,lighttpd)

define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/lighttpd.conf $(ROOT_DIR)/conf $(LOG)
  $(call INSTALL_PKGPKG_INIT_XFILES_F, lighttpd.rc)
  $(IW) $(MKDIR_P) $(ROOT_DIR)/www/nonssl       $(LOG)
  $(IW) $(MKDIR_P) $(ROOT_DIR)/var/log/lighttpd $(LOG)
endef


include $(RULES)
