PKG_ORIG_DIR          = $(BLD_DIR)/onvif-wsdl/client
PKG_FETCH_METHOD      = DONE
PKG_HAS_NO_SOURCES    = defined
SEPARATE_SRC_BLD_DIRS = defined
PLATFORM              = build

include $(DEFS)


configure : $(call tstamp_f,build,onvif-wsdl)

# some warnings off
STD_CFLAGS += -Wno-cast-qual
STD_CFLAGS += -Wno-unused-parameter
STD_CFLAGS += -Wno-inline

BUILD_TYPE     = SHARED_LIB
LIB_NAME       = onvif2-client
MAKES_MAKEFILE = $(TOOLS_DIR)/makes/all.Makefile
include          $(TOOLS_DIR)/makes/conf.mk

CONFIGURE_CMD  = $(MAKES_CONFIGURE_CMD)


#MK_VARS += Q=
build : $(call tstamp_f,install,toolchain)

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(INSTALL_ROOT_CMD_DEFAULT)


include $(RULES)
