PKG_TYPE      = DEBIAN1
PKG_VERSION   = 2.25b
PKG_VER_PATCH = -11
PKG_SITE_PATH = pool/main/t/$(PKG_BASE)

include $(DEFS)


include $(TOOLS_PKG_DIR)/dpatch.mk

PKG_PATCH3_FILES += $(PKG_PKG_DIR)/*.patch


pre-configure : $(call bstamp_f,install,autotools-dev)

define PRE_CONFIGURE_CMD
  $(RM)  $(PKG_BLD_DIR)/config.guess     $(PKG_BLD_DIR)/config.sub $(LOG)
  cp $(BSHARE_DIR)/misc/config.guess $(BSHARE_DIR)/misc/config.sub \
			$(PKG_BLD_DIR) $(LOG)
endef


configure : $(TOOLCHAIN_INSTALL_STAMP)

ifeq ($(PLATFORM),build)
  DEFAULT_USER := $(shell id -nu)
  _ccopt  = -DDEFAULT_USER='\"$(DEFAULT_USER)\"'
  _ccopt += -DCGI_PATH='\"$(RBIN_DIR):/usr/bin:/bin\"'
  _ccopt += -DCGI_LD_LIBRARY_PATH='\"$(RLIB_DIR):/usr/lib:/lib\"'
endif

ccopt="$(STD_CFLAGS) $(STD_CPPFLAGS) $(_ccopt)"

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
	CC=$(CC)		\
	CFLAGS=$(ccopt)		\
	LDFLAGS=$(LDFLAGS)	\
	./configure	$(LOG)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--prefix=$(INST_DIR)	\
	--sysconfdir=$(RETC_DIR)/thttpd
endef

      MK_VARS = CCOPT=$(ccopt)
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/thttpd $(RSBIN_DIR) $(LOG)
endef


.PHONY: start stop
start:
	@$(PRJ_PKG_DIR)/scripts/thttpd-native.sh $(ROOT_DIR) start

stop:
	@$(PRJ_PKG_DIR)/scripts/thttpd-native.sh $(ROOT_DIR) stop


include $(RULES)
