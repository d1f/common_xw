# polarssl become mbedtls
PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.3.9
PKG_VER_PATCH     = -2.1
PKG_SITE_PATH     = pool/main/p/$(PKG_BASE)

include $(DEFS)


build : $(call tstamp_f,install,toolchain)

STD_CFLAGS  += -I../include -D_FILE_OFFSET_BITS=64
STD_LDFLAGS += -L../library -lpolarssl

MK_VARS += DESTDIR=$(INST_DIR) PREFIX=polarssl_ $(STD_VARS) OFLAGS=$(STD_OFLAG)

    BUILD_CMD_TARGET = no_test
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
