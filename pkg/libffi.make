PKG_TYPE      = DEBIAN3
PKG_VERSION   = 3.1
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/libf/$(PKG_BASE)

include $(DEFS)


PRE_CONFIGURE_CMD = touch $(PKG_BLD_SUB_DIR)/doc/libffi.info $(LOG)

configure : $(TOOLCHAIN_INSTALL_STAMP)

define CONFIGURE_CMD
  cd $(PKG_BLD_SUB_DIR) &&	\
      ./configure -C $(LOG)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--prefix=$(INST_DIR)	\
	--disable-static	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libffi.so* $(RLIB_DIR) $(LOG)


include $(RULES)
