PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.3.3
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/x/$(PKG_BASE)

include $(DEFS)


pre-configure : $(call bstamp_f,install,sed libtool)

AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk

AM_VER = 1.11
include $(TOOLS_PKG_DIR)/automake.mk


PKG_SUB_DIR = build/generic

LIBTOOLIZE_FLAGS = --force --copy --verbose

define PRE_CONFIGURE_CMD
  cd $(PKG_BLD_SUB_DIR) && $(AUTOCONF)  $(AUTOCONF_FLAGS) $(LOG)
  cd $(PKG_BLD_SUB_DIR) && libtoolize $(LIBTOOLIZE_FLAGS) $(LOG)
endef


configure : $(call tstamp_f,install,toolchain)

define CONFIGURE_CMD
  cd $(PKG_BLD_SUB_DIR) &&	\
	./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
        --build=$(BUILD)	\
	--host=$(TARGET)	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD =  $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libxvidcore.so* $(RLIB_DIR) $(LOG)


include $(RULES)
