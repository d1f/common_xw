PKG_TYPE          = DEBIAN3
PKG_VERSION       = 2014.01.13
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/libl/$(PKG_BASE)

include $(DEFS)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./genMakefiles linux
endef


MK_VARS += $(STD_VARS)
MK_VARS += C_COMPILER=$(CC)
MK_VARS += CFLAGS="$(STD_CFLAGS) $(STD_CPPFLAGS)"
MK_VARS += CPLUSPLUS_COMPILER=$(CXX_)
MK_VARS += LINK="$(CXX_) -o "
MK_VARS += LIBRARY_LINK="$(AR) cr "

BUILD_CMD =   $(BUILD_CMD_DEFAULT)


define INSTALL_CMD
  $(IWSH) "cd $(PKG_BLD_DIR) && \
	  for f in BasicUsageEnvironment UsageEnvironment liveMedia groupsock; do \
		$(INST_FILES) \$${f}/include/*  $(IINC_DIR)/\$${f};	\
		$(INST_FILES) \$${f}/lib\$${f}.a $(ILIB_DIR);		\
	  done" $(LOG)
endef

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/mediaServer/live555MediaServer $(RBIN_DIR) $(LOG)

  $(IWSH) "cd $(PKG_BLD_DIR) && \
	  for f in openRTSP playSIP sapWatch testAMRAudioStreamer \
		testMP3Receiver testMP3Streamer \
		testMPEG1or2AudioVideoStreamer \
		testMPEG1or2AudioVideoToDarwin \
		testMPEG1or2ProgramToTransportStream \
		testMPEG1or2Splitter testMPEG1or2VideoReceiver \
		testMPEG1or2VideoStreamer testMPEG2TransportStreamer \
		testMPEG4VideoStreamer testMPEG4VideoToDarwin \
		testOnDemandRTSPServer testRelay testWAVAudioStreamer \
		MPEG2TransportStreamIndexer \
		vobStreamer; do \
	  $(INSTxFILES) testProgs/\$${f} $(RBIN_DIR); \
	done" $(LOG)
endef


include $(RULES)
