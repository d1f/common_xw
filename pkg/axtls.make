# https://sourceforge.net/projects/axtls/files/2.0.1/axTLS-2.0.1.tar.gz
PKG_TYPE      = ORIGIN
PKG_VERSION   = 2.0.1
PKG_SITES     = $(SF_NET_SITES)
PKG_BASE      = axTLS
PKG_SITE_PATH = projects/axtls/files/$(PKG_VERSION)
PKG_REMOTE_FILE_SUFFICES = .tar.gz
PLATFORM      = build

include $(DEFS)


MCONF_MK_VARS +=      HOSTNCURSES='-I$(BINC_DIR) -DCURSES_LOC="<ncurses.h>" -DLOCALE'
MCONF_MK_VARS +=   NATIVE_LDFLAGS='-L$(BLIB_DIR) -Wl,-rpath=$(BLIB_DIR) -lncurses -ltinfo'
MCONF_MK_VARS +=   LIBS="-L$(BLIB_DIR) -Wl,-rpath=$(BLIB_DIR) -lncurses -ltinfo"
MCONF_MK_VARS += HOST_EXTRACFLAGS='-I$(BINC_DIR) -DCURSES_LOC="<ncurses.h>" -DLOCALE'
MCONF_MK_VARS +=   HOST_LOADLIBES='-L$(BLIB_DIR) -Wl,-rpath=$(BLIB_DIR) -lncurses -ltinfo'

PKG_MENUCONFIG_SUBDIR = config
    MENUCONFIG_CMD    = $(MENUCONFIG_CMD_DEFAULT)

define CONFIGURE_CMD
  cp $(PKG_MENUCONFIG_FILE) $(PKG_BLD_DIR)/$(PKG_MENUCONFIG_SUBDIR)/.config $(LOG)
  yes '' 2>/dev/null | $(DOMAKE) -C $(PKG_BLD_DIR) $(MK_VARS)     oldconfig $(LOG)
endef


      MK_VARS = PREFIX=$(INST_DIR)

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
