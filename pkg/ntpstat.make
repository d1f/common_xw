PKG_TYPE      = DEBIAN3
PKG_VERSION   = 0.0.0.1
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/n/$(PKG_BASE)

include $(DEFS)


build : $(call tstamp_f,install,toolchain)

MK_VARS = $(STD_VARS)

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/$(PKG) $(RBIN_DIR) $(LOG)


include $(RULES)
