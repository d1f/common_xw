PKG_TYPE      = DEBIAN3
PKG_VERSION   = 2.2
PKG_VER_PATCH = -5
PKG_SITE_PATH = pool/main/c/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


configure : $(call bstamp_f,install,gawk libtool)

AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk

AM_VER = 1.14
include $(TOOLS_PKG_DIR)/automake.mk

define PRE_CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && $(ACLOCAL)       $(ACLOCAL_FLAGS) $(LOG)
  cd $(PKG_BLD_DIR) && libtoolize --copy --force         $(LOG)
  cd $(PKG_BLD_DIR) && $(AUTOCONF)     $(AUTOCONF_FLAGS) $(LOG)
  cd $(PKG_BLD_DIR) && $(AUTOHEADER) $(AUTOHEADER_FLAGS) $(LOG)
  cd $(PKG_BLD_DIR) && $(AUTOMAKE)     $(AUTOMAKE_FLAGS) $(LOG)
  $(RM_R) $(PKG_BLD_DIR)/autom4te.cache                  $(LOG)
endef


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--with-gnu-ld		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
