# osoleted, use pkg/perl instead

PKG_TYPE      = DEBIAN3
PKG_VERSION   = 0.421000
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/libm/$(PKG_BASE)

PLATFORM      = build

include $(DEFS)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && perl Makefile.PL $(LOG)
endef


define BUILD_CMD
  cd $(PKG_BLD_DIR) && perl Build $(LOG)
endef


define INSTALL_CMD
  cd $(PKG_BLD_DIR) && $(IW) perl Build install --prefix $(INST_DIR) $(LOG)
endef


include $(RULES)
