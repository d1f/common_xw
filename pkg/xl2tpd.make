PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.3.6+dfsg
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/x/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES += $(PKG_PKG_DIR)/$(PKG)*.patch


build : $(call tstamp_f,install,toolchain libpcap)


STD_CPPFLAGS += -DDEFAULT_AUTH_FILE=\""/etc/xl2tp-secrets\""
STD_CPPFLAGS += -DDEFAULT_CONFIG_FILE=\""/etc/xl2tpd.conf\""
STD_CPPFLAGS += -DCONTROL_PIPE=\""/var/run/xl2tp-control"\"
STD_CPPFLAGS += -DPPPD=\""/sbin/pppd"\"
STD_CPPFLAGS += -DIP_ALLOCATION -DLINUX -DSANITY
STD_CPPFLAGS += -DDEBUG_PPPD #-DTRUST_PPPD_TO_DIE

ifeq ($(shell $(VERCMP) $(LK_VERSION) '>=' 2.6.23),true)
  STD_CPPFLAGS += -I$(LK_INC_DIR)
  STD_CPPFLAGS += -DUSE_KERNEL
endif

STD_CFLAGS += $(STD_CPPFLAGS)

MK_VARS += $(STD_VARS)
MK_VARS += PREFIX= DESTDIR=$(INST_DIR)


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/xl2tpd         $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/xl2tpd-control $(RSBIN_DIR) $(LOG)
 #$(IW) $(INSTxFILES)  $(IBIN_DIR)/pfc             $(RBIN_DIR) $(LOG)
endef

#install TODO:
#	cp $(CURDIR)/doc/l2tpd.conf.sample   $(CURDIR)/debian/xl2tpd/etc/xl2tpd/xl2tpd.conf
#	cp $(CURDIR)/doc/l2tp-secrets.sample $(CURDIR)/debian/xl2tpd/etc/xl2tpd/l2tp-secrets
#	chmod 600 $(CURDIR)/debian/xl2tpd/etc/xl2tpd/l2tp-secrets


include $(RULES)
