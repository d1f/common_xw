PKG_TYPE      = DEBIAN3
PKG_VERSION   = 4.3.3
PKG_VER_PATCH = -8
PKG_SITE_PATH = pool/main/i/$(PKG_BASE)

include $(DEFS)


AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


configure : $(call tstamp_f,install,toolchain bind9)
configure : $(call bstamp_f,install,gawk)

STD_CPPFLAGS += -I$(IINC_DIR)/bind9
STD_LDFLAGS  += -L$(ILIB_DIR)/bind9

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ac_cv_file__dev_random=yes	\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--enable-log-pid	\
	--with-srv-pid-file=/var/run/dhcpd.pid \
	--with-cli-pid-file=/var/run/dhclient.pid \
	$(STD_VARS)
endef


MK_VARS = noinst_PROGRAMS=
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD =     $(CLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(INST_DIR)/sbin/dhclient $(RSBIN_DIR) $(LOG)


include $(RULES)
