PKG_TYPE      = DEBIAN1
PKG_VERSION   = 1.51
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/n/$(PKG_BASE)

PLATFORM      = build

include $(DEFS)


define INSTALL_CMD
  $(IWSH) 'echo "#!/bin/sh"                                  > $(BBIN_DIR)/naturaldocs'
  $(IWSH) 'echo "exec perl $(PKG_SRC_DIR)/NaturalDocs \$$@" >> $(BBIN_DIR)/naturaldocs'
  $(IW) chmod +x $(BBIN_DIR)/naturaldocs
endef


include $(RULES)
