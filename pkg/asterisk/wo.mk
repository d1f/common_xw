ifeq ($(shell $(VERCMP) $(PKG_VERSION) '>=' 13.0.0),true)
wo  = pjproject-bundled
endif
wo += asound
wo += bfd
wo += execinfo
wo += bluetooth
wo += cap
ifeq ($(shell $(VERCMP) $(PKG_VERSION) '>=' 13.0.0),true)
wo += codec2
endif
wo += cpg
wo += curses
wo += crypt
ifeq ($(shell $(VERCMP) $(PKG_VERSION) '>' 13.0.0),true)
wo += crypto
endif
wo += dahdi
wo += avcodec
#wo += gsm	# use 'internal' GSM
wo += ilbc	# use 'internal' iLBC
wo += gtk2
wo += gmime
wo += hoard	# Hoard Memory Allocator
wo += ical
wo += iconv
wo += iksemel
wo += imap
wo += inotify
wo += iodbc
wo += isdnnet
wo += jack
#wo += jansson	# required
wo += uriparser
wo += kqueue
wo += ldap
wo += libcurl
wo += libedit	# use 'internal' Editline
#wo += libxml2	# required for menuselect
wo += libxslt
wo += ltdl
wo += lua
wo += misdn
wo += mysqlclient
wo += nbs
#wo += ncurses	# required for menuselect
wo += neon
wo += neon29
wo += netsnmp
wo += newt
wo += ogg
wo += openr2
ifeq ($(shell $(VERCMP) $(PKG_VERSION) '>=' 13.0.0),true)
wo += opus
wo += opusfile
endif
wo += osptk
wo += oss
wo += postgres
wo += pjproject
wo += popt
wo += portaudio
wo += pri
wo += radius
ifeq ($(shell $(VERCMP) $(PKG_VERSION) '>=' 13.0.0),true)
wo += fftw3
endif
wo += resample
ifeq ($(shell $(VERCMP) $(PKG_VERSION) '>=' 13.0.0),true)
wo += sndfile
endif
wo += sdl
wo += SDL_image
wo += spandsp
wo += ss7
wo += speex
wo += speexdsp
wo += sqlite
#wo += sqlite3	# required
wo += srtp
ifeq ($(shell $(VERCMP) $(PKG_VERSION) '>' 13.0.0),true)
wo += ssl
endif
wo += suppserv
wo += tds
#wo += termcap
wo += timerfd
#wo += tinfo
wo += tonezone
ifeq ($(shell $(VERCMP) $(PKG_VERSION) '>=' 13.0.0),true)
wo += unbound
endif
wo += unixodbc
wo += vorbis
wo += vpb
wo += x11
wo += z
