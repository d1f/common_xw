PKG_TYPE      = ORIGIN
#PKG_VERSION   = 16.2.1
#PKG_VERSION   = 15.3.0
#PKG_VERSION   = 14.7.6
#PKG_VERSION   = 13.20.0
#PKG_VERSION   = 13.0.0
PKG_VERSION   = 12.8.2
PKG_SITES     = http://downloads.asterisk.org
PKG_SITE_PATH = pub/telephony/asterisk/releases
PKG_REMOTE_FILE_SUFFICES = .tar.gz
#PLATFORM      = build

include $(DEFS)


PKG_PATCH_FILES = $(wildcard $(PKG_PKG_DIR)/$(PKG_VERSION)/*.patch)


include $(PKG_PKG_DIR)/wo.mk

configure : $(call bstamp_f,install,sed gawk libxml2)

deplibs = ncurses util-linux libxml2 jansson sqlite3 zlib
ifeq ($(shell $(VERCMP) $(PKG_VERSION) '<=' 13.0.0),true)
deplibs += openssl-0.9
endif

configure : $(call tstamp_f,install,pkg-config $(deplibs))
#configure : $(call tstamp_f,install,pjproject)

ifneq ($(filter libcurl,$(wo)),)
  CONF2_CMD = $(SED) -i 's/CURL=.*/CURL=0/;' $(PKG_BLD_DIR)/build_tools/menuselect-deps $(LOG)
else
  CONF2_CMD =
endif

ASTINST = $(INST_DIR)/astinst

ifeq ($(PLATFORM),build)
     platform = build
     ASTVAR   = $(ASTINST)/varast
       prefix = $(ASTINST)
  exec_prefix = $(ROOT_DIR)
   sysconfdir = $(RETC_DIR)
     lstatdir = $(ASTVAR)
      destdir =
else
     platform = target
     ASTVAR   = $(ROOT_DIR)/varast
   build_host = --build=$(BUILD) --host=$(TARGET)
  configure : $(TOOLCHAIN_INSTALL_STAMP)
#  STD_OLEVEL = 0

       prefix = /
  exec_prefix = /
   sysconfdir = /etc
     lstatdir = /var
      destdir = $(ASTINST)
endif


# to build menuselect during target build:
std_vars  = 
std_vars += CC=gcc CXX=g++ LD=ld AS=as AR=ar NM=nm RANLIB=ranlib
std_vars += READELF=readelf STRIP=strip OBJCOPY=objcopy OBJDUMP=objdump
std_vars += BUILD_CC=gcc BUILD_CPP='gcc -E' BUILD_CXX=g++ HOSTCC=gcc HOSTCPP='gcc -E' HOSTCXX=g++
std_vars += CPPFLAGS=-I$(BINC_DIR)
std_vars += LDFLAGS='-L$(BLIB_DIR) -Wl,-rpath=$(BLIB_DIR) -ltinfo'
std_vars += PKG_CONFIG=$(BBIN_DIR)/pkg-config

define CONFIGURE_CMD
  cp -vf  $(PKG_PKG_DIR)/$(platform)/$(PKG_VERSION)/menuselect.makeopts $(PKG_BLD_DIR)/ $(LOG)

  cd $(PKG_BLD_DIR) &&			\
     ac_cv_file_bridges_bridge_softmix_include_hrirs_h=yes \
     ./configure -C $(LOG)		\
	--prefix=$(prefix)		\
	--exec-prefix=$(exec_prefix)	\
	--sysconfdir=$(sysconfdir)	\
	--localstatedir=$(lstatdir)	\
	$(build_host)			\
	--disable-xmldoc		\
	--disable-asteriskssl		\
	--with-gnu-ld			\
	$(addprefix --without-,$(wo))	\
	$(STD_VARS)

  cd $(PKG_BLD_DIR)/menuselect &&	\
	./configure -C $(std_vars) $(LOG)

  $(CONF2_CMD)
endef
#	--localstatedir=$(ROOT_DIR)/var	\
#	--libdir=$(RLIB_DIR)		\


MK_VARS +=     NOISY_BUILD=1
MK_VARS +=     DEBUG=-g0

MK_VARS +=     "ASTCFLAGS+=-I$(IINC_DIR) $(STD_CFLAGS)"
MK_VARS +=    "ASTLDFLAGS+=-L$(ILIB_DIR) -ltinfo"
# $(STD_LDFLAGS)

#MK_VARS += "BUILD_CFLAGS+=-I$(IINC_DIR)"
MK_VARS += "BUILD_LDFLAGS+=-L$(BLIB_DIR)"

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


.PHONY: menuselect
menuselect : configure
	@cp -vf  $(PKG_PKG_DIR)/$(platform)/$(PKG_VERSION)/menuselect.makeopts $(PKG_BLD_DIR)/ $(LOG)

	@cd $(PKG_BLD_DIR) && $(DOMAKE) menuselect $(std_vars) \
		CFLAGS="-D_GNU_SOURCE -I$(BINC_DIR)/libxml2 -I$(BINC_DIR)" \
		LIBXML2_LIB="-lncurses -ltinfo -lxml2"

	@cp -vf $(PKG_BLD_DIR)/menuselect.makeopts $(PKG_PKG_DIR)/$(platform)/$(PKG_VERSION)/


MK_VARS += DESTDIR=$(destdir)
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

var_lib_doc = $(notdir $(ASTVAR))/lib/asterisk/documentation/

define inst_root_cmd
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/etc/asterisk/*.conf $(RETC_DIR)/asterisk $(LOG)
  $(IW) $(INSTxFILES) $(PKG_PKG_DIR)/init/*             $(RINIT_DIR)/         $(LOG)

  $(IW) $(MKDIR_P) $(ROOT_DIR)/$(notdir $(ASTVAR))/lib/asterisk        $(LOG)
  $(IW) $(MKDIR_P) $(ROOT_DIR)/$(notdir $(ASTVAR))/lib/asterisk/keys   $(LOG)
  $(IW) $(MKDIR_P) $(ROOT_DIR)/$(notdir $(ASTVAR))/lib/asterisk/sounds $(LOG)
  $(IW) $(MKDIR_P) $(ROOT_DIR)/$(notdir $(ASTVAR))/run/asterisk        $(LOG)
  $(IW) $(MKDIR_P) $(ROOT_DIR)/$(notdir $(ASTVAR))/run/asterisk        $(LOG)

  if test -e $(PKG_BLD_DIR)/doc/core-en_US.xml; then \
     $(IW) $(INST_FILES) $(PKG_BLD_DIR)/doc/core-en_US.xml  $(ROOT_DIR)/$(var_lib_doc)/ $(LOG); \
     $(IW) $(INST_FILES) $(PKG_BLD_DIR)/doc/appdocsxml.xslt $(ROOT_DIR)/$(var_lib_doc)/ $(LOG); \
     $(IW) $(INST_FILES) $(PKG_BLD_DIR)/doc/appdocsxml.dtd  $(ROOT_DIR)/$(var_lib_doc)/ $(LOG); \
  fi
endef

ifeq ($(PLATFORM),build)

define INSTALL_ROOT_CMD
  $(inst_root_cmd)

  $(IW) $(RM_R) $(addprefix $(RSBIN_DIR)/,safe_asterisk rasterisk autosupport astversion astgenkey) $(LOG)
  $(IW) $(RM) $(RLIB_DIR)/pkgconfig/asterisk.pc $(LOG)
endef

else


isndpfx = $(ASTINST)/var/lib/asterisk/sounds/ru
rsndpfx = $(ROOT_DIR)/lib/asterisk/sounds/ru

snd_files  = tt-monkeysintro
snd_files += vm-enter-num-to-call
snd_files += vm-nobodyavail
snd_files += pbx-invalid
snd_files += vm-goodbye
snd_files += transfer

snd_dirs   = digits

define INSTALL_ROOT_CMD
  $(inst_root_cmd)

  $(IW) $(INSTxFILES) $(ASTINST)/sbin/asterisk             $(RSBIN_DIR)/                   $(LOG)
  $(IW) $(INSTxFILES) $(ASTINST)/lib/asterisk/modules/*.so  $(RLIB_DIR)/asterisk/modules/  $(LOG)
  $(IW) $(INST_FILES) $(addprefix $(isndpfx)/,$(addsuffix .*,$(snd_files))) $(rsndpfx)/    $(LOG)
  $(IW) $(INST_FILES) $(addprefix $(isndpfx)/,$(snd_dirs))                  $(rsndpfx)/    $(LOG)
endef

endif

install-root : $(call tstamp_f,install-root,$(deplibs))


include $(RULES)
