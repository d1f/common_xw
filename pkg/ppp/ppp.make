PKG_TYPE      = DEBIAN1
PKG_VERSION   = 2.4.5
PKG_VER_PATCH = -5.1+deb7u2
PKG_SITE_PATH = pool/main/p/$(PKG_BASE)

include $(DEFS)


include $(TOOLS_PKG_DIR)/quilt.mk

PKG_PATCH2_FILES = $(PKG_PKG_DIR)/*.patch


pre-configure : $(call bstamp_f,install,sed)

define PRE_CONFIGURE_CMD
  $(RELINK) $(PKG_BLD_DIR)/configure $(LOG)
  $(SED) -i -r -e 's/`uname -s`/Linux/;s/`uname -r`/$(LK_VERSION)/;s/`uname -m`/$(NARCH)/' \
	$(PKG_BLD_DIR)/configure $(LOG)
endef


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure	$(LOG)	\
	--prefix= --sysconfdir=/etc
endef

MK_VARS += INSTROOT=$(INST_DIR)

build : $(call tstamp_f,install,toolchain libpcap)
build : $(call tstamp_f,depend,linux)

STD_LDFLAGS += -Wl,--dynamic-list=$(PKG_PKG_DIR)/dynamic-list

MK_VARS += ETCDIR=/etc/ppp
MK_VARS += MFLAGS=
MK_VARS += CFLAGS="$(STD_CFLAGS) $(STD_CPPFLAGS)"
MK_VARS += LDFLAGS="$(STD_LDFLAGS) -lcrypt"
MK_VARS += CC=$(CC)


# Midge (OpenWrt) configuration:
BR2_PACKAGE_PPP=y
BR2_PACKAGE_PPP_WITH_FILTER=y
BR2_PACKAGE_PPP_MOD_PPPOA=
BR2_PACKAGE_PPP_MOD_PPPOE=y
BR2_PACKAGE_PPP_MOD_RADIUS=
BR2_PACKAGE_CHAT=y
BR2_PACKAGE_PPPDUMP=
BR2_PACKAGE_PPPSTATS=y
#FIXME: build,install pptp:
BR2_PACKAGE_PPTP=y

MK_VARS += CHAPMS=y
MK_VARS += USE_CRYPT=y NO_CRYPT_HACK=1
MK_VARS += MSLANMAN= MPPE=y
ifeq ($(BR2_PACKAGE_PPP_WITH_FILTER),y)
MK_VARS += FILTER=y PRECOMPILED_FILTER=1
endif
MK_VARS += HAVE_MULTILINK= USE_TDB=
MK_VARS += HAS_SHADOW= USE_PAM=
MK_VARS += HAVE_INET6=1
MK_VARS += CBCP= USE_SRP= MAXOCTETS= USE_BUILTIN_CRYPTO=1

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


RETC_PPP_DIR = $(RETC_DIR)/ppp
PETC_PPP_DIR = $(PKG_PKG_DIR)/etc-ppp

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/extra/pon  $(RBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/extra/poff $(RBIN_DIR) $(LOG)
  $(SED) -i -e 's|/usr||g' $(RBIN_DIR)/pon  $(LOG)
  $(SED) -i -e 's|/usr||g' $(RBIN_DIR)/poff $(LOG)
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/pppd      $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/chat      $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES)  $(ILIB_DIR)/pppd/$(PKG_VERSION)/pppol2tp.so $(RLIB_DIR)/pppd/$(PKG_VERSION) $(LOG)
  $(IW) $(INSTxFILES)  $(ILIB_DIR)/pppd/$(PKG_VERSION)/openl2tp.so $(RLIB_DIR)/pppd/$(PKG_VERSION) $(LOG)
  $(IW) $(INSTxFILES)  $(ILIB_DIR)/pppd/$(PKG_VERSION)/rp-pppoe.so $(RLIB_DIR)/pppd/$(PKG_VERSION) $(LOG)
endef

define _INSTALL_ROOT_CMD
  $(IW) $(MKDIR_P)    $(RETC_PPP_DIR)                              $(LOG)
  $(IW) $(LN_S) -f /tmp/resolv.conf $(RETC_PPP_DIR)/resolv.conf    $(LOG)
  $(IW) $(INST_FILES) $(PETC_PPP_DIR)/chap-secrets $(RETC_PPP_DIR) $(LOG)
  $(IW) chmod 0600    $(RETC_PPP_DIR)/chap-secrets                 $(LOG)
  $(IW) $(INST_FILES) $(PETC_PPP_DIR)/options      $(RETC_PPP_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PETC_PPP_DIR)/ip-down      $(RETC_PPP_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PETC_PPP_DIR)/ip-up        $(RETC_PPP_DIR) $(LOG)
  if test "$(BR2_PACKAGE_PPP_WITH_FILTER)" = "y"; then \
  $(IW) $(INST_FILES) $(PETC_PPP_DIR)/filter $(RETC_PPP_DIR) $(LOG); fi

  $(IW) $(INSTxFILES) $(ISBIN_DIR)/pppd $(RSBIN_DIR) $(LOG)

  if test "$(BR2_PACKAGE_CHAT)" = "y"; then \
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/chat $(RSBIN_DIR) $(LOG); fi

  if test "$(BR2_PACKAGE_PPPDUMP)" = "y"; then \
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/pppdump $(RSBIN_DIR) $(LOG); fi

  if test "$(BR2_PACKAGE_PPPSTATS)" = "y"; then \
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/pppstats $(RSBIN_DIR) $(LOG); fi

  if test "$(BR2_PACKAGE_PPP_MOD_PPPOE)" = "y"; then \
  $(IW) $(INSTxFILES) $(ILIB_DIR)/pppd/$(PKG_VERSION)/rp-pppoe.so \
		$(RLIB_DIR)/pppd/$(PKG_VERSION) $(LOG); fi
endef


include $(RULES)
