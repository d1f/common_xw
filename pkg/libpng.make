PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.2.50
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/libp/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


configure : $(call bstamp_f,install,zlib)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR)	&&	\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--disable-shared	\
	--with-gnu-ld		\
	$(STD_VARS)
endef


  BUILD_CMD =   $(BUILD_CMD_DEFAULT)
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)


include $(RULES)
