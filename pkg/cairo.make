PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.14.6
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/c/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk

AM_VER = 1.11
include $(TOOLS_PKG_DIR)/automake.mk

pre-configure : $(call bstamp_f,install,libtool)

LIBTOOLIZE_FLAGS = --force --copy #--verbose

define PRE_CONFIGURE_CMD
  cd $(PKG_BLD_SUB_DIR) && libtoolize    $(LIBTOOLIZE_FLAGS) $(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(ACLOCAL)       $(ACLOCAL_FLAGS) $(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(AUTOHEADER) $(AUTOHEADER_FLAGS) $(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(AUTOMAKE)     $(AUTOMAKE_FLAGS) $(LOG) || true
  cd $(PKG_BLD_SUB_DIR) && $(AUTORECONF) $(AUTORECONF_FLAGS) $(LOG)
endef


configure : $(call bstamp_f,install,libpng pixman freetype)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR)	&&		\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--disable-shared		\
	--enable-static			\
	--disable-gtk-doc		\
	--disable-gtk-doc-html		\
	--disable-gtk-doc-pdf		\
	--disable-valgrind		\
	--disable-xlib			\
	--disable-xlib-xrender		\
	--disable-xcb			\
	--disable-xlib-xcb		\
	--disable-xcb-shm		\
	--disable-qt			\
	--disable-quartz		\
	--disable-quartz-font		\
	--disable-quartz-image		\
	--disable-win32			\
	--disable-win32-font		\
	--disable-skia			\
	--disable-os2			\
	--disable-beos			\
	--disable-drm			\
	--disable-gallium		\
	--enable-png=yes		\
	--disable-gl			\
	--disable-glesv2		\
	--disable-cogl			\
	--disable-directfb		\
	--disable-vg			\
	--disable-egl			\
	--disable-glx			\
	--disable-wgl			\
	--enable-script=yes		\
	--enable-ft=yes			\
	--disable-fc			\
	--enable-ps=yes			\
	--disable-pdf			\
	--disable-svg			\
	--disable-test-surfaces		\
	--disable-tee			\
	--disable-xml			\
	--enable-pthread=auto		\
	--disable-gobject		\
	--disable-full-testing		\
	--disable-trace			\
	--enable-interpreter=yes	\
	--disable-symbol-lookup		\
	--disable-perf-utils		\
	--with-gnu-ld			\
	--without-html-dir		\
	--without-x			\
	$(STD_VARS)
endef


define BUILD_CMD
  $(RELINK) $(PKG_BLD_DIR)/boilerplate/cairo-boilerplate-constructors.c $(LOG)
  $(RELINK) $(PKG_BLD_DIR)/test/cairo-test-constructors.c               $(LOG)
  $(BUILD_CMD_DEFAULT)
endef

INSTALL_CMD = $(INSTALL_CMD_DEFAULT)


include $(RULES)
