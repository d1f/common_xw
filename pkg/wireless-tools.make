PKG_TYPE        = DEBIAN3

pkg_version     = 30

ifneq ($(filter DEBIAN%,$(PKG_TYPE)),)
  PKG_VERSION   = $(pkg_version)~pre9
  PKG_VER_PATCH = -8
  PKG_SITE_PATH = pool/main/w/$(PKG_BASE)
else
  PKG_VERSION   = $(pkg_version)
  PKG_NAME	= wireless_tools.$(PKG_VERSION).pre9
  PKG_SITES     = http://www.hpl.hp.com/
  PKG_SITE_PATH = personal/Jean_Tourrilhes/Linux/
endif

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)


MK_VARS  = INSTALL_DIR=$(ISBIN_DIR)
MK_VARS += INSTALL_LIB=$(ILIB_DIR)
MK_VARS += INSTALL_INC=$(IINC_DIR)
MK_VARS += INSTALL_MAN=$(IMAN_DIR)
MK_VARS += CC=$(CC)
MK_VARS += CFLAGS="$(STD_CFLAGS) $(STD_CPPFLAGS) -I. -I$(LK_INC_DIR)"
MK_VARS += LDCONFIG=true

DISTCLEAN_CMD_TARGET=realclean

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

sbins = iwspy iwpriv iwlist iwgetid iwevent iwconfig ifrename

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libiw.so*                $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(addprefix $(ISBIN_DIR)/,$(sbins)) $(RSBIN_DIR) $(LOG)
endef


include $(RULES)
