PKG_TYPE          = DEBIAN3
pkg_version       = 2.1.0
PKG_VERSION       = $(pkg_version)+repack
PKG_VER_PATCH     = -4
PKG_SITE_PATH     = pool/main/s/$(PKG_BASE)

include $(DEFS)


pre-configure : $(call bstamp_f,install,autotools-dev)

define PRE_CONFIGURE_CMD
  $(RM)  $(PKG_BLD_DIR)/config.guess     $(PKG_BLD_DIR)/config.sub $(LOG)
  cp $(BSHARE_DIR)/misc/config.guess $(BSHARE_DIR)/misc/config.sub $(PKG_BLD_DIR) $(LOG)
  $(RM_R) $(PKG_BLD_DIR)/autom4te.cache $(LOG)
endef


configure : $(TOOLCHAIN_INSTALL_STAMP)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=$(INST_DIR)			\
	--enable-shared --disable-static	\
	--with-gnu-ld				\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

bins = get_module get_driver get_device dlist_test

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libsysfs.so*          $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(addprefix $(IBIN_DIR)/,$(bins)) $(RBIN_DIR) $(LOG)
endef


include $(RULES)
