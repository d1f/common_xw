PKG_TYPE              = DEBIAN_NATIVE
PKG_VERSION           = 3.58
PKG_SITE_PATH         = pool/main/m/$(PKG_BASE)

include $(DEFS)


INSTALL_ROOT_CMD = $(IW) $(INST_FILES) $(PKG_SRC_DIR)/mime.types $(RETC_DIR) $(LOG)


include $(RULES)
