PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install-root : $(call tstamp_f,install-root,busybox)

define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/httpd.conf $(ROOT_DIR)/conf               $(LOG)
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/index.html $(RWEB_DIR)/nonssl             $(LOG)
  $(IW) $(LN_S) -f ../favicon.ico               $(RWEB_DIR)/nonssl/favicon.ico $(LOG)
  $(call INSTALL_PKGPKG_INIT_XFILES_F, httpd.rc)
endef


include $(RULES)
