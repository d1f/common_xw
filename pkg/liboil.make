PKG_TYPE      = DEBIAN1
PKG_VERSION   = 0.3.17
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/libo/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_STRIP_NUM = 0
PKG_PATCH2_FILES = $(PKG_PATCHED_SRC_DIR)/debian/patches/01_*.patch

PKG_PATCH3_STRIP_NUM = 1
PKG_PATCH3_FILES = $(PKG_PATCHED_SRC_DIR)/debian/patches/0[23]_*.patch


configure : $(call bstamp_f,install,sed)
configure : $(call tstamp_f,install,toolchain pkg-config glib2)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure $(LOG)	\
	--prefix=$(INST_DIR)			\
	--build=$(BUILD) --host=$(TARGET)	\
	--enable-shared				\
	--disable-static			\
	--enable-glib				\
	--disable-gtk-doc			\
	--with-gnu-ld				\
	$(STD_VARS)
endef


       MK_VARS = pkgincludedir=$(IINC_DIR)/liboil
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/liboil-0.3.so* $(RLIB_DIR) $(LOG)


include $(RULES)
