PKG_VERSION   = 1.0.0
PKG_SITES     = $(SF_NET_SITES)
PKG_SITE_PATH = $(PKG_BASE)
PKG_REMOTE_FILE_SUFFICES = .tar.gz

PLATFORM      = build

include $(DEFS)


define INSTALL_CMD
  $(IW) $(INSTxFILES) $(PKG_SRC_DIR)/mkdoxy $(BBIN_DIR) $(LOG)
endef


include $(RULES)
