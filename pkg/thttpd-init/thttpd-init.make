PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install-root : $(call tstamp_f,install-root,thttpd mime-support stunnel-init)

define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/thttpd.conf $(RWEB_DIR) $(LOG)
  $(call INSTALL_PKGPKG_INIT_XFILES_F, thttpd.rc)
  #$(IW) $(MKDIR_P) $(RWEB_DIR)/nonssl $(LOG)
  $(IW) $(LN_S) /etc/htpasswd $(RWEB_DIR)/.htpasswd $(LOG)
endef


include $(RULES)
