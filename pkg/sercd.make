# sercd is based on sredird

PKG_VERSION   = 3.0.0
PKG_SITES     = $(SF_NET_SITES)
PKG_SITE_PATH = $(PKG_BASE)
PKG_REMOTE_FILE_SUFFICES = .tar.gz

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=$(INST_DIR)			\
	--with-gnu-ld				\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ISBIN_DIR)/sercd $(RSBIN_DIR) $(LOG)


include $(RULES)
