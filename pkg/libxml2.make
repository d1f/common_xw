PKG_TYPE      = DEBIAN3
version       = 2.9.4
PKG_VERSION   = $(version)+dfsg1
PKG_VER_PATCH = -4
PKG_SITE_PATH = pool/main/libx/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES = $(PKG_PKG_DIR)/$(PKG)_*.patch


AC_VER = 2.69
AM_VER = 1.14
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


STD_LDFLAGS += -lz

configure : $(call tstamp_f,install,toolchain zlib)

ifneq ($(PLATFORM),build)
  build_host = --build=$(BUILD) --host=$(TARGET)
endif

ifneq ($(CONFIG_WITH_IPv6),y)
 configure_opts += --disable-ipv6
endif

define CONFIGURE_CMD
# $(RM) $(PKG_BLD_DIR)/doc/xmlcatalog.1	$(LOG)
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C 	$(LOG)	\
	--prefix=$(INST_DIR)	\
	$(build_host)		\
	--disable-static	\
	--with-gnu-ld		\
	--with-writer		\
	--with-html		\
	--without-iconv		\
	--without-icu		\
	--without-python	\
	--without-readline	\
	--with-threads		\
	--with-xpath		\
	--with-zlib=$(INST_DIR)	\
	--without-lzma		\
	$(configure_opts)	\
	$(STD_VARS)
endef
#	--with-minimum				\
#	--with-valid				\
#	--with-tree				\
#	--with-sax1				\
#	--with-sax2				\
#	--with-push				\
#	--with-output				\
#	--without-catalog			\
#	--without-debug				\
#	--without-iso8859x			\


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libxml2.so* $(RLIB_DIR) $(LOG)


include $(RULES)
