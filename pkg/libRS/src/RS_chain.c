#include "RS.h"

// sets new chain removing old one
void RS_chain(RS* rs, RS* chain)
{
    RS_delete(rs->chain);
    rs->chain = chain;
}
