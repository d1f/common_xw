#include "RS.h"
#include <errno.h>  /* ENOMEM */
#include <stdlib.h> /* NULL, malloc */
#include <string.h> /* memset */


// creates new empty RS;
// check for rs->rc for -ENOMEM and if it is true, return with this error.
RS* RS_create(void)
{
    static char msg[] = "Can't create error structure";
    static RS nomem =
    {
	rc  : -ENOMEM,
	file: __FILE__,
	line: __LINE__,
	func: __FUNCTION__,
	errmsg:          msg,
	chain:           NULL,
	rs_const:        1,
	rs_malloced:     0,
	errmsg_malloced: 0
    };

    RS* rs = malloc(sizeof(*rs));
    if (rs != NULL)
    {
	memset(rs, 0, sizeof(*rs));
        rs->rs_malloced = 1;
    }
    else
	rs = &nomem;

    return rs;
}

// creates new empty RS with __FILE__, __LINE__, __FUNCTION__;
// check for rs->rc for -ENOMEM and if it is true, return with this error.
RS* RS_create2(const char* file, size_t line, const char* func)
{
    RS* rs = RS_create();

    if (rs->rc != -ENOMEM)
    {
	rs->file = file;
	rs->line = line;
        rs->func = func;
    }

    return rs;
}
