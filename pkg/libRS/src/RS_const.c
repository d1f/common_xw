#include "RS.h"

/*
    file:line func(): Can't open file name %s: No such file or directory;
    \--> file:line func(): Can't create error message: invalid printf format;
         \--> file:line func(): Can't create error message: invalid printf format;
*/

const char RS_spc[] =  "     ";
const char RS_arr[] = "\\--> ";
