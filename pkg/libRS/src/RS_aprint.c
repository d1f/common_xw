#ifndef  _GNU_SOURCE
# define _GNU_SOURCE
#endif

#include "RS.h"
#include <stdio.h>
#include <string.h>


// RS_print() to malloc'ed line
// returns NULL if ENOMEM
char* RS_aprint(const RS_cursor *cursor)
{
    char *buf = NULL;
    size_t len;
    const RS *rs = cursor->current;
    FILE *stream = open_memstream (&buf, &len);

    if (stream==NULL) return NULL;

    for (ssize_t i=0; i < cursor->level-1; ++i)
	fprintf(stream, "%s", RS_spc);
    if (cursor->level > 0)
	fprintf(stream, "%s", RS_arr);

    if (cursor->file_line_func)
	fprintf(stream, "%s:%u %s(): ", rs->file, rs->line, rs->func);

    fprintf(stream, "%s", rs->errmsg);

    if (rs->rc < 0)
	fprintf(stream, ": %s", strerror(-(rs->rc)) );

    fflush(stream);
    if ( ferror(stream) )
    {
	free(buf);
        buf=NULL;
    }
    fclose(stream);

    return buf;
}
