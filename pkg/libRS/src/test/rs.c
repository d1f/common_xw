#include "RS.h"
#include <errno.h>

int main(void)
{
    RS_cursor cursor;

    RS_CREATE(rs0);
    RS_CREATE(rs1);
    RS_CREATE(rs2);
    RS_CREATE(rs3);
    RS_CREATE(rs4);

    RS_MSG(rs0, 1, "Just Test 0"); RS_chain(rs0, rs1);
    RS_MSG(rs1, 1, "Just Test 1"); RS_chain(rs1, rs2);
    RS_MSG(rs2, 1, "Just Test 2"); RS_chain(rs2, rs3);
    RS_MSG(rs3, 1, "Just Test 3"); RS_chain(rs3, rs4);
    RS_MSG(rs4, -ENOMEM, "Just Test 4");

    RS_print(rs0, stdout, 1);

    printf("\n");

    RS_first(&cursor, rs0, 1);
    do
    {
	char *line = RS_aprint(&cursor);
	printf("%s\n", line);
        free(line);
    } while ( RS_next(&cursor) );

    return 0;
}
