#include "RS.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void RS_print(const RS* rs, FILE* f, int file_line_func)
{
    for (size_t pfx_count=0; rs!=NULL; rs=rs->chain, ++pfx_count)
    {
	for (size_t i=0; i<pfx_count-1; ++i)
	    fputs(RS_spc, f);
        if (pfx_count > 0)
	    fputs(RS_arr, f);

        if (file_line_func)
	    fprintf(f, "%s:%u %s(): ", rs->file, rs->line, rs->func);

	fputs(rs->errmsg, f);

	if (rs->rc < 0)
	    fprintf( f, ": %s\n", strerror(-(rs->rc)) );
	else
	    fputs("\n", f);
    }
}
