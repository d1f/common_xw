#include "RS.h"
#include <stdlib.h> /* free, NULL */


// free RS recursively with respect to static one.
// rs become invalid pointer.
void RS_delete(RS* rs)
{
    if (rs == NULL)
        return;

    if (rs->rs_const)
        return;

    if (rs->chain != NULL)
    {
	RS_delete(rs->chain);
	rs->chain = NULL;
    }

    // last in chain

    if ( rs->rs_malloced )
    {
        //+ for safety
	rs->rc   = 0;
	rs->file = NULL;
	rs->line = 0;
	rs->func = NULL;
        //- for safety

	if ( rs->errmsg_malloced )
	    free(rs->errmsg);

	rs->errmsg = NULL; // for safety

        free(rs);
    }
    else // static
    {
	if ( rs->errmsg_malloced )
	{
	    free(rs->errmsg);
	    rs->errmsg=NULL;
	}
    }
}
