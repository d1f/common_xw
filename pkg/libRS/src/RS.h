#ifndef  RS_H
# define RS_H

# include <stdlib.h> /* size_t */
# include <stdio.h>  /* FILE */
# include <errno.h>  /* for user' convenience */


struct RS
{
    int rc;		// 0 - success, <0 - -errno, >0 - some other error
    const char* file;	// __FILE__, static string;
    size_t      line;	// __LINE__, integer constant
    const char* func;	// __FUNCTION__, static string;
    char* errmsg;	// malloc'ed at most; if errno, use in error(3)
    struct RS* chain;	// nested scructure
    char rs_const;
    char rs_malloced;
    char errmsg_malloced;
};
typedef struct RS RS;

#define RS RS


// creates new empty RS;
// check rs->rc and if it is true, return with this error.
RS* RS_create(void);

// creates new empty RS with __FILE__, __LINE__, __FUNCTION__;
// check rs->rc and if it is true, return with this error.
RS* RS_create2(const char* file, size_t line, const char* func);

# define RS_CREATE(name) \
    RS* name = RS_create2(__FILE__, __LINE__, __FUNCTION__)

// builds errmsg field;
// uses vasprintf(3);
// returns NULL on success
// or pointer to chained rs if memory exhausted or other errors
RS* RS_msg(RS* rs, const char* fmt, ...)
    __attribute__(( __format__ (__printf__, 2, 3) ));

# define RS_MSG(rs, _rc, fmt, args...) \
    ({ rs->rc=_rc; rs->line=__LINE__; RS_msg(rs, fmt, ##args); })

# define RS_MSG_ERRNO(rs, fmt, args...) \
    RS_MSG(rs, -errno, fmt, ##args)

// free RS recursively with respect to static one.
// rs become invalid pointer.
void RS_delete(RS* rs);

# define RS_DELETE(name) \
    do { RS_delete(name); name=NULL; } while(0)


// sets new chain removing old one
void RS_chain(RS* rs, RS* chain);


void RS_print(const RS* rs, FILE* f, int file_line_func);



struct RS_cursor
{
    ssize_t level;
    const RS *current;
    int file_line_func; // set this one for RS_aprint()
};
typedef struct RS_cursor RS_cursor;


// init cursor structure with first RS in a chain
void  RS_first(RS_cursor *cursor, const RS *first, int file_line_func);

// advance to next RS in a chain
// returns TRUE on advance or FALSE after last item
int   RS_next (RS_cursor *cursor);


// RS_print() to malloc'ed line
// returns NULL if ENOMEM
char* RS_aprint(const RS_cursor *cursor);



// pretty printing constants
extern const char RS_spc[];
extern const char RS_arr[];


#endif /* RS_H */
