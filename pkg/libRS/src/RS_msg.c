#ifndef  _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include <stdio.h>  /* vasprintf */

#include "RS.h"
#include <stdlib.h> /* NULL */
#include <stdarg.h> /* va_start va_end */



// builds errmsg field;
// uses vasprintf(3);
// returns NULL on success
// or pointer to chained rs if memory exhausted or other errors
RS* RS_msg(RS* rs, const char* fmt, ...)
{
    static char cant_create       [] = "Can't create error message";
    static RS nomem  =
    {
	rc  : -ENOMEM,
	file: __FILE__,
	line: __LINE__,
	func: __FUNCTION__,
	errmsg:          cant_create,
	chain:           NULL,
	rs_const:        1,
	rs_malloced:     0,
	errmsg_malloced: 0
    };

    static char cant_create_invfmt[] = "Can't create error message: invalid printf format";
    static RS invfmt =
    {
	rc  : 1,
	file: __FILE__,
	line: __LINE__,
	func: __FUNCTION__,
	errmsg:          cant_create_invfmt,
	chain:           NULL,
	rs_const:        1,
	rs_malloced:     0,
	errmsg_malloced: 0
    };

    static char cant_create_const[] = "Can't create error message: non-volatile RS struct";
    static RS rs_const =
    {
	rc  : 1,
	file: __FILE__,
	line: __LINE__,
	func: __FUNCTION__,
	errmsg:          cant_create_const,
	chain:           NULL,
	rs_const:        1,
	rs_malloced:     0,
	errmsg_malloced: 0
    };

    va_list ap;
    char *buf;
    int rc;

    if (rs->rs_const)
        return &rs_const;


    //+ remove old errmsg
    if (rs->errmsg_malloced)
	free(rs->errmsg);
    rs->errmsg=NULL;
    rs->errmsg_malloced = 0;
    //- remove old errmsg

    errno=0;
    va_start(ap, fmt);
    rc = vasprintf( &buf, fmt, ap );
    va_end  (ap);

    if (rc < 0)
    {
	union { const char* cs; char* s; } ufmt;
	ufmt.cs=fmt;

	if (errno == ENOMEM)
	{
	    rs->errmsg = ufmt.s;  // try to keep message
	    rs->errmsg_malloced = 0;
            RS_chain(rs, &nomem);
	}
	else if (errno == EINVAL)
	{
	    rs->errmsg = ufmt.s;  // try to keep message
            rs->errmsg_malloced = 0;
            RS_chain(rs, &invfmt);
	}
	else
	{
            rs->rc     = -errno;
	    rs->errmsg = cant_create;
            rs->errmsg_malloced = 0;
            RS_chain(rs, NULL);
	}

	return rs;
    }

    rs->errmsg = buf;
    rs->errmsg_malloced = 1;

    return NULL;
}
