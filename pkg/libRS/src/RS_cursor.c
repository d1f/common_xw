#include "RS.h"


// init cursor structure with first RS in a chain
void  RS_first(RS_cursor *cursor, const RS *first, int file_line_func)
{
    cursor->level          = 0;
    cursor->current        = first;
    cursor->file_line_func = file_line_func;
}

// advance to next RS in a chain
// returns TRUE on advance or FALSE after last item
int   RS_next (RS_cursor *cursor)
{
    if ( cursor->current->chain != NULL )
    {
	cursor->current = cursor->current->chain;
	cursor->level++;
        return 1;
    }
    else
        return 0;
}
