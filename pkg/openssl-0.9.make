PKG_TYPE          = DEBIAN3

PKG_BASE          = openssl
PKG_VERSION       = 0.9.8o
PKG_VER_PATCH     = -4squeeze23
PKG_DIR           = $(PKG)
PKG_SITE_PATH     = pool/main/o/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES = $(wildcard $(PKG_PKG_DIR)/$(PKG_BASE)_$(PKG_VERSION)*.patch)


define PRE_CONFIGURE_CMD
  $(RELINK)	$(PKG_BLD_DIR)/crypto/objects/obj_mac.num	\
		$(PKG_BLD_DIR)/crypto/objects/obj_mac.h		\
		$(PKG_BLD_DIR)/crypto/objects/obj_dat.h		\
		$(PKG_BLD_DIR)/crypto/objects/obj_xref.h	\
		$(PKG_BLD_DIR)/crypto/bn/bn_prime.h		\
		$(PKG_BLD_DIR)/apps/progs.h			\
		$(PKG_BLD_DIR)/test/evptests.txt		$(LOG)
endef


configure : $(call tstamp_f,install,toolchain zlib)

CONFARGS  = --prefix=/ --openssldir=/
CONFARGS += --install_prefix=$(INST_DIR)
CONFARGS += no-idea no-mdc2 no-rc5
CONFARGS += enable-tlsext no-ssl2
CONFARGS += no-hw no-montasm no-asm no-krb5
CONFARGS += zlib-dynamic no-static-engine
# rijndael aes des ec ecdsa ecdh md5 rsa sha dsa dh ssl2 ssl3 tls1
#
# for i in aes bf camellia cast des dh dsa ec hmac idea md2 md5 mdc2 rc2 rc4 rc5 ripemd rsa seed sha
#	test -d crypto/$i
#
# md2 md4 md5 sha hmac ripemd des aes rc2 rc4 bf cast bn ec rsa dsa ecdsa dh
# ecdh dso engine buffer bio stack lhash rand err evp asn1 pem x509 x509v3
# conf txt_db pkcs7 pkcs12 comp ocsp ui krb5 store pqueue


ifneq ($(CONFIG_WITH_IPv6),y)
  STD_CPPFLAGS += -DIPV6_PMTUDISC_DO=2
  STD_CPPFLAGS += -DIPV6_MTU_DISCOVER=23
  STD_CPPFLAGS += -DIPV6_MTU=24
endif

CONFARGS += "$(STD_CFLAGS)" "$(STD_CPPFLAGS)" "$(STD_LDFLAGS)"

ifeq ($(NARCH),arm)
  ifeq ($(ENDIANESS),BIG)
    target=debian-armeb
  else
    target=debian-armel
  endif
else ifeq ($(ARCH),ppc)
  target=linux-ppc
else ifeq ($(ARCH),powerpc)
  target=linux-ppc
else ifeq ($(ARCH),x86_64)
  target=linux-x86_64
else ifeq ($(NARCH),i386)
  ifeq ($(CPU),i486)
    target = debian-i386-i486
  else ifeq ($(CPU),i586)
    target = debian-i386-i586
  else ifeq ($(CPU),i686)
    target = debian-i386-i686/cmov
  else
    target = debian-i386
  endif
else ifeq ($(ARCH),mipsel)
    target = debian-mipsel
else # ($(NARCH),i386)
  $(error Add ARCH support here!)
endif
#target=linux-generic32

ENV_VARS  = CC=$(CC)
ENV_VARS += RANLIB=$(RANLIB) AR=$(AR) #ARFLAGS=

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && $(ENV_VARS) ./Configure shared $(CONFARGS) $(target) $(LOG)
endef


MK_VARS += CC=$(CC)
MK_VARS += AR="$(AR) r"
MK_VARS += ARD="$(AR) d"
MK_VARS += RANLIB=$(RANLIB)
MK_VARS += MAKEDEPPROG=$(CC)
# FIPSLIBDIR=/usr/local/ssl/fips-1.0/lib/


MK_JOBS=1
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD_TARGET = install_sw
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(IBIN_DIR)/openssl	$(RBIN_DIR)             $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/ssl/engines/* $(RLIB_DIR)/ssl/engines $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libssl.so*    $(RLIB_DIR)             $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libcrypto.so* $(RLIB_DIR)             $(LOG)
endef


include $(RULES)
