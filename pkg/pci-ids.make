file           = pci.ids
PKG_SITES      = http://pciids.sf.net
PKG_FILE_BASE  = $(file)
PKG_REMOTE_FILE_SUFFICES = .bz2 .gz
PKG_LOCAL__FILE_SUFFICES = .gz .bz2
PKG_UPDATEABLE = defined

include $(DEFS)


EXTRACT_FILES = $(file)

define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(PKG_SRC_DIR)/$(file) $(RSHARE_DIR)/misc $(LOG)
endef


include $(RULES)
