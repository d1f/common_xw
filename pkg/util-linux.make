# for sfdisk and libuuid only
PKG_TYPE      = DEBIAN1
PKG_VERSION   = 2.20.1
PKG_VER_PATCH = -5.3
PKG_SITE_PATH = pool/main/u/$(PKG_BASE)

include $(DEFS)


AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk

AM_VER = 1.11
include $(TOOLS_PKG_DIR)/automake.mk

pre-configure : $(call bstamp_f,install,sed pkg-config gettext libtool)

define PRE_CONFIGURE_CMD
  $(SED) -i -e 's/ENABLE_GTK_DOC/BUILD_ELVTUNE/g'      $(PKG_BLD_SUB_DIR)/libblkid/Makefile.am      $(LOG)
  $(SED) -i -e 's/ENABLE_GTK_DOC/BUILD_ELVTUNE/g'      $(PKG_BLD_SUB_DIR)/libblkid/docs/Makefile.am $(LOG)
  $(SED) -i -e 's/ENABLE_GTK_DOC/BUILD_ELVTUNE/g'      $(PKG_BLD_SUB_DIR)/libmount/Makefile.am      $(LOG)
  $(SED) -i -e 's/ENABLE_GTK_DOC/BUILD_ELVTUNE/g'      $(PKG_BLD_SUB_DIR)/libmount/docs/Makefile.am $(LOG)
  $(SED) -i -e 's/ENABLE_GTK_DOC/BUILD_ELVTUNE/g'      $(PKG_BLD_SUB_DIR)/config/gtk-doc.make       $(LOG)
  $(SED) -i -e 's/GTK_DOC_USE_LIBTOOL/BUILD_ELVTUNE/g' $(PKG_BLD_SUB_DIR)/config/gtk-doc.make       $(LOG)

  $(SED) -i -e 's/GTK_DOC_CHECK\(.*\)//g'              $(PKG_BLD_SUB_DIR)/configure.ac              $(LOG)

  $(SED) -i -e 's/AX_CHECK_TLS//g'                     $(PKG_BLD_SUB_DIR)/configure.ac              $(LOG)

  $(SED) -i -e 's/login-utils/include/g'               $(PKG_BLD_SUB_DIR)/Makefile.am               $(LOG)
  $(SED) -i -e 's/misc-utils/include/g'                $(PKG_BLD_SUB_DIR)/Makefile.am               $(LOG)
  $(SED) -i -e 's/sys-utils/include/g'                 $(PKG_BLD_SUB_DIR)/Makefile.am               $(LOG)
  $(SED) -i -e 's/term-utils/include/g'                $(PKG_BLD_SUB_DIR)/Makefile.am               $(LOG)
  $(SED) -i -e 's/text-utils/include/g'                $(PKG_BLD_SUB_DIR)/Makefile.am               $(LOG)
  $(SED) -i -e 's/tests/include/g'                     $(PKG_BLD_SUB_DIR)/Makefile.am               $(LOG)

  cd $(PKG_BLD_SUB_DIR) && libtoolize    $(LIBTOOLIZE_FLAGS) $(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(ACLOCAL)       $(ACLOCAL_FLAGS) $(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(AUTOCONF)     $(AUTOCONF_FLAGS) $(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(AUTOHEADER) $(AUTOHEADER_FLAGS) $(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(AUTOMAKE)     $(AUTOMAKE_FLAGS) $(LOG)
endef


configure : $(call tstamp_f,install,pkg-config)

disable  = static
 enable  = largefile libuuid
disable += mount fsck partx uuidd libblkid libmount libmount-mount
disable += mountpoint nls arch ddate agetty cramfs
disable += switch_root pivot_root fallocate unshare elvtune kill
disable += last line mesg raw rename reset login-utils schedutils
disable += wall write pg-bell
disable += makeinstall-chown makeinstall-setuid

with     = gnu-ld pic
without  = libiconv-prefix libintl-prefix
without += ncurses slang utempter pam selinux audit

ifneq ($(PLATFORM),build)
  build_host = --build=$(BUILD) --host=$(TARGET)
  configure : $(call tstamp_f,install,toolchain)
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
    scanf_cv_type_modifier=no	\
    ./configure -C	$(LOG)	\
	--prefix=$(INST_DIR)	\
	$(build_host)		\
	$(addprefix --disable-,$(disable))	\
	$(addprefix --without-,$(without))	\
	$(addprefix --enable-,$(enable))	\
	$(addprefix --with-,$(with))		\
	$(STD_VARS)
endef


MK_VARS += test_blkdev_SOURCES=
MK_VARS += test_ismounted_SOURCES=
MK_VARS += test_wholedisk_SOURCES=
MK_VARS += test_mangle_SOURCES=
MK_VARS += test_at_SOURCES=
MK_VARS += test_at_CFLAGS=
MK_VARS += test_strutils_SOURCES=
MK_VARS += test_procutils_SOURCES=
MK_VARS += test_cpuset_SOURCES=
MK_VARS += test_sysfs_SOURCES=
MK_VARS += test_sysfs_CFLAGS=
MK_VARS += test_loopdev_SOURCES= 
MK_VARS += test_loopdev_CFLAGS=
MK_VARS += test_tt_SOURCES=
MK_VARS += test_canonicalize_SOURCES=
MK_VARS += test_canonicalize_CFLAGS=
MK_VARS += noinst_PROGRAMS=

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(LN_S) libuuid.so.1 $(ILIB_DIR)/libuuid.so $(LOG)
endef

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES)  $(ILIB_DIR)/libuuid.so* $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/sfdisk     $(RSBIN_DIR) $(LOG)
  $(IW) $(LN_S) libuuid.so.1 $(RLIB_DIR)/libuuid.so        $(LOG)
endef


include $(RULES)
