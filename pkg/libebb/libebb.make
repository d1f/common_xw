PKG_ORIG_DIR     = $(PRJ_SRC_DIR)/libebb
PKG_FETCH_METHOD = DONE
PKG_LN_CNT_FILE  = .gitignore

include $(DEFS)


PKG_PATCH_FILES += $(PKG_PKG_DIR)/$(PKG)*.patch


build : $(call tstamp_f,install,toolchain libev)
build : $(call bstamp_f,install,ragel)

STD_CFLAGS += $(STD_CPPFLAGS) -fPIC
MK_VARS += PREFIX=$(INST_DIR)
MK_VARS += EVINC=$(IINC_DIR) EVLIB=$(RLIB_DIR)
MK_VARS += LIBS=-L$(RLIB_DIR) -lev
MK_VARS += $(STD_VARS)

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libebb.so.0.1 $(RLIB_DIR)           $(LOG)
  $(IW) $(LN_S) -fv               libebb.so.0.1 $(RLIB_DIR)/libebb.so $(LOG)
endef


include $(RULES)
