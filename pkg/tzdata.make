PKG_TYPE          = DEBIAN3
PKG_VERSION       = 2019a
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/t/$(PKG_BASE)

include $(DEFS)


EXTRACT_STRIP_NUM = 0


build : $(call bstamp_f,install,fdupes sed)

TZGEN = $(PKG_BLD_DIR)/tzgen


TIMEZONES  =
TIMEZONES += africa
TIMEZONES += antarctica
TIMEZONES += asia
TIMEZONES += australasia
TIMEZONES += europe
TIMEZONES += northamerica
TIMEZONES += southamerica
TIMEZONES += etcetera
#TIMEZONES += factory
#TIMEZONES += backward
#TIMEZONES += systemv
TIMEZONES += pacificnew

define BUILD_CMD
  #echo "$(I1)Build the 'default' version ..."
  #cd $(PKG_BLD_DIR) && for zone in $(TIMEZONES); do \
  #	/usr/sbin/zic -d $(TZGEN) -L /dev/null -y yearistype.sh $${zone} $(LOG); \
  #done

  echo "$(I1)Build the 'posix'  versions ..."
  cd $(PKG_BLD_DIR) && for zone in $(TIMEZONES); do \
  	/usr/sbin/zic -d $(TZGEN)/posix -L /dev/null -y yearistype.sh $${zone} $(LOG); \
  done; $(LOG)

  #echo "$(I1)Build the 'right'  versions ..."
  #cd $(PKG_BLD_DIR) && for zone in $(TIMEZONES); do \
  #	/usr/sbin/zic -d $(TZGEN)/right -L leapseconds -y yearistype.sh $${zone} $(LOG); \
  #done; $(LOG)

  echo "$(I1)Generate a posixrules file ..."
  cd $(PKG_BLD_DIR) && /usr/sbin/zic -d $(TZGEN)/posix -p America/New_York $(LOG)

  echo "$(I1)Replace hardlinks by symlinks ..."
  #rdfind -outputname /dev/null -makesymlinks true -removeidentinode false $(TZGEN)
  #symlinks -r -s -c $(TZGEN)
  cd $(TZGEN) ; \
  fdupes -1 -H -q -R . | while read line ; do \
    set -- $${line} ; \
    tgt="$${1##./}" ; \
    shift ; \
    while [ "$$#" != 0 ] ; do \
      link="$${1##./}" ; \
      reltgt="$$(echo $$link | $(SED) -e 's,[^/]\+$$,,g' -e 's,[^/]\+,..,g')$${tgt}" ; \
      $(LN_S) -f $${reltgt} $${link} $(LOG) ; \
      shift ; \
    done ; \
  done; $(LOG)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


# TZDIR=/share/zoneinfo/posix TZ=Europe/Paris
define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(PKG_BLD_DIR)/tzgen/* $(RSHARE_DIR)/zoneinfo $(LOG)
  $(IW) $(MKDIR_P)                                     $(RETC_DIR)  $(LOG)
  $(IW) $(LN_S) /share/zoneinfo/posix/Asia/Novosibirsk $(RETC_DIR)/localtime $(LOG)
endef


include $(RULES)
