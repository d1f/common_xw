PKG_TYPE          = DEBIAN3
PKG_VERSION       = 3.17
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/i/$(PKG_BASE)

include $(DEFS)


build : $(call tstamp_f,install,toolchain pkg-config libnl-1.x)

MK_VARS  = PREFIX=$(INST_DIR)
MK_VARS += MANDIR=$(INST_DIR)/share/man
MK_VARS += PKG_CONFIG=$(PKG_CONFIG)
MK_VARS += CC=$(CC) CFLAGS="$(STD_CFLAGS) $(STD_CPPFLAGS)"
#MK_VARS += V=1

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

ifeq ($(filter $(NOT_BUILD_TARGETS),$(MAKECMDGOALS)),)
  INSTALL_ROOT_CMD = $(error INSTALL_ROOT_CMD not finished)
endif


include $(RULES)
