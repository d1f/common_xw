PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install : $(call tstamp_f,install,busybox base-init)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_PKG_DIR)/init/* $(RINIT_DIR) $(LOG)
endef


include $(RULES)
