PKG_TYPE          = DEBIAN3
PKG_VERSION       = 3.0.27
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/d/$(PKG_BASE)

include $(DEFS)


STD_CPPFLAGS += -D_GNU_SOURCE -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64

MK_VARS  = $(STD_VARS)

MK_VARS += PREFIX=$(INST_DIR)

BUILD_CMD = $(BUILD_CMD_DEFAULT)


INSTALL_CMD_TARGET = install-bin
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES)    $(ISBIN_DIR)/mkfs.fat $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES)    $(ISBIN_DIR)/fsck.fat $(RSBIN_DIR) $(LOG)
  $(IW) $(LN_S) mkfs.fat $(RSBIN_DIR)/mkfs.vfat              $(LOG)
  $(IW) $(LN_S) fsck.fat $(RSBIN_DIR)/fsck.vfat              $(LOG)
endef


include $(RULES)
