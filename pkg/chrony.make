PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.30
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/c/$(PKG_BASE)

include $(DEFS)


#FIXME: texinfo, bison, libedit-dev, libnss3-dev, libtomcrypt-dev
configure : $(TOOLCHAIN_INSTALL_STAMP)

ifneq ($(CONFIG_WITH_IPv6),y)
 configure_opts += --disable-ipv6
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR)	&&	\
     ./configure	$(LOG)	\
	--prefix=''		\
	--disable-readline	\
	--without-readline	\
	--without-editline	\
	--without-nss		\
	--without-tomcrypt	\
	--disable-pps		\
	--disable-linuxcaps	\
	$(configure_opts)	\
	--host-system=Linux	\
	--host-release=$(LK_VERSION) \
	--host-machine=$(BFD_ARCH)   \
	$(STD_VARS) # FIXME: does not works
endef


      MK_VARS = $(STD_VARS) DESTDIR=$(INST_DIR)
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(INST_DIR)/usr/local/sbin/chronyd $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(INST_DIR)/usr/local/bin/chronyc   $(RBIN_DIR) $(LOG)
endef


include $(RULES)
