#define _GNU_SOURCE

#include <stdio.h>  // printf
#include <stdlib.h> // EXIT_*
#include <string.h> // basename

int main(int ac, char *av[])
{
    if (ac != 3)
    {
	printf("\nUsage: %s 'sep-string' 'string'\n\n", basename(av[0]));
	return EXIT_FAILURE;
    }

    char *sep_string = av[1];
    char *src_string = av[2];

    char *curr = src_string;
    char *token = curr;

    while ((token = strsep(&curr, sep_string)) != NULL)
    {
	printf("token: /%s/\n", token);
    }

    return EXIT_SUCCESS;
}
