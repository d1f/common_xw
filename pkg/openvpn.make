PKG_TYPE      = DEBIAN3
PKG_VERSION   = 2.3.4
PKG_VER_PATCH = -5
PKG_SITE_PATH = pool/main/o/$(PKG_BASE)

include $(DEFS)


define PRE_CONFIGURE_CMD
  $(SED) -i -e 's/\.gitattributes//g' $(PKG_BLD_DIR)/Makefile.in $(LOG)
endef

configure : $(call tstamp_f,install,toolchain openssl-0.9) #lzo
configure : $(call bstamp_f,install,sed gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure $(LOG)			\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--prefix=$(INST_DIR)		\
	--disable-lzo			\
	--disable-plugins		\
	--disable-eurephia		\
	--disable-management		\
	--disable-pkcs11		\
	--disable-socks			\
	--enable-http-proxy		\
	--disable-debug			\
	--enable-password-save		\
	--disable-pf			\
	--disable-plugin-auth-pam	\
	--disable-plugin-down-root	\
	--enable-shared			\
	--disable-static		\
	--with-gnu-ld			\
	$(STD_VARS)
endef

define _cc_
	--disable-multi			\
	--disable-server		\
	--disable-crypto		\
	--disable-ssl			\
	--enable-lzo-stub	don't compile LZO compression support but still
				allow limited interoperability with LZO-enabled
				peers [default=no]
	--enable-iproute2

	--localstatedir=/var			\
	--config-cache				\
	--enable-pthread			\
	--with-ssl-headers=$(IINC_DIR)		\
	--with-ssl-lib=$(RLIB_DIR)		\
	--with-lzo-headers=$(IINC_DIR)		\
	--with-lzo-lib=$(ILIB_DIR)		\
	--with-ifconfig-path=/sbin/ifconfig	\
	 --with-iproute-path=/sbin/ip		\
	   --with-route-path=/sbin/route	\

endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


install      : $(call tstamp_f,uninstall,openvpn-2.1.1)
install-root : $(call tstamp_f,uninstall-root,openvpn-2.1.1)
install-root : $(call tstamp_f,install-root,openssl-0.9) #lzo

INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/openvpn  $(RSBIN_DIR)         $(LOG)
 #$(IW) $(INSTxFILES) $(PINIT_DIR)/openvpn  $(RINIT_DIR)         $(LOG)
 #$(IW) $(INST_FILES)  $(PETC_DIR)/openvpn/* $(RETC_DIR)/openvpn $(LOG)
endef


include $(RULES)
