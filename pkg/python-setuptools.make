PKG_TYPE      = DEBIAN1

# build host has easy_install - python-setuptools
ifneq ($(strip $(shell easy_install -q --version 2>/dev/null)),)

PKG_HAS_NO_SOURCES = defined

else	# build host has no easy_install - python-setuptools

PKG_VERSION       = 0.6c8
PKG_VER_PATCH     = -4
PKG_SITE_PATH     = pool/main/p/$(PKG_BASE)

PLATFORM          = build

include $(DEFS)


define BUILD_CMD
  cd $(PKG_BLD_DIR) && python setup.py build $(LOG)
endef

pyver := $(shell python -c 'import sys; print sys.version[:3]')

define INSTALL_CMD
  $(RELINK) $(PKG_BLD_DIR)/setuptools.egg-info/* $(LOG)
  $(IWSH) "cd $(PKG_BLD_DIR) && python setup.py install --no-compile \
	--prefix $(INST_DIR) --root /" $(LOG)

  $(IW) $(LN_S) setuptools-$(PKG_VERSION)-py$(pyver).egg-info \
  	$(BLIB_DIR)/python$(pyver)/site-packages/setuptools.egg-info $(LOG)
endef

    CLEAN_CMD = $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(CLEAN_CMD_DEFAULT)

endif # build host has no easy_install - python-setuptools


include $(RULES)
