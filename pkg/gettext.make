PKG_TYPE          = DEBIAN3
PKG_VERSION       = 0.18.3.1
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/g/$(PKG_BASE)
SEPARATE_SRC_BLD_DIRS=defined

include $(DEFS)


configure : $(call bstamp_f,install,sed true-false)

common_conf_opts  = --disable-java
common_conf_opts += --disable-native-java
common_conf_opts += --disable-csharp
common_conf_opts += --disable-nls
common_conf_opts += --disable-c++
common_conf_opts += --disable-libasprintf
common_conf_opts += --disable-openmp
common_conf_opts += --disable-acl
common_conf_opts += --disable-curses

common_conf_opts += --with-gnu-ld
common_conf_opts += --without-libpth-prefix
common_conf_opts += --without-libiconv-prefix
common_conf_opts += --with-included-gettext
common_conf_opts += --without-libintl-prefix
#common_conf_opts += --with-included-glib
common_conf_opts += --without-libglib-2.0-prefix
common_conf_opts += --without-libcroco-0.6-prefix
common_conf_opts += --without-libunistring-prefix
common_conf_opts += --without-libxml2-prefix
common_conf_opts += --without-libncurses-prefix
common_conf_opts += --without-libtermcap-prefix
common_conf_opts += --without-libxcurses-prefix
common_conf_opts += --without-libcurses-prefix
common_conf_opts += --without-included-regex
common_conf_opts += --without-libexpat-prefix
common_conf_opts += --without-emacs
common_conf_opts += --without-lispdir
common_conf_opts += --without-git
common_conf_opts += --without-cvs
common_conf_opts += --without-bzip2
common_conf_opts += --without-xz

common_conf_vars  = GCJ=$(BBIN_DIR)/false
common_conf_vars += EMACS=no
common_conf_vars += EMACSLOADPATH=.

ifeq ($(PLATFORM),build)

define CONFIGURE_CMD
  $(MLN) $(BBIN_DIR) true jar gij java javac $(LOG)
  cd $(PKG_BLD_DIR) 	&&		\
     $(PKG_SRC_DIR)/configure $(LOG)	\
	--prefix=$(INST_DIR)		\
	--disable-shared		\
	--enable-threads=posix		\
	$(common_conf_opts)		\
	$(common_conf_vars)		\
	$(STD_VARS)
endef

else # cross

configure : $(TOOLCHAIN_INSTALL_STAMP)

define CONFIGURE_CMD
  $(MLN) $(BBIN_DIR) true jar gij java javac $(LOG)
  cd $(PKG_BLD_DIR)	&&		\
     $(PKG_SRC_DIR)/configure $(LOG)	\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--prefix=$(INST_DIR)		\
	--disable-static		\
	--enable-threads=posix		\
	$(common_conf_opts)		\
	$(common_conf_vars)		\
	$(STD_VARS)
endef
#	--enable-relocatable		\

endif


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libgettextsrc*.so $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libgettextpo.so*  $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libgettextlib*.so $(RLIB_DIR) $(LOG)
endef


include $(RULES)
