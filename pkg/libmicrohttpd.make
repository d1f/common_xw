PKG_TYPE          = DEBIAN3
PKG_VERSION       = 0.9.37+dfsg
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/libm/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ./configure -C $(LOG)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--prefix=$(INST_DIR)	\
	--disable-static	\
	--enable-shared		\
	--disable-curl		\
	--enable-largefile	\
	--enable-messages	\
	--enable-postprocessor	\
	--disable-https		\
	--enable-dauth		\
	--disable-coverage	\
	--with-gnu-ld		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libmicrohttpd.so* $(RLIB_DIR) $(LOG)


include $(RULES)
