include $(TOP_DIR)/common_xw/pkg/apr.mk

PKG_TYPE        = $(aprutil_pkg_type)

ifeq ($(PKG_TYPE),DEBIAN1)
  PKG_VERSION   = $(aprutil_version)
  PKG_VER_PATCH = $(aprutil_ver_patch)
  PKG_SITE_PATH = pool/main/a/$(PKG_BASE)
else ifeq ($(PKG_TYPE),DEBIAN3)
  PKG_VERSION   = $(aprutil_version)
  PKG_VER_PATCH = $(aprutil_ver_patch)
  PKG_SITE_PATH = pool/main/a/$(PKG_BASE)
else
  PKG_VERSION   = $(aprutil_version)
  PKG_SITES     = $(APACHE_ORG_SITES)
  PKG_SITE_PATH = apr
endif

include $(DEFS)


ifeq ($(PKG_TYPE),DEBIAN1)
include $(TOOLS_PKG_DIR)/dpatch.mk
#PKG_PATCH2_FILES += $(PKG_PKG_DIR)/*.patch
endif


AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk

AM_VER = 1.11
include $(TOOLS_PKG_DIR)/automake.mk

pre-configure : $(call bstamp_f,install,sed gawk autotools-dev libtool python2.6 apr)
pre-configure : $(call tstamp_f,install,apr)

define PRE_CONFIGURE_CMD
  $(RELINK)			$(PKG_BLD_SUB_DIR)/buildconf $(LOG)
  $(SED) -i	-e 's,/usr/share/misc/config.sub,,'	\
		-e 's,/usr/share/misc/config.guess,,' 	\
				$(PKG_BLD_SUB_DIR)/buildconf		$(LOG)

  cd $(PKG_BLD_SUB_DIR) && $(RELINK) build-outputs.mk apr-util.spec	$(LOG)
  cd $(PKG_BLD_SUB_DIR) && ./buildconf --with-apr=$(BLD_DIR)/apr	$(LOG)
endef

without  = lber ldap gdbm ndbm berkeley-db pgsql mysql sqlite3 sqlite2
without += oracle freetds odbc iconv

prefix     = --prefix=$(INST_DIR)

ifeq ($(PLATFORM),build)
  with_apr   = --with-apr=$(IBIN_DIR)/apr-1-config
  INSTALL_MK_VARS = installbuilddir=$(INST_DIR)/build-1
else
  build_host = --build=$(BUILD) --host=$(TARGET)
  with_apr   = --with-apr=$(IBIN_DIR)/apr-1-config
  STD_CPPFLAGS += -DAPR_IOVEC_DEFINED
  INSTALL_MK_VARS = installbuilddir=$(INST_DIR)/build-1
  configure: $(call tstamp_f,install,expat)
  install-root: $(call tstamp_f,install-root,expat)
endif

define CONFIGURE_CMD
  cp -v $(BSHARE_DIR)/misc/config.guess \
        $(BSHARE_DIR)/misc/config.sub   \
                                $(PKG_BLD_SUB_DIR)/build                $(LOG)

  cd $(PKG_BLD_SUB_DIR) && ./configure	$(LOG)	\
	$(prefix)				\
	$(build_host)				\
	$(with_apr)				\
	$(addprefix --without-,$(without))	\
	--disable-util-dso			\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libaprutil-1.so* $(RLIB_DIR) $(LOG)


include $(RULES)
