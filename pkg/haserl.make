PKG_TYPE          = DEBIAN3
PKG_VERSION       = 0.9.35
PKG_VER_PATCH     = -2
PKG_SITE_PATH     = pool/main/h/$(PKG_BASE)

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)

STD_CPPFLAGS += -DMAX_UPLOAD_KB=8192

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&				\
	ac_cv_func_memcmp_working=yes		\
	./configure -C $(LOG)			\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=$(INST_DIR)			\
	--disable-luashell			\
	--disable-luacshell			\
	--enable-bashshell			\
	--disable-bash-extensions		\
	--enable-subshell=/bin/sh		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(IBIN_DIR)/$(PKG) $(RBIN_DIR) $(LOG)


include $(RULES)
