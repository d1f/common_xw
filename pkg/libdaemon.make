PKG_TYPE      = DEBIAN3
PKG_VERSION   = 0.14
PKG_VER_PATCH = -6
PKG_SITE_PATH = pool/main/libd/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&				\
  ac_cv_func_setpgrp_void=yes			\
	./configure -C $(LOG)			\
	--prefix=$(INST_DIR)			\
	--build=$(BUILD) --host=$(TARGET)	\
	--disable-static --enable-shared	\
	--disable-assert			\
	--disable-largefile			\
	--disable-lynx				\
	--disable-examples			\
	--with-gnu-ld				\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

INSTALL_CMD      = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libdaemon.so* $(RLIB_DIR) $(LOG)


include $(RULES)
