PKG_VERSION   = 0.2.2
PKG_SITES     = http://www.greenend.org.uk/rjk
PKG_SITE_PATH = sftpserver
PKG_REMOTE_FILE_SUFFICES = .tar.gz

include $(DEFS)


PKG_PATCH2_FILES = $(wildcard $(PKG_PKG_DIR)/sftpserver_*.patch)


DROPBEAR_SFTPSERVER_PATH = /libexec/sftp-server


AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk

AM_VER = 1.11
include $(TOOLS_PKG_DIR)/automake.mk

pre-configure : $(call bstamp_f,install,libtool)

LIBTOOLIZE_FLAGS = -f -c --verbose

define PRE_CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && $(ACLOCAL)       $(ACLOCAL_FLAGS)	$(LOG)
  cd $(PKG_BLD_DIR) && if grep ^AC_PROG_LIBTOOL configure.ac; then \
                       libtoolize    $(LIBTOOLIZE_FLAGS);	fi
  $(MKDIR_P) $(PKG_BLD_DIR)/config.aux $(LOG)
  cd $(PKG_BLD_DIR) && $(AUTOCONF)     $(AUTOCONF_FLAGS)	$(LOG)
  cd $(PKG_BLD_DIR) && $(AUTOHEADER) $(AUTOHEADER_FLAGS)	$(LOG)
  cd $(PKG_BLD_DIR) && $(AUTOMAKE)     $(AUTOMAKE_FLAGS)	$(LOG)
  $(RM_R) $(PKG_BLD_DIR)/config.cache
endef


configure : $(call tstamp_f,install,toolchain dropbear)

STD_LDFLAGS += -lpthread
define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure	$(LOG)	\
	--prefix=$(INST_DIR)			\
	--build=$(BUILD) --host=$(TARGET)	\
	--enable-daemon				\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(MKDIR_P) $(dir $(ROOT_DIR)$(DROPBEAR_SFTPSERVER_PATH)) $(LOG)
  $(IW) $(INSTxFILE) $(INST_DIR)/libexec/gesftpserver \
	$(ROOT_DIR)$(DROPBEAR_SFTPSERVER_PATH) $(LOG)
endef


include $(RULES)
