PKG_TYPE          = DEBIAN3
PKG_VERSION       = 4.6.2
PKG_VER_PATCH     = -3
PKG_SITE_PATH     = pool/main/t/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain libpcap)

STD_CFLAGS += $(STD_CPPFLAGS)

ifneq ($(CONFIG_WITH_IPv6),y)
 configure_opts += --disable-ipv6
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && 				\
	./configure -C	$(LOG)			\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=$(INST_DIR)			\
	--disable-smb				\
	--without-smi				\
	--without-chroot			\
	--without-crypto			\
	$(configure_opts)			\
	ac_cv_linux_vers=$(LK_V1)		\
	td_cv_buggygetaddrinfo=no		\
	ac_cv_path_PCAP_CONFIG=			\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ISBIN_DIR)/tcpdump $(RSBIN_DIR) $(LOG)


include $(RULES)
