#PKG_TYPE       = DEBIAN3

PKG_VER_MAJOR   = 1.2
PKG_VERSION     = $(PKG_VER_MAJOR).101

ifeq ($(PKG_TYPE),DEBIAN3)
  PKG_VER_PATCH = -1
  PKG_SITE_PATH = pool/main/c/$(PKG_BASE)
else
  PKG_REMOTE_FILE_SUFFICES = .tar.gz

  PKG_SITE_PATH = $(PKG_VER_MAJOR)/$(PKG_VERSION)

  #PKG_SITES = http://www.cherokee-project.com/downloads.html
  PKG_SITES += http://mirror.yandex.ru/mirrors/cherokee-project.com
  PKG_SITES += http://mirrors.secution.com/cherokee
  PKG_SITES += http://www.gtlib.gatech.edu/pub/cherokee
  PKG_SITES += http://mirror.its.uidaho.edu/pub/cherokee
  PKG_SITES += http://cherokee.osuosl.org
  PKG_SITES += http://cherokee.pubcrawler.com
  PKG_SITES += http://cherokee.hyperial.com
  PKG_SITES += http://ftp.nluug.nl/internet/cherokee
  PKG_SITES += http://www.comunidadhosting.com/mirror/cherokee
  PKG_SITES += http://ftp.rediris.es/mirror/cherokee
  PKG_SITES += http://ftp.saix.net/Cherokee
  PKG_SITES += http://cherokee.bum.si/mirror
  PKG_SITES += http://mirror.nus.edu.sg/cherokee
  PKG_SITES += http://download.srv.ro/pub/cherokee
  PKG_SITES += http://mirrors.dominios.pt/cherokee
  PKG_SITES += http://ftp.wsisiz.edu.pl/pub/Linux/cherokee
  PKG_SITES += http://ftp.icm.edu.pl/packages/cherokee
  PKG_SITES += http://mirror.yongbok.net/cherokee
  PKG_SITES += http://www.ftp.ne.jp/infosystems/cherokee
  PKG_SITES += http://www.ring.gr.jp/archives/net/cherokee
  PKG_SITES += http://ftp.yz.yamagata-u.ac.jp/pub/network/cherokee
  PKG_SITES += http://cherokee.mirror.garr.it/mirrors/cherokee
  PKG_SITES += http://ftp.heanet.ie/mirrors/cherokee
  PKG_SITES += http://ftp.easynet.be/ftp/cherokee
  PKG_SITES += http://mirror.cpsc.ucalgary.ca/mirror/cherokee
  PKG_SITES += http://www.cherokee-project.de/mirrors/cherokee
  PKG_SITES += http://ftp.uni-erlangen.de/pub/mirrors/cherokee
  PKG_SITES += http://ftp.ntua.gr/pub/www/cherokee
  PKG_SITES += http://ftp.cc.uoc.gr/mirrors/cherokee
  PKG_SITES += http://cherokee.labs.itb.ac.id
  PKG_SITES += http://mirror.aarnet.edu.au/pub/cherokee
endif

include $(DEFS)


configure : $(call bstamp_f,install,sed)

conf_opts += $(if $(CONFIG_WITH_IPv6), --enable-ipv6, --disable-ipv6)

ifeq ($(CONFIG_CHEROKEE_WITH_INTERNAL_PCRE),y)
  conf_opts += --enable-internal-pcre
else
  configure : $(call tstamp_f,install,pcre3)
  conf_opts += --disable-internal-pcre
  STD_VARS  += PCRECONFIG=$(IBIN_DIR)/pcre-config
endif

ifeq ($(CONFIG_CHEROKEE_WITH_OPENSSL),y)
  configure : $(call tstamp_f,install,openssl-0.9)
  conf_opts += --with-libssl
else
  conf_opts += --without-libssl
endif

ifeq ($(CONFIG_CHEROKEE_WITH_MYSQL),y)
  configure : $(call tstamp_f,install,mysql)
  conf_opts += --with-mysql
else
  conf_opts += --without-mysql
endif

ifeq ($(CONFIG_CHEROKEE_WITH_GEOIP),y)
  configure : $(call tstamp_f,install,geoip)
  conf_opts += --with-geoip
else
  conf_opts += --without-geoip
endif

ifeq ($(CONFIG_CHEROKEE_WITH_FFMPEG),y)
  configure : $(call tstamp_f,install,ffmpeg-0.5)
  conf_opts += --with-ffmpeg
else
  conf_opts += --without-ffmpeg
endif

ifeq ($(CONFIG_CHEROKEE_WITH_PYTHON),y)
  configure : $(call tstamp_f,install,python)
  conf_opts += --with-python
else
  conf_opts += --without-python
endif

ifeq ($(CONFIG_CHEROKEE_WITH_PHP),y)
  configure : $(call tstamp_f,install,php)
  conf_opts += --with-php
else
  conf_opts += --without-php
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&				\
  ac_cv_func_malloc_0_nonnull=yes		\
  ac_cv_func_realloc_0_nonnull=yes		\
  ./configure -C $(LOG)				\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=				\
	--enable-os-string="Linux"		\
	--disable-shared --enable-static	\
	--enable-static-module=all		\
	--disable-largefile			\
	--enable-pthread			\
	--enable-tmpdir=/tmp			\
	--disable-pam				\
	--disable-nls				\
	--disable-admin				\
	$(conf_opts)				\
	--with-gnu-ld				\
	--with-sendfile-support			\
	--with-wwwroot=$(WEB_DIR)		\
	--with-cgiroot=$(WEB_DIR)/cgi-bin	\
	$(STD_VARS)

  $(SED) -i -r -e 's/#define +CHEROKEE_CONFIG_ARGS.+/#define CHEROKEE_CONFIG_ARGS ""/g' \
		$(PKG_BLD_DIR)/config.h $(LOG)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


modules  = error_redir error_nn server_info file dirlist cgi fcgi scgi uwsgi
modules += proxy redir common ssi secdownload empty_gif drop admin
modules += custom_error dbslayer streaming gzip deflate ncsa combined custom
modules += pam ldap mysql htpasswd plain htdigest authlist round_robin ip_hash
modules += failover directory extensions request header exists fullpath method
modules += from bind tls geoip url_arg v_or wildcard rehost target_ip evhost
modules += post_track post_report libssl render_rrd rrd not and or

sbinlist  = cherokee-worker cherokee
 liblist  = libcherokee-server.so* libcherokee-base.so* # libcherokee-client.so*

pluglist  = *
#pluglist  = wildcard v_or url_arg tls target_ip round_robin request rehost
#pluglist += redir plain or not ncsa method libssl ip_hash htpasswd header gzip
#pluglist += fullpath from file failover extensions exists evhost drop dirlist
#pluglist += directory deflate custom common combined cgi bind authlist and

INSTALL_MK_VARS = DESTDIR=$(INST_DIR)

INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define _INSTALL_ROOT_CMD_
  $(IW) $(INSTxFILES) $(addprefix  $(ILIB_DIR)/,$(liblist))   $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(addsuffix .so,$(addprefix  $(ILIB_DIR)/cherokee/libplugin_,$(pluglist))) \
					$(RLIB_DIR)/cherokee $(LOG)
endef

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(addprefix $(ISBIN_DIR)/,$(sbinlist)) $(RSBIN_DIR) $(LOG)
  $(IW) $(INST_FILES) $(ISHARE_DIR)/cherokee/themes/default \
                      $(RSHARE_DIR)/cherokee/themes $(LOG)
endef


include $(RULES)
