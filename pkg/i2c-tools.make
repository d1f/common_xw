PKG_TYPE      = DEBIAN3
PKG_VERSION   = 3.1.1
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/i/$(PKG_BASE)

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)

MK_VARS  = CC=$(CC)
MK_VARS += CFLAGS="$(STD_CFLAGS) $(STD_CPPFLAGS)"
MK_VARS += KERNELVERSION=$(LK_VERSION)
MK_VARS += DESTDIR= prefix=
MK_VARS +=  bindir=$(IBIN_DIR)
MK_VARS += sbindir=$(ISBIN_DIR)
MK_VARS +=  mandir=$(IMAN_DIR)
MK_VARS += man8dir=$(IMAN_DIR)/man8
MK_VARS +=  incdir=$(IINC_DIR)

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(addprefix $(ISBIN_DIR)/,i2cset i2cget i2cdump i2cdetect) $(RSBIN_DIR) $(LOG)


include $(RULES)
