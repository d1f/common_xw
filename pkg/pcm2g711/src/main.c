#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h> // basename
#include <unistd.h> // read

#include <df/error.h>
#include <df/log.h>
#include <df/write-all.h>

#include <SnackG711.h>
#include <g711table.h>


static void usage(const char *av0)
{
    fprintf(stderr, "Usage: %s a|mu <pcm16 >g711\n", basename(av0));
    exit(EXIT_FAILURE);
}

int main(int ac, char *av[])
{
    dflog_open(av[0], DFLOG_STDERR | DFLOG_BASENAME);

    if (ac != 2)
	usage(av[0]);

    if (strcmp(av[1], "a-law") == 0)
    {
	g711table_init(Snack_Lin2Alaw);
    }
    else if (strcmp(av[1], "mu-law") == 0)
    {
	g711table_init(Snack_Lin2Mulaw);
    }
    else
    {
	dferror(EXIT_FAILURE, 0, "Unknown G711 law: %s", av[1]);
    }

    while (1)
    {
	static short          inbuf[4096];
	static unsigned char outbuf[4096];

	ssize_t readen = read(STDIN_FILENO, inbuf, sizeof(inbuf));

	if (readen < 0)
	    dferror(EXIT_FAILURE, errno, "read error");

	if (readen == 0) // EOF
	    break;

	if (readen % sizeof(short)) // FIXME
	    dferror(EXIT_FAILURE, 0, "odd bytes read: %zd", readen);

	size_t samples = readen / sizeof(short);

	g711table_convert_array(outbuf, inbuf, samples);

	ssize_t rc = df_write_all(STDOUT_FILENO, outbuf, samples);
	if (rc < 0)
	    dferror(EXIT_FAILURE, errno, "write failed");
    }

    return EXIT_SUCCESS;
}
