PKG_VERSION   = 2_0
PKG_SITES     = $(SF_NET_SITES)
PKG_SITE_PATH = rtsp
PKG_REMOTE_FILE_SUFFICES = .tar.gz
PKG_INFIX     = _

include $(DEFS)


PKG_PATCH_FILES = $(PKG_PKG_DIR)/*.patch


  CFLAGS = $(STD_CFLAGS) $(STD_CPPFLAGS) -DNDEBUG -D_UNIX -D_LINUX -Wall
CXXFLAGS = $(CFLAGS)
 LDFLAGS = $(STD_LDFLAGS)
 LD      = $(CXX)
 ARFLAGS = -rc
 SYSLIBS = -lpthread

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     for D in . libapp librtsp rtspproxy; do		\
	 cat $$D/Makefile.in | sed "s%@CC@%"${CC}"%g;	\
	 s%@CXX@%"${CXX}"%g;				\
	 s%@AR@%${AR}%g;				\
	 s%@LD@%"${LD}"%g;				\
	 s%@CFLAGS@%${CFLAGS}%g;			\
	 s%@CXXFLAGS@%${CXXFLAGS}%g;			\
	 s%@ARFLAGS@%${ARFLAGS}%g;			\
	 s%@LDFLAGS@%${LDFLAGS}%g;			\
	 s%@SYSLIBS@%${SYSLIBS}%g" > $$D/Makefile;	\
     done $(LOG)
endef


MK_JOBS = 1
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/rtspproxy/rtspproxy $(RSBIN_DIR) $(LOG)
endef


include $(RULES)
