# depmod from busybox or module-init-tools
PKG_HAS_NO_SOURCES = defined

include $(DEFS)


ifeq ($(filter $(NOT_BUILD_TARGETS),$(MAKECMDGOALS)),)
include $(PLATFORM_DIR)/busybox.config
endif

install-root : $(call tstamp_f,install-root,base-init)

ifdef CONFIG_DEPMOD
  install-root : $(call tstamp_f,install-root,busybox)
else
  install-root : $(call tstamp_f,install,module-init-tools)
endif

INSTALL_ROOT_CMD = $(call INSTALL_PKGPKG_INIT_XFILES_F, depmod)


include $(RULES)
