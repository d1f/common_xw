PKG_TYPE      = DEBIAN3
PKG_VERSION   = 4.6.1
PKG_VER_PATCH = -7
PKG_SITE_PATH = pool/main/w/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain openssl-0.9 zlib)

STD_CPPFLAGS += -DNDEBUG -D__ASSERT_VOID_CAST=static_cast\<void\>

ENV_VARS  = $(STD_VARS)
ENV_VARS += WV_EXCLUDES="streams/tests/% ipstreams/tests/% crypto/tests/% \
			utils/tests/% urlget/tests/% linuxstreams/tests/% uniconf/tests/%"

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
        --build=$(BUILD)	\
	--host=$(TARGET)	\
	--disable-debug		\
	--disable-testgui	\
	--without-dbus		\
	--with-openssl		\
	--without-pam		\
	--without-tcl		\
	--without-qt		\
	--with-zlib		\
	--without-valgrind	\
	$(STD_VARS)
  cd $(PKG_BLD_DIR) && $(ENV_VARS) $(DOMAKE) CC CXX $(LOG)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

inst_list = wvutils wvstreams wvbase uniconf

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(addsuffix .so*,$(addprefix $(ILIB_DIR)/lib,$(inst_list))) $(RLIB_DIR) $(LOG)
endef


include $(RULES)
