ifeq (0,1)
   PKG_TYPE      = DEBIAN3
   PKG_VERSION   = 0.1.2
   PKG_VER_PATCH = -1
   PKG_SITE_PATH = pool/non-free/f/$(PKG_BASE)
else
   PKG_TYPE      = ORIGIN
   PKG_VERSION   = 0.1.3
   PKG_SITES     = $(SF_NET_SITES)
   PKG_SITE_PATH = opencore-amr/$(PKG_BASE)
   PKG_REMOTE_FILE_SUFFICES = .tar.gz
endif


include $(DEFS)


AC_VER = 2.69
AM_VER = 1.11

include $(TOOLS_PKG_DIR)/autoconf.mk
include $(TOOLS_PKG_DIR)/automake.mk

define PRE_CONFIGURE_CMD
  cd $(PKG_BLD_SUB_DIR) && libtoolize    $(LIBTOOLIZE_FLAGS) $(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(ACLOCAL)       $(ACLOCAL_FLAGS) $(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(AUTOMAKE)     $(AUTOMAKE_FLAGS) $(LOG)
  cd $(PKG_BLD_SUB_DIR) && $(AUTORECONF) $(AUTORECONF_FLAGS) $(LOG)
endef


configure : $(call tstamp_f,install,toolchain)
configure : $(call bstamp_f,install,gawk sed)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)			\
	--build=$(BUILD) --host=$(TARGET)	\
	--disable-example			\
	--enable-shared --disable-static	\
	--with-gnu-ld				\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libfdk-aac.so* $(RLIB_DIR) $(LOG)


include $(RULES)
