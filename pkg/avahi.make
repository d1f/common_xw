PKG_TYPE          = DEBIAN3
PKG_VERSION       = 0.6.31
PKG_VER_PATCH     = -4
PKG_SITE_PATH     = pool/main/a/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain libdaemon expat pkg-config libcap2)
install-root : $(call tstamp_f,install-root,libdaemon expat libcap2)
configure : $(call bstamp_f,install,intltool gettext)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
  ac_cv_prog_have_pkg_config=yes	\
  ./configure -C $(LOG)			\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--prefix=			\
	--disable-stack-protector	\
	--enable-shared			\
	--disable-static		\
	--disable-nls			\
	--disable-glib			\
	--disable-gobject		\
	--enable-introspection=no	\
	--disable-qt3			\
	--disable-qt4			\
	--disable-gtk			\
	--disable-gtk3			\
	--disable-dbus			\
	--disable-dbm			\
	--disable-gdbm			\
	--enable-libdaemon		\
	--disable-python		\
	--disable-pygtk			\
	--disable-python-dbus		\
	--disable-mono			\
	--disable-monodoc		\
	--disable-doxygen-doc		\
	--disable-doxygen-dot		\
	--disable-doxygen-man		\
	--disable-doxygen-rtf		\
	--disable-doxygen-xml		\
	--disable-doxygen-chm		\
	--disable-doxygen-chi		\
	--disable-doxygen-html		\
	--disable-doxygen-ps		\
	--disable-doxygen-pdf		\
	--disable-core-docs		\
	--disable-manpages		\
	--disable-xmltoman		\
	--disable-tests			\
	--enable-compat-libdns_sd	\
	--enable-compat-howl		\
	--with-gnu-ld			\
	--with-distro=debian		\
	--with-xml=expat		\
	$(STD_VARS)			\
	PERL5LIB=$(BLIB_DIR)/perl:$(PERL5LIB)
endef


      MK_VARS = DESTDIR=$(INST_DIR)
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/avahi-dnsconfd     $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/avahi-daemon       $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ISBIN_DIR)/avahi-autoipd      $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES)  $(ILIB_DIR)/libavahi-core.so*   $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES)  $(ILIB_DIR)/libavahi-common.so* $(RLIB_DIR) $(LOG)
endef


include $(RULES)
