#define _GNU_SOURCE
#include <df/log.h>
#include <df/error.h>
#include <df/fd.h>  // df_is_fd_opened
#include <df/x.h>   // df_xsigact
#include <stdbool.h>
#include <stdio.h>  // sprintf
#include <unistd.h> // pipe, fork, dup2
#include <limits.h> // PIPE_BUF
#include <fcntl.h>  // open
#include <signal.h> // signal
#include <sched.h>  // sched_yield
#include <poll.h>
#include <sys/wait.h> // waitpid


static void close_p(int *fdp)
{
    if (fdp != NULL)
    {
	if (*fdp >= 0)
	{
	    close(*fdp);
	    *fdp = -1;
	}
    }
}

// returns pid from fork(2) or -1 on error.
static int pipe2_from_fork(int *stdout_d, int *stderr_d)
{
    int err = 0;

    *stdout_d  = -1;
    *stderr_d  = -1;

    int pipe_stdout[2] = { -1, -1 };
    if (pipe(pipe_stdout) < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): pipe() failed", __func__);
	return -1;
    }

    int pipe_stderr[2] = { -1, -1 };
    if (pipe(pipe_stderr) < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): pipe() failed", __func__);
	goto fail;
    }

    pid_t pid = fork();
    if (pid < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): fork() failed", __func__);
	goto fail;
    }

    if (pid != 0)   // parent
    {
	*stdout_d = pipe_stdout[0];  //  read end
	df_fd_set_nonblock(pipe_stdout[0]);
	close(      pipe_stdout[1]); // write end

	*stderr_d = pipe_stderr[0];  //  read end
	df_fd_set_nonblock(pipe_stderr[0]);
	close(      pipe_stderr[1]); // write end
    }
    else            // child
    {
	close(      pipe_stderr[0]); // read  end
	*stderr_d = pipe_stderr[1];  // write end

	close(      pipe_stdout[0]); //  read end
	*stdout_d = pipe_stdout[1];  // write end
    }

    return pid;

fail:
    err = errno;

    close_p(&pipe_stdout[0]);
    close_p(&pipe_stdout[1]);
    close_p(&pipe_stderr[0]);
    close_p(&pipe_stderr[1]);

    errno = err;
    return -1;
}


enum { STDOUT_CHAN, STDERR_CHAN, CHANS };
static char buf[PIPE_BUF];

// returns EOF/ERROR flag
static bool read_log(struct pollfd pollfd[CHANS], size_t chann)
{
    static const char *name[CHANS] = { [STDOUT_CHAN] = "stdout", [STDERR_CHAN] = "stderr" };
    static const int  level[CHANS] = { [STDOUT_CHAN] = LOG_INFO, [STDERR_CHAN] =  LOG_ERR };

    if (pollfd[chann].fd >= 0)
    {
	if (pollfd[chann].revents & POLLIN)
	{
	    ssize_t rc = read(pollfd[chann].fd, buf, PIPE_BUF - 1);
	    if (rc < 0)
	    {
		if (errno == EAGAIN || errno == EWOULDBLOCK)
		{
		    return false;
		}
		else
		{
		    dferror(EXIT_SUCCESS, errno, "%s read failed", name[chann]);
		    close_p(&pollfd[chann].fd);
		    return true;
		}
	    }
	    if (rc == 0)
	    {
		close_p(&pollfd[chann].fd);
		return true;
	    }
	    else // rc > 0
	    {
		char *p, *q;
		buf[rc] = '\0';

		for (p = buf; (q = strchr(p, '\n')); p = q + 1)
		    syslog(LOG_USER | level[chann], "%*.*s", (q - p), (q - p), p);

		if (*p && (*p != '\n' || p[1] != '\0'))
		    syslog(LOG_USER | level[chann], "%s", p);

		return false;
	    }
	}
	else if (pollfd[chann].revents & (POLLHUP|POLLERR|POLLNVAL|POLLRDHUP))
	{
	    close_p(&pollfd[chann].fd);
	    return true;
	}
    }
    else
    {
	return true;
    }

    return false;
}

static void run(int stdout_fd, int stderr_fd)
{
    struct pollfd pollfd[CHANS]; DF_MEMZERO(pollfd);

    pollfd[STDOUT_CHAN].fd     = stdout_fd;
    pollfd[STDOUT_CHAN].events = POLLIN;

    pollfd[STDERR_CHAN].fd     = stderr_fd;
    pollfd[STDERR_CHAN].events = POLLIN;

    while (pollfd[STDERR_CHAN].fd >= 0 ||
	   pollfd[STDOUT_CHAN].fd >= 0
	   /*!signal_stop*/
	  )
    {
	int rc = poll(pollfd, CHANS, -1);
	if (rc < 0)
	{
	    if (errno == EINTR)
		break;
	    dferror(EXIT_FAILURE, errno, "poll failed");
	}
	else if (rc == 0)
	{
	    dferror(EXIT_FAILURE, 0, "poll timeout");
	}

	// rc > 0 - pipes ready

	if (read_log(pollfd, STDERR_CHAN) &&
	    read_log(pollfd, STDOUT_CHAN))
	    break;
    } // poll loop
}

static void sa_h(int sig)
{
    (void)sig;
}

int main(int ac, char *av[])
{
    if ( ! df_is_fd_opened(STDIN_FILENO) )
    {
	if (open("/dev/console", O_NOCTTY | O_RDONLY | O_NONBLOCK) < 0)
	    open("/dev/tty"    , O_NOCTTY | O_RDONLY | O_NONBLOCK);
    }

    if ( ! df_is_fd_opened(STDOUT_FILENO) )
    {
	if (open("/dev/console", O_NOCTTY | O_WRONLY | O_NONBLOCK) < 0)
	    open("/dev/tty"    , O_NOCTTY | O_WRONLY | O_NONBLOCK);
    }

    if ( ! df_is_fd_opened(STDERR_FILENO) )
    {
	if (open("/dev/console", O_NOCTTY | O_WRONLY | O_NONBLOCK) < 0)
	    open("/dev/tty"    , O_NOCTTY | O_WRONLY | O_NONBLOCK);
    }

    const char *parent_name = dflog_basename(av[0]);

    dflog_open(parent_name, DFLOG_STDERR | DFLOG_SYS | DFLOG_PID);
    if (ac < 2)
    {
	dflog(LOG_ERR, "Invalid parameters.");
	dflog_open("", DFLOG_STDERR);
	dflog(LOG_INFO, "Usage: %s prog-path [args...]", parent_name);
	return EXIT_FAILURE;
    }

    const char *child_name = dflog_basename(av[1]);

    dflog_open(parent_name, DFLOG_SYS | DFLOG_PID);

    df_xsigact(SIGCHLD, SA_NOCLDSTOP | SA_RESTART, sa_h);

    int stdout_fd = -1, stderr_fd = -1;

    int pid = pipe2_from_fork(&stdout_fd, &stderr_fd);
    if (pid < 0)
	return EXIT_FAILURE;

    int ret = EXIT_SUCCESS;

    if (pid != 0) // parent
    {
	char *ident = NULL;
	asprintf(&ident, "%s[%d] %s[%d]", parent_name, getpid(), child_name, pid);
	dflog_open(ident, DFLOG_SYS);

	run(stdout_fd, stderr_fd);

	dflog_open(parent_name, DFLOG_SYS | DFLOG_PID);

	int wstatus = 0;
	int rc = waitpid(pid, &wstatus, WNOHANG);
	if (rc < 0)
	{
	    dferror(EXIT_SUCCESS, errno, "waitpid failed");
	}
	else
	{
	    if (WIFEXITED(wstatus))
	    {
		ret = WEXITSTATUS(wstatus); // exit  status  of  the  child.
	    }
	    if (WIFSIGNALED(wstatus)) // terminated by a signal
	    {
#ifdef WCOREDUMP
		if (WCOREDUMP(wstatus))
		    dflog(LOG_INFO, "%s[%d] core dumped", child_name, pid);
#endif
		dflog(LOG_INFO, "%s[%d] terminated by signal %d (%s)",
		      child_name, pid,
		      WTERMSIG(wstatus), strsignal(WTERMSIG(wstatus)));
		ret = EXIT_FAILURE;
	    }
	}

	free(ident);
    }
    else // child
    {
	sched_yield();

	dflog_open(av[0], DFLOG_BASENAME | DFLOG_SYS | DFLOG_PID);

	if (dup2(stdout_fd, STDOUT_FILENO) < 0)
	    dferror(EXIT_FAILURE, errno, "dup2(%d, STDOUT_FILENO) failed", stdout_fd);
	close(stdout_fd); stdout_fd = -1;

	if (dup2(stderr_fd, STDERR_FILENO) < 0)
	    dferror(EXIT_FAILURE, errno, "dup2(%d, STDERR_FILENO) failed", stdout_fd);
	close(stderr_fd); stderr_fd = -1;

	execvp(av[1], &av[1]);
	dferror(EXIT_FAILURE, errno, "execvp() failed");
    }

    return ret;
}
