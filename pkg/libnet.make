PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.1.6+dfsg
PKG_VER_PATCH = -3
PKG_SITE_PATH = pool/main/libn/$(PKG_BASE)

include $(DEFS)


pre-configure : $(call tstamp_f,install,toolchain)

AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


configure : $(call tstamp_f,configure,linux)
configure : $(call bstamp_f,install,gawk sed)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     libnet_cv_have_packet_socket=yes	\
     ac_cv_libnet_linux_procfs=yes	\
     ./configure -C	$(LOG)	\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--disable-shared	\
	--enable-static		\
	--without-pic		\
	--with-gnu-ld		\
	--with-link-layer=linux	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
