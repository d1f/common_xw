PKG_VERSION       = snapshot-20120123-2245-stable
PKG_SITES         = http://download.videolan.org
PKG_SITE_PATH     = pub/x264/snapshots

include $(DEFS)


pre-configure : $(call bstamp_f,install,sed)

define PRE_CONFIGURE_CMD
  $(SED) -i -e 's,\$${cross_prefix}pkg-config,$(BBIN_DIR)/$(TARGET)-pkg-config,g' \
	$(PKG_BLD_DIR)/configure $(LOG)
endef


configure : $(call tstamp_f,install,pkg-config)

AS = $(CC)
define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && $(STD_VARS)	\
     ./configure $(LOG)			\
	--disable-cli			\
	--disable-asm			\
	--disable-static		\
	--enable-shared			\
	--host=$(TARGET)		\
	--cross-prefix=$(CROSS_PREFIX)	\
	--extra-cflags="$(STD_CPPFLAGS) $(STD_CFLAGS)" \
	--extra-ldflags="$(STD_LDFLAGS)"
endef


INSTALL_MK_VARS = prefix=$(INST_DIR)
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libx264.so* $(RLIB_DIR) $(LOG)
endef


include $(RULES)
