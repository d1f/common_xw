#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h> // strcpy
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>

#include <df/log.h>
#include <df/error.h>
#include <df/x.h>

#include "unsocktest.h"

sig_atomic_t signal_stop = 0;

static void sig_handler(int sig)
{
    signal_stop = sig;
    //dflog(LOG_INFO, "%s signal received", strsignal(signal_stop));
}


int main(int ac, char *av[])
{
    (void)ac;
    dflog_open(av[0], DFLOG_SYS | DFLOG_PID | DFLOG_BASENAME);
    dflog(LOG_INFO, "starting");

    unlink(UNSOCKTEST_NAME);

    int sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (sock < 0)
	dferror(EXIT_FAILURE, errno, "socket(AF_UNIX, SOCK_DGRAM, 0)");

    struct sockaddr srvr_name;
    srvr_name.sa_family = AF_UNIX;
    strcpy(srvr_name.sa_data, UNSOCKTEST_NAME);
    if (bind(sock, &srvr_name, strlen(srvr_name.sa_data) +
	     sizeof(srvr_name.sa_family)) < 0)
	dferror(EXIT_FAILURE, errno, "bind()");

    dflog(LOG_INFO, "binded to %s", UNSOCKTEST_NAME);

    df_xsigact(SIGHUP , SA_RESTART, sig_handler);
    df_xsigact(SIGINT , SA_RESTART, sig_handler);
    df_xsigact(SIGQUIT, SA_RESTART, sig_handler);
    df_xsigact(SIGTERM, SA_RESTART, sig_handler);

    if (daemon(0, 0) < 0)
	dferror(EXIT_FAILURE, errno, "Can't daemonize");

    dflog_open(av[0], DFLOG_SYS | DFLOG_PID | DFLOG_BASENAME);
    dflog(LOG_INFO, "running");

    struct pollfd pollfd = { .fd = sock, .events = POLLIN, .revents = 0 };

    while (!signal_stop)
    {
	int rc = poll(&pollfd, 1, -1);
	if (rc < 0)
	{
	    if (errno == EINTR)
		break;
	    dferror(EXIT_FAILURE, errno, "poll failed");
	}
	if (rc == 0)
	    dferror(EXIT_FAILURE, 0, "poll timeout");

	// rc > 0: pipes ready
	if (pollfd.revents & POLLIN)
	{
	    char buf[UNSOCKBUF_SIZE];
	    ssize_t bytes = recvfrom(sock, buf, sizeof(buf),  MSG_WAITALL, NULL, NULL);
	    if (bytes < 0)
		dferror(EXIT_FAILURE, errno, "recvfrom()");

	    buf[bytes] = 0;
	    dflog(LOG_INFO, "%zd characters received: \"%s\"", bytes, buf);
	}
    }

    if (signal_stop)
	dflog(LOG_INFO, "%s signal catched, exit", strsignal(signal_stop));

    close(sock);
    unlink(UNSOCKTEST_NAME);

    dflog(LOG_INFO, "exit");
    return EXIT_SUCCESS;
}
