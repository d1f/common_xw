#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <df/log.h>
#include <df/error.h>

#include "unsocktest.h"

int main(int ac, char *av[])
{
    dflog_open(NULL, DFLOG_STDERR | DFLOG_PID);

    if (ac < 2)
    {
	dflog(LOG_INFO, "Usage: %s msg[ msg ...]", basename(av[0]));
	return EXIT_FAILURE;
    }

    dflog_open(basename(av[0]), DFLOG_STDERR | DFLOG_PID);
    int sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (sock < 0)
	dferror(EXIT_FAILURE, errno, "socket(AF_UNIX, SOCK_DGRAM, 0)");

    struct sockaddr srvr_name;
    srvr_name.sa_family = AF_UNIX;
    strcpy(srvr_name.sa_data, UNSOCKTEST_NAME);

    ssize_t i;
    for (i=1; i < ac; ++i)
    {
	ssize_t buflen = strlen(av[i]);
	ssize_t rc = sendto(sock, av[i], buflen, 0, &srvr_name,
			    strlen(srvr_name.sa_data) + sizeof(srvr_name.sa_family));
	if (rc < 0)
	    dferror(EXIT_FAILURE, errno, "sendto()");
	if (rc != buflen)
	    dferror(EXIT_FAILURE, errno, "sendto(): %zd characters sent instead of %zd", rc, buflen);
    }

    return EXIT_SUCCESS;
}
