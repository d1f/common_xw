PKG_TYPE      = DEBIAN1
PKG_VERSION   = 9.9.5.dfsg
PKG_VER_PATCH = -9+deb8u5
PKG_SITE_PATH = pool/main/b/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES = $(wildcard $(PKG_PKG_DIR)/$(PKG)_*.patch)


pre-configure : $(call bstamp_f,install,sed gawk libtool)

#SUBDIRS =	make unit lib bin doc @LIBEXPORT@
define PRE_CONFIGURE_CMD
  $(SED) -i -e 's/ bin / /' $(PKG_BLD_DIR)/Makefile.in $(LOG)
endef


configure : $(call tstamp_f,install,toolchain)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--disable-shared	\
	--enable-static		\
	--enable-exportlib	\
	--disable-chroot	\
	--disable-backtrace	\
	--disable-symtable	\
	--with-gnu-ld		\
	--with-libtool		\
	--without-openssl	\
	--without-python	\
	--with-randomdev=/dev/urandom \
	--without-libxml2	\
	$(STD_VARS)
endef


MK_VARS = ALL_TESTDIRS=nulldir
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
