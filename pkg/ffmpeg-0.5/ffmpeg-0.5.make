PKG_TYPE      = DEBIAN1
PKG_VERSION   = 0.5.10
PKG_VER_PATCH = -1
PKG_BASE      = ffmpeg

PKG_SITE_PATH = pool/main/f/$(PKG_BASE)

include $(DEFS)


include $(TOOLS_PKG_DIR)/quilt.mk
#PKG_PATCH2_FILES += $(PKG_PKG_DIR)/0.5*.patch


disable  = static
 enable  = shared
disable += nonfree
 enable += gpl
disable += ffplay
 enable += postproc
 enable += swscale
 enable += avfilter avfilter-lavf
disable += beosthreads os2threads w32threads
 enable += pthreads
disable += x11grab vdpau
 enable += network
disable += mpegaudio-hp		# faster (but less accurate) MPEG audio decoding
disable += gray			# full grayscale support (slower color)
 enable += fastdiv		# table-based division
 enable += small		# optimize for size instead of speed
disable += aandct fft mdct rdft
disable += golomb
 enable += runtime-cpudetect	# detect cpu capabilities at runtime (bigger binary)
 enable += hardcoded-tables	# use hardcoded tables instead of runtime generation
disable += memalign-hack	# emulate memalign, interferes with memory debuggers
disable += beos-netserver	# BeOS netserver
 enable += encoders decoders	# all encoders, decoders
 enable += muxers demuxers	# all muxers, demuxers
 enable += parsers		# all parsers
disable += bsfs			# all bitstream filters
 enable += protocols		# all protocols
#disable += indevs		# input devices
#disable += outdevs		# output devices
#disable += devices		# all devices
disable += filters		# all filters

# External library support:
disable += avisynth		# reading of AVISynth script files
disable += bzlib
disable += libopencore-amrnb	# AMR-NB de/encoding via libopencore-amrnb
disable += libopencore-amrwb	# AMR-WB decoding via libopencore-amrwb
disable += libdc1394		# IIDC-1394 grabbing using libdc1394 and libraw1394
disable += libdirac		# Dirac support via libdirac
disable += libfaac		# FAAC support via libfaac
disable += libfaad		# FAAD support via libfaad
disable += libfaadbin		# open libfaad.so.0 at runtime
disable += libgsm		# GSM support via libgsm
disable += libmp3lame		# MP3 encoding via libmp3lame
disable += libnut		# NUT (de)muxing via libnut, native (de)muxer exists
disable += libopenjpeg		# JPEG 2000 decoding via OpenJPEG
disable += libschroedinger	# Dirac support via libschroedinger
disable += libspeex		# Speex decoding via libspeex
disable += libtheora		# Theora encoding via libtheora
disable += libvorbis		# Vorbis encoding via libvorbis, native implementation exists
disable += libx264		# H.264 encoding via x264
disable += libxvid		# Xvid encoding via xvidcore, native MPEG-4/Xvid encoder exists
disable += mlib			# Sun medialib
 enable += zlib			# zlib [autodetect]


disable += debug gprof
disable += yasm			# use of yasm assembler
 enable += stripping


#  --list-decoders          show all available decoders
#  --list-encoders          show all available encoders
#  --list-muxers            show all available muxers
#  --list-demuxers          show all available demuxers
#  --list-parsers           show all available parsers
#  --list-protocols         show all available protocols
#  --list-bsfs              show all available bitstream filters
#  --list-indevs            show all available input devices
#  --list-outdevs           show all available output devices
#  --list-filters           show all available filters


# All disabled above, now enable separate items:
ifeq (0,1)
 enable += decoder=jpegls	# JPEG LS
 enable += decoder=mjpeg	# Motion JPEG (format A)
 enable += decoder=mjpegb	# Motion JPEG (format B)
	# adpcm_ima_smjpeg libopenjpeg ?
 enable += encoder=jpegls	# JPEG LS
 enable += encoder=ljpeg	# Lossless JPEG
 enable += encoder=mjpeg
 enable += demuxer=mjpeg
 enable +=   muxer=mjpeg
 enable +=   muxer=mpjpeg
 enable +=  parser=mjpeg
endif

ifeq (1,1)
 enable += ffmpeg ffserver

 ifeq (0,1)
 enable += decoder=h264
 enable += muxer=h264
 enable += parser=h264
 enable += encoder=libx264
 enable += bsf=h264_mp4toannexb

 enable += decoder=libopenjpeg
 enable += decoder=mpeg4
 enable += encoder=mpeg4
 enable += parser=mpeg4video

 enable += muxer=image2pipe
 enable += muxer=mpegts
 enable += muxer=rtp		# ffserver_deps
 enable += muxer=ffm		# ffserver_deps
 enable += muxer=yuv4mpegpipe

 enable += demuxer=rtsp		# ffserver_deps

 enable += protocol=file
 enable += protocol=http
 enable += protocol=pipe
 enable += protocol=rtp
 enable += protocol=tcp
 enable += protocol=udp

 enable += bsf=mjpega_dump_header
 endif
else
disable += ffmpeg ffserver
endif


configure : $(call tstamp_f,install,toolchain)

# --sysroot=PATH		# root of cross-build tree
# --sysinclude=PATH		# location of cross-build system headers
# --target-exec=CMD		# command to run executables on target
# --target-path=DIR		# path to view of build directory on target

# --host-ldflags=HLDFLAGS	# use HLDFLAGS when linking for host
# --host-libs=HLIBS		# use libs HLIBS when linking for host

# --extra-cflags=ECFLAGS	# add ECFLAGS to CFLAGS [ -D_ISOC99_SOURCE -D_POSIX_C_SOURCE=200112]
# --extra-ldflags=ELDFLAGS	# add ELDFLAGS to LDFLAGS []
# --extra-libs=ELIBS		# add ELIBS []
# --extra-version=STRING	# version string suffix []
# --build-suffix=SUFFIX		# library name suffix []

#  --logfile=FILE           log tests and output to FILE [config.err]
#  --disable-logging        do not log configure debug information
define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&				\
	./configure $(LOG)			\
	--prefix=$(INST_DIR)			\
	--bindir=$(RBIN_DIR)			\
	--datadir=$(RSHARE_DIR)/ffmpeg		\
	--libdir=$(ILIB_DIR)			\
	--shlibdir=$(RLIB_DIR)			\
	--incdir=$(IINC_DIR)			\
	--mandir=$(IMAN_DIR)			\
	$(addprefix --disable-,$(disable))	\
	$(addprefix --enable-,$(enable))	\
	--cross-prefix=$(CROSS_PREFIX)		\
	--enable-cross-compile			\
	--target-os=linux			\
	--cc=$(CC)				\
	--nm=$(NM)				\
	--host-cc=$(BUILD_CC)			\
	--host-cflags='-O -pipe -g0'		\
	--arch=$(NARCH)				\
	--cpu=$(BFD_ARCH)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


  INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/ffserver.conf  $(RETC_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_PKG_DIR)/ffserver-cast* $(RBIN_DIR) $(LOG)
endef


include $(RULES)
