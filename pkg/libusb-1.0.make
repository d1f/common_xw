PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.0.19
PKG_VER_PATCH = -1~bpo70+1
PKG_SITE_PATH = pool/main/libu/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain pkg-config)
configure : $(call bstamp_f,install,gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--disable-shared	\
	--enable-static		\
	--with-gnu-ld		\
	--enable-timerfd	\
	--disable-udev		\
	--enable-system-log	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  #$(IW) $(INSTxFILES) $(ILIB_DIR)/libusb-1.0.so* $(RLIB_DIR) $(LOG)
endef


include $(RULES)
