PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install : $(call tstamp_f,install,cherokee)
install : $(call tstamp_f,uninstall,httpd-init)

define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/cherokee.conf $(ROOT_DIR)/conf $(LOG)
  $(call INSTALL_PKGPKG_INIT_XFILES_F, cherokee.rc)
  $(IW) $(MKDIR_P) $(RWEB_DIR)/nonssl $(LOG)
endef


include $(RULES)
