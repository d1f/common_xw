PKG_TYPE          = DEBIAN3
PKG_VERSION       = 3.5.2
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/p/$(PKG_BASE)

include $(DEFS)


  ZLIB=no
   DNS=no
SHARED=yes


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call bstamp_f,install,sed)

ch = $(PKG_BLD_DIR)/lib/config.h
cm = $(PKG_BLD_DIR)/lib/config.mk
uarch = $(shell echo $(NARCH) | tr 'a-z' 'A-Z')

define CONFIGURE_CMD
  echo      > $(ch) '#define PCI_CONFIG_H'
  echo     >> $(ch) '#define PCI_ARCH_$(uarch)'
  echo     >> $(ch) '#define PCI_OS_LINUX'

  if test $(LK_V1) -ge 2 -a $(LK_V2) -ge 6; then	\
     echo  >> $(ch) '#define PCI_HAVE_PM_LINUX_SYSFS';	\
  fi

# echo     >> $(ch) '#define PCI_HAVE_PM_LINUX_PROC'
  echo     >> $(ch) '#define PCI_HAVE_LINUX_BYTEORDER_H'
  echo     >> $(ch) '#define PCI_PATH_PROC_BUS_PCI "/proc/bus/pci"'
  echo     >> $(ch) '#define PCI_PATH_SYS_BUS_PCI "/sys/bus/pci"'

  if test "$(NARCH)" = "i386"; then			\
     echo  >> $(ch) '#define PCI_HAVE_PM_INTEL_CONF';	\
  fi

  echo     >> $(ch) '#define PCI_HAVE_64BIT_ADDRESS'
  echo     >> $(ch) '#define PCI_HAVE_PM_DUMP'

  if test "$(ZLIB)" = "yes" ; then			\
     echo  >> $(ch) '#define PCI_COMPRESSED_IDS';	\
     echo  >> $(ch) '#define PCI_IDS "pci.ids.gz"';	\
  else							\
     echo  >> $(ch) '#define PCI_IDS "pci.ids"';	\
  fi

  echo     >> $(ch) '#define PCI_PATH_IDS_DIR "/share"'
  if test "$(DNS)" = "yes" ; then			\
     echo  >> $(ch) '#define PCI_USE_DNS';		\
     echo  >> $(ch) '#define PCI_ID_DOMAIN "pci.id.ucw.cz"'; \
  fi
  if test "$(SHARED)" = "yes"; then			\
     echo  >> $(ch) '#define PCI_SHARED_LIB';		\
  fi
  echo     >> $(ch) '#define PCILIB_VERSION "$(PKG_VERSION)"'


  echo      > $(cm) 'WITH_LIBS='

  if test "$(ZLIB)" = "yes" ; then			\
     echo  >> $(cm) 'LIBZ=-lz';				\
     echo  >> $(cm) 'WITH_LIBS+=$$(LIBZ)';		\
  fi

  if test "$(DNS)" = "yes" ; then			\
     echo  >> $(cm) 'WITH_LIBS+=-lresolv';		\
  fi

  if test "$(SHARED)" != "yes"; then			\
     echo  >> $(cm) 'PCILIB=$$(LIBNAME).a';		\
     echo  >> $(cm) 'LDLIBS=$$(WITH_LIBS)';		\
     echo  >> $(cm) 'LIB_LDLIBS=';			\
  else							\
     echo  >> $(cm) 'PCILIB=$$(LIBNAME).so.$$(VERSION)'; \
     echo  >> $(cm) 'LDLIBS=';				\
     echo  >> $(cm) 'LIB_LDLIBS=$$(WITH_LIBS)';		\
     echo  >> $(cm) 'SONAME=-Wl,-soname,$$(LIBNAME).so$$(ABI_VERSION)'; \
  fi
  echo     >> $(cm) 'PCILIBPC=$$(LIBNAME).pc'

  $(SED) -e '/"/{s/^#define \([^ ]*\) "\(.*\)"$$/\1=\2/;p;d;};s/^#define \(.*\)/\1=1/' < $(ch) >> $(cm)
endef


ifeq ($(SHARED),yes)
STD_CFLAGS += -fPIC
endif

MK_VARS  = CROSS_COMPILE=$(CROSS_PREFIX)
MK_VARS += CC=$(CC) AR=$(AR) RANLIB=$(RANLIB)
MK_VARS += CFLAGS="$(STD_CFLAGS) $(STD_CPPFLAGS)"
MK_VARS += LDFLAGS="$(STD_LDFLAGS)"
MK_VARS += IDSDIR=/share
MK_VARS += HOST=$(TARGET) ZLIB=$(ZLIB) DNS=$(DNS) SHARED=$(SHARED)

targets = lspci #setpci pcimodules
    BUILD_CMD_TARGET = $(targets)
    BUILD_CMD = $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


ifeq ($(SHARED),yes)
  define INSTALL_SHARED_CMD
    $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/lib/libpci.so.$(PKG_VERSION) $(RLIB_DIR)  $(LOG)
    $(IW) $(LN_S)                          libpci.so.$(PKG_VERSION) \
                               $(RLIB_DIR)/libpci.so.$(firstword $(subst ., ,$(PKG_VERSION))) $(LOG)
  endef
endif

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(addprefix $(PKG_BLD_DIR)/,$(targets)) $(RSBIN_DIR) $(LOG)
# $(IW) $(INST_FILES) $(PKG_BLD_DIR)/pci.ids $(RSHARE_DIR) $(LOG)
  $(INSTALL_SHARED_CMD)
endef


include $(RULES)
