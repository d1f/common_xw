#ifndef  SNACK_G711_PRIV_H
# define SNACK_G711_PRIV_H


short
Snack_search(short val, short *table, short size);


#define	SIGN_BIT	(0x80)		/* Sign bit for a A-law byte. */
#define	QUANT_MASK	(0xf)		/* Quantization field mask. */
#define	NSEGS		(8)		/* Number of A-law segments. */
#define	SEG_SHIFT	(4)		/* Left shift for segment number. */
#define	SEG_MASK	(0x70)		/* Segment field mask. */

#define	BIAS		(0x84)		/* Bias for linear code. */
#define CLIP            8159


#endif //SNACK_G711_PRIV_H
