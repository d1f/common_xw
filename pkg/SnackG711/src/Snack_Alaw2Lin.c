#include "g711_priv.h"

/*
 * Snack_Alaw2Lin() - Convert an A-law value to 16-bit linear PCM
 *
 */
short
Snack_Alaw2Lin(
	unsigned char	a_val)
{
	short		t;
	short		seg;

	a_val ^= 0x55;

	t = (a_val & QUANT_MASK) << 4;
	seg = ((unsigned)a_val & SEG_MASK) >> SEG_SHIFT;
	switch (seg) {
	case 0:
		t += 8;
		break;
	case 1:
		t += 0x108;
		break;
	default:
		t += 0x108;
		t <<= seg - 1;
	}
	return ((a_val & SIGN_BIT) ? t : -t);
}
