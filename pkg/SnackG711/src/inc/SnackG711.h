#ifndef  SNACK_G711_H
# define SNACK_G711_H


/*
 * Snack_Lin2Alaw() - Convert a 16-bit linear PCM value to 8-bit A-law
 * accepts an 16-bit integer and encodes it as A-law data.
 */
unsigned char
Snack_Lin2Alaw(short pcm_val);	/* 2's complement (16-bit range) */

/*
 * Snack_Alaw2Lin() - Convert an A-law value to 16-bit linear PCM
 */
short
Snack_Alaw2Lin(unsigned char a_val);

/*
 * Snack_Lin2Mulaw() - Convert a linear PCM value to u-law
 */
unsigned char
Snack_Lin2Mulaw(short pcm_val);	/* 2's complement (16-bit range) */

/*
 * Snack_Mulaw2Lin() - Convert a u-law value to 16-bit linear PCM
 *
 * Note that this function expects to be passed the complement of the
 * original code word. This is in keeping with ISDN conventions.
 */
short
Snack_Mulaw2Lin(unsigned char u_val);

/* A-law to u-law conversion */
unsigned char
alaw2ulaw(unsigned char	aval);

/* u-law to A-law conversion */
unsigned char
ulaw2alaw(unsigned char	uval);


#endif //SNACK_G711_H
