short
Snack_search(
	short		val,
	short		*table,
	short		size)
{
	short		i;

	for (i = 0; i < size; i++) {
		if (val <= *table++)
			return (i);
	}
	return (size);
}
