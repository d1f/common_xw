PKG_URL       = http://www.boa.org/boa-0.94.13.tar.gz
PKG_VERSION   = 0.94.13
PKG_FILE_BASE = boa-$(PKG_VERSION)

PKG_SUB_DIR = src

include $(DEFS)


PKG_PATCH_FILES = $(PKG_PKG_DIR)/*.patch


configure : $(call bstamp_f,install,flex bison)
configure : $(call tstamp_f,install,toolchain)


define CONFIGURE_CMD
  cd $(PKG_BLD_SUB_DIR) && \
     ac_cv_func_memcmp_working=yes	\
     ac_cv_func_setvbuf_reversed=no	\
     $(STD_VARS)			\
     ./configure $(LOG)			\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--disable-gunzip		\
	--disable-debug			\
	--with-poll
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_SUB_DIR)/boa         $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_SUB_DIR)/boa_indexer $(RSBIN_DIR) $(LOG)
endef


include $(RULES)
