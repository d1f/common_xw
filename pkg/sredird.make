#PKG_TYPE       = DEBIAN_NATIVE

ifeq ($(PKG_TYPE),DEBIAN_NATIVE)
  pkg_ver_major = 2.2.1
  pkg_ver_minor = -1.1
  PKG_VERSION   = $(pkg_ver_major)$(pkg_ver_minor)
  PKG_SITE_PATH = pool/main/s/$(PKG_BASE)
else
  PKG_VERSION   = 2.2.2
  PKG_SITES     = http://www.ibiblio.org
  PKG_SITE_PATH = pub/Linux/system/serial
endif

include $(DEFS)


build : $(TOOLCHAIN_INSTALL_STAMP)

    MK_VARS   = $(STD_VARS)
    BUILD_CMD_TARGET = sredird
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/sredird $(RSBIN_DIR) $(LOG)


include $(RULES)
