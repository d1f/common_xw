PKG_TYPE      = DEBIAN3
PKG_VERSION   = 2.17
PKG_VER_PATCH = -48
PKG_SITE_PATH = pool/main/s/$(PKG_BASE)

include $(DEFS)


pre-configure : $(call bstamp_f,install,autotools-dev)

define PRE_CONFIGURE_CMD
  $(RM)  $(PKG_BLD_DIR)/config.guess     $(PKG_BLD_DIR)/config.sub $(LOG)
  cp $(BSHARE_DIR)/misc/config.guess $(BSHARE_DIR)/misc/config.sub \
			$(PKG_BLD_DIR) $(LOG)
endef


configure : $(TOOLCHAIN_INSTALL_STAMP)

STD_CFLAGS += $(STD_LDFLAGS)

define CONFIGURE_CMD
  touch $(PKG_BLD_DIR)/gorhack.h $(LOG)
  cd $(PKG_BLD_DIR) && $(STD_VARS)	\
	./configure $(LOG)		\
	--prefix=$(INST_DIR)		\
        --build=$(BUILD) --host=$(TARGET)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/setserial $(RBIN_DIR) $(LOG)


include $(RULES)
