PKG_TYPE      = DEBIAN3
PKG_VERSION   = 0.1.3
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/v/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain)
configure : $(call bstamp_f,install,gawk sed)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)			\
	--build=$(BUILD) --host=$(TARGET)	\
	--disable-example			\
	--enable-shared --disable-static	\
	--with-gnu-ld				\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libvo-aacenc.so* $(RLIB_DIR) $(LOG)
# $(IW) $(INSTxFILES) $(IBIN_DIR)/aac-enc          $(RBIN_DIR) $(LOG)
endef


include $(RULES)
