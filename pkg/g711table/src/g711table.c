#include "g711table.h"

#include <limits.h>

static unsigned char table[USHRT_MAX+1];

void g711table_init(g711table_linear2g711_f f)
{
    size_t i;
    for (i=0; i < sizeof(table); ++i)
	table[i] = f( (short)i );
}

unsigned char g711table_convert(short pcm)
{
    return table[ (unsigned short) pcm ];
}

void g711table_convert_array(unsigned char out[],
			     const short   pcm[],
			     size_t count)
{
    size_t i;
    for (i=0; i < count; ++i)
	out[i] = table[ (unsigned short) pcm[i] ];
}
