#ifndef  G711_TABLE_H
# define G711_TABLE_H

# include <stddef.h> // size_t


typedef unsigned char (*g711table_linear2g711_f)(short pcm);

void
g711table_init(g711table_linear2g711_f f);

unsigned char
g711table_convert(short pcm);

void
g711table_convert_array(unsigned char out[],
			const short   pcm[],
			size_t count);


#endif //G711_TABLE_H
