PKG_TYPE          = DEBIAN3
PKG_VERSION       = 3.16
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/e/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain)
configure : $(call bstamp_f,install,sed)

define PRE_CONFIGURE_CMD
endef

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C $(LOG)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--prefix=$(INST_DIR)	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
