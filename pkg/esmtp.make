PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.2
PKG_VER_PATCH     = -12
PKG_SITE_PATH     = pool/main/e/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain libesmtp)
configure : $(call bstamp_f,install,sed gawk bison flex)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
  PATH=$(IBIN_DIR):$(PATH)	\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
        --build=$(BUILD)	\
	--host=$(TARGET)	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(IBIN_DIR)/esmtp $(RBIN_DIR) $(LOG)


include $(RULES)
