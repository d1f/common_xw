PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.2.3+dfsg
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/s/$(PKG_BASE)

PLATFORM          = build

include $(DEFS)


build : $(call bstamp_f,install,python-setuptools)

define BUILD_CMD
  $(RELINK) $(PKG_BLD_DIR)/Sphinx.egg-info/* $(LOG)
  cd $(PKG_BLD_DIR) && python setup.py build --build-lib build/py2/ $(LOG)
endef


define INSTALL_CMD
  $(MKDIR_P) $(INST_DIR)/sphinx
  $(IWSH) "cd $(PKG_BLD_DIR) && python setup.py install --no-compile \
	--prefix $(INST_DIR) --root /" $(LOG)
endef


    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
