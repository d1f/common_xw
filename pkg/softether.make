PKG_VERSION   = 4.11-9506-beta
PKG_VER_PATCH = -2014.10.22
PKG_SITES     = http://softether-download.com
PKG_SITE_PATH = files/softether/v$(PKG_VERSION)$(PKG_VER_PATCH)-tree/Source%20Code
PKG_BASE      = $(PKG)-src-v
PKG_DIR       = $(PKG)
PKG_INFIX     =
PKG_REMOTE_FILE_SUFFICES = .tar.gz

include $(DEFS)


build     : $(call tstamp_f,install,toolchain openssl-0.9 readline5)
configure : $(call bstamp_f,install,sed)

define CONFIGURE_CMD
  cp -v $(PKG_BLD_DIR)/src/makefiles/linux_32bit.mak                             $(PKG_BLD_DIR)/Makefile $(LOG)
  chmod u+w                                                                      $(PKG_BLD_DIR)/Makefile $(LOG)
  $(SED) -i -e 's/OPTIONS_COMPILE_RELEASE=/OPTIONS_COMPILE_RELEASE=$$(CFLAGS) /' $(PKG_BLD_DIR)/Makefile $(LOG)
  $(SED) -i -e 's/OPTIONS_LINK_RELEASE=/OPTIONS_LINK_RELEASE=$$(LDFLAGS) /'      $(PKG_BLD_DIR)/Makefile $(LOG)
  $(SED) -i -e 's/ranlib/$$(RANLIB)/g'                                           $(PKG_BLD_DIR)/Makefile $(LOG)
  $(MKDIR_P) $(PKG_BLD_DIR)/tmp/objs/Mayaqua $(LOG)
  $(SED) -i -e 's/$$(HAMCORE_FILES)//'                                           $(PKG_BLD_DIR)/Makefile $(LOG)
  if test $(PLATFORM) != build; then \
  $(SED) -i -e 's%hamcore.se2: tmp/hamcorebuilder%hamcore.se2:%'                 $(PKG_BLD_DIR)/Makefile $(LOG); \
  fi
endef


BUILD_MK_VARS += CC=$(CC)
BUILD_MK_VARS += CFLAGS="$(STD_CPPFLAGS) $(STD_CFLAGS)"
BUILD_MK_VARS += LDFLAGS="$(STD_LDFLAGS)"
BUILD_MK_VARS += RANLIB=$(RANLIB)

BUILD_CMD_TARGET = build

hamdir = src/bin/BuiltHamcoreFiles/unix

ifneq ($(PLATFORM),build)
  build : $(call bstamp_f,build,$(PKG))
  define BUILD_CMD
    $(INST_FILES) $(_BLD_DIR)/build/$(PKG)/tmp/hamcorebuilder \
                                       $(PKG_BLD_DIR)/tmp/ $(LOG)
    touch                              $(PKG_BLD_DIR)/tmp/hamcorebuilder
    $(BUILD_CMD_DEFAULT)
  endef
else
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
endif

define _c_
    $(INST_FILES) $(_BLD_DIR)/build/$(PKG)/$(hamdir)/hamcore.se2 \
                                       $(PKG_BLD_DIR)/$(hamdir)/ $(LOG)
    $(TOUCH)                           $(PKG_BLD_DIR)/$(hamdir)/hamcore.se2
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_MK_VARS += INSTALL_BINDIR=$(IBIN_DIR)/
INSTALL_MK_VARS += INSTALL_VPNSERVER_DIR=$(INST_DIR)/vpnserver/
INSTALL_MK_VARS += INSTALL_VPNBRIDGE_DIR=$(INST_DIR)/vpnbridge/
INSTALL_MK_VARS += INSTALL_VPNCLIENT_DIR=$(INST_DIR)/vpnclient/
INSTALL_MK_VARS += INSTALL_VPNCMD_DIR=$(INST_DIR)/vpncmd/

define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  if test $(PLATFORM) != build; then \
     $(IW) $(INSTxFILES) $(INST_DIR)/vpnclient/vpnclient   $(RLIB_DIR)/vpnclient $(LOG); \
     $(IW) $(INST_FILES) $(INST_DIR)/vpnclient/hamcore.se2 $(RLIB_DIR)/vpnclient $(LOG); \
  fi
endef


include $(RULES)
