PKG_TYPE      = DEBIAN1
PKG_VERSION   = 1.7.2
PKG_VER_PATCH = -7
PKG_SITE_PATH = pool/main/p/$(PKG_BASE)

include $(DEFS)


# FIXME: stropts.h absent in uClibc and unused in the source. replace it by something
define CONFIGURE_CMD
  $(RELINK) $(PKG_BLD_DIR)/pptp_compat.c $(LOG)
  $(RELINK) $(PKG_BLD_DIR)/pptpsetup.8   $(LOG)
  $(SED) -i -e 's/stropts.h/string.h/' $(PKG_BLD_DIR)/pptp_compat.c $(LOG)
endef


build : $(TOOLCHAIN_INSTALL_STAMP)

MK_VARS  = PPPD=/sbin/pppd
MK_VARS += CC=$(CC)
# FIXME: AI_ADDRCONFIG is hided in uClibc
MK_VARS +=  CFLAGS="$(STD_CFLAGS) $(STD_CPPFLAGS) -DAI_ADDRCONFIG=0"
MK_VARS += LDFLAGS="$(STD_LDFLAGS)"

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/pptp $(RSBIN_DIR) $(LOG)


include $(RULES)
