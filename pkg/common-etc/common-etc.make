PKG_HAS_NO_SOURCES = defined

include $(DEFS)


define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/etc/* $(RETC_DIR) $(LOG)
  $(IW) chmod 0600 $(RETC_DIR)/shadow $(RETC_DIR)/gshadow $(LOG)
endef


include $(RULES)
