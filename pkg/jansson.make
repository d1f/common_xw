PKG_TYPE      = DEBIAN3
PKG_VERSION   = 2.12
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/j/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--enable-shared			\
	--disable-static		\
	--disable-windows-cryptoapi	\
	--with-gnu-ld			\
	$(STD_VARS)
endef

# --disable-urandom


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libjansson.so* $(RLIB_DIR) $(LOG)


include $(RULES)
