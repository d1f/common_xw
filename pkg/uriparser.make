PKG_TYPE          = DEBIAN3
PKG_VERSION       = 0.8.0.1
PKG_VER_PATCH     = -2
PKG_SITE_PATH     = pool/main/u/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain)
configure : $(call bstamp_f,install,gawk sed libtool)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
        --build=$(BUILD)		\
	--host=$(TARGET)		\
	--enable-static			\
	--disable-shared		\
	--enable-sizedown		\
	--disable-test			\
	--disable-doc			\
	--with-gnu-ld			\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
