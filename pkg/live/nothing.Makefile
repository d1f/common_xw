# Don't define any builtin rules and variables.
MAKEFLAGS := $(MAKEFLAGS)R

# Delete default suffixes
.SUFFIXES:

# Delete default rules
.DEFAULT:

.DEFAULT:
	$(error no rules for target $@)


.PHONY: all clean distclean install uninstall

all clean distclean install uninstall:
