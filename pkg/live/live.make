#PKG_URL      = http://www.live555.com/liveMedia/public/live.2016.02.22.tar.gz
PKG_SITES     = http://www.live555.com
PKG_SITE_PATH = liveMedia/public
PKG_VERSION   = 2016.02.22
PKG_INFIX     = .
PKG_REMOTE_FILE_SUFFICES = .tar.gz

include $(DEFS)


define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./genMakefiles linux $(LOG)
  $(MKDIR_P)                         $(PKG_BLD_DIR)/nothing          $(LOG)
  cp $(PKG_PKG_DIR)/nothing.Makefile $(PKG_BLD_DIR)/nothing/Makefile $(LOG)
endef


build : $(TOOLCHAIN_INSTALL_STAMP)

MK_VARS  = $(STD_VARS)
MK_VARS += C_COMPILER=$(CC)
MK_VARS += CPLUSPLUS_COMPILER=$(CXX)
MK_VARS += PREFIX=$(INST_DIR)
MK_VARS += LINK="$(CXX) -o "
MK_VARS += LIBRARY_LINK="$(AR) cr "
MK_VARS += TESTPROGS_DIR=nothing
MK_VARS += MEDIA_SERVER_DIR=nothing

    BUILD_CMD =   $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =   $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD =   $(CLEAN_CMD_DEFAULT)
  INSTALL_CMD = $(INSTALL_CMD_DEFAULT) # conflicts with ipnc-live-lib


include $(RULES)
