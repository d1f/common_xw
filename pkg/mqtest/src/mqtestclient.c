#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <fcntl.h>           /* For O_* constants */
#include <mqueue.h>

#include <df/log.h>
#include <df/error.h>

#include "mqtest.h"

int main(int ac, char *av[])
{
    dflog_open(NULL, DFLOG_STDERR | DFLOG_PID);

    if (ac < 2)
    {
	dflog(LOG_INFO, "Usage: %s msg[ msg ...]", basename(av[0]));
	return EXIT_FAILURE;
    }

    dflog_open(basename(av[0]), DFLOG_STDERR | DFLOG_PID);

    mqd_t mq = mq_open(MQTEST_NAME, O_CREAT|O_WRONLY|O_NONBLOCK, 0660, NULL);
    if (mq < 0)
	dferror(EXIT_FAILURE, errno, "Can't open/create posix message queue %s", MQTEST_NAME);

    ssize_t i;
    for (i=1; i < ac; ++i)
    {
	ssize_t buflen = strlen(av[i]);

	int rc = mq_send(mq, av[i], buflen, 0);
	if (rc < 0)
	{
	    if (errno != EAGAIN)
		dferror(EXIT_FAILURE, errno, "Can't send message");
	}
    }

    return EXIT_SUCCESS;
}
