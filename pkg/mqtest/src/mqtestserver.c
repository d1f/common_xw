#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <fcntl.h>           /* For O_* constants */
#include <mqueue.h>
#include <time.h>

#include <df/log.h>
#include <df/error.h>
#include <df/x.h>

#include "mqtest.h"


sig_atomic_t signal_stop = 0;

static void sig_handler(int sig)
{
    signal_stop = sig;
    dflog(LOG_INFO, "%s signal received", strsignal(signal_stop));
}


int main(int ac, char *av[])
{
    dflog_open(NULL, DFLOG_STDERR | DFLOG_PID);

    if (ac != 2)
    {
	dflog(LOG_INFO, "Usage: %s timeout-sec", basename(av[0]));
	return EXIT_FAILURE;
    }

    dflog_open(basename(av[0]), DFLOG_STDERR | DFLOG_SYS | DFLOG_PID);

    time_t timeout_sec = atoi(av[1]);
    if (timeout_sec < 0)
	dferror(EXIT_FAILURE, 0, "timeout-sec parameter < 0: %ld", timeout_sec);

    mqd_t mq = mq_open(MQTEST_NAME, O_CREAT|O_RDONLY, 0660, NULL);
    if (mq < 0)
	dferror(EXIT_FAILURE, errno, "Can't open/create posix message queue %s", MQTEST_NAME);

    struct mq_attr attr;
    ssize_t rc = mq_getattr(mq, &attr);
    if (rc < 0)
	dferror(EXIT_FAILURE, errno, "mq_getattr() failed");

#if 0
    dflog(LOG_INFO, "mq_maxmsg: %ld, mq_msgsize: %ld, mq_curmsgs: %ld",
	  attr.mq_maxmsg, attr.mq_msgsize, attr.mq_curmsgs);
#endif

    dflog_open(basename(av[0]), DFLOG_SYS | DFLOG_PID);
    dflog(LOG_INFO, "starting");

    //sigact.sa_flags   = SA_RESTART; // do not set or receive will not EINTR
    df_xsigact(SIGHUP , 0, sig_handler);
    df_xsigact(SIGINT , 0, sig_handler);
    df_xsigact(SIGQUIT, 0, sig_handler);
    df_xsigact(SIGTERM, 0, sig_handler);

    if (daemon(0, 0) < 0)
	dferror(EXIT_FAILURE, errno, "Can't daemonize");

    dflog_open(basename(av[0]), DFLOG_SYS | DFLOG_PID);
    dflog(LOG_INFO, "running");


    char buf[attr.mq_msgsize];

    while (!signal_stop)
    {
	struct timespec curr_time;
	rc = clock_gettime(CLOCK_REALTIME, &curr_time);
	if (rc < 0)
	{
	    dferror(EXIT_SUCCESS, errno, "clock_gettime(CLOCK_REALTIME) failed");
	    break;
	}

	struct timespec abs_timeout =
	{
	    .tv_sec  = curr_time.tv_sec + timeout_sec,
	    .tv_nsec = curr_time.tv_nsec
	};

	rc = mq_timedreceive(mq, buf, attr.mq_msgsize, NULL, &abs_timeout);
	if (rc < 0)
	{
	    dferror(EXIT_SUCCESS, errno, "Can't receive message");
	    break;
	}

	buf[rc] = 0;
	dflog(LOG_INFO, "%zd characters received: \"%s\"", rc, buf);
    }

    if (signal_stop)
	dflog(LOG_INFO, "%s signal catched, exit", strsignal(signal_stop));

    dflog(LOG_INFO, "exit");
    return EXIT_SUCCESS;
}
