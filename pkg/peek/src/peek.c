#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h> // open
#include <error.h>
#include <errno.h>
#include <unistd.h> // lseek

int main(int ac, char *av[])
{
    if (ac != 3)
    {
	fprintf(stderr, "Usage: %s offset size\n", av[0]);
	exit(EXIT_FAILURE);
    }

    unsigned long offset = strtol(av[1], NULL, 16);
    unsigned long size   = strtol(av[2], NULL, 16);

    int fd = open("/dev/mem", O_RDONLY);
    if (fd < 0)
	error(EXIT_FAILURE, errno, "Can't open /dev/mem");

    off_t off = lseek(fd, offset, SEEK_SET);
    if (off == ((off_t)-1))
	error(EXIT_FAILURE, errno, "Can't seek to %ld", offset);

    char m[size];
    int rc = read(fd, m, size);
    if (rc < 0)
	error(EXIT_FAILURE, errno, "Can't read %ld bytes at %ld", size, offset);
    if (rc == 0)
	error(EXIT_FAILURE, 0, "Unexpected EOF at %ld", offset);
    if ((typeof(size))rc != size)
	error(EXIT_FAILURE, 0, "Readed %d instead of %ld", rc, size);

    size_t i;
    for(i=0; i<size; ++i)
	printf(" %02x", m[i]);
    printf("\n");

    close(fd);
    return EXIT_SUCCESS;
}
