PKG_TYPE      = DEBIAN3
pkg_version   = 7.6
PKG_VERSION   = $(pkg_version).q
PKG_VER_PATCH = -25
PKG_SITE_PATH = pool/main/t/$(PKG_BASE)

include $(DEFS)


build : $(call tstamp_f,install,toolchain)

INCLUDE="-I$(PKG_BLD_DIR)/include"
INCLUDE+="-I$(IINC_DIR)"
INCLUDE+="-Iinclude"

MK_VARS = "CROSS_COMPILE=$(CROSS_PREFIX)"
MK_VARS += "CROSS_ARCH=Linux"
MK_VARS += "CROSS_PROC=$(ARCH)"
#MK_VARS += "OPTIONS=$(ASTCFLAGS)"
#MK_VARS += "ASTCFLAGS=$(ASTCFLAGS)"
MK_VARS += "INCLUDE=$(INCLUDE)"
MK_VARS += "INSTALL_PREFIX=$(INST_DIR)"
#MK_VARS += 'LIBS=-L$(RLIB_DIR) -lssl -lcrypto -ldl -lpthread -lncurses -lm -lresolv'
MK_VARS += 'SOLLIBS=-L$(RLIB_DIR)  -L$(ILIB_DIR)'
MK_VARS += "LDFLAGS=$(STD_LDFLAGS)"
MK_VARS += "AR=$(AR)"
MK_VARS += "CC=$(CC)"
MK_VARS += "RANLIB=$(RANLIB)"


    BUILD_CMD_TARGET = linux
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(IW) $(INST_FILES) $(PKG_BLD_DIR)/tcpd.h $(IINC_DIR) $(LOG)

#install -o root -g root -m 0755 tcpdchk ${DESTDIR}/usr/sbin/
#install -o root -g root -m 0755 tcpdmatch ${DESTDIR}/usr/sbin/
define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/tcpd		               $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/shared/libwrap.so.0.$(pkg_version) $(RLIB_DIR) $(LOG)
  $(IW) $(LN_S) -fv libwrap.so.0.$(pkg_version) $(RLIB_DIR)/libwrap.so.0            $(LOG)
  $(IW) $(LN_S) -fv libwrap.so.0                $(RLIB_DIR)/libwrap.so              $(LOG)
endef


include $(RULES)
