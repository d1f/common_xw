PKG_TYPE      = DEBIAN3
# cadaver requires neon 0.27 .. 0.29
PKG_VERSION   = 0.29.6
PKG_VER_PATCH = -3
PKG_SITE_PATH = pool/main/n/$(PKG_BASE)

include $(DEFS)


include $(TOOLS_PKG_DIR)/configure-menuconfig.mk


configure : $(call bstamp_f,install,sed)
configure : $(call tstamp_f,install,toolchain)

deps += $(if $(findstring openssl-0.9,$(CONFIGURE_OPTIONS)),openssl)
deps += $(if $(findstring gnutls,$(CONFIGURE_OPTIONS)),gnutls26)
deps += $(if $(findstring without-zlib,$(CONFIGURE_OPTIONS)),,zlib)
deps += $(if $(findstring with-expat,$(CONFIGURE_OPTIONS)),expat)

configure : $(call tstamp_f,install,$(deps) pkg-config)
install-root : $(call tstamp_f,install-root,$(deps))

define CONFIGURE_CMD
  cd $(PKG_BLD_SUB_DIR)		&&	\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	$(CONFIGURE_OPTIONS)		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD      = $(INSTALL_CMD_DEFAULT)
define INSTALL_ROOT_CMD
  if test -n "$(wildcard $(ILIB_DIR)/libneon-NODAV.so*)"; then \
  $(IW) $(INSTxFILES)    $(ILIB_DIR)/libneon-NODAV.so* $(RLIB_DIR) $(LOG); \
  fi
  $(IW) $(INSTxFILES)    $(ILIB_DIR)/libneon.so*       $(RLIB_DIR) $(LOG)
endef


include $(RULES)
