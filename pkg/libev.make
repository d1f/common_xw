PKG_TYPE          = DEBIAN3
PKG_VERSION       = 4.15
PKG_VER_PATCH     = -3
PKG_SITE_PATH     = pool/main/libe/$(PKG_BASE)

include $(DEFS)


AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)

configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call bstamp_f,install,gawk)

# FIXME: for single CPU or without threads only
STD_CPPFLAGS += -DECB_NO_SMP

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)			\
	--build=$(BUILD) --host=$(TARGET)	\
	--enable-shared				\
	--disable-static			\
	--with-gnu-ld				\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libev.so* $(RLIB_DIR) $(LOG)


include $(RULES)
