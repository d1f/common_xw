PKG_TYPE      = DEBIAN1
PKG_VERSION   = 5.2+dfsg
PKG_VER_PATCH = -2~deb7u1
PKG_SITE_PATH = pool/main/r/$(PKG_BASE)

include $(DEFS)


install : $(call tstamp_f,uninstall,readline6)


PKG_PATCH_FILES += $(PKG_PKG_DIR)/dpatch-fix.diff
include $(TOOLS_PKG_DIR)/dpatch.mk
PKG_PATCH2_FILES = $(PKG_PKG_DIR)/*.patch


pre-configure : $(call bstamp_f,install,autotools-dev)

define PRE_CONFIGURE_CMD
  $(RM)  $(PKG_BLD_DIR)/config.guess     $(PKG_BLD_DIR)/config.sub $(LOG)
  cp $(BSHARE_DIR)/misc/config.guess $(BSHARE_DIR)/misc/config.sub \
			$(PKG_BLD_DIR)/support $(LOG)
endef


configure : $(call tstamp_f,install,toolchain ncurses)
install-root : $(call tstamp_f,install-root,ncurses)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
	$(PKG_SRC_DIR)/configure -C $(LOG)	\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=$(INST_DIR)			\
	--disable-multibyte			\
	--enable-shared --disable-static	\
	--disable-largefile			\
	--with-curses				\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libreadline.so* $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libhistory.so*  $(RLIB_DIR) $(LOG)
endef


include $(RULES)
