/*
	rt.c

	Runs process on realtime priority.

	$Id: rt.c,v 1.10 2003/11/01 19:19:37 fedorov Exp $
*/

#define VERSION "2.2"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>
#include <string.h>			/* memset(3)  */
#include <errno.h>
#include <error.h>
#include <ctype.h>			/* isdigit(3) */
#include <getopt.h>


/* QNX-scheduler patch tolerance */
union sched_param_extended
{
    struct sched_param param;
    int pad[16];
};


static void __attribute__((noreturn)) usage(void)
{
	printf(
"\n"
"Linux real time scheduling client, v" VERSION "\n"
"Copyleft (C) 1996, Boris Tobotras <boris@xtalk.msk.su>\n"
"             1998, Dmitry Fedorov <fedorov@cpan.org>\n"
"\n"
"This software may be used and distributed according to the terms\n"
"of the GNU Public License, incorporated herein by reference.\n"
"\n"
"Usage: rt [options] [command]\n"
"\n"
"Sets real-time priority and execute command.\n"
"\n"
"Recognized options are:\n"
"\t-r[N]        Use SCHED_RR scheduling policy (default);\n"
"\t-f[N]        Use SCHED_FIFO scheduling policy;\n"
"\t   N         Priority level. Default is sched_get_priority_min()\n"
"\t-o           Use SCHED_OTHER scheduling policy (not real time);\n"
"\t-s[pid]      Show priority of running process by pid and exit;\n"
"\t-S[pid]      Set  priority of running process by pid and exit;\n"
"\t-v --verbose Be verbose;\n"
"\t-V --version Print version and exit;\n"
"\t-h --help    Print help text and exit;\n"
"\t--           End of options.\n"
"\n"
    );

    exit(EXIT_SUCCESS);
}


static const char* strpolicy(int policy)
{
    static const char unknown_format[] = "UNKNOWN %d";
    static char unknown[ sizeof(unknown_format)+11 ];

    switch (policy)
    {
	case SCHED_OTHER:
	    return __STRING(SCHED_OTHER);
	case SCHED_FIFO:
	    return __STRING(SCHED_FIFO);
	case SCHED_RR:
	    return __STRING(SCHED_RR);
	default:
	    sprintf(unknown, unknown_format, policy);
	    return unknown;
    }
}


struct params
{
    int policy, priority, prio_min, prio_max;
    int set, show, policy_present, prio_present, verbose;
    pid_t pid;
};

#define PARAMS_INIT() { -1, -1, -1, -1, 0, 0, 0, 0, 0, 0 }


static void do_options(struct params* p, int ac, char* av[])
{
    static struct option lopts[] =
    {
	{ "help"   , no_argument, NULL, 'h' },
	{ "version", no_argument, NULL, 'V' },
	{ "verbose", no_argument, NULL, 'v' },
	{      NULL,           0, NULL,  0  }
    };

    int opt;

    if (ac<2)
	error(EXIT_FAILURE, 0, "invalid number of parameters, try --help");

    while ( (opt=getopt_long(ac,av,"r::f::os::S::hvV",lopts,NULL)) != EOF )
    {
#ifdef DEBUG
	fprintf(stderr, "opt = '%c'  optind = %d  optarg = '%s'\n",
		opt, optind, optarg);
#endif
	switch (opt)
	{
	    case 'r':
		p->policy = SCHED_RR;
		if (optarg != NULL) p->priority = atoi(optarg);
		break;
	    case 'f':
		p->policy = SCHED_FIFO;
		if (optarg != NULL) p->priority = atoi(optarg);
		break;
	    case 'o':
		p->policy = SCHED_OTHER;
		p->priority = 0;
		break;
	    case 's':
		p->show = 1;
		if (optarg != NULL) p->pid = atoi(optarg);
		break;
	    case 'S':
		p->set = 1;
		if (optarg != NULL) p->pid = atoi(optarg);
		break;
	    case 'v':
		p->verbose = 1;
		break;
	    case 'h':
		usage();
	    case 'V':
		printf(VERSION "\n");
		exit(EXIT_SUCCESS);
	    default:
		error(EXIT_FAILURE, 0, "invalid option %c, try --help", opt);
	}
    }

    if (p->set && p->show)
    {
	fprintf(stderr, "Set and Show options are mutually exclusive.\n");
	exit(EXIT_FAILURE);
    }

    if ( p->set || p->show )
    {
	if ( optind < ac )
	    error(EXIT_FAILURE, 0,
		  "Set and Show options don't needs command.\n");
    }
    else
    {
	if ( optind >= ac )
	    error(EXIT_FAILURE, 0, "command to run is not specified");
    }

    if (p->policy >= 0)
	p->policy_present = 1;

    if (p->priority >= 0)
	p->prio_present = 1;

    if ( p->show && (p->policy_present || p->prio_present) )
	error(EXIT_FAILURE, 0,
	      "Show option don't need priority or policy options");
}


/* returns error flag */
static int check_priority_limits(const struct params* p)
{
    if ( p->priority < p->prio_min || p->priority > p->prio_max )
    {
	fprintf(stderr,
		"Bad priority value %d, should be %d..%d for %s policy.\n",
		p->priority, p->prio_min, p->prio_max, strpolicy(p->policy) );
	return 1;
    }

    return 0;
}


int main(int ac, char *av[])
{
    union sched_param_extended xparam;
    struct params params = PARAMS_INIT();

    memset( &xparam, 0, sizeof(xparam) );

    do_options( &params, ac, av );

    /*--------------------------------------------------------------------*/

    /* + get current policy or set default */
    if ( params.show || (params.set && !params.policy_present) )
    {
	int rc = sched_getscheduler(params.pid); /* get policy */
	if (rc<0)
	    error(EXIT_FAILURE, errno, "can't get current scheduler policy");
	params.policy = rc;
    }
    else
	if (!params.policy_present)
	    params.policy = SCHED_RR; /* default policy */
    /* - get current policy or set default */


    /* + get priority limits */
    params.prio_min = sched_get_priority_min(params.policy);
    if (params.prio_min<0)
	error(EXIT_FAILURE, errno, "can't get min priority value");

    params.prio_max = sched_get_priority_max(params.policy);
    if (params.prio_max < 0)
	error(EXIT_FAILURE, errno, "can't get max priority value");
    /* - get priority limits */

    /* + get current priority or set default */
    if ( params.show || (params.set && !params.prio_present) )
    {
	if (sched_getparam(params.pid, &xparam.param) != 0) /* get priority */
	    error(EXIT_FAILURE, errno, "can't get current priority");
	params.priority = xparam.param.sched_priority;
    }
    else
	if ( !params.prio_present )
	    params.priority = params.prio_min;	/* default priority */
    /* - get current priority or set default */

    xparam.param.sched_priority = params.priority;

    /*--------------------------------------------------------------------*/

    if (params.show)
    {
	fprintf(stdout,
		"process id %d, policy %s, priority %d, "
		"priority range %d..%d\n",
		params.pid, strpolicy(params.policy), params.priority,
		params.prio_min, params.prio_max);

	check_priority_limits( &params );

	return EXIT_SUCCESS;	/* show done */
    }
    else
	if ( check_priority_limits( &params ) )
	    return EXIT_FAILURE;


    if (params.verbose)
    {
	if (params.set)
	    fprintf(stdout,
		    "Setting process %d with policy %s, priority %d.\n",
		    params.pid, strpolicy(params.policy), params.priority );
	else
	    fprintf(stdout,
		    "Starting %s with policy %s, priority %d, "
		    "real uid %u, real gid %u.\n",
		    av[optind], strpolicy(params.policy), params.priority,
		    getuid(), getgid() );
    }


    if ( sched_setscheduler(params.set ? params.pid : 0,
			    params.policy, &xparam.param) )
	error(EXIT_FAILURE, errno, "can't set priority and policy");

    if (params.set) return EXIT_SUCCESS;	/* set done */


    execvp( av[optind], av+optind );

    /* exec error */
    error( EXIT_FAILURE, errno, "can't exec %s", av[optind] );
    return EXIT_FAILURE; /* supress compiler warning */
}
