PKG_HAS_NO_SOURCES = defined

include $(DEFS)


build : $(TOOLCHAIN_INSTALL_STAMP)
build : $(call tstamp_f,configure,linux)

STD_CPPFLAGS += -I$(LK_INC_DIR)
define BUILD_CMD
  '$(CC)' $(STD_CPPFLAGS) $(STD_CFLAGS) $(STD_LDFLAGS) \
	$(PKG_PKG_DIR)/$(PKG).c -o $(IBIN_DIR)/$(PKG) $(LOG)
endef

    CLEAN_CMD = $(RM) $(IBIN_DIR)/$(PKG)
DISTCLEAN_CMD = $(CLEAN_CMD)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(IBIN_DIR)/$(PKG) $(RSBIN_DIR) $(LOG)


include $(RULES)
