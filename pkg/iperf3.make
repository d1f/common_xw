PKG_TYPE          = DEBIAN3
PKG_VERSION       = 3.0.7
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/i/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain)
configure : $(call bstamp_f,install,gawk)

ifneq ($(CONFIG_WITH_IPv6),y)
 configure_opts += --disable-ipv6
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ac_cv_func_fork_works=yes		\
     ac_cv_func_malloc_0_nonnull=yes	\
     ac_cv_func_memcmp_working=yes	\
     ac_cv_func_pthread_cancel=yes	\
     ./configure -C $(LOG)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--prefix=$(INST_DIR)		\
	$(configure_opts)		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libiperf.so* $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(IBIN_DIR)/iperf3       $(RBIN_DIR) $(LOG)
endef


include $(RULES)
