PKG_TYPE      = DEBIAN1
PKG_VERSION   = 0.33.6
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/p/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


include $(TOOLS_PKG_DIR)/quilt.mk


AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


configure : $(call bstamp_f,install,libpng pkg-config)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR)	&&	\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--disable-shared	\
	--enable-libpng		\
	--with-gnu-ld		\
	$(STD_VARS)
endef


BUILD_CMD = $(BUILD_CMD_DEFAULT)


define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(IW) $(LN_S) libpixman-1.a $(ILIB_DIR)/libpixman.a $(LOG)
endef


include $(RULES)
