PKG_TYPE      = DEBIAN3
PKG_VERSION   = 5.9
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/m/$(PKG_BASE)

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     libmonit_cv_setjmp_available=yes	\
     libmonit_cv_vsnprintf_c99_conformant=yes \
     $(PKG_SRC_DIR)/configure -C $(LOG)	\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--prefix=$(INST_DIR)		\
	--enable-shared			\
	--with-gnu-ld			\
	--without-pam			\
	--without-ssl			\
	$(STD_VARS)
endef
#	--enable-optimized		\


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
