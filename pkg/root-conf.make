PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install-root : $(call bstamp_f,install,sysvbanner)
Label ?= Sigrand

define INSTALL_ROOT_CMD
  $(MKDIR_P)                                         $(RCONF_DIR) $(LOG)
  $(IWSH) 'echo "PLATFORM=$(PLATFORM)"             > $(RCONF_DIR)/platform'
  printf "$(foreach prm,$(PLATFORM_PARAMS),$(if $($(prm)),$(prm)=$($(prm))\n))" >> $(RCONF_DIR)/platform

  $(IWSH) 'echo "$(PRJ_RELEASE).`$(XWMAKE_SCRIPT_DIR)/git-rev`" > $(RCONF_DIR)/release'

  $(IWSH) 'banner $(Label)                          > $(RCONF_DIR)/banner'
  echo "$(Label) router platform"                  >> $(RCONF_DIR)/banner
  echo "Release: `cat $(RCONF_DIR)/release`"       >> $(RCONF_DIR)/banner
  echo "Built at: `date -u '+%Y-%m-%d %H:%M'` UTC" >> $(RCONF_DIR)/banner
  echo ""                                          >> $(RCONF_DIR)/banner
endef


include $(RULES)
