# all dffont and its dependencies

PKG_HAS_NO_SOURCES = defined

include $(DEFS)


list = dffont% %cairo unicode pixman freetype libpng
NESTED_PKGES = $(filter $(list),$(filter-out all% %all,$(ALL_PKGES)))


include $(RULES)
