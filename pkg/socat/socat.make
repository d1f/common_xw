PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.7.2.4
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/s/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES += $(PKG_PKG_DIR)/*.patch


configure : $(TOOLCHAIN_INSTALL_STAMP)

# -w prevents configure to detect correct size_t,mode_t and other size
STD_CFLAGS := $(subst -w,,$(STD_CFLAGS))

ifeq ($(PLATFORM),build)
  build_host_prefix  = --prefix=$(INST_DIR)
else
  build_host_prefix  = --prefix=$(INST_DIR)
  build_host_prefix += --build=$(BUILD) --host=$(TARGET)
endif

define CONFIGURE_CMD
  $(RM)	$(PKG_BLD_DIR)/config.cache		\
	$(PKG_BLD_DIR)/config.status
  cd	$(PKG_BLD_DIR)		&&		\
    sc_cv_termios_ispeed="no"			\
    sc_cv_type_sizet_basic="0"			\
    sc_cv_type_modet_basic="0"			\
    sc_cv_type_pidt_basic="0"			\
    sc_cv_type_uidt_basic="0"			\
    sc_cv_type_gidt_basic="0"			\
    sc_cv_type_timet_basic="0"			\
    sc_cv_type_socklent_basic="0"		\
    sc_cv_type_off_basic="0"			\
    sc_cv_type_off64_basic="0"			\
    sc_cv_type_dev_basic="0"			\
    sc_cv_type_stat_stino_basic="0"		\
    sc_cv_type_stat_stnlink_basic="0"		\
    sc_cv_type_stat_stsize_basic="0"		\
    sc_cv_type_stat_stblksize_basic="0"		\
    sc_cv_type_stat_stblocks_basic="0"		\
    sc_cv_type_stat64_stdev_basic="0"		\
    sc_cv_type_stat64_stino_basic="0"		\
    sc_cv_type_stat64_stnlink_basic="0"		\
    sc_cv_type_stat64_stsize_basic="0"		\
    sc_cv_type_stat64_stblksize_basic="0"	\
    sc_cv_type_stat64_stblocks_basic="0"	\
    sc_cv_type_struct_timeval_tv_usec="0"	\
    sc_cv_type_rlimit_rlimmax_basic="0"		\
    sc_cv_typeof_struct_cmsghdr_cmsg_len="0"	\
    ./configure -C $(LOG)			\
	$(build_host_prefix)			\
	--disable-ip6				\
	--disable-ext2				\
	--disable-readline			\
	--disable-openssl			\
	--disable-fips				\
	$(STD_VARS)
endef


define  BUILD_CMD
  $(SED) -i -r -e '\
s/CRDLY_SHIFT *$$/CRDLY_SHIFT 9/;\
s/TABDLY_SHIFT *$$/TABDLY_SHIFT 11/;\
s/CSIZE_SHIFT *$$/CSIZE_SHIFT 4/;' \
	$(PKG_BLD_DIR)/config.h

  $(BUILD_CMD_DEFAULT)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(IBIN_DIR)/socat $(RBIN_DIR) $(LOG)


include $(RULES)
