PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install-root : $(call tstamp_f,install-root,stunnel openssl-0.9 openssl-init)

define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/stunnel.conf $(RWEB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_PKG_DIR)/stunnel.rc  $(RINIT_DIR) $(LOG)
endef


include $(RULES)
