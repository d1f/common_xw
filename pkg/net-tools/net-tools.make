# Note: Some of net-tools may conflict with busybox applets:
#	ifconfig nameif ...

PKG_TYPE      = DEBIAN1
PKG_VERSION   = 1.60
PKG_VER_PATCH = -26
PKG_SITE_PATH = pool/main/n/$(PKG_BASE)

include $(DEFS)


include $(TOOLS_PKG_DIR)/quilt.mk


configure : $(call tstamp_f,install,toolchain)

define CONFIGURE_CMD
  cp -v	$(PKG_PKG_DIR)/net-tools.config.h	\
	   $(PKG_BLD_DIR)/config.h				$(LOG)
  cp -v	$(PKG_PKG_DIR)/net-tools.config.mk	\
	   $(PKG_BLD_DIR)/config.make				$(LOG)
  touch $(PKG_BLD_DIR)/config.h $(PKG_BLD_DIR)/config.make	$(LOG)
endef


MK_VARS  = COPTS="$(STD_CFLAGS) $(STD_CPPFLAGS) -D_GNU_SOURCE"
MK_VARS += CC=$(CC)
MK_VARS += AR=$(AR)
MK_VARS += INSTALL_PROGRAM="install -p -m 755" BASEDIR=$(INST_DIR)

define BUILD_CMD
  $(DOMAKE) $(MKJ) -C $(PKG_BLD_DIR) config.h version.h $(MK_VARS) $(LOG)
  $(DOMAKE) $(MKJ) -C $(PKG_BLD_DIR) libdir             $(MK_VARS) $(LOG)
  $(BUILD_CMD_DEFAULT)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD_TARGET = clobber
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD_TARGET = installbin
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

sbins = slattach route rarp plipconfig nameif mii-tool iptunnel ipmaddr ifconfig arp
 bins = ypdomainname nisdomainname netstat hostname domainname dnsdomainname

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(addprefix $(ISBIN_DIR)/,$(sbins)) $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(addprefix  $(IBIN_DIR)/,$(bins))   $(RBIN_DIR) $(LOG)
endef


include $(RULES)
