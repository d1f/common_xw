PKG_VERSION   = 1.14
PKG_SITES     = $(GNU_ORG_SITES)
PKG_SITE_PATH = $(PKG_BASE)

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call bstamp_f,install,sed gawk libtool)

define CONFIGURE_CMD
 cd $(PKG_BLD_DIR) &&				\
	ac_cv_func_malloc_0_nonnull=yes		\
	ac_cv_func_realloc_0_nonnull=yes	\
	./configure -C		$(LOG)		\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=$(INST_DIR)			\
	--disable-extra-encodings		\
	--enable-shared --disable-static	\
	--disable-nls				\
	--with-gnu-ld				\
	--without-libiconv-prefix		\
	--without-libintl-prefix		\
	$(STD_VARS)
endef


    BUILD_CMD =   $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =   $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD =   $(CLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libiconv.so* $(RLIB_DIR) $(LOG)


include $(RULES)
