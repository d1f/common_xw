#ifndef  LIBswAACenc_H
# define LIBswAACenc_H

# include <stddef.h> // size_t


// returns 0 on success or -1 on error
int  swAACenc_create(size_t sample_rate, size_t bit_rate);

void swAACenc_delete (void);

// returns encoded length or -1 on error
int  swAACenc_process(void *dst, size_t dstlen, const void *src, size_t srclen);


#endif //LIBswAACenc_H
