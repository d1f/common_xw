#include <swaacenc.h>
#include <df/log.h>
#include <signal.h> // sig_atomic_t

#include <vo-aacenc/voAAC.h>
#include <vo-aacenc/cmnMemory.h>


static VO_AUDIO_CODECAPI codec_api;

static VO_MEM_OPERATOR mem_operator =
{
    .Alloc = cmnMemAlloc, .Copy = cmnMemCopy, .Free = cmnMemFree,
    .Set = cmnMemSet, .Check = cmnMemCheck
};

static VO_CODEC_INIT_USERDATA user_data =
{
    .memflag = VO_IMF_USERMEMOPERATOR, .memData = &mem_operator
};

static VO_HANDLE handle = 0;

static sig_atomic_t use_count = 0;


// returns 0 on success or -1 on error
int swAACenc_create(size_t sample_rate, size_t bit_rate)
{
    if (++use_count == 1)
    {
	voGetAACEncAPI(&codec_api);
	codec_api.Init(&handle, VO_AUDIO_CodingAAC, &user_data);

	AACENC_PARAM params =
	{
	    .sampleRate = sample_rate, .bitRate = bit_rate, .nChannels = 1, .adtsUsed = 1
	};
	if (codec_api.SetParam(handle, VO_PID_AAC_ENCPARAM, &params) != VO_ERR_NONE)
	{
	    dflog(LOG_ERR, "%s(): Unable to set encoding parameters", __func__);
	    codec_api.Uninit(handle);
	    --use_count;
	    return -1;
	}
    }

    return 0;
}

void swAACenc_delete(void)
{
    if (--use_count == 0)
	codec_api.Uninit(handle);
    else if (use_count < 0)
	use_count=0;
}

// returns encoded length or -1 on error
int swAACenc_process(void *dst, size_t dstlen, const void *src, size_t srclen)
{
    VO_CODECBUFFER input  = { .Buffer = src, .Length = srclen, .Time = 0 };
    codec_api.SetInputData(handle, &input);

    VO_CODECBUFFER output = { .Buffer = dst, .Length = dstlen, .Time = 0 };
    VO_AUDIO_OUTPUTINFO output_info;
    if (codec_api.GetOutputData(handle, &output, &output_info) != VO_ERR_NONE)
    {
	dflog(LOG_ERR, "%s(): Unable to encode frame", __func__);
	return -1;
    }

    return output.Length;
}
