PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.1.29
PKG_VER_PATCH = -2.1
PKG_SITE_PATH = pool/main/libx/$(PKG_BASE)

include $(DEFS)


AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


configure : $(call tstamp_f,install,toolchain libxml2)

build_host_prefix  = --prefix=$(INST_DIR)
ifneq ($(PLATFORM),build)
  build_host_prefix += --build=$(BUILD) --host=$(TARGET)
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&				\
  ac_cv_func_setpgrp_void=yes			\
	./configure -C $(LOG)			\
	$(build_host_prefix)			\
	--disable-static --enable-shared	\
	--with-gnu-ld				\
	--without-python			\
	--without-crypto			\
	--without-debug				\
	--without-mem-debug			\
	--without-debugger			\
	--with-libxml-prefix=$(INST_DIR)	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libxslt.so*  $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libexslt.so* $(RLIB_DIR) $(LOG)
endef


include $(RULES)
