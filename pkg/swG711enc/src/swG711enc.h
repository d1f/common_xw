#ifndef  LIBswG711enc_H
# define LIBswG711enc_H

# include <stddef.h> // size_t


void swG711mu_enc_init(void);
void swG711a__enc_init(void);

// returns encoded length or -1 on error
int  swG711_process(void *dst, const void *src, size_t srclen);


#endif //LIBswG711enc_H
