#include <swG711enc.h>
#include <g711table.h>
#include <SnackG711.h>

void swG711mu_enc_init(void)
{
    g711table_init(Snack_Lin2Mulaw);
}

void swG711a__enc_init(void)
{
    g711table_init(Snack_Lin2Alaw);
}

// returns encoded length or -1 on error
int  swG711_process(void *dst, const void *src, size_t srclen)
{
    g711table_convert_array(dst, src, srclen/2);
    return srclen/2;
}
