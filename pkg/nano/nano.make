PKG_TYPE      = DEBIAN3
PKG_VERSION   = 2.8.0
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/n/$(PKG_BASE)

include $(DEFS)


include $(TOOLS_PKG_DIR)/configure-menuconfig.mk


configure : $(call tstamp_f,install,toolchain ncurses)
install-root : $(call tstamp_f,install-root,ncurses)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure	$(LOG)	\
	--prefix=/				\
	--sysconfdir=/etc			\
	--localedir=/share/locale		\
	--build=$(BUILD) --host=$(TARGET)	\
	$(CONFIGURE_OPTIONS)			\
	   --includedir=$(IINC_DIR)		\
	--oldincludedir=$(IINC_DIR)		\
	--enable-nanorc --enable-color		\
	$(STD_VARS)
endef


    MK_VARS += prefix=$(INST_DIR)
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


nanorc = xml sh nanorc java javascript json html css awk

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(IBIN_DIR)/nano $(RBIN_DIR) $(LOG)
  $(IW) $(INST_FILES) $(addsuffix .nanorc,$(addprefix $(ISHARE_DIR)/nano/,$(nanorc))) \
						$(RSHARE_DIR)/nano $(LOG)
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/nanorc $(RETC_DIR) $(LOG)
endef


include $(RULES)
