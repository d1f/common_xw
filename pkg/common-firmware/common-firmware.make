PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install : $(call tstamp_f,install-root,mtd-utils) # for factory-reset, write-kernel
install : $(call tstamp_f,install-root,common-init)

scripts = write-kernel write-root extract-firmware factory-reset

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(addprefix $(PKG_PKG_DIR)/,$(scripts)) $(RSBIN_DIR) $(LOG)
endef


include $(RULES)
