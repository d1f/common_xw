PKG_TYPE      = DEBIAN3
PKG_VER_MAJOR = 9
PKG_VERSION   = 9a
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/libj/$(PKG_BASE)

include $(DEFS)


configure : $(call bstamp_f,install,autotools-dev)
configure : $(TOOLCHAIN_INSTALL_STAMP)

define CONFIGURE_CMD
  $(RM)       $(PKG_BLD_DIR)/config.sub   $(PKG_BLD_DIR)/config.guess $(LOG)
  $(LN_S) $(BSHARE_DIR)/misc/config.sub   $(PKG_BLD_DIR)/config.sub   $(LOG)
  $(LN_S) $(BSHARE_DIR)/misc/config.guess $(PKG_BLD_DIR)/config.guess $(LOG)

  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	     --prefix=$(INST_DIR)		\
	--build=$(BUILD) --host=$(TARGET)	\
	--disable-shared			\
	--enable-static				\
	--with-gnu-ld				\
	$(STD_VARS)
endef

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
