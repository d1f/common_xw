PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.1
PKG_VER_PATCH     = -8
PKG_BASE          = libnl
PKG_SITE_PATH     = pool/main/libn/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES += $(wildcard $(PKG_PKG_DIR)/*.patch)


configure : $(TOOLCHAIN_INSTALL_STAMP) $(call tstamp_f,depend,linux)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT); $(IW) chmod +x $(ILIB_DIR)/libnl.so.* $(LOG)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libnl.so* $(RLIB_DIR) $(LOG)


include $(RULES)
