PKG_TYPE      = DEBIAN3
pkg_ver_major = 1.53
PKG_VERSION   = $(pkg_ver_major).0
PKG_VER_PATCH = -4
PKG_SITE_PATH = pool/main/b/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain sed)

BOOST_NARCH = $(subst x86_64,x86,$(subst i386,x86,$(NARCH)))

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./bootstrap.sh --prefix=$(INST_DIR) --with-toolset=gcc $(LOG)
  echo "using gcc : $(BOOST_NARCH) : $(CROSS_PREFIX)g++ ;" >> $(PKG_BLD_DIR)/project-config.jam
  $(SED) -r -e 's/using gcc ;//' -i $(PKG_BLD_DIR)/project-config.jam
endef


define BUILD_CMD
  cd $(PKG_BLD_DIR) && ./b2 \
	variant=release link=shared runtime-link=shared threading=multi \
	architecture=$(BOOST_NARCH) \
	cflags="$(STD_CFLAGS) $(STD_CPPFLAGS)" \
	cxxflags="$(STD_CXXFLAGS) $(STD_CPPFLAGS)" \
	linkflags="$(STD_LDFLAGS)" \
	--with-system --with-thread $(LOG)
endef

    CLEAN_CMD = cd $(PKG_BLD_DIR) && ./b2 --clean-all $(LOG)
DISTCLEAN_CMD = $(CLEAN_CMD)
  INSTALL_CMD = $(IWSH) "cd $(PKG_BLD_DIR) && ./b2 install --with-system --with-thread" $(LOG)

INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libboost*.so* $(RLIB_DIR) $(LOG)


include $(RULES)
