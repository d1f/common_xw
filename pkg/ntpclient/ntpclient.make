pkg_ver_year  = 2010
pkg_ver_day   = 365
PKG_VERSION   = $(pkg_ver_year)_$(pkg_ver_day)
PKG_INFIX     = _
PKG_SITES     = http://doolittle.icarus.com
PKG_SITE_PATH = ntpclient

include $(DEFS)


PKG_PATCH_FILES = $(PKG_PKG_DIR)/*.patch

ENV_VARS = $(STD_VARS)


    build :  $(TOOLCHAIN_INSTALL_STAMP)
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/ntpclient $(RSBIN_DIR) $(LOG)
endef


include $(RULES)
