PKG_TYPE          = DEBIAN3
PKG_VERSION       = 2.24
PKG_VER_PATCH     = -6
PKG_SITE_PATH     = pool/main/libc/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES = $(PKG_PKG_DIR)/$(PKG)_*.patch


build : $(call tstamp_f,install,toolchain)
build : $(call tstamp_f,configure,linux)

MK_VARS += topdir=$(PKG_BLD_DIR)
MK_VARS += FAKEROOT=$(INST_DIR)
MK_VARS += lib=lib prefix=
MK_VARS += PAM_CAP=no LIBATTR=no RAISE_SETFCAP=no
MK_VARS += USE_GPERF_OUTPUT= INCLUDE_GPERF_OUTPUT=
MK_VARS += KERNEL_HEADERS=$(LK_INC_DIR)
MK_VARS += SYSTEM_HEADERS=$(IINC_DIR)
MK_VARS += DEBUG="" WARNINGS=""
MK_VARS += $(STD_VARS)
MK_VARS += "BUILD_CFLAGS=\$$(IPATH)"
MK_VARS += LD="$(CC) -Wl,-x -shared"

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libcap.so* $(RLIB_DIR) $(LOG)


include $(RULES)
