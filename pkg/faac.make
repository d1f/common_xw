PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.28
PKG_VER_PATCH = -6
PKG_SITE_PATH = pool/non-free/f/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES += $(PKG_PKG_DIR)/$(PKG)*.patch


AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


configure : $(call tstamp_f,install,toolchain)
configure : $(call bstamp_f,install,gawk sed)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)			\
	--build=$(BUILD) --host=$(TARGET)	\
	--disable-drm				\
	--enable-shared --disable-static	\
	--without-mp4v2				\
	--with-gnu-ld				\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD  = $(IW) $(INSTxFILES) $(ILIB_DIR)/libfaac.so* $(RLIB_DIR) $(LOG)


include $(RULES)
