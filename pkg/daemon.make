PKG_TYPE          = DEBIAN3
PKG_VERSION       = 0.6.4
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/d/$(PKG_BASE)

include $(DEFS)


build : $(call tstamp_f,install,toolchain)

MK_VARS += $(STD_VARS) PREFIX=$(INST_DIR)
MK_VARS += DEB_BUILD_OPTIONS=nostrip

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(IBIN_DIR)/daemon $(RBIN_DIR) $(LOG)


include $(RULES)
