PKG_TYPE      = DEBIAN3
PKG_VERSION   = 007
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/u/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain pkg-config libusb-1.0)
configure : $(call bstamp_f,install,gawk)

STD_VARS += LIBS="-lusb-1.0 -lpthread -lrt"

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C $(LOG)	\
	--prefix=		\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--disable-zlib		\
	--enable-usbids		\
	$(STD_VARS)
endef


MK_VARS += DESTDIR=$(INST_DIR)
#MK_VARS += AM_DEFAULT_VERBOSITY=1

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(IBIN_DIR)/lsusb       $(RBIN_DIR) $(LOG)
 #$(IW) $(INSTxFILES) $(IBIN_DIR)/usb-devices $(RBIN_DIR) $(LOG)
 #$(IW) $(INSTxFILES) $(IBIN_DIR)/usbhid-dump $(RBIN_DIR) $(LOG)
  $(IW) $(INST_FILES) $(ISHARE_DIR)/usb.ids $(RSHARE_DIR) $(LOG)
endef


include $(RULES)
