# now only for libmd5.a
PKG_TYPE      = DEBIAN3
PKG_VERSION   = 4.2
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/m/$(PKG_BASE)

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call bstamp_f,install,sed)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure	$(LOG)	\
	--prefix=$(INST_DIR)			\
	--build=$(BUILD) --host=$(TARGET)	\
	$(STD_VARS)
endef

define BUILD_CMD
  $(DOMAKE) $(MKJ) -C $(PKG_BLD_DIR)/src                         md5.o $(LOG)
  $(AR) rcsf          $(PKG_BLD_DIR)/libmd5.a $(PKG_BLD_DIR)/src/md5.o $(LOG)
  #$(BUILD_CMD_DEFAULT)
endef

define INSTALL_CMD
  $(IW) $(INST_FILES) $(PKG_BLD_DIR)/libmd5.a $(ILIB_DIR)     $(LOG)
  $(IW) $(INST_FILES) $(PKG_BLD_DIR)/src/*.h  $(IINC_DIR)/md5 $(LOG)
  #$(INSTALL_CMD_DEFAULT)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
