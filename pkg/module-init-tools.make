PKG_TYPE      = DEBIAN1
PKG_VERSION   = 3.12
PKG_VER_PATCH = -2
PKG_SITE_PATH = pool/main/m/$(PKG_BASE)

include $(DEFS)


include $(TOOLS_PKG_DIR)/quilt.mk

configure : $(TOOLCHAIN_INSTALL_STAMP)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=$(INST_DIR)			\
	--disable-zlib				\
	--disable-static-utils			\
	$(STD_VARS) DOCBOOKTOMAN=true
endef


MK_VARS = AR=$(AR)
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

sbins = rmmod modprobe modinfo insmod depmod
 bins = lsmod
define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(addprefix $(ISBIN_DIR)/,$(sbins)) $(RSBIN_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(addprefix $(IBIN_DIR)/,$(bins))    $(RBIN_DIR) $(LOG)
endef


include $(RULES)
