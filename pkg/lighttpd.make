PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.4.35
PKG_VER_PATCH = -3
PKG_SITE_PATH = pool/main/l/$(PKG_BASE)

include $(DEFS)


configure : $(call tstamp_f,install,toolchain)
#configure : $(call tstamp_f,install,php5 sqlite3)

conf_opts += $(if $(CONFIG_WITH_IPv6), --enable-ipv6, --disable-ipv6)

ifeq ($(CONFIG_LIGHTTPD_WITH_OPENSSL),y)
 configure : $(call tstamp_f,install,openssl-0.9)
 conf_opts += --with-openssl
 conf_opts += --with-openssl-includes=$(IINC_DIR)/openssl
 conf_opts += --with-openssl-libs=$(RLIB_DIR)
else
 conf_opts += --without-openssl
endif

conf_opts += $(if $(CONFIG_LIGHTTPD_WITH_KERBEROS), --with-kerberos5, --without-kerberos5)

ifeq ($(CONFIG_LIGHTTPD_WITH_PCRE),y)
 configure : $(call tstamp_f,install,pcre3)
 conf_opts += --with-pcre
 STD_VARS  += PCRECONFIG=$(IBIN_DIR)/pcre-config
else
 conf_opts += --without-pcre
endif

ifeq ($(CONFIG_LIGHTTPD_WITH_ZLIB),y)
 configure : $(call tstamp_f,install,zlib)
 conf_opts += --with-zlib
else
 conf_opts += --without-zlib
endif

ifeq ($(CONFIG_LIGHTTPD_WITH_BZIP2),y)
 configure : $(call tstamp_f,install,bzip2)
 conf_opts += --with-bzip2
else
 conf_opts += --without-bzip2
endif

# FIXME: dependencies
conf_opts += $(if $(CONFIG_LIGHTTPD_WITH_LIBEV)   , --with-libev    , --without-libev)
	# FIXME: =PATH to include and lib
conf_opts += $(if $(CONFIG_LIGHTTPD_WITH_MYSQL)   , --with-mysql    , --without-mysql)
	# FIXME: =PATH to mysql_config
conf_opts += $(if $(CONFIG_LIGHTTPD_WITH_LDAP)    , --with-ldap     , --without-ldap)
conf_opts += $(if $(CONFIG_LIGHTTPD_WITH_ATTR)    , --with-attr     , --without-attr)
conf_opts += $(if $(CONFIG_LIGHTTPD_WITH_FAM)     , --with-fam      , --without-fam)
conf_opts += $(if $(CONFIG_LIGHTTPD_WITH_GDBM)    , --with-gdbm     , --without-gdbm)
conf_opts += $(if $(CONFIG_LIGHTTPD_WITH_MEMCACHE), --with-memcache , --without-memcache)
conf_opts += $(if $(CONFIG_LIGHTTPD_WITH_LUA)     , --with-lua      , --without-lua)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
     ./configure		$(LOG)	\
	--prefix=			\
	--libdir=/lib/lighttpd		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--disable-static		\
	--enable-shared			\
	--disable-lfs			\
	--with-gnu-ld			\
	--without-valgrind		\
	$(conf_opts)			\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


mod_list_f = $(eval mod_list += $(if $(CONFIG_LIGHTTPD_MOD_$(1)),$(1)))

all_modules += access accesslog alias auth cgi cml compress dirlisting evasive
all_modules += evhost expire extforward fastcgi flv_streaming indexfile magnet
all_modules += mysql_vhost proxy redirect rewrite rrdtool scgi secdownload setenv
all_modules += simple_vhost ssi staticfile status trigger_b4_dl userdir usertrack
all_modules += webdav

$(foreach mod, $(all_modules), $(call mod_list_f,$(mod)))

ifneq ($(strip $(mod_list)),)
define MOD_INSTALL_CMD
 $(IW) $(INSTxFILES) $(addprefix \
	$(ILIB_DIR)/lighttpd/mod_,$(addsuffix .so,$(mod_list))) \
	$(RLIB_DIR)/lighttpd		$(LOG)
endef
endif

INSTALL_MK_VARS = DESTDIR=$(INST_DIR)
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
 $(IW) $(INSTxFILES) $(ISBIN_DIR)/lighttpd $(RSBIN_DIR) $(LOG)
 $(MOD_INSTALL_CMD)
endef


include $(RULES)
