PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install : $(call bstamp_f,install,sed)
install : $(call tstamp_f,install-root,busybox reverse rcorder)

define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/init/* $(RINIT_DIR) $(LOG)

  $(IW) $(MKDIR_P) $(RETC_DIR) $(LOG)
  $(IWSH) "$(SED) -e 's/@TTY@/$(CONS_DEV)/g;s/@SPEED@/$(CONS_SPEED)/g' \
		$(PKG_PKG_DIR)/inittab > $(RETC_DIR)/inittab"
endef


include $(RULES)
