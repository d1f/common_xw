PKG_TYPE      = DEBIAN3
PKG_VERSION   = 2.11.0
PKG_VER_PATCH = -6.3
PKG_SITE_PATH = pool/main/f/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


configure : $(call bstamp_f,install,gawk sed freetype pkg-config gperf libxml2)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR)	&&	\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--disable-shared	\
	--enable-static		\
	--enable-libxml2	\
	--disable-docs		\
	--with-gnu-ld		\
	--without-libiconv	\
	--without-expat		\
	--with-default-fonts=/usr/share/fonts/type1/gsfonts \
	$(STD_VARS)
endef


  BUILD_CMD =   $(BUILD_CMD_DEFAULT)
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)


include $(RULES)
