PKG_TYPE      = DEBIAN3
PKG_VERSION   = 9
PKG_VER_PATCH = -3
PKG_SITE_PATH = pool/main/k/$(PKG_BASE)

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call tstamp_f,install,linux)
configure : $(call bstamp_f,install,sed gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&		\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--build=$(BUILD)	\
	--host=$(TARGET)	\
	--disable-shared	\
	--with-gnu-ld		\
	--with-rootprefix=""	\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(IBIN_DIR)/kmod $(ROOT_DIR)/kmod          $(LOG)
  $(IW) $(LN_S)                   kmod $(ROOT_DIR)/kmod/lsmod    $(LOG)
  $(IW) $(LN_S)                   kmod $(ROOT_DIR)/kmod/rmmod    $(LOG)
  $(IW) $(LN_S)                   kmod $(ROOT_DIR)/kmod/insmod   $(LOG)
  $(IW) $(LN_S)                   kmod $(ROOT_DIR)/kmod/modinfo  $(LOG)
  $(IW) $(LN_S)                   kmod $(ROOT_DIR)/kmod/modprobe $(LOG)
  $(IW) $(LN_S)                   kmod $(ROOT_DIR)/kmod/depmod   $(LOG)
endef


include $(RULES)
