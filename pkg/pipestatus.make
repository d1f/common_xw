PKG_VERSION   = 0.6.0
PKG_SITES     = $(SF_NET_SITES)
PKG_SITE_PATH = $(PKG_BASE)
PKG_REMOTE_FILE_SUFFICES = .tar.gz

PLATFORM      = build

include $(DEFS)


INSTALL_CMD =   $(IW) $(INST_FILES) $(PKG_BLD_DIR)/pipestatus $(BBIN_DIR) $(LOG)


include $(RULES)
