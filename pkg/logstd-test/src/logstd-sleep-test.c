#include <stdio.h>
#include <stdlib.h> // atoi
#include <unistd.h>

int main(int ac, char *av[])
{
    if (ac != 2)
    {
	fprintf(stderr, "Usage: %s seconds\n", av[0]);
	return EXIT_FAILURE;
    }

    int secs = atoi(av[1]);

    sleep(secs);
    return EXIT_SUCCESS;
}
