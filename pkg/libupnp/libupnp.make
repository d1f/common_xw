PKG_TYPE      = DEBIAN1
PKG_VERSION   = 1.6.6
PKG_VER_PATCH = -5
PKG_SITE_PATH = pool/main/libu/$(PKG_BASE)

include $(DEFS)


include $(TOOLS_PKG_DIR)/quilt.mk

PKG_PATCH2_FILES = $(PKG_PKG_DIR)/*.patch


AC_VER = 2.69
AM_VER = 1.11
include $(TOOLS_PKG_DIR)/autotools.mk
PRE_CONFIGURE_CMD = $(AUTOTOOLS_CMD)


configure : $(call tstamp_f,install,toolchain)
configure : $(call bstamp_f,install,gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)			\
	--build=$(BUILD) --host=$(TARGET)	\
	--enable-shared				\
	--disable-static			\
	--disable-debug				\
	--disable-webserver			\
	--disable-tools				\
	--disable-client			\
	--disable-samples			\
	--without-documentation			\
	--with-gnu-ld				\
	$(STD_VARS)
endef
#--disable-device


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

define  INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libupnp.so*       $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libthreadutil.so* $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libixml.so*       $(RLIB_DIR) $(LOG)
endef


include $(RULES)
