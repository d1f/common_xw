PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.7.0
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/libo/$(PKG_BASE)

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call bstamp_f,install,libtool)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
  ac_cv_func_malloc_0_nonnull=yes	\
  ./configure $(LOG)			\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--disable-shared		\
	--enable-static			\
	--disable-debug			\
	--without-pic			\
	--with-gnu-ld			\
	--without-perl-bindings		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)


include $(RULES)
