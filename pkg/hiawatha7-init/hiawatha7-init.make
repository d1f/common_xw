PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install : $(call tstamp_f,install-root,hiawatha7 mime-support)

define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/hiawatha.conf $(ROOT_DIR)/conf $(LOG)
  $(call INSTALL_PKGPKG_INIT_XFILES_F, hiawatha.rc)
  $(IW) $(MKDIR_P) $(RWEB_DIR)/nonssl $(LOG)
endef


include $(RULES)
