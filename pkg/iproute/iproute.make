# iproute AKA iproute2

PKG_TYPE          = DEBIAN3
PKG_VERSION       = 20120521
PKG_VER_PATCH     = -3
PKG_SITE_PATH     = pool/main/i/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES = $(PKG_PKG_DIR)/*.patch


configure : $(call tstamp_f,install,toolchain db)
configure : $(call bstamp_f,install,sed)
    build : $(call bstamp_f,install,flex)

ifneq ($(CONFIG_WITH_IPv6),y)
  STD_CFLAGS += -DND_OPT_PREFIX_INFORMATION=3

  define ipv6_off_cmd
    $(RELINK)                                        $(PKG_BLD_DIR)/ip/Makefile
    $(SED) -r -i -e 's/ip6tunnel.o//g'               $(PKG_BLD_DIR)/ip/Makefile
    $(RELINK)                                        $(PKG_BLD_DIR)/ip/iptunnel.c
    $(SED) -r -i -e 's/do_ip6tunnel/do_del/g'        $(PKG_BLD_DIR)/ip/iptunnel.c
    $(RELINK)                                        $(PKG_BLD_DIR)/ip/ipprefix.c
    $(SED) -r -i -e 's,#include <netinet/icmp6.h>,,' $(PKG_BLD_DIR)/ip/ipprefix.c
  endef
endif

define CONFIGURE_CMD
  $(ipv6_off_cmd)

  echo "TC_CONFIG_ATM:=n"           > $(PKG_BLD_DIR)/Config
  echo "TC_CONFIG_XT:=n"           >> $(PKG_BLD_DIR)/Config
  echo "TC_CONFIG_XT_OLD:=n"       >> $(PKG_BLD_DIR)/Config
  echo "TC_CONFIG_XT_OLD_H:=n"     >> $(PKG_BLD_DIR)/Config
  echo "IPT_LIB_DIR=/lib/iptables" >> $(PKG_BLD_DIR)/Config
endef


# glibc 2.2.* does not have this constant
ifeq ($(LIBC_TYPE),g)
ifneq ($(filter 2.2.%,$(LIBC_VERSION)),)
  sctp = -DIPPROTO_SCTP=132
endif
endif

MK_VARS  = CC=$(CC) AR=$(AR) HOSTCC=$(BUILD_CC)
MK_VARS += DESTDIR=$(INST_DIR) LIBDIR=/lib
MK_VARS += DBM_INCLUDE=$(IINC_DIR) # db_185.h
MK_VARS += CCOPTS="-D_GNU_SOURCE $(STD_CFLAGS) $(sctp)"
MK_VARS += LIBUTIL=../lib/libutil.a
#KERNEL_INCLUDE=./include

ENV_VARS += LDFLAGS="$(STD_LDFLAGS)"

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES)	$(PKG_BLD_DIR)/tc/tc \
			$(PKG_BLD_DIR)/ip/ip \
						$(RSBIN_DIR)	$(LOG)
endef


include $(RULES)
