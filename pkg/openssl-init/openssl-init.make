PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install-root : $(call tstamp_f,install-root,openssl-0.9)

define INSTALL_ROOT_CMD
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/ssl.conf     $(RCONF_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_PKG_DIR)/openssl-keys $(RINIT_DIR) $(LOG)
endef


include $(RULES)
