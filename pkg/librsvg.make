PKG_TYPE      = DEBIAN3
PKG_VERSION   = 2.40.11
PKG_VER_PATCH = -1
PKG_SITE_PATH = pool/main/libr/$(PKG_BASE)
PLATFORM      = build

include $(DEFS)


configure : $(call bstamp_f,install,gawk sed pkg-config)
configure : $(call bstamp_f,install,glib2 libxml2)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR)	&&	\
     ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)	\
	--disable-shared	\
	--enable-static		\
	--disable-pixbuf-loader	\
	--disable-tools		\
	--disable-introspection	\
	--disable-vala		\
	--with-gnu-ld		\
	$(STD_VARS)
endef


  BUILD_CMD =   $(BUILD_CMD_DEFAULT)
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)



include $(RULES)
