# pstree,killall only
PKG_TYPE          = DEBIAN3
PKG_VERSION       = 22.21
PKG_VER_PATCH     = -2
PKG_SITE_PATH     = pool/main/p/$(PKG_BASE)

include $(DEFS)


PKG_PATCH2_FILES = $(PKG_PKG_DIR)/*.patch


configure : $(call tstamp_f,install,toolchain ncurses)
install-root : $(call tstamp_f,install-root,ncurses)

ifneq ($(CONFIG_WITH_IPv6),y)
 configure_opts += --disable-ipv6
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
	ac_cv_func_malloc_0_nonnull=yes	\
	ac_cv_func_realloc_0_nonnull=yes \
	ac_cv_func_memcmp_working=yes	\
	./configure -C	$(LOG)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--prefix=$(INST_DIR)		\
	--disable-harden-flags		\
	--disable-nls			\
	--with-gnu-ld			\
	--without-libiconv-prefix	\
	--without-libintl-prefix	\
	$(configure_opts)		\
	$(STD_VARS)
endef


bins = pstree killall
MK_VARS += bin_PROGRAMS="$(bins)"
BUILD_CMD = $(BUILD_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(addprefix $(PKG_BLD_DIR)/src/,$(bins)) $(RBIN_DIR) $(LOG)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
