PKG_TYPE          = DEBIAN3
PKG_VERSION       = 2.64
PKG_VER_PATCH     = -8
PKG_SITE_PATH     = pool/main/s/$(PKG_BASE)

include $(DEFS)


PKG_PATCH3_FILES = $(PKG_PKG_DIR)/$(PKG)*.patch


configure : $(call tstamp_f,install,toolchain gnutls26)
configure : $(call bstamp_f,install,gawk)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && $(STD_VARS)	\
     ./configure $(LOG)			\
	--prefix=""			\
	--exec-prefix=""		\
        --build=$(BUILD)		\
	--host=$(TARGET)		\
	--enable-logfile		\
	--enable-ssl			\
	--enable-md5auth
endef

#  --disable-rewrite-domain
#                          support for rewriting the sending domain
#  --enable-inet6          support for IPv6 transport


MK_VARS +=  LDFLAGS="$(STD_LDFLAGS)"
MK_VARS += CPPFLAGS="$(STD_CFLAGS) $(STD_CPPFLAGS)"

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/ssmtp $(RBIN_DIR) $(LOG)


include $(RULES)
