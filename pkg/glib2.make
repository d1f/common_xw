PKG_TYPE          = DEBIAN1
PKG_VER_MAJOR     = .0
#PKG_TYPE          = DEBIAN3
#PKG_VERSION       = 2.33.1
#PKG_VER_PATCH     = -1
#PKG_VERSION       = 2.32.3
#PKG_VER_PATCH     = -1
#PKG_VERSION       = 2.32.0
#PKG_VER_PATCH     = -4
PKG_VERSION       = 2.24.2
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/g/$(PKG_BASE)

include $(DEFS)


# these sources are generated:
define PRE_CONFIGURE_CMD
  $(RM) $(addprefix $(PKG_BLD_DIR)/gobject/,\
    stamp-gmarshal.h gobjectalias.h gobjectaliasdef.c)          $(LOG)
  $(RM) $(addprefix $(PKG_BLD_DIR)/glib/,galias.h galiasdef.c)  $(LOG)
  $(RM) $(PKG_BLD_DIR)/gio/tests/gdbus-test-codegen-generated.h $(LOG)
  $(RM) $(PKG_BLD_DIR)/gio/tests/gdbus-test-codegen-generated.c $(LOG)
  $(RM) $(PKG_BLD_DIR)/gio/tests/gdbus-object-manager-example/gdbus-example-objectmanager-generated.h $(LOG)
  $(RM) $(PKG_BLD_DIR)/gio/tests/gdbus-object-manager-example/gdbus-example-objectmanager-generated.c $(LOG)
  $(RM) $(PKG_BLD_DIR)/gio/tests/test_resources.c   $(LOG)
  $(RM) $(PKG_BLD_DIR)/gio/tests/test_resources2.c  $(LOG)
  $(RM) $(PKG_BLD_DIR)/gio/tests/test_resources2.h  $(LOG)
  $(RM) $(PKG_BLD_DIR)/gio/tests/plugin_resources.c $(LOG)
endef


configure : $(call tstamp_f,install,pkg-config zlib)
configure : $(call bstamp_f,install,gawk gettext)

  enable += debug=no
 disable += mem-pools
#disable += rebuilds
#disable += visibility
# enable += iconv-cache=no
 disable += iconv-cache
#disable += static
# enable += shared
 disable += selinux
 disable += fam
 disable += xattr
  enable += regex
#disable += Bsymbolic

#   with += runtime-libdir=RELPATH #Install runtime libraries relative to libdir
    with += libiconv=no
    with += gnu-ld
#   with += threads=posix
    with += pcre=internal
#   with += xml-catalog=CATALOG # path to xml catalog to use


conf_opts  = $(addprefix --disable-,$(disable))
conf_opts += $(addprefix --enable-,$(enable))
conf_opts += $(addprefix --with-,$(with))
conf_opts += $(addprefix --without-,$(without))
conf_opts += $(STD_VARS)
#MSGFMT=/bin/true # use gettext


ifeq ($(PLATFORM),build)
# glib-genmarshal glib-compile-schemas

configure : $(call bstamp_f,install,libffi)

define CONFIGURE_CMD
  $(RM)	$(PKG_BLD_SUB_DIR)/config.cache	  $(LOG)
  cd    $(PKG_BLD_SUB_DIR) && ./configure $(LOG) \
	--prefix=$(INST_DIR)			\
	--enable-shared				\
	--disable-static			\
	$(conf_opts)
endef

  BUILD_CMD =   $(BUILD_CMD_DEFAULT)
INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

else # PLATFORM != build

configure : $(call tstamp_f,install,toolchain libffi)
configure : $(call bstamp_f,install,glib2)

ifeq ($(LIBC_TYPE),uc)
  disable += threads
endif

define CONFIGURE_CMD
  $(RM)					    $(PKG_BLD_SUB_DIR)/config.cache $(LOG)
  echo "glib_cv_stack_grows=no"		 >> $(PKG_BLD_SUB_DIR)/config.cache
  echo "glib_cv_uscore=no"		 >> $(PKG_BLD_SUB_DIR)/config.cache
  echo "ac_cv_func_posix_getpwuid_r=yes" >> $(PKG_BLD_SUB_DIR)/config.cache
  echo "ac_cv_func_posix_getgrgid_r=yes" >> $(PKG_BLD_SUB_DIR)/config.cache

  cd $(PKG_BLD_SUB_DIR) && ./configure $(LOG)	\
	--config-cache				\
	--build=$(BUILD) --host=$(TARGET)	\
	--prefix=$(INST_DIR)			\
	--disable-static			\
	$(conf_opts)
endef

BUILD_CMD = $(BUILD_CMD_DEFAULT)

MK_VARS += CATALOGS= GMOFILES= POFILES=
MK_VARS += pkgconfigdir=$(ILIB_DIR)/pkgconfig

ifeq ($(shell $(VERCMP) $(PKG_VERSION) '>=' 2.30.0),true)
mvbins = gtester* gsettings gobject-query glib-* gio-* gdbus
else
mvbins = gtester*           gobject-query glib-* gio-*
endif

INSTALL_CMD = $(INSTALL_CMD_DEFAULT)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libgthread-2.0.so* $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libgobject-2.0.so* $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libgmodule-2.0.so* $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libglib-2.0.so*    $(RLIB_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(ILIB_DIR)/libgio-2.0.so*     $(RLIB_DIR) $(LOG)
endef

endif # PLATFORM == build


    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
