PKG_TYPE      = ORIGIN
PKG_VERSION   = 1.2.9
PKG_BASE      = ffmpeg
PKG_SITES     = http://ffmpeg.org
PKG_SITE_PATH = releases

include $(DEFS)


PKG_PATCH_FILES = $(PKG_PKG_DIR)/*.patch


include $(TOOLS_PKG_DIR)/configure-menuconfig.mk


configure : $(call tstamp_f,install,toolchain)

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&				\
     ./configure $(LOG)				\
	--prefix=$(INST_DIR)			\
	--bindir=$(RBIN_DIR)			\
	--datadir=$(RSHARE_DIR)/ffmpeg		\
	--libdir=$(ILIB_DIR)			\
	--shlibdir=$(RLIB_DIR)			\
	--incdir=$(IINC_DIR)			\
	--mandir=$(IMAN_DIR)			\
	--cross-prefix=$(CROSS_PREFIX)		\
	--nm=$(NM) --ar=$(AR) --as=$(AS)	\
	--cc=$(CC) --cxx=$(CXX) --dep-cc=$(CC)	\
	--ld=$(CC)				\
	--host-cc=$(BUILD_CC)			\
	--host-cflags='-O -pipe -g0'		\
	--extra-cflags="$(STD_CFLAGS) $(STD_CPPFLAGS)" \
	--extra-ldflags="$(STD_LDFLAGS)"	\
	--arch=$(NARCH) --cpu=$(BFD_ARCH)	\
	$(CONFIGURE_OPTIONS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

  INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
define INSTALL_ROOT_CMD
# $(IW) $(INST_FILES) $(PKG_PKG_DIR)/ffserver.conf  $(RETC_DIR) $(LOG)
# $(IW) $(INSTxFILES) $(PKG_PKG_DIR)/ffserver-cast* $(RBIN_DIR) $(LOG)
endef


include $(RULES)
