PKG_VERSION = 0.1.1
PKG_SITES   = http://lscube.org/files/downloads/netembryo

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call bstamp_f,install,gawk)

ifneq ($(CONFIG_WITH_IPv6),y)
 configure_opts += --disable-ipv6
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && ./configure -C $(LOG)	\
	--prefix=$(INST_DIR)			\
	--build=$(BUILD) --host=$(TARGET)	\
	--enable-shared				\
	--disable-static			\
	--with-gnu-ld				\
	--disable-sctp				\
	--without-openssl			\
	$(configure_opts)			\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)

INSTALL_CMD = $(INSTALL_CMD_DEFAULT)
INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libnetembryo.so* $(RLIB_DIR) $(LOG)


include $(RULES)
