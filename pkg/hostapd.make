ifeq (1,1)
  PKG_TYPE      = DEBIAN3
  PKG_VERSION   = 0.6.10
  PKG_VER_PATCH = -2+squeeze1
  PKG_SITE_PATH = pool/main/h/$(PKG_BASE)
  PKG_SUB_DIR   = $(PKG)
else ifeq (0,1)
  PKG_TYPE      = ORIGIN
  PKG_VERSION   = 2.0
  PKG_SITES     = http://w1.fi
  PKG_SITE_PATH = releases
  PKG_SUB_DIR   = $(PKG)
else ifeq (0,1)
  PKG_TYPE      = ORIGIN
  PKG_VERSION   = 1.1
  PKG_SITES     = http://w1.fi
  PKG_SITE_PATH = releases
  PKG_SUB_DIR   = $(PKG)
endif

include $(DEFS)


configure : $(call tstamp_f,install,toolchain libnl-1.x)
#configure : $(call tstamp_f,link,madwifi)
build : $(call tstamp_f,install,openssl-0.9)

include $(COMMON_PKG_DIR)/$(PKG).mk

define  CONFIGURE_CMD
  $(RM) $(PKG_BLD_DIR)/.config	$(LOG)
  for i in $(CONFIG_FILE); do echo $$i >> $(PKG_BLD_SUB_DIR)/.config; done
endef


ENV_VARS  = CFLAGS="$(CFLAGS) $(STD_CPPFLAGS)"
ENV_VARS += LIBS="$(STD_LDFLAGS)"
 MK_VARS  = CC=$(CC)

BUILD_CMD = $(BUILD_CMD_DEFAULT)


define  INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_SUB_DIR)/hostapd \
		      $(PKG_BLD_SUB_DIR)/hostapd_cli $(RSBIN_DIR) $(LOG)
# $(IW) $(INST_FILES) $(PKG_BLD_DIR)/hostapd.conf     $(RETC_DIR) $(LOG)
# $(IW) $(INST_FILES)    $(PETC_DIR)/hostapd*         $(RETC_DIR) $(LOG)
# $(IW) $(INST_FILES)    $(PTPL_DIR)/hostapd.conf.tpl $(RTPL_DIR) $(LOG)
endef


    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
