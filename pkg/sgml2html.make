PKG_HAS_NO_SOURCES = defined
PLATFORM=build

include $(DEFS)


install : $(call bstamp_f,install,true-false)

INSTALL_CMD = $(IWSH) "$(call lnsdir_f,$(BBIN_DIR),true,sgml2html)" $(LOG)


include $(RULES)
