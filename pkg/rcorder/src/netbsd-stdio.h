#ifndef	_NETBSD_STDIO_H_
#define	_NETBSD_STDIO_H_

#include <stdio.h>


#define	FPARSELN_UNESCESC	0x01
#define	FPARSELN_UNESCCONT	0x02
#define	FPARSELN_UNESCCOMM	0x04
#define	FPARSELN_UNESCREST	0x08
#define	FPARSELN_UNESCALL	0x0f


int	 asprintf(char ** __restrict, const char * __restrict, ...)
	    __attribute__((__format__(__printf__, 2, 3)));
char	*fgetln(FILE * __restrict, size_t * __restrict);
char	*fparseln(FILE *, size_t *, size_t *, const char[3], int);
int	 fpurge(FILE *);

char *__fgetstr(FILE *fp, size_t *lenp, int sep);



#define _DIAGASSERT(a)

#define FLOCKFILE(fp)
#define FUNLOCKFILE(fp)


#endif /* _NETBSD_STDIO_H_ */
