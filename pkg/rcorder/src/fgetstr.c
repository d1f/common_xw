/*	$NetBSD: fgetstr.c,v 1.2 2004/05/10 16:47:11 drochner Exp $	*/

/* Re-written from scratch by dm.fedorov@gmail.com
 * (C) 2006 Xwire Technologies, Inc.
 * (C) 2009,2018 Sigrand LLC
 */


#include <sys/cdefs.h>
#include <stdlib.h>
#include <string.h>
#include "netbsd-stdio.h"


static char  *cbuf = NULL;
static size_t clen = 0;

static void c_free(void)
{
    if (cbuf != NULL)
    {
	free(cbuf);
	cbuf = NULL;
	clen = 0;
    }
}

static void c_append_char(int c)
{
    if (cbuf == NULL)
    {
	cbuf = malloc(2);
	if (cbuf == NULL) // -ENOMEM
	    return;
	cbuf[0] = c;
	cbuf[1] = 0;
	clen    = 1;
	return;
    }

    char *nbuf = realloc(cbuf, clen + 2);
    if (nbuf == NULL) // ENOMEM
    {
	c_free();
	return;
    }

    cbuf         = nbuf;
    cbuf[clen++] = c;
    cbuf[clen]   = 0;
}

/*
 * Get an input line.  The returned pointer often (but not always)
 * points into a stdio buffer.  Fgetline does not alter the text of
 * the returned line (which is thus not a C string because it will
 * not necessarily end with '\0'), but does allow callers to modify
 * it if they wish.  Thus, we set __SMOD in case the caller does.
 */
char *
__fgetstr(FILE *fp, size_t *lenp, int sep)
{
    int c;

    c_free();

    while ((c = fgetc(fp)) != EOF)
    {
	c_append_char(c);

	if (c == sep)
	    goto out;
    }

    if (ferror(fp))
        goto fail;
    else	// EOF && !error == EOF
    {
	if (clen != 0)
	    c_append_char(sep);
	else
	    goto fail; // EOF
    }

out:
    *lenp = clen + 1;
    return  cbuf;

fail:
    c_free();
    *lenp = 0;
    return NULL;
}
