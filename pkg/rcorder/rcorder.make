PKG_ORIG_DIR     = $(PKG_PKG_DIR)/src
PKG_FETCH_METHOD = DONE
SEPARATE_SRC_BLD_DIRS = defined

include $(DEFS)


configure : $(call tstamp_f,install-root,toolchain $(dflibs))

BUILD_TYPE     = MULTI_SRC_BIN
BIN_NAME       = $(PKG)
CFLAGS_        = -Dlint=0 -DHAVE_NBTOOL_CONFIG_H=0
MAKES_MAKEFILE = $(TOOLS_DIR)/makes/all.Makefile
include          $(TOOLS_DIR)/makes/conf.mk

CONFIGURE_CMD  = $(MAKES_CONFIGURE_CMD)


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD      = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/$(BIN_NAME) $(ISBIN_DIR) $(LOG)
  INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/$(BIN_NAME) $(RSBIN_DIR) $(LOG)


include $(RULES)
