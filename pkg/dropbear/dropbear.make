PKG_TYPE      = ORIGIN
PKG_VERSION   = 2017.75
PKG_SITES     = http://matt.ucc.asn.au
PKG_SITE_PATH = dropbear/releases

include $(DEFS)


PKG_PATCH3_FILES += $(PKG_PKG_DIR)/*.patch


configure : $(call tstamp_f,install,toolchain)
configure : $(call bstamp_f,install,sed)

STD_CPPFLAGS += -DMAX_UNAUTH_CLIENTS=3
STD_CPPFLAGS += -DMAX_AUTH_TRIES=3
STD_CPPFLAGS += -DSFTPSERVER_PATH=\"/libexec/sftp-server\"
STD_CPPFLAGS += -DDROPBEAR_BLOWFISH

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) && \
     ./configure	-C	$(LOG)	\
	     --prefix=$(INST_DIR)	\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--enable-largefile		\
	--disable-zlib			\
	--disable-pam			\
	--enable-openpty		\
	--enable-syslog			\
	--enable-shadow			\
	--enable-bundled-libtom		\
	--disable-lastlog		\
	--enable-utmp			\
	--enable-utmpx			\
	--enable-wtmp			\
	--enable-wtmpx			\
	--enable-loginfunc		\
	--disable-pututline		\
	--disable-pututxline		\
	--without-zlib			\
	--without-pam			\
	--without-lastlog		\
	$(STD_VARS)

  $(SED) -i -e 's/-lcrypt//' $(PKG_BLD_DIR)/Makefile $(LOG)
endef


MK_VARS  = PROGRAMS="dropbear dbclient dropbearkey dropbearconvert scp"
MK_VARS += MULTI=1 STATIC=0 SCPPROGRESS=1

BUILD_CMD = export LIBS=-lcrypt; $(BUILD_CMD_DEFAULT)


define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/dropbearmulti $(RSBIN_DIR)		$(LOG)
  $(IW) $(LN_S) -fv         dropbearmulti $(RSBIN_DIR)/dropbear		$(LOG)
  $(IW) $(LN_S) -fv ../sbin/dropbearmulti  $(RBIN_DIR)/dropbearkey	$(LOG)
  $(IW) $(LN_S) -fv ../sbin/dropbearmulti  $(RBIN_DIR)/dropbearconvert	$(LOG)
  $(IW) $(LN_S) -fv ../sbin/dropbearmulti  $(RBIN_DIR)/ssh		$(LOG)
  $(IW) $(LN_S) -fv ../sbin/dropbearmulti  $(RBIN_DIR)/scp		$(LOG)
endef

    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


include $(RULES)
