include $(TOP_DIR)/common_xw/pkg/apr.mk

PKG_TYPE        = $(apr_pkg_type)

ifeq ($(PKG_TYPE),DEBIAN1)
  PKG_VERSION   = $(apr_version)
  PKG_VER_PATCH = $(apr_ver_patch)
  PKG_SITE_PATH = pool/main/a/$(PKG_BASE)
else ifeq ($(PKG_TYPE),DEBIAN3)
  PKG_VERSION   = $(apr_version)
  PKG_VER_PATCH = $(apr_ver_patch)
  PKG_SITE_PATH = pool/main/a/$(PKG_BASE)
else
  PKG_VERSION   = $(apr_version)
  PKG_SITES     = $(APACHE_ORG_SITES)
  PKG_SITE_PATH = apr
endif

include $(DEFS)


ifeq ($(PKG_TYPE),DEBIAN1)
include $(TOOLS_PKG_DIR)/dpatch.mk
endif


AC_VER = 2.69
include $(TOOLS_PKG_DIR)/autoconf.mk

AM_VER = 1.11
include $(TOOLS_PKG_DIR)/automake.mk

pre-configure : $(call bstamp_f,install,libtool python2.6)

define PRE_CONFIGURE_CMD
  cd $(PKG_BLD_SUB_DIR) && $(RELINK) build-outputs.mk apr.spec $(LOG)
  cd $(PKG_BLD_SUB_DIR) && ./buildconf   $(LOG)
endef

ifneq ($(CONFIG_WITH_IPv6),y)
 configure_opts += --disable-ipv6
endif

prefix     = --prefix=$(INST_DIR)

ifeq ($(PLATFORM),build)
  build_host =
else
  build_host = --build=$(BUILD) --host=$(TARGET)
  STD_CPPFLAGS += -DAPR_IOVEC_DEFINED
  configure : $(TOOLCHAIN_INSTALL_STAMP)
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_SUB_DIR)		&&	\
     ac_cv_file__dev_zero=/dev/zero	\
     ac_cv_func_setpgrp_void=yes	\
     apr_cv_process_shared_works=no	\
     apr_cv_tcp_nodelay_with_cork=yes	\
     ac_cv_struct_rlimit=yes		\
     apr_cv_osuuid=no			\
     ac_cv_search_uuid_create=no	\
     ac_cv_search_uuid_generate=no	\
     apr_cv_func_uuid_create=no		\
     apr_cv_func_uuid_generate=no	\
     ./configure -C $(LOG)		\
	$(prefix)			\
	$(build_host)			\
	--disable-static		\
	--enable-shared			\
	--enable-pool-debug=no		\
	--enable-nonportable-atomics	\
	--enable-threads		\
	--enable-other-child		\
	--with-gnu-ld			\
	--with-sendfile			\
	--with-devrandom=/dev/random	\
	$(configure_opts)		\
	$(STD_VARS)
endef


MK_VARS += RANLIB=$(RANLIB)

    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)

INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libapr-1.so* $(RLIB_DIR) $(LOG)


include $(RULES)
