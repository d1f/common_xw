PKG_HAS_NO_SOURCES = defined

include $(DEFS)


install : $(call tstamp_f,install,boa mime-support)

define INSTALL_ROOT_CMD
  $(IW) $(MKDIR_P)       $(ROOT_DIR)/var/log/boa            $(LOG)
  $(IW) $(INSTxFILES) $(PKG_PKG_DIR)/boa.rc    $(RINIT_DIR) $(LOG)
  $(IW) $(INST_FILES) $(PKG_PKG_DIR)/boa.conf   $(RWEB_DIR) $(LOG)
endef


include $(RULES)
