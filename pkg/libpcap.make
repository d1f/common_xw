PKG_TYPE          = DEBIAN3
PKG_VERSION       = 1.6.2
PKG_VER_PATCH     = -2
PKG_SITE_PATH     = pool/main/libp/$(PKG_BASE)

include $(DEFS)


#AC_VER = 2.69
#include $(TOOLS_PKG_DIR)/autoconf.mk
#PRE_CONFIGURE_CMD = cd $(PKG_BLD_DIR) && $(AUTOCONF) $(AUTOCONF_FLAGS) $(LOG)


configure : $(call bstamp_f,install,bison flex)
configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call tstamp_f,configure,linux)

STD_CFLAGS += $(STD_CPPFLAGS)

ifneq ($(CONFIG_WITH_IPv6),y)
 configure_opts += --disable-ipv6
endif

define CONFIGURE_CMD
  cd $(PKG_BLD_DIR) &&			\
	./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	--build=$(BUILD)		\
	--host=$(TARGET)		\
	--disable-protochain		\
	--disable-optimizer-dbg		\
	--disable-yydebug		\
	--disable-universal		\
	--enable-shared			\
	--disable-bluetooth		\
	--disable-canusb		\
	--disable-can			\
	--disable-dbus			\
	--with-pcap=linux		\
	--without-libnl			\
	--without-dag			\
	--without-septel		\
	--without-snf			\
	$(configure_opts)		\
	ac_cv_linux_vers=$(LK_V1)	\
	$(STD_VARS)
endef
# --without-sita breaks configure with wrong detection


MK_VARS += LD=$(LD)
    BUILD_CMD_TARGET = version.h shared
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


       INSTALL_CMD_TARGET = install-shared
define INSTALL_CMD
  $(INSTALL_CMD_DEFAULT)
  $(IW) chmod a+x        $(ILIB_DIR)/libpcap.so*                    $(LOG)
  $(IW) $(LN_S) libpcap.so.$(PKG_VERSION) $(ILIB_DIR)/libpcap.so.1  $(LOG)

  $(IW) $(INST_FILES) $(PKG_BLD_DIR)/pcap/*.h      $(IINC_DIR)/pcap $(LOG)
  $(IW) $(INST_FILES) $(PKG_BLD_DIR)/pcap.h        $(IINC_DIR)      $(LOG)
  $(IW) $(INST_FILES) $(PKG_BLD_DIR)/pcap-bpf.h    $(IINC_DIR)      $(LOG)
  $(IW) $(INST_FILES) $(PKG_BLD_DIR)/pcap-namedb.h $(IINC_DIR)      $(LOG)
endef

INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libpcap.so* $(RLIB_DIR) $(LOG)


include $(RULES)
