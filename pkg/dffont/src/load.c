#define _GNU_SOURCE

#include <df/font.h>
#include <df/error.h>

#include "zalloc.h"    // dffont_zalloc
#include <string.h>    // memset
#include <fcntl.h>     // open
#include <unistd.h>    // close
#include <sys/stat.h>  // fstat
#include <sys/mman.h>  // mmap
#include <df/x.h>      // df_freep


void dffont_delete(dffont_t **font_ptr)
{
    if (font_ptr == NULL)
	return;

    dffont_t *font = *font_ptr;

    if (font == NULL)
	return;

    df_freep(&font->glyphs);

    if (font->header != MAP_FAILED && font->header != NULL)
	munmap(font->header, font->size);

    if (font->descr >= 0)
    {
	close(font->descr);
	font->descr = -1;
    }

    memset(font, 0, sizeof(*font));

    df_freep(font_ptr);
}

// returns number of scanned glyphs.
// stores glyph pointers to glyphs[] if not NULL.
static ssize_t dffont_scan(const dffont_t *font,
			   const dffont_glyph_t **glyphs)
{
    if (font == NULL)
	return 0;
    if (font->header == NULL)
	return 0;

    size_t glyph_count = 0;

    uint32_t last_unicode_point = 0;

    const dffont_glyph_t *glyph = dffont_glyph_first(font);
    do
    {
	if (dffont_check_glyph("dffont_scan(): ",
			       glyph, font->header->height) < 0)
	    goto fail;

	if (le32toh(glyph->unicode_point) == 0) // correct EOF
	    break;

	if (le32toh(glyph->unicode_point) > last_unicode_point)
	{
	    last_unicode_point = le32toh(glyph->unicode_point);
	}
	else
	{
	    dferror(EXIT_SUCCESS, 0,
		    "%s(): wrong order of code points, new %04x <= last %04x",
		    __func__, le32toh(glyph->unicode_point), last_unicode_point);
	    goto fail;
	}

	if (glyphs != NULL)
	    glyphs[glyph_count] = glyph;

	++glyph_count;

	glyph = dffont_glyph_next(glyph);

	if (((const char*)glyph + DFFONT_GLYPH_ALIGN(DFFONT_GLYPH_HEADER_SIZE) -
	     (const char*)font->header) > font->size)
	{
	    dferror(EXIT_SUCCESS, 0,
		    "%s(): glyph %zu is out of file size %ld",
		    __func__, glyph_count, font->size);
	    goto fail;
	}

    } while (1);

    return glyph_count;

fail:
    return -1;
}

dffont_t *dffont_load(const char *file)
{
    dffont_t *font = dffont_zalloc(__func__, sizeof(dffont_t));
    if (font == NULL)
	return NULL;

    font->name = file;

    font->descr = open(file, O_RDONLY|O_NOATIME);
    if (font->descr < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): Can't open %s", __func__, file);
	goto fail;
    }

    struct stat sb;
    if (fstat(font->descr, &sb) < 0)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): fstat on %s failed", __func__, file);
	goto fail;
    }

    font->size = sb.st_size;

    font->header = mmap(NULL, font->size, PROT_READ,
			MAP_SHARED|MAP_NORESERVE,
			font->descr, 0);
    if (font->header == MAP_FAILED)
    {
	dferror(EXIT_SUCCESS, errno, "%s(): mmap failed", __func__);
	goto fail;
    }

    if (dffont_check_header(font->header))
	goto fail;

    madvise(font->header, font->size, MADV_SEQUENTIAL);

    ssize_t glyph_count = dffont_scan(font, NULL);
    if (glyph_count < 0)
	goto fail;

    font->glyphs = dffont_zalloc(__func__, glyph_count * sizeof(void*));
    if (font->glyphs == NULL)
	goto fail;

    dffont_scan(font, font->glyphs);
    font->glyphs_num = glyph_count;

    madvise(font->header, font->size, MADV_RANDOM);

    return font;

fail:
    dffont_delete(&font);
    return NULL;
}
