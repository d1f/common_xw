#include <df/font.h>


void dffont_glyphs_to_charmap(const dffont_glyph_t * const glyphs[],
			      const size_t glyphs_length,
			      df_charmap_t *charmap,
			      char out_value)
{
    size_t gi, xoffs;
    for (gi = 0, xoffs=0; gi < glyphs_length; ++gi)
    {
	const dffont_glyph_t *glyph = glyphs[gi];
	if (glyph == NULL)
	    continue;

	if (glyph->width == 0)
	    continue;

	xoffs += dffont_glyph_to_charmap(glyph, charmap, xoffs, out_value);
    }
}
