#include <df/font.h>
#include <limits.h> // CHAR_BIT


void dffont_out_ascii_bitmap(FILE *out, const char *data,
			     size_t stride, size_t width, size_t height)
{
    size_t char_width = DFFONT_BIT_WIDTH_TO_CHAR_STRIDE(width);

    size_t line;
    for (line=0; line < height; ++line)
    {
	size_t i, line_start = line * stride;
	for (i=0; i < char_width; ++i)
	{
	    unsigned char ch = data[line_start + i];
	    //printf(" %02x", ch);
	    size_t b;
	    for (b=0; b<CHAR_BIT; ++b)
	    {
		putc( ch & (1<<b) ? '*' : '0', out );
	    }
	}
	putc('\n', out);
    }
}
