#include <df/font.h>
#include <df/error.h>
#include <wchar.h>          // wcslen


// returns text width in pixels, -1 on error
// if length == 0, it is measured
ssize_t dffont_text_to_glyphs(const wchar_t *text, size_t length,
			      const dffont_t *font,
			      const dffont_glyph_t *glyphs[])
{
    if (text == NULL)
    {
	dferror(EXIT_SUCCESS, 0, "%s(): text == NULL", __func__);
	return -1;
    }

    if (font == NULL)
    {
	dferror(EXIT_SUCCESS, 0, "%s(): font == NULL", __func__);
	return -1;
    }

    if (glyphs == NULL)
    {
	dferror(EXIT_SUCCESS, 0, "%s(): glyphs == NULL", __func__);
	return -1;
    }

    if (length == 0)
	length = wcslen(text);

    if (length == 0)
	return 0;

    const dffont_glyph_t *default_glyph = dffont_default_glyph(font);
    if (default_glyph == NULL)
	return -1;


    size_t width = 0;
    size_t i;
    for (i = 0; i < length; ++i)
    {
	const dffont_glyph_t *glyph = dffont_find_glyph(font, text[i]);
	if (glyph == NULL)
	    glyph = default_glyph;

	width += glyph->width;
	glyphs[i] = glyph;
    }

    return width;
}
