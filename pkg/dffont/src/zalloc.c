#include "zalloc.h" // dffont_zalloc
#include <string.h> // memset
#include <df/error.h>

void *dffont_zalloc(const char *func, size_t size)
{
    void *p = malloc(size);
    if (p == NULL)
	dferror(EXIT_SUCCESS, errno, "%s(): malloc(%zu) failed",
		func, size);

    memset(p, 0, size);

    return p;
}
