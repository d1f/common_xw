#include <df/font.h>
#include <limits.h> // CHAR_BIT
#include <string.h> // memset


// converts bit array to char array setting char to value
void dffont_bits_to_chars(const char bits[], size_t bit_width,
			  const char value,
			  char chars[])
{
    size_t stride =  DFFONT_BIT_WIDTH_TO_CHAR_STRIDE(bit_width);

    size_t bi;
    for (bi = 0; bi < stride; ++bi)
    {
	unsigned char ch = bits[bi];
	if (ch == 0)
	    continue;

	char *chp = &chars[bi*CHAR_BIT];

	if (ch == UCHAR_MAX)
	{
	    memset(chp, value, CHAR_BIT);
	    continue;
	}

	if (ch & 0b00001111)
	{
	    if (ch & 0b00000001)
		chp[0] = value;
	    if (ch & 0b00000010)
		chp[1] = value;
	    if (ch & 0b00000100)
		chp[2] = value;
	    if (ch & 0b00001000)
		chp[3] = value;
	}
	if (ch & 0b11110000)
	{
	    if (ch & 0b00010000)
		chp[4] = value;
	    if (ch & 0b00100000)
		chp[5] = value;
	    if (ch & 0b01000000)
		chp[6] = value;
	    if (ch & 0b10000000)
		chp[7] = value;
	}
    }
}
