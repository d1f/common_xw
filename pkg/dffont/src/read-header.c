#include <df/font.h>
#include <df/error.h>
#include "zalloc.h" // dffont_zalloc


// returns NULL on error or EOF
dffont_header_t *dffont_read_header(FILE *in)
{
    dffont_header_t *hdr = dffont_zalloc(__func__, sizeof(*hdr));
    if (hdr == NULL)
	return NULL;

    int rc = dffont_xfread(hdr, sizeof(*hdr), in);
    if (rc != 1)
    {
	if (rc == 0) // EOF
	    dferror(EXIT_SUCCESS, 0, "%s(): Unexpected EOF", __func__);
	goto fail;
    }

    if (dffont_check_header(hdr))
	goto fail;

    return hdr;

fail:
    free(hdr);
    return NULL;
}
