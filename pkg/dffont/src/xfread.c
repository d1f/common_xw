#include <df/font.h>
#include <df/error.h>
#include <stdio.h>  // fread

// returns 1 on success, 0 on EOF, -1 on error
int dffont_xfread(void *dst, size_t size, FILE *in)
{
    if (size == 0)
	return 1;

    size_t rc = fread(dst, size, 1, in);
    if (rc != 1)
    {
	if (feof(in))
	    return 0; //EOF
	else if (ferror(in))
	    dferror(EXIT_SUCCESS, errno, "Can't read glyph");
	else
	    dferror(EXIT_SUCCESS, 0, "Can't read glyph by unknown reason");

	return -1;
    }

    return 1;
}
