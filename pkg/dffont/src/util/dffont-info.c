#define _GNU_SOURCE
#include <string.h> // basename strerror
#include <stdio.h>  // printf
#include <utf8.h>
#include <df/log.h>
#include <df/error.h>
#include <df/opt.h>
#include <df/font.h>


int main(int ac, char *av[])
{
    dflog_open(basename(av[0]), DFLOG_STDERR);

    if (ac != 2)
    {
	printf("Usage: %s file\n", basename(av[0]));
	return EXIT_FAILURE;
    }

    const char *in_dffont = av[1];


    FILE *in = fopen(in_dffont, "r");
    if (in == NULL)
	dferror(EXIT_FAILURE, errno, "Can't open %s", in_dffont);

    dffont_header_t *hdr = dffont_read_header(in);
    if (hdr == NULL)
	return EXIT_FAILURE;

    printf("Height: %u, Font: %s\n", hdr->height, hdr->name);


    return EXIT_SUCCESS;
}
