#define _GNU_SOURCE
#include <string.h> // basename
#include <stdlib.h> // EXIT_FAILURE EXIT_SUCCESS
#include <stdio.h>  // printf
#include <df/log.h>
#include <df/font.h>

int main(int ac, char *av[])
{
    dflog_open(basename(av[0]), DFLOG_STDERR);

    if (ac != 3)
    {
	printf("Usage: %s font-file code-point-hex\n", basename(av[0]));
	return EXIT_FAILURE;
    }

    const char *font_file =         av[1];
    unsigned long code    = strtoul(av[2], NULL, 16);

    dffont_t *font = dffont_load(font_file);
    if (font == NULL)
	return EXIT_FAILURE;

    const dffont_glyph_t *glyph = dffont_find_glyph(font, code);
    if (glyph == NULL)
    {
	printf("No code point %04lx found\n", code);
	return EXIT_FAILURE;
    }

    printf("code: %04x, width: %u, first_row: %u, last_row: %u\n",
	   le32toh(glyph->unicode_point), glyph->width,
	   glyph->first_row, glyph->last_row);

    dffont_delete(&font);
    return EXIT_SUCCESS;
}
