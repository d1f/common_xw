#include <df/font.h>
#include <df/error.h>
#include <string.h> // strncmp


// returns -1 on error
int dffont_check_header(const dffont_header_t *header)
{
    if (header == NULL)
    {
	dferror(EXIT_SUCCESS, 0, "%s(): header is NULL", __func__);
	return -1;
    }

    if (strncmp(       header->signature, DFFONT_SIGNATURE,
		sizeof(header->signature)) != 0)
    {
	dferror(EXIT_SUCCESS, 0, "%s(): signature is not matched", __func__);
	return -1;
    }

    if (header->version != DFFONT_VERSION)
    {
	dferror(EXIT_SUCCESS, 0, "%s(): version is not matched", __func__);
	return -1;
    }

    return 0;
}
