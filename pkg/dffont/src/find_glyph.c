#include <df/font.h>
#include <stdlib.h> // bsearch

typedef void* vptr_t;

static int compare(const void *keyptr, const void *itemptr)
{
    const dffont_glyph_t * key = *(const vptr_t*) keyptr;
    const dffont_glyph_t *item = *(const vptr_t*)itemptr;

    if (key->unicode_point < le32toh(item->unicode_point))
	return -1;
    else if (key->unicode_point == item->unicode_point)
	return  0;
    else // if (key->unicode_point > le32toh(item->unicode_point))
	return  1;
}

const dffont_glyph_t *dffont_find_glyph(const dffont_t *font, wchar_t code)
{
    if (font->glyphs_num == 0 || font->header == NULL || font->glyphs == NULL)
	return NULL;

    dffont_glyph_t key = { .unicode_point = code };
    vptr_t keyptr = &key;
    vptr_t *p = bsearch(&keyptr,
			font->glyphs, font->glyphs_num, sizeof(vptr_t),
			compare);

    if (p != NULL)
	return *p;

    return NULL;
}
