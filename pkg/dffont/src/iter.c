#include <df/font.h>
#include <df/error.h>

const dffont_glyph_t *dffont_glyph_first(const dffont_t *font)
{
    return (const void *) (font->header + 1);
}

const dffont_glyph_t *dffont_glyph_next(const dffont_glyph_t *glyph)
{
    const dffont_glyph_t *next = NULL;

    if (glyph->width == 0)
    {
	next = (const void *)
	    ((const char*)glyph +
	     DFFONT_GLYPH_ALIGN(DFFONT_GLYPH_HEADER_SIZE));
    }
    else
    {
	size_t img_size = (glyph->last_row - glyph->first_row + 1) *
	    DFFONT_BIT_WIDTH_TO_CHAR_STRIDE(glyph->width);

	next = (const void *)
	    ((const char*)glyph +
	     DFFONT_GLYPH_ALIGN(DFFONT_GLYPH_HEADER_SIZE + img_size));
    }

    return next;
}
