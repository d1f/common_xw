#include <df/font.h>


// returns glyph->width
size_t dffont_glyph_to_charmap(const dffont_glyph_t *glyph,
			       df_charmap_t *charmap,
			       size_t xoffs, char out_value)
{
    size_t glyph_stride = DFFONT_BIT_WIDTH_TO_CHAR_STRIDE(glyph->width);

    size_t out_row, glyph_row;
    for (out_row  = glyph->first_row, glyph_row = 0;
	 out_row <= glyph-> last_row;
	 ++out_row, ++glyph_row)
    {
	char *out_row_start = charmap->map + out_row * charmap->width + xoffs;
	const char *glyph_row_start = &glyph->bits[glyph_row * glyph_stride];

	dffont_bits_to_chars(glyph_row_start, glyph->width, out_value, out_row_start);
    }

    return glyph->width;
}
