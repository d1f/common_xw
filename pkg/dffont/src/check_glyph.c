#include <df/font.h>
#include <df/error.h>


// returns 0 or -1 on error, message issued
int dffont_check_glyph(const char *pfx, const dffont_glyph_t *glyph, size_t height)
{
    if (glyph == NULL)
    {
	dferror(EXIT_SUCCESS, 0, "%sglyph is NULL", pfx);
	return -1;
    }

    if (glyph->first_row > glyph->last_row)
    {
	dferror(EXIT_SUCCESS, 0, "%sglyph %04x: first row %u > last row %u",
		pfx, le32toh(glyph->unicode_point),
		glyph->first_row, glyph->last_row);
	return -1;
    }

    if (glyph->first_row >= height)
    {
	dferror(EXIT_SUCCESS, 0, "%sglyph %04x: first row %u > height %u",
		pfx, le32toh(glyph->unicode_point),
		glyph->first_row, height);
	return -1;
    }

    if (glyph-> last_row >= height)
    {
	dferror(EXIT_SUCCESS, 0, "%sglyph %04x: first row %u > height %u",
		pfx, le32toh(glyph->unicode_point),
		glyph->last_row, height);
	return -1;
    }

    return 0;
}
