#include <df/font.h>
#include <df/error.h>
#include <df/charmap.h>
#include <utf8.h>
#include "zalloc.h" // dffont_zalloc
#include <string.h> // strlen


// returns malloced struct with data.
// NULL on error, message issued.
df_charmap_t *dffont_text_to_charmap(const char *utext, const dffont_t *font,
				     const unsigned char value)
{
    const dffont_glyph_t **glyphs = NULL;
    wchar_t *wtext = NULL;
    df_charmap_t *charmap = NULL;

    if (utext == NULL)
    {
	dferror(EXIT_SUCCESS, 0, "%s(): UTF-8 text parameter is NULL", __func__);
	return NULL;
    }

    if (font == NULL)
    {
	dferror(EXIT_SUCCESS, 0, "%s(): font parameter is NULL", __func__);
	return NULL;
    }

    size_t ulen = strlen(utext);
    if (ulen == 0)
	return NULL;

    size_t wlen = utf8_to_wchar(utext, ulen, NULL, 0, 0);
    if (wlen == 0)
    {
	dferror(EXIT_SUCCESS, 0, "%s(): UTF-8 text error", __func__);
	return NULL;
    }

    wtext = dffont_zalloc(__func__, sizeof(wchar_t) * (wlen+1));
    if (wtext == NULL)
	return NULL;

    utf8_to_wchar(utext, ulen, wtext, wlen+1, 0);
    wtext[wlen] = 0;

    glyphs = dffont_zalloc(__func__, wlen * sizeof(void*));
    if (glyphs == NULL)
	goto fail;

    // returns text width in pixels, -1 on error
    ssize_t width = dffont_text_to_glyphs(wtext, wlen, font, glyphs);
    if (width < 0)
	goto fail;

    charmap = df_charmap_create(font->header->height, width);
    if (charmap == NULL)
	goto fail;

    dffont_glyphs_to_charmap(glyphs, wlen, charmap, value);

    free(wtext);
    free(glyphs);
    return charmap;

fail:
    df_charmap_delete(&charmap);
    free(wtext);
    free(glyphs);
    return NULL;
}
