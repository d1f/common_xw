#ifndef  DF_FONT_H
# define DF_FONT_H

# include <stdint.h> // uintXX_t
# include <stddef.h> // size_t
# include <stdio.h>  // FILE
# include <wchar.h>  // wchar_t
# include <limits.h> // CHAR_BIT
# include <sys/types.h> // ssize_t
# include <df/charmap.h>


/*
 file structure:

 header
 glyph
 ...
 glyph
 empty glyph, all fields zeroed, no bits
 EOF
*/

#define DFFONT_SIGNATURE "df_font"
#define DFFONT_VERSION   1

typedef struct
{
    char signature[8];  // zero terminated string
    uint8_t version;
    uint8_t height;     // rows
    char name[86];      // zero terminated string
} dffont_header_t;      // 96 bytes, alignment 8

typedef struct
{
    uint32_t unicode_point; // in LSB first order
    uint8_t  width;  // pixel/bit width; stride in chars == (width+CHAR_BIT-1)/CHAR_BIT
    uint8_t  first_row;
    uint8_t   last_row;
    char     bits[0]; // must be padded to 4 bytes alignment
} dffont_glyph_t;


#define DFFONT_GLYPH_ALIGN(glyph_size) (((glyph_size) + 3) & ~0x3)
#define DFFONT_GLYPH_PAD_SIZE(glyph_size) (DFFONT_GLYPH_ALIGN(glyph_size) - (glyph_size))

#define DFFONT_GLYPH_HEADER_SIZE        offsetof(dffont_glyph_t, bits)

#define DFFONT_BIT_WIDTH_TO_CHAR_STRIDE(bit_width) \
    ((((bit_width) + CHAR_BIT - 1)) / CHAR_BIT)


// API:

// returns -1 on error
int dffont_write_header(FILE *out, size_t height, const char *font_name);

// returns NULL on error or EOF
dffont_header_t *dffont_read_header(FILE *in);

// returns 1 on success, 0 on EOF, -1 on error
int dffont_xfread(void *dst, size_t size, FILE *in);

// returns -1 on error
int dffont_check_header(const dffont_header_t *header);

// returns -1 on error
int dffont_write_glyph(FILE *out, uint32_t unicode_point, const char *bits,
		       size_t width, size_t src_stride, size_t src_heigth);

// returns -1 on error
int dffont_write_empty_glyph(FILE *out);

void dffont_out_ascii_bitmap(FILE *out, const char *data,
			     size_t stride, size_t width, size_t height);


// returns 0 or -1 on error, message issued
int dffont_check_glyph(const char *pfx, const dffont_glyph_t *glyph, size_t height);


// converts bit array to char array setting char to value
void dffont_bits_to_chars(const char bits[], size_t bit_width,
			  const char value,
			  char chars[]);

// returns glyph->width
size_t dffont_glyph_to_charmap(const dffont_glyph_t *glyph,
			       df_charmap_t *charmap,
			       size_t xoffs, char out_value);

void   dffont_glyphs_to_charmap(const dffont_glyph_t * const glyphs[],
				const size_t glyphs_length,
				df_charmap_t *charmap,
				char out_value);




typedef struct
{
    const char *name;
    off_t       size;
    int         descr;
    dffont_header_t *header; // mapped address
    const dffont_glyph_t **glyphs;
    size_t                 glyphs_num;
} dffont_t;

dffont_t *dffont_load(const char *file);
void      dffont_delete(dffont_t **font_ptr);

const dffont_glyph_t *dffont_find_glyph(const dffont_t *font, wchar_t code);

const dffont_glyph_t *dffont_glyph_first(const dffont_t *font);
const dffont_glyph_t *dffont_glyph_next (const dffont_glyph_t *glyph);


// returns malloced struct with data.
// NULL on error, message issued.
df_charmap_t *dffont_text_to_charmap(const char *utext, const dffont_t *font,
				     const unsigned char value);

// returns text width in pixels, -1 on error
// if length == 0, it is measured
ssize_t dffont_text_to_glyphs(const wchar_t *text, size_t length,
			      const dffont_t *font,
			      const dffont_glyph_t *glyphs[]);

// returns default glyph or NULL on failure
const dffont_glyph_t *dffont_default_glyph(const dffont_t *font);


#endif //DF_FONT_H
