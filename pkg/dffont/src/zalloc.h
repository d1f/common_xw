#ifndef  DF_FONT_ZALLOC_H
# define DF_FONT_ZALLOC_H

# include <stddef.h> // size_t
# include <stdlib.h> // malloc, free


void *dffont_zalloc(const char *func, size_t size);


#endif //DF_FONT_ZALLOC_H
