#include <df/font.h>
#include <df/error.h>
#include <stdio.h>  // fwrite
#include <string.h> // memset


static void
dffont_find_rows(const char *bits, size_t stride, size_t height,
		 uint8_t *first, uint8_t *last)
{
    if (height > UINT8_MAX)
	dferror(EXIT_FAILURE, 0, "%s(): height %zu > %zu", __func__, height, UINT8_MAX);

    size_t first_row, last_row;
    df_charmap_squeeze_standalone(height, stride, (const void*)bits,
				  &first_row, &last_row);

    *first = first_row;
    *last  = last_row;

    if (*last < *first)
	dferror(EXIT_FAILURE, 0, "%s(): last row %zu < first row %zu", __func__, last_row, first_row);
}

static int xfwrite(const void *src, size_t size, FILE *out)
{
    if (size == 0)
	return 0;

    size_t rc = fwrite(src, size, 1, out);
    if (rc != 1)
    {
	if (ferror(out))
	    dferror(EXIT_SUCCESS, errno, "Can't write glyph");
	else
	    dferror(EXIT_SUCCESS, 0, "Can't write glyph by unknown reason");

	return -1;
    }

    return 0;
}

static int dffont_write_pad(FILE *out, size_t glyph_size)
{
    static const char pad[4];
    return xfwrite(pad, DFFONT_GLYPH_PAD_SIZE(glyph_size), out);
}

// returns -1 on error
int dffont_write_glyph(FILE *out, uint32_t unicode_point, const char *bits,
		       size_t width, size_t src_stride, size_t src_heigth)
{
    if (width == 0) // skip zero width glyph, e.g. no-breaking space
	return 0;

    dffont_glyph_t glyph =
    {
	.unicode_point = htole32(unicode_point),
	.width = width,
    };
    dffont_find_rows(bits, src_stride, src_heigth,
		     &glyph.first_row, &glyph.last_row);

    int rc = xfwrite(&glyph, DFFONT_GLYPH_HEADER_SIZE, out);
    if (rc < 0)
	return rc;

    size_t dst_stride = DFFONT_BIT_WIDTH_TO_CHAR_STRIDE(width);
    if (dst_stride > src_stride)
	dferror(EXIT_FAILURE, 0, "%s(): dst_stride %zu > src_stride %zu",
		__func__, dst_stride, src_stride);

    size_t glyph_size = DFFONT_GLYPH_HEADER_SIZE;

    size_t src_line;
    for (src_line  = glyph.first_row;
	 src_line <= glyph.last_row; ++src_line)
    {
	rc = xfwrite(&bits[src_line * src_stride], dst_stride, out);
	if (rc < 0)
	    return rc;
	glyph_size += dst_stride;
    }

    return dffont_write_pad(out, glyph_size);
}

// returns -1 on error
int dffont_write_empty_glyph(FILE *out)
{
    dffont_glyph_t glyph;
    memset(&glyph, 0, sizeof(glyph));

    int rc = xfwrite(&glyph, DFFONT_GLYPH_HEADER_SIZE, out);
    if (rc < 0)
	return rc;

    return dffont_write_pad(out, DFFONT_GLYPH_HEADER_SIZE);
}

// returns -1 on error
int dffont_write_header(FILE *out, size_t height, const char *name)
{
    dffont_header_t hdr;
    memset(&hdr, 0, sizeof(hdr));
    strncpy(hdr.signature, DFFONT_SIGNATURE, sizeof(hdr.signature));
    strncpy(hdr.name, name, sizeof(hdr.name));
    hdr.version = DFFONT_VERSION;
    hdr.height = height;

    return xfwrite(&hdr, sizeof(hdr), out);
}
