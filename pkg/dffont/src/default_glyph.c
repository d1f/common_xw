#include <df/font.h>
#include <df/error.h>


// returns default glyph or NULL on failure
const dffont_glyph_t *dffont_default_glyph(const dffont_t *font)
{
    const wchar_t default_code1 = 0xFFFD; // FFFD unicode replacement char

    const dffont_glyph_t *glyph = dffont_find_glyph(font, default_code1);
    if (glyph == NULL)
    {
	const wchar_t default_code2 = 0x003F; // '?' question mark as a fallback
	glyph = dffont_find_glyph(font, default_code2);
	if (glyph == NULL)
	{
	    dferror(EXIT_SUCCESS, 0, "%s(): default codes %04lx and %04lx not found in font %s",
		    __func__, default_code1, default_code2, font->header->name);
	}
    }

    return glyph;
}
