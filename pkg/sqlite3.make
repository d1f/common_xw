PKG_TYPE          = DEBIAN3
PKG_VERSION       = 3.8.7
PKG_VER_PATCH     = -1
PKG_SITE_PATH     = pool/main/s/$(PKG_BASE)

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)
configure : $(call bstamp_f,install,sed)

ifneq ($(PLATFORM),build)
  conf_opts  = --build=$(BUILD) --host=$(TARGET)
  MK_VARS += CROSS_BUILDING=yes
endif

define CONFIGURE_CMD
  $(RELINK) $(PKG_BLD_DIR)/src/sqlite.h.in	\
            $(PKG_BLD_DIR)/src/parse.y		\
            $(PKG_BLD_DIR)/Makefile.in	\
					$(LOG)

  $(SED) -i -r -e 's/utf.o/utf.lo/' $(PKG_BLD_DIR)/Makefile.in

  cd $(PKG_BLD_DIR) &&			\
     ./configure -C $(LOG)		\
	--prefix=$(INST_DIR)		\
	$(conf_opts)			\
	--with-gnu-ld			\
	--disable-static		\
	--enable-shared			\
	--enable-threadsafe		\
	--enable-tempstore=always	\
	--disable-tcl			\
	--disable-readline		\
	--disable-amalgamation		\
	$(STD_VARS)
endef


    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)
  INSTALL_CMD =   $(INSTALL_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(ILIB_DIR)/libsqlite3.so* $(RLIB_DIR) $(LOG)


include $(RULES)
