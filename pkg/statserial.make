PKG_TYPE      = DEBIAN1
PKG_VERSION   = 1.1
PKG_VER_PATCH = -22
PKG_SITE_PATH = pool/main/s/$(PKG_BASE)

include $(DEFS)


build : $(call tstamp_f,install,toolchain ncurses)
install-root : $(call tstamp_f,install-root,ncurses)

STD_CFLAGS += $(STD_CPPFLAGS)

MK_VARS = $(STD_VARS) LD=$(CC)
    BUILD_CMD_TARGET =
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/$(PKG) $(RBIN_DIR) $(LOG)


include $(RULES)
