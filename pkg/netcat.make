PKG_TYPE      = DEBIAN3
PKG_VERSION   = 1.10
PKG_VER_PATCH = -41
PKG_SITE_PATH = pool/main/n/$(PKG_BASE)

include $(DEFS)


configure : $(TOOLCHAIN_INSTALL_STAMP)


MK_VARS += CC=$(CC)
MK_VARS += XFLAGS="$(STD_CFLAGS) $(STD_CPPFLAGS) $(STD_LDFLAGS)"
MK_VARS += STATIC=''
MK_VARS += DFLAGS='-DLINUX -DTELNET -DGAPING_SECURITY_HOLE -DIP_TOS -DDEBIAN_VERSION=\"0\"'

    BUILD_CMD_TARGET = linux
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/nc $(RBIN_DIR)        $(LOG)
  $(IW) $(LN_S)                      nc $(RBIN_DIR)/netcat $(LOG)
endef


include $(RULES)
