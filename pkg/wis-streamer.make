#PKG_URL      = http://www.live555.com/wis-streamer/source/wis-streamer.tar.gz
PKG_SITES     = http://www.live555.com
PKG_SITE_PATH = wis-streamer/source
PKG_REMOTE_FILE_SUFFICES = .tar.gz

include $(DEFS)


PKG_PATCH_FILES = $(PKG_PKG_DIR)/$(PKG)_*.patch


configure : $(call bstamp_f,install,sed)
define CONFIGURE_CMD
  $(SED) -i -e 's/DarwinStreaming.o//' $(PKG_BLD_DIR)/Makefile $(LOG)
endef


build : $(call tstamp_f,install,live)

STD_CPPFLAGS += -I. $(addprefix -I$(IINC_DIR)/,BasicUsageEnvironment UsageEnvironment groupsock liveMedia)

MK_VARS += LIVE_DIR=$(BLD_DIR)/live
MK_VARS += CFLAGS="$(STD_CPPFLAGS) $(STD_CFLAGS) $(STD_CXXFLAGS)"
MK_VARS += CC=$(CC) CPLUSPLUS=$(CXX)
MK_VARS += LD="$(LD) -r -Bstatic"

    BUILD_CMD = $(BUILD_CMD_DEFAULT)
    CLEAN_CMD = $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(CLEAN_CMD_DEFAULT)
  INSTALL_ROOT_CMD = $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/$(PKG) $(RBIN_DIR) $(LOG)


include $(RULES)
