PKG_TYPE          = ORIGIN
PKG_VERSION       = 3.1
PKG_SITES         = http://ndevilla.free.fr
PKG_SITE_PATH     = iniparser
EXTRACT_STRIP_NUM = 2

include $(DEFS)


build : $(call tstamp_f,install,toolchain)

MK_VARS = $(STD_VARS) LDSHFLAGS="$(STD_LDFLAGS) -shared"
    BUILD_CMD_TARGET = libiniparser.a
    BUILD_CMD =     $(BUILD_CMD_DEFAULT)
    CLEAN_CMD =     $(CLEAN_CMD_DEFAULT)
DISTCLEAN_CMD = $(DISTCLEAN_CMD_DEFAULT)


define INSTALL_CMD
  #$(IW) $(INSTxFILES) $(PKG_BLD_DIR)/lib$(PKG).so.0 $(RLIB_DIR) $(LOG)
  #$(IW) $(LN_S) lib$(PKG).so.0 $(RLIB_DIR)/lib$(PKG).so  $(LOG)
  $(IW) $(INSTxFILES) $(PKG_BLD_DIR)/lib$(PKG).a    $(ILIB_DIR) $(LOG)

  $(IW) $(INST_FILES) $(PKG_SRC_DIR)/src/*.h        $(IINC_DIR) $(LOG)
endef


include $(RULES)
