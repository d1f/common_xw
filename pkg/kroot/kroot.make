# initramfs source.
# See linux/Documentation/early-userspace/README

PKG_HAS_NO_SOURCES = defined

include $(DEFS)


include $(TOOLS_PKG_DIR)/klibc.mk

INST_KROOT_DIR = $(LK_DIR)/usr/kroot
   KROOT_FILES = $(LK_DIR)/usr/kroot_files


klibc_bin_list_full  = cat chroot cpio dd dmesg false fstype gunzip gzip halt
klibc_bin_list_full += insmod ipconfig kill kinit kinit.shared klcc ln minips
klibc_bin_list_full += mkdir mkfifo mknod mount nfsmount nuke pivot_root
klibc_bin_list_full += poweroff readlink reboot resume run-init sh.shared
klibc_bin_list_full += sleep sync true umount uname zcat


klibc_bin_list  = cat false
klibc_bin_list += kill mkdir mount nuke reboot run-init
klibc_bin_list += sleep sync true umount
klibc_bin_list += flash_erase nandwrite #nandcmp #flashcp # mtd-utils-klibc

install : $(call tstamp_f,reinstall-root,root-conf)
install : $(call tstamp_f,install,klibc mtd-utils-klibc)
install : $(call tstamp_f,link,linux)

define INSTALL_CMD
  $(IW) $(INSTxFILES) $(INST_KLIBC_DIR)/lib/klibc*.so $(INST_KROOT_DIR)/lib    $(LOG)
  $(IW) $(MKDIR_P)                                    $(INST_KROOT_DIR)/bin    $(LOG)
  $(IW) $(INSTxFILE)  $(INST_KLIBC_DIR)/bin/sh.shared $(INST_KROOT_DIR)/bin/sh $(LOG)
 #$(IW) $(INSTxFILE) $(INST_KLIBC_DIR)/bin/minips     $(INST_KROOT_DIR)/bin/ps $(LOG)

  $(IW) $(INSTxFILES) $(addprefix $(INST_KLIBC_DIR)/bin/,$(klibc_bin_list)) \
                                  $(INST_KROOT_DIR)/bin/                      $(LOG)

  $(IW) cp              $(PLATFORM_DIR)/kroot_files          $(KROOT_FILES)   $(LOG)

  $(IW) $(INSTxFILE)     $(PKG_PKG_DIR)/kroot_init           $(INST_KROOT_DIR)/init $(LOG)

  $(IW) $(MKDIR_P)                                           $(INST_KROOT_DIR)/etc  $(LOG)
  $(IW) $(INST_FILES) $(COMMON_PKG_DIR)/common-init/init/tty $(INST_KROOT_DIR)/etc  $(LOG)
  $(IW) $(INST_FILES)      $(RCONF_DIR)/platform             $(INST_KROOT_DIR)/etc  $(LOG)

  $(RM) $(call tstamp_f,build,linux) $(call tstamp_f,install,linux)
endef


include $(RULES)
